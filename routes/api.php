<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */


Route::group([
    //'namespace' => 'Auth',
    'middleware' => [
        'App\Http\Middleware\MyLanguage',

    ]

], function ()
{
    
    Route::post('/upload-files', ['as' => 'uploadFiles', 'uses' => 'FilesController@uploadFiles']);
    Route::get('/remove-file/{id}/{dep}', ['as' => 'removeFile', 'uses' => 'FilesController@removeFile']);
    Route::get('/prdcts-by-catId/{categoryId}/{subcatId?}', ['as' => 'prdctsByCatId', 'uses' => 'ProductController@prdctsByCatId']);
    Route::get('/prdcts-by-featureId/{featureId}', ['as' => 'prdctsByFeatureId', 'uses' => 'ProductController@prdctsByFeatureId']);
  
    Route::post('/login', ['as' => 'login', 'uses' => 'UserController@login']);
    Route::post('/register', ['as' => 'register', 'uses' => 'UserController@register']);
    Route::post('/reset-password', ['as' => 'resetPassword', 'uses' => 'UserController@resetPassword']);
    Route::post('/activation', ['as' => 'activation', 'uses' => 'UserController@activation']);
    Route::post('/resend-activation', ['as' => 'resendActivation', 'uses' => 'UserController@resendActivation']);
    Route::post('/sv-prdct-prps', ['as' => 'svPrdctPrps', 'uses' => 'ProductController@svPrdctPrps']);
    Route::post('/sv-prdct-prps-vls', ['as' => 'svPrdctPrpsVls', 'uses' => 'ProductController@svPrdctPrpsVls']);
    Route::get('/prdct-prp-vls/{prdctId}/{prpId}', ['as' => 'getPrdctPrpsVls', 'uses' => 'ProductController@getPrdctPrpsVls']);
    Route::post('/rate-product', ['as' => 'rateProduct', 'uses' => 'ProductController@rateProduct']);
    Route::get('/user-albums/{userId}', ['as' => 'userAlbums', 'uses' => 'AlbumController@userAlbums']);

    Route::post('/verify-product', ['as' => 'verifyProduct', 'uses' => 'ProductController@verifyProduct']);

    Route::get('/random-products', ['as' => 'rndmPrdcts', 'uses' => 'ProductController@rndmPrdcts']);
    Route::get('/gallery-products', ['as' => 'galleryProducts', 'uses' => 'ProductController@galleryProducts']);
    Route::get('/last-products/{limit?}', ['as' => 'lastProducts', 'uses' => 'ProductController@lastProducts']);
    Route::get('/offer-products', ['as' => 'offerProducts', 'uses' => 'ProductController@offerProducts']);
    Route::get('/last-articles', ['as' => 'lastArticles', 'uses' => 'ArticleController@lastArticles']);
    Route::get('/categories/status/{status?}', ['as' => 'activeCategories', 'uses' => 'CategoryController@index']);
    Route::get('/languages/status/{status?}', ['as' => 'activeLangs', 'uses' => 'LanguageController@index']);
    Route::get('/features/status/{status?}', ['as' => 'activeFeatures', 'uses' => 'FeatureController@index']);
    Route::get('/products/status/{status?}', ['as' => 'activeProducts', 'uses' => 'ProductController@index']);
    Route::get('/subcats/status/{status?}', ['as' => 'activeSubcats', 'uses' => 'SubcatController@index']);
    Route::get('/currencies/status/{status?}', ['as' => 'activeCurrs', 'uses' => 'CurrencyController@index']);

    Route::get('/questions-by-exam/{branchId}/{examId?}', ['as' => 'examQuestions', 'uses' => 'QuestionController@index']);
    Route::get('/exams-by-course', ['as' => 'courseExams', 'uses' => 'ExamController@index']);
    Route::post('/update-multiple-questions', ['as' => 'updateMultipleQuestions', 'uses' => 'QuestionController@updateMultipleQuestions']);
    Route::post('/store-student-answers', ['as' => 'strStdntAnsrs', 'uses' => 'QuestionController@strStdntAnsrs']);
    Route::get('/exams-by-level/{branchId}/{userId?}', ['as' => 'levelExams', 'uses' => 'ExamController@examsByLevel']);
    Route::get('/exams-history/{userId?}', ['as' => 'examsHistory', 'uses' => 'ExamController@examsHistoryByUser']);

    
    Route::get('/cpages/status/{status?}', ['as' => 'activeCpages', 'uses' => 'CpageController@index']);

    Route::get('/countries/status/{status?}', ['as' => 'activeCountries', 'uses' => 'CountryController@index']);
    Route::get('/roles/status/{status?}', ['as' => 'activeRoles', 'uses' => 'RoleController@index']);
    Route::get('/country-cities/{countryCode}', ['as' => 'countryCities', 'uses' => 'CityController@citiesByCountry']);
    Route::get('/user-shpmnt-adrs/{userId}', ['as' => 'userShpmntAdrs', 'uses' => 'ShipmentadrsController@shpmntAdrsByUserId']);
    Route::get('/users-by-group/{branchId}/{groupId}', ['as' => 'usersByGroup', 'uses' => 'UserController@usersByGroup']);
    
    Route::get('/users-by-teacher/{teacherId}', ['as' => 'usersByTeacher', 'uses' => 'UserController@usersByTeacher']);
    Route::get('/scores-by-exam/{examId}', ['as' => 'scoresByExam', 'uses' => 'StudentScoreController@scoresByExam']); 
    
    Route::get('/subcat-by-cat/{categoryId}', ['as' => 'subcatByCat', 'uses' => 'SubcatController@subcatByCat']);
    Route::get('/country-by-code/{countryCode}', ['as' => 'countryByCode', 'uses' => 'CountryController@countryByCode']);

    
    Route::get('/user-orders', ['as' => 'userOrders', 'uses' => 'OrderController@ordersByUserId']);
    Route::get('/user-favourites/{userId}', ['as' => 'userFavs', 'uses' => 'FavouriteController@favouritesByUserId']);
    Route::get('/orders/list', ['as' => 'ordersList', 'uses' => 'OrderController@list']);
     
    Route::get('/outstanding-users/{branchCode?}', ['as' => 'outstandingUsers', 'uses' => 'UserController@outstandingUsers']);

    Route::get('/coupon-info/{couponCode?}', ['as' => 'couponInfo', 'uses' => 'CouponController@couponInfo']);
    
    Route::get('/settings', ['as' => 'getSettings', 'uses' => 'SettingController@show']);
    Route::put('/settings', ['as' => 'updateSettings', 'uses' => 'SettingController@update']);
    
    Route::post('/store-devices-tokens', ['as' => 'storeDevicesTokens', 'uses' => 'NotificationController@storeDevicesTokens']);
    Route::get('/get-devices-tokens', ['as' => 'getDevicesTokens', 'uses' => 'NotificationController@getDevicesTokens']);
    
    Route::get('/counts', ['as' => 'counts', 'uses' => 'ReportController@counts']);
    Route::get('/product-images/{id?}', ['as' => 'productImages', 'uses' => 'ProductController@productImages']);
    Route::get('/products/list', ['as' => 'productsList', 'uses' => 'ProductController@list']);

    
    Route::resource('products', 'ProductController');
    Route::resource('products-trans', 'ProductTransController');
    Route::resource('products-categories', 'ProductCategoryController');
    Route::resource('categories-trans', 'CategoryTransController');
    Route::resource('articles', 'ArticleController');
    Route::resource('articles-trans', 'ArticleTransController');
    Route::resource('courses', 'CourseController');
    Route::resource('courses-trans', 'CourseTransController');
    Route::resource('cpages', 'CpageController');
    Route::resource('cpages-trans', 'CpageTransController');
    Route::resource('services', 'ServiceController');
    Route::resource('colours', 'ColourController');
    Route::resource('subcats', 'SubcatController');
    Route::resource('cargos', 'CargoController');
    Route::resource('coupons', 'CouponController');
    Route::resource('sizes', 'SizeController');
    Route::resource('users', 'UserController');
    Route::resource('shipmentadrs', 'ShipmentadrsController');
    Route::resource('orders', 'OrderController');
    Route::resource('contact', 'ContactController');
    Route::resource('favourites', 'FavouriteController');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');
    Route::resource('faqs', 'FaqController');
    Route::resource('faqs-trans', 'FaqTransController');

    Route::resource('custommsgs', 'CustommsgController');
    Route::resource('orderitems', 'OrderItemController');
    Route::resource('guarantees', 'GuaranteeController');
    Route::resource('tabs', 'TabController');
    Route::resource('tabs-trans', 'TabTransController');

    Route::resource('newsletters', 'NewsletterController');
    Route::resource('features', 'FeatureController');
    Route::resource('features-trans', 'FeatureTransController');
    Route::resource('product-accessories', 'ProductAccessoriesController');
    Route::resource('register-product', 'ProductRegisterController');
    
    Route::resource('languages', 'LanguageController');
    Route::resource('currencies', 'CurrencyController');
    
    Route::resource('exams', 'ExamController');
    Route::resource('questions', 'QuestionController');
    Route::resource('gifts', 'GiftController');
    Route::resource('albums', 'AlbumController');
    Route::resource('students-scores', 'StudentScoreController');
}); 