<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPropValue extends Model
{
    
    protected $guarded = ['id'];
    protected $table = "product_props_values";
    public $timestamps = false;

   
    public function sizes()
    {
        return $this->belongsTo('App\Http\Models\Size', 'valueId');
    }
    
}
