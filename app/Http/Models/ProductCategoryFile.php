<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryFile extends Model {

    protected $table = 'products_categories_files';

    protected $guarded = [
        'id',
    ];

    /**
     * Set timestamps off
     */
    public $timestamps = true;


}
