<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UserFile extends Model {

    protected $table = 'user_file';

    protected $guarded = [ 
        'id',
    ];
    protected $appends = ['imgUrl'];

    public function getImgUrlAttribute() {
        return url('uploads/files/'.$this->fileName);
    }
    /**
     * Set timestamps off
     */
    public $timestamps = true;


}
