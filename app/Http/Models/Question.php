<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = ['id', 'langCode'];

    public $timestamps = false;
    protected $table = 'exams_questions';

    public function exam()
    {
        return $this->belongsTo('App\Http\Models\Exam', 'examId');
    }

    public function answers()
    {
        return $this->hasMany('App\Http\Models\Answer', 'questionId');
    }
}
