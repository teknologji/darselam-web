<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class StudentScore extends Model
{
    protected $guarded = ['id', 'langCode'];

    public $timestamps = true;
    protected $table = 'students_scores';

    public function exam()
    {
        return $this->belongsTo('App\Http\Models\Exam', 'examId');
    }
    public function user() 
    {
        return $this->belongsTo('App\Http\Models\User', 'userId');
    }
    
}
