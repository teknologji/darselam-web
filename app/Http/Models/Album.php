<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $guarded = ['id', 'langCode'];
    public $timestamps = false;


    protected $appends = ['imgUrl', 'thumbUrl96px', 'thumbUrl512px'];

    public function getImgUrlAttribute() {
        return url('/uploads/files/'.$this->photo);
    }

    public function getThumbUrl96pxAttribute() {
        return url('/uploads/thumbnails/96px'.$this->photo);
    }
    public function getThumbUrl512pxAttribute() {
        return url('/uploads/thumbnails/512px'.$this->photo);
    }
} 
