<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFile extends Model {

    protected $table = 'products_files';

    protected $guarded = [
        'id',
    ];

    /**
     * Set timestamps off
     */
    public $timestamps = false;

    protected $appends = ['thumb512', 'thumb96']; 

    public function getThumb96Attribute() {
        //return url('/uploads/files/'.$this->fileName);
        return url('/uploads/thumbnails/96px'.$this->fileName);
    }

    public function getThumb512Attribute() {
        //return url('/uploads/files/'.$this->fileName);
        return url('/uploads/thumbnails/512px'.$this->fileName);
    }
}
