<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTrans extends Model
{
    protected $guarded = ['id', 'langCode'];
    protected $table = 'products_trans';

    public $timestamps = false;

    public function setDescriptionAttribute($description) {

        $this->attributes['description'] = html_entity_decode($description);

    } 

    public function product()
    {
        return $this->belongsTo('App\Http\Models\Product', 'rowId')->with('files');
    }
    public function files() 
    {
        return $this->hasMany('App\Http\Models\ProductFile', 'rowId');
    }
}
