<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    protected $guarded = ['id', 'langCode'];
    public $timestamps = false;


    protected $appends = ['imgUrl'];

    public function getImgUrlAttribute() {
        return url('/uploads/files/'.$this->photo);
    }
} 
