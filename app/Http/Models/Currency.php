<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $guarded = ['id', 'langCode'];

    public $timestamps = false;

}
