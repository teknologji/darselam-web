<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class StudentAnswer extends Model
{
    protected $guarded = ['id', 'langCode'];

    public $timestamps = true;
    protected $table = 'students_answers';

    public function exam()
    {
        return $this->belongsTo('App\Http\Models\Exam', 'examId');
    }
    public function answers()
    {
        return $this->hasMany('App\Http\Models\Answer', 'questionId');
    }
    
}
