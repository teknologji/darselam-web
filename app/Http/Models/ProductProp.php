<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductProp extends Model
{
    protected $table = "product_props";

    protected $guarded = ['id', 'langCode'];
    public $timestamps = false;

    
    public function props_values()
    {
        return $this->belongsToMany('App\Http\Models\Size', 'product_props_values', 'propId', 'valueId', 'propId')
        ->withPivot(['sku', 'theCount', 'productId']);
    }

    public function colour()
    {
        return $this->belongsTo('App\Http\Models\Colour', 'propId');
    }
    
}
