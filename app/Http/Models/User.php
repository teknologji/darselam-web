<?php

namespace App\Http\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens; // include this

class User extends Authenticatable
{
    use Notifiable, HasApiTokens; // update this line

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'langCode', 'roles', 'user_courses', 'passwordForAdmin', 'countryCode'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token', 
        'passwordForAdmin', 
        'isDeleted',
        'created_at',
        'updated_at',
        'activationCode',
        'fromMobApp',
        'phone2',
        'website',    
        'notes'    
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['full_name'];

    public function setPasswordAttribute($pass)
    {

        $this->attributes['password'] = Hash::make($pass);
    }

    public function getFullNameAttribute()
    {
        return ucfirst($this->firstName) . ' ' . ucfirst($this->lastName);
    }
    public function roles()
    {
        return $this->belongsToMany('App\Http\Models\Role', 'user_role', 'userId', 'roleId')->with('permissions');
    }
    public function user_courses()
    {
        return $this->belongsToMany('App\Http\Models\Course', 'users_courses', 'userId', 'courseId')
            ->withPivot(['level']);
    }

    public function files()
    {
        return $this->hasMany('App\Http\Models\UserFile', 'rowId');
    }
}
