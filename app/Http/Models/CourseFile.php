<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CourseFile extends Model {

    protected $table = 'course_file';

    protected $guarded = [
        'id',
    ];
    protected $appends = ['imgUrl']; 

    /**
     * Set timestamps off
     */
    public $timestamps = true;

    public function getImgUrlAttribute() {
        return url('/uploads/files/'.$this->fileName);
    }
}
