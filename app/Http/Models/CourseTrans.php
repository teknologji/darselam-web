<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CourseTrans extends Model
{
    protected $guarded = ['id', 'langCode'];
    protected $table = 'courses_trans';
    
    //protected $hidden = ['courses_trans'];

    public $timestamps = false; 

    public function setDescriptionAttribute($description) {

        $this->attributes['description'] = html_entity_decode($description);

    } 
}
