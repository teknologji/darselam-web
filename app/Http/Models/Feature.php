<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $fillable = ['id', 'status'];
    public $timestamps = false;

    public function languages()
    {
        return $this->belongsToMany('App\Http\Models\Language', 'features_trans', 'rowId', 'languageCode');
    }

    public function elment_trans()
    {
        return $this->hasMany('App\Http\Models\FeatureTrans', 'rowId');
    }
}

