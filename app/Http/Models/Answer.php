<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $guarded = ['id', 'langCode'];

    public $timestamps = false; 
    protected $table = 'questions_answers';

    public function question()
    {
        return $this->belongsTo('App\Http\Models\Question', 'questionId');
    }

   /*  public function answers()
    {
        return $this->hasMany('App\Http\Models\Answer', 'questionId');
    } */
}
