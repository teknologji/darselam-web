<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CpageTrans extends Model
{
    protected $guarded = ['id', 'langCode'];
    protected $table = 'cpages_trans';

    public $timestamps = false;

    public function setDescriptionAttribute($description) {

        $this->attributes['description'] = html_entity_decode($description);

    } 
}
