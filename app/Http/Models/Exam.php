<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $guarded = ['id', 'langCode'];

    public $timestamps = true;

    public function course()
    {
        return $this->belongsTo('App\Http\Models\Course', 'courseId');
    }

    public function questions()
    {
        return $this->hasMany('App\Http\Models\Question', 'examId')->with('answers');
    }
}
