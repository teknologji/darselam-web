<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Cpage extends Model
{
    protected $fillable = ['status', 'photo', 'video'];

    
    public function elment_trans()
    {
        return $this->hasMany('App\Http\Models\CpageTrans', 'rowId');
    }
    
}
