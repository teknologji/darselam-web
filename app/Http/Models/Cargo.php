<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $guarded = ['id', 'langCode'];

    public $timestamps = false;

}
