<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRegister extends Model
{
    protected $table = "product_registers";

    protected $guarded = ['id', 'langCode'];
    public $timestamps = true;

    
}
