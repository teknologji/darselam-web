<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = ['id', 'langCode', 'permissions'];
    public $timestamps = false;

    public function permissions()
    {
        return $this->belongsToMany('App\Http\Models\Permission', 'role_permission', 'roleId', 'permissionId');
    }
}
