<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id', 'langCode', 'items'];

    public $timestamps = true;
    
    protected $hidden = [
        'isDeleted',
        'customerId'
    ];

    public function customer() 
    {
        return $this->belongsTo('App\Http\Models\User', 'customerId');
    }
  

    public function order_items()
    {
        return $this->hasMany('App\Http\Models\OrderItems', 'orderId')
        ->with(['product' => function($q) {
            $q->select('id', 'title');
        }])
        ->with(['seller' => function($q) {
            $q->select('id', 'firstName', 'lastName');
        }]);
    }

    public function files() 
    {
        return $this->hasMany('App\Http\Models\OrderFile', 'orderId');
    }

    public function order_sellers()
    {
        return $this->belongsToMany('App\Http\Models\User', 'orders_sellers', 'orderId', 'sellerId')
        ->withPivot(['orderStatus']);
    }
} 
