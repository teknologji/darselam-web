<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $guarded = ['id', 'langCode'];

    public $timestamps = false; 

    public function setLanguageCodeAttribute($value)
    {
        $this->attributes['languageCode'] = strtoupper($value);
    }

}
