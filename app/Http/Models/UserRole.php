<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model {

    protected $table = 'user_role';

    protected $guarded = [
        'id',
    ];

    /**
     * Set timestamps off
     */
    public $timestamps = false;



}
