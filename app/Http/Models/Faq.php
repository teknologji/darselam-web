<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = ['id', 'status'];
    public function languages()
    {
        return $this->belongsToMany('App\Http\Models\Language', 'faqs_trans', 'rowId', 'languageCode');
    }

    public function elment_trans()
    {
        return $this->hasMany('App\Http\Models\FaqTrans', 'rowId');
    }
}
