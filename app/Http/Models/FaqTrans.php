<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class FaqTrans extends Model
{
    protected $guarded = ['id', 'langCode'];
    protected $table = 'faqs_trans';

    public $timestamps = false;

    public function setDescription1Attribute($description1) {

        $this->attributes['description1'] = html_entity_decode($description1);

    } 

    public function setDescription2Attribute($description2) {

        $this->attributes['description2'] = html_entity_decode($description2);

    } 
}
