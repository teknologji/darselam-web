<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderVendors extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false; 

    protected $table = 'orders_vendors'; 

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'userId');
    }
}
