<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Custommsg extends Model {

    protected $table = 'custom_msg';

    protected $guarded = ['id', 'langCode'];

    /**
     * Set timestamps off
     */
    public $timestamps = false;

    public static function getAll()
    {
        return Custommsg::where('forOrders', '1')->orderBy('id', 'asc')->get();
    }
    public static function getOnly($customMsgsIds = [])
    {
        return Custommsg::where('forOrders', '1')
            ->orderBy('id', 'asc')
            ->whereIn('id', $customMsgsIds)
            ->get();
    }

    public function delegate()
    {
        return $this->belongsToMany('App\Http\Models\User', 'custom_msg_delegate', 'customMsgId', 'delegateId');

    }
    /*public static function getByUserGroupId($userGroupId = 0) // i should remove this after completing workflow task.
    {
        return Custommsg::where('forOrders', '1')
            ->orderBy('id', 'asc')
            ->where('whoSeeIt', $userGroupId)
            ->get();
    }*/
    /*public static function getForOrdersExcept() // i should remove this after completing workflow task.
    {
        return Custommsg::where('forOrders', '1')
            ->orderBy('id', 'asc')
            ->whereNotIn('id', [18,19,20,21,22,23])
            ->get();
    }*/


}
