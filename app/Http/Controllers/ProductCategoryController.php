<?php

namespace App\Http\Controllers;

use App\Http\Models\Product;
use App\Http\Models\ProductCategory;
use App\Http\Models\ProductFile;
use Illuminate\Http\Request;
use App\Http\Requests\ProductCategoryRequest;

class ProductCategoryController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show', 'index']]);
	}

    public function index(Request $request) 
    { 
       
        $rows = ProductCategory::orderBy('theOrder', 'asc')
        
        ->where('branchId', $request->branchId)
        ->get(['id', 'title']);
        
        return response()->json(['rows' => $rows]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCategoryRequest $request)
    {
        $row = ProductCategory::create($request->all());

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = ProductCategory::where('status', 1)->find($id);
        return response()->json($row);

    }

    public function edit($id)
    {
        $row = ProductCategory::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = ProductCategory::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductCategory::where('id', $id)->delete();
        Product::where('categoryId', $id)->update(['isDeleted' => 1]);
    }
}
