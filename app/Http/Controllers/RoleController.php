<?php

namespace App\Http\Controllers;

use App\Http\Models\Role;
use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;

class RoleController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index($status = '')
    {        
       
        
        if( $status != '') {
            $rows = Role::where('status', $status)
            ->orderBy('id', 'desc')
            ->with('permissions')->get();
        } else {
            $rows = Role::with('permissions')
            ->orderBy('id', 'desc')
            ->get();
        }
        
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $row = Role::create($request->all());
        $row->permissions()->sync((array)$request->permissions);

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Role::where('status', 1)->find($id);
        return response()->json($row);

    }
    public function edit($id)
    {
        $row = Role::with('permissions')->find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request)
    {
        $row = Role::findOrFail($request->id);
        $row->update($request->all());
        $row->permissions()->sync((array)$request->permissions);

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::find($id)->delete();
    }
}
