<?php

namespace App\Http\Controllers;

//use App\Cpage;
use App\Http\Models\Cpage;
use App\Http\Models\CpageTrans;
use Illuminate\Http\Request;
use App\Http\Requests\CpageRequest;

class CpageTransController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show', 'index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index($status = '')
    {        

        if( $status != '') {
            $rows = CpageTrans::with('elment_trans')->where('status', $status)->get();
        } else {
            $rows = CpageTrans::with('elment_trans')->orderBy('id', 'desc')->get();
        } 
        
        return response()->json(['rows' => $rows]);
    } */

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CpageRequest $request)
    {
        $row = CpageTrans::create($request->all());       

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    public function edit($id)
    {
        $row = CpageTrans::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cpage  $cpage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = CpageTrans::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cpage  $cpage
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CpageTrans::find($id)->delete();
    }
}
