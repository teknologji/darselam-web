<?php

namespace App\Http\Controllers;

//use App\Feature;
use App\Http\Models\ProductAccessories;
use Illuminate\Http\Request;

class ProductAccessoriesController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show', 'index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index($status = '')
    {        

        if( $status != '') {
            $rows = FeatureTrans::with('elment_trans')->where('status', $status)->get();
        } else {
            $rows = FeatureTrans::with('elment_trans')->orderBy('id', 'desc')->get();
        } 
        
        return response()->json(['rows' => $rows]);
    } */

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $row = ProductAccessories::create([
            'productId' => $request->productId,
            'accessoryId' => $request->accessoryId,
            'priceBeforeDiscount' => $request->priceBeforeDiscount,
            'priceAfterDiscount' => $request->priceAfterDiscount,
        ]);       

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /* public function edit($id)
    {
        $row = FeatureTrans::find($id);
        return response()->json($row);

    } */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    /* public function update(Request $request)
    {
        $row = FeatureTrans::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    } */

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductAccessories::find($id)->delete();
    }
    public function deleteAccessory($accessoryId, $productId)
    {
        ProductAccessories::where('productId', $productId)
        ->where('accessoryId', $accessoryId)
        ->delete();
    }
}
