<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use App\Http\Models\Order;
use App\Http\Models\Product;
use App\Http\Models\Category;
class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    public function counts()
    {        
        
        $usrsCnt = User::where('groupId', 2)->count();
        $prdctsCnt = Product::where('isDeleted', 0)->count();
        $catsCnt = Category::where('isDeleted', 0)->count();
        $ordrsCnt = Order::where('isDeleted', 0)->count();
        
        return response()->json([
            'usrsCnt' => $usrsCnt,
            'prdctsCnt' => $prdctsCnt,
            'catsCnt' => $catsCnt,
            'ordrsCnt' => $ordrsCnt,
            ]);
    }
   
}
