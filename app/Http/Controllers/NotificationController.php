<?php

namespace App\Http\Controllers;

use App\Http\Models\Notification;
use Illuminate\Http\Request;

//use App\Http\Requests\NotificationRequest;

class NotificationController extends Controller
{
    public function __construct()
	{
        //$this->middleware('auth:api', ['except' => ['index', 'show', 'userNotifications']]);
    }
    

    public function getDevicesTokens()
    { 
        $rows = Notification::get(['token']);
         
        return response()->json(['rows' => $rows]);

    }

    public function storeDevicesTokens(Request $request)
    { 
        if(Notification::where('token', $request->token)->count() == 0) {
            $row = Notification::create($request->all());
        }
         
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'success' => true
            ]);

    }
    
}
