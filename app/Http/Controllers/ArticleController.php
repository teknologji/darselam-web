<?php

namespace App\Http\Controllers;

use App\Http\Models\Article;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use App\Http\Models\ArticleTrans;


class ArticleController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['index', 'show', 'lastArticles']]);
    }
    
    public function index()
    { 
        $rows = Article::with('elment_trans')
        ->get();
        return response()->json(['rows' => $rows]);
    }
    public function lastArticles()
    {
        $rows = Article::where('status', 1)
        ->orderBy('id', 'desc')
        ->take(3)->get();
        return response()->json($rows);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $row = Article::create($request->all());

        // save trans.
        ArticleTrans::where('rowId', $row->id)
        ->where('languageCode', $request->languageCode)->delete();

        $trans = ArticleTrans::create([
            'rowId' => $row->id,
            'languageCode' => $request->languageCode,
            'title' => $request->title,
            'description' => $request->description,
            
        ]); 

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $Article
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Article::with('elment_trans')->where('status', 1)->find($id);
        return response()->json($row);
    }

    public function edit($id)
    {
        $row = Article::find($id);
        return response()->json($row);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $Article
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request)
    {
        $row = Article::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $Article
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::findOrFail($id)->delete();
    }
}
