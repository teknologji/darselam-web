<?php

namespace App\Http\Controllers;

use App\Http\Models\Order;
use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Http\Models\OrderItems;
use App\Http\Models\Product;
use App\Http\Models\Custommsg;
use App\Http\Models\User;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['store', 'update']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {

        $rows = Order::where('isDeleted', 0)
            ->with('order_items')
            ->with(['customer' => function($q) {
                $q->select('id', 'firstName', 'lastName');
            }])
            ->addSelect([
                'totalPrice' => OrderItems::selectRaw('sum(salePrice*qty) as totalPrice')
                    ->whereColumn('orderId', 'orders.id')
            ])
            ->orderBy('id', 'desc');

            if (isset($request->offset) and isset($request->limit)) {
                $rows =  $rows->offset($request->offset)->limit($request->limit);
            }
    
            // Get by sort.
            if (isset($request->sortBy) and isset($request->sortSign)) {
                $rows =  $rows->orderBy($request->sortBy, $request->sortSign);
            }

            $rows =  $rows->get();

        return response()->json(['rows' => $rows]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */



    public function store(OrderRequest $request)
    {

        // Check if user with this phone exists.
        $ckUser = User::where('phone', $request->phone)
            ->where('isDeleted', 0)
            ->first();

        if ($ckUser) {
            $customerId = $ckUser->id;
        } else {
            $newUser = User::create([
                'phone' => $request->phone,
                'password' => '123',
                'groupId' => 5, // customer
                'firstName' => 'زبون',
                'lastName' => 'جديد'
            ]);
            $customerId = isset($newUser->id) ? $newUser->id : 0;
        }

        //$customerId = isset(auth('api')->user()->id) ? auth('api')->user()->id : 0;

        // Create a new order.
        $order = Order::create([
            'customerId' => $customerId, //$request->userId,
            'paymentStatus' => 'Pending',
            'orderStatus' => 'Pending',
            'paymentType' => 'Cash',
        ]);

        // create order items.
        // prepare order items & order vendors.
        $orderItems = isset($request->items) ? $request->items : [];
        $orderItemsList = [];
        $orderSellers = [];

        foreach ($orderItems as $item) {

            // Get product details.
            $prdct = Product::where('id', $item['id'])->get(['regularPrice', 'salePrice', 'sellerId'])->first();

            //
            $orderItemsList[] = [

                'orderId'   => $order->id,
                'productId' => $item['id'],
                'qty'  => $item['qty'],
                'regularPrice' => $prdct->regularPrice,
                'salePrice' => $prdct->salePrice,
                'sellerId' => $prdct->sellerId,
            ];

            $orderSellers[] = $prdct->sellerId;

            // update quantity of the sold product.
            $newQty = $prdct->qty - $item['qty'];
            $prdct->qty = $newQty;
            $prdct->save();
            // end udpate qty.
        }

        // Store order items.
        OrderItems::insert($orderItemsList);

        // Store order vendors.
        $order->order_sellers()->sync($orderSellers);

        return response()->json([
            'msg' => trans('general.orderedSuccessfully'), //trans('general.paidSuccessfully').
            'rowId' => $order->id,
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Order::with('user')
            ->with('shipmentadrs')
            ->with('order_items')
            ->findOrFail($id);
        return response()->json($row);
    }

    public function edit($id)
    {
        $row = Order::find($id);
        return response()->json($row);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        \App::setLocale($request->get('langCode'));

        $row = Order::with('customer')->findOrFail($request->id);
        //echo($request->orderStatus);die;

        // if an order status is udpated, then notify the user.
        if ($row->orderStatus != $request->orderStatus && $request->has('orderStatus')) {

            //$orderStatusMsg = Custommsg::find($request->orderStatus);

            // secondly preapre the msg content to be sent.
           /*  $msg = ($request->langCode == 'AR') ? $orderStatusMsg->contentAR : $orderStatusMsg->contentEN;
            $title = ($request->langCode == 'AR') ? $orderStatusMsg->titleAR : $orderStatusMsg->titleEN;

            $msg = str_replace('orderId', $row->orderId, $msg);
            $msg = str_replace('customer_name', $row['user']->full_name, $msg);
 */
            // send sms. 
            //$this->sendSmsMsg($msg, $row['user']->phone);
            //$this->sendEmail('sun.stronghold@gmail.com', $title, $msg);

        }

        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::where('id', $id)->update(['isDeleted' => 1]);
    }


    public function ordersByUserId()
    {
        $rows = Order::where('isDeleted', 0)
            ->where('customerId', auth('api')->user()->id)
            ->with('order_items')
            ->with(['customer' => function($q) {
                $q->select('id', 'firstName', 'lastName');
            }])
            ->addSelect([
                'totalPrice' => OrderItems::selectRaw('sum(salePrice*qty) as totalPrice')
                    ->whereColumn('orderId', 'orders.id')
            ])
            ->orderBy('id', 'desc')
            ->get();

        return response()->json(['rows' => $rows]);
    }
}
