<?php

namespace App\Http\Controllers;

use App\Http\Models\Favourite;
use Illuminate\Http\Request;

class FavouriteController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
       
        $rows = Favourite::with('product')->orderBy('id', 'desc')->get();
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = Favourite::where('userId', $request->userId)
        ->where('itemId', $request->itemId)->count();

        if($check != 0) {
            return response()->json(['msg' => trans('general.addedBefore'), 'success' => false]);
        }
        $row = Favourite::create($request->all());
        return response()->json([
            'msg' => trans('general.addedSuccessfully'),
            'success' => true,
            'favId' => $row->id
            ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Favourite  $favourite
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Favourite::find($id)->delete();
    }

    
    
    public function favouritesByUserId($userId = 0)
    {        
        $rows = Favourite::where('userId', $userId)
        ->with('product')
        ->orderBy('id', 'desc')
        ->get();

        return response()->json(['rows' => $rows]);
    }
}
