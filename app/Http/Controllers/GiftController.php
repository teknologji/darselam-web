<?php

namespace App\Http\Controllers;

use App\Http\Models\Gift;
use App\Http\Requests\GiftRequest;
use Illuminate\Http\Request;

class GiftController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {    
        $rows = Gift::where('branchId', $request->branchId);

        if (isset($request->offset) and isset($request->limit)) {
            $rows =  $rows->offset($request->offset)->limit($request->limit);
        }

        // Get by sort.
        if (isset($request->sortBy) and isset($request->sortSign)) {
            $rows =  $rows->orderBy($request->sortBy, $request->sortSign);
        }
        
        $rows =  $rows->get();
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GiftRequest $request)
    {
        $row = Gift::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'success' => true,
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gift  $gift
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Gift::find($id);
        return response()->json($row);
    }

    public function edit($id)
    {
        $row = Gift::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gift  $gift
     * @return \Illuminate\Http\Response
     */
    public function update(GiftRequest $request)
    {
        $row = Gift::findOrFail($request->id);
        $row->update($request->all());

        return response()->json([
            'msg' => trans('general.updatedSuccessfully'),
            'success' => true,
            'rowId' => $row->id
            ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gift  $gift
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gift::find($id)->delete();
    }
}
