<?php

namespace App\Http\Controllers;

use App\Http\Models\Album;
use App\Http\Models\User;
use App\Http\Models\UserCourse;
use App\Http\Requests\AlbumRequest;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['index', 'show', 'userAlbums']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 

    public function userAlbums($userId = 0)
    {    
        $rows = Album::where('userId', $userId)
        ->where('type', 1)
        ->get();
        return response()->json(['rows' => $rows]);
    }

    public function index(Request $request)
    {    
        
        $rows = Album::where('branchId', $request->branchId);

        if (isset($request->userId) ) { 
            $rows =  $rows->where('userId', $request->userId); 
        }

        if (isset($request->offset) ) { 
            $rows =  $rows->offset($request->offset)->limit(10); 
        }
        // Get by sort.
        if (isset($request->sortBy) and isset($request->sortSign)) {
            $rows =  $rows->orderBy($request->sortBy, $request->sortSign);
        }

        $rows =  $rows->inRandomOrder()->get();
        
        return response()->json(['rows' => $rows]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AlbumRequest $request)
    {
        $row = Album::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'success' => true,
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Album::find($id);
        return response()->json($row);
    }

    public function edit($id)
    {
        $row = Album::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(AlbumRequest $request)
    {
        $row = Album::findOrFail($request->id);
        $row->update($request->all());

        return response()->json([
            'msg' => trans('general.updatedSuccessfully'),
            'success' => true,
            //'rowId' => $row->id
            ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Album::find($id)->delete();
    }
}
