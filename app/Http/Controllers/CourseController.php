<?php

namespace App\Http\Controllers;

use App\Http\Models\Course;
use Illuminate\Http\Request;
use App\Http\Requests\CourseRequest;
use App\Http\Requests\CourseTransRequest;
use App\Http\Models\CourseTrans;
use App\Http\Models\CourseFile;


class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show', 'lastCourses', 'store', 'destroy', 'edit', 'update']]);
    }

    public function index(Request $request)
    {
        $rows = Course::with(['one_trans' =>  function ($q) {
            $q->where('languageCode', '=', 'AR'); //strtoupper(app()->getLocale())
        }])
            ->where('isDeleted', 0)
            ->where('branchId', $request->branchId)
            ->with('files')
            ->orderBy('id', 'desc');

        if (auth('api')->user() != null and auth('api')->user()->groupId == 1) {
            if (isset($request->status)) {

                $rows = $rows->where('status', $request->status);
            }
        } else {
            $rows = $rows->where('status', 1);
        }

        if (isset($request->offset) and isset($request->limit)) {
            $rows =  $rows->offset($request->offset)->limit($request->limit);
        }

        // Get by sort.
        if (isset($request->sortBy) and isset($request->sortSign)) {
            $rows =  $rows->orderBy($request->sortBy, $request->sortSign);
        }

        if (isset($request->translations) and $request->translations == 'all') {
            $rows =  $rows->with('elment_trans');
        }
        
        $rows = $rows->get(['id', 'price', 'levels']);
        return response()->json(['rows' => $rows]);
    }
    public function lastCourses(Request $request)
    {
        $rows = Course::where('status', 1)
            ->orderBy('id', 'desc')
            ->where('branchId', $request->branchId)
            ->take(6)->get(['id', 'price', 'levels']);
        return response()->json($rows);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseTransRequest $request)
    {
        $row = Course::create($request->all());

        // save trans.
        CourseTrans::where('rowId', $row->id)
            ->where('languageCode', $request->languageCode)->delete();

        $trans = CourseTrans::create([
            'rowId' => $row->id,
            'languageCode' => $request->languageCode,
            'title' => $request->title,
            'description' => $request->description,

        ]);

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id,
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $Course
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Course::with('files')
            ->with(['one_trans' =>  function ($q) {
                $q->where('languageCode', '=', strtoupper(app()->getLocale()));
            }])
            ->where('status', 1)
            ->find($id);
        return response()->json($row);
    }

    public function edit($id)
    {
        $row = Course::with('files')->find($id);
        return response()->json($row);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $Course
     * @return \Illuminate\Http\Response
     */
    public function update(CourseRequest $request)
    {
        $row = Course::findOrFail($request->id);
        $row->update($request->all());

        return response()->json([
            'msg' => trans('general.updatedSuccessfully'),
            'rowId' => $row->id,
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $Course
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Course::findOrFail($id)->delete();
        CourseTrans::where('rowId', $id)->delete();
        CourseFile::where('rowId', $id)->delete();
    }
}
