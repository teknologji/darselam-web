<?php

namespace App\Http\Controllers;

use App\Http\Models\Cpage;
use Illuminate\Http\Request;
use App\Http\Requests\CpageRequest;
use App\Http\Models\CpageTrans;


class CpageController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }
    
    public function index()
    { 
        $rows = Cpage::with('elment_trans')
        ->get();
        return response()->json(['rows' => $rows]);
    }
    public function lastCpages()
    {
        $rows = Cpage::where('status', 1)
        ->orderBy('id', 'desc')
        ->take(3)->get();
        return response()->json($rows);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CpageRequest $request)
    {
        $row = Cpage::create($request->all());

        // save trans.
        CpageTrans::where('rowId', $row->id)
        ->where('languageCode', $request->languageCode)->delete();

        $trans = CpageTrans::create([
            'rowId' => $row->id,
            'languageCode' => $request->languageCode,
            'title' => $request->title,
            'description' => $request->description,
            
        ]); 

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cpage  $Cpage
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Cpage::with('elment_trans')->where('status', 1)->find($id);
        return response()->json($row);
    }

    public function edit($id)
    {
        $row = Cpage::find($id);
        return response()->json($row);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cpage  $Cpage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = Cpage::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cpage  $Cpage
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cpage::findOrFail($id)->delete();
    }
}
