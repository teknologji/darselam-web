<?php

namespace App\Http\Controllers;

use App\Http\Models\OrderItems;
use Illuminate\Http\Request;

class OrderItemController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {        
         
        $rows = OrderItems::where('isDeleted', 0)->orderBy('id', 'desc')->get();
        return response()->json(['rows' => $rows]);
    } */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $row = OrderItems::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }
    
    public function update(Request $request)
    {
        $row = OrderItems::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    public function destroy($id)
    {
        OrderItems::find($id)->delete();
    }
}
