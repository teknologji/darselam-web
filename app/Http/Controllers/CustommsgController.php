<?php

namespace App\Http\Controllers;

use App\Http\Models\Custommsg;
use Illuminate\Http\Request;

class CustommsgController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
             
        $rows = Custommsg::all();
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $row = Custommsg::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Custommsg  $Custommsg
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Custommsg::where('status', 1)->find($id);
        return response()->json($row);
    }

    public function edit($id)
    {
        $row = Custommsg::find($id);
        return response()->json($row);
    }

    public function lastCustommsgs()
    {
        $rows = Custommsg::where('status', 1)
        ->orderBy('id', 'desc')
        ->take(3)->get();
        return response()->json($rows);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Custommsg  $Custommsg
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = Custommsg::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Custommsg  $Custommsg
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Custommsg::findOrFail($id)->delete();
    }
}
