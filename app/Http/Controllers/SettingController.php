<?php

namespace App\Http\Controllers;

use App\Http\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show']]);
    }

    public function show(Request $request)
    {
         
        $row = Setting::find($request->branchId);
        return response()->json($row); 

    }
   
    public function update(Request $request)
    {
        $row = Setting::where('branchId', $request->branchId)->update($request->except(['langCode', 'kToken']));

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

}
