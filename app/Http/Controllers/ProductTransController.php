<?php

namespace App\Http\Controllers;

//use App\Product;
use App\Http\Models\Product;
use App\Http\Models\ProductTrans;
use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;

class ProductTransController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show', 'index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index($status = '')
    {        

        if( $status != '') {
            $rows = ProductTrans::with('elment_trans')->where('status', $status)->get();
        } else {
            $rows = ProductTrans::with('elment_trans')->orderBy('id', 'desc')->get();
        } 
        
        return response()->json(['rows' => $rows]);
    } */

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        
        ProductTrans::where('rowId', $request->rowId)
        ->where('languageCode', $request->languageCode)->delete(); 
        
        $row = ProductTrans::create($request->all());       

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id 
            ]);

    }

    public function edit($id)
    {
        $row = ProductTrans::with('files')->find($id);
        return response()->json($row); 

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) 
    {
        $row = ProductTrans::findOrFail($request->id);

        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductTrans::find($id)->delete();
    }
}
