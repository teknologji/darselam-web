<?php

namespace App\Http\Controllers;

use App\Http\Models\Subcat;
use Illuminate\Http\Request;
use App\Http\Requests\SubcatRequest;

class SubcatController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['index', 'subcatByCat']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function subcatByCat($categoryId)
    {        
        $rows = Subcat::where('isDeleted', 0)
        ->where('categoryId', $categoryId)
        ->get();
        return response()->json(['rows' => $rows]);
    }

    public function index($status = '')
    {        
        $rows = Subcat::where('isDeleted', 0)
        ->orderBy('id', 'desc')
        ->with('category');

        if( $status != '') {
            $rows = $rows->where('status', $status)->get();
        } 
        
        $rows = $rows->get();
        return response()->json(['rows' => $rows]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubcatRequest $request)
    {
        $row = Subcat::create($request->all());
        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subcat  $subcat
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $row = Subcat::where('status', 1)->find($id);
        return response()->json($row);

    }
    public function edit($id)
    {
        $row = Subcat::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subcat  $subcat
     * @return \Illuminate\Http\Response
     */
    public function update(SubcatRequest $request)
    {
        $row = Subcat::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subcat  $subcat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subcat::where('id', $id)->update(['isDeleted' => 1]);
    }
}
