<?php

namespace App\Http\Controllers;

//use App\Tab;
use App\Http\Models\Tab;
use App\Http\Models\TabTrans;
use Illuminate\Http\Request;
use App\Http\Requests\TabTransRequest;

class TabTransController extends Controller
{
    public function __construct()
	{
        $this->middleware('auth:api', ['except' => ['show', 'index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index($status = '')
    {        

        if( $status != '') {
            $rows = TabTrans::with('elment_trans')->where('status', $status)->get();
        } else {
            $rows = TabTrans::with('elment_trans')->orderBy('id', 'desc')->get();
        } 
        
        return response()->json(['rows' => $rows]);
    } */

    /** 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TabTransRequest $request)
    {
        $row = TabTrans::create($request->all());       

        return response()->json([
            'msg' => trans('general.savedSuccessfully'),
            'rowId' => $row->id
            ]);

    }

    public function edit($id)
    {
        $row = TabTrans::find($id);
        return response()->json($row);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tab  $tab
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $row = TabTrans::findOrFail($request->id);
        $row->update($request->all());

        return response()->json(['msg' => trans('general.updatedSuccessfully')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tab  $tab
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TabTrans::find($id)->delete();
    }
}
