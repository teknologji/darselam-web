<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use App\Http\Models\Country;
use App\Http\Models\Course;
use App\Http\Models\Favourite;
use App\Http\Models\Custommsg;

use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use App\Http\Models\UserCourse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'outstandingUsers', 'usersByGroup', 'resetPassword', 'register', 'store', 'login', 'resendActivation', 'activation']]);
    }


    public function resetPassword(ResetPasswordRequest $request)
    {


        $row = User::where('phone', $request->phone)->get()->first();

        if ($row == null) {
            return response()->json([
                'success' => false,
                'msg' => trans('general.accountNotExist')
            ]);
        }

        $newPassword = rand(100000, 999999);
        $row->update(['password' => $newPassword]); //bcrypt()

        //echo $row['phone'];die;
        $smsResult = $this->sendSmsMsg($newPassword, $row->phone);

        if ($smsResult == 1) {
            return response()->json([
                'success' => true,
                'msg' => trans('general.passwordSent')
            ]);
        } else {
            return response()->json([
                'success' => true,
                'msg' => $newPassword
            ]);
        }
    }

    public function login(LoginRequest $request)
    {

        $row = User::where('tcNr', $request->username)
            ->orWhere('phone', $request->username)
            ->with('roles')
            ->first();

        if (!is_null($row)) {

            if (Hash::check($request->password, $row->password) == false) {
                return response()->json(['success' => false, 'msg' => trans('general.wrongPassword')]);
            }
            if ($row->status == 1) {
                
                $userPermissions = [];

                foreach ($row['roles'] as $role) {
                    foreach ($role['permissions'] as $permission) {
                        $userPermissions[] = $permission['methodName'];
                    }
                }

                $accessToken = $row->createToken('authToken')->accessToken;

                return response()->json([
                    'msg' => '',
                    'success' => true,
                    'user' => $row,
                    'userPermissions' => $userPermissions,
                    'accessToken' => $accessToken
                ]);
            }

            return response()->json(['msg' => trans('general.cntctAdmn2ActvtAccnt'), 'success' => false]);
        }

        return response()->json(['msg' => trans('general.wrongUsername'), 'success' => false]);
    }

    public function register(RegisterRequest $request)
    {

        if (in_array($request->groupId, ['student', 'teacher'])) { // Student, Teacher.
            $checkTC = User::where('tcNr', $request->tcNr)
                ->where('isDeleted', 0)
                ->whereIn('groupId', ['student', 'teacher'])
                ->count();

            if ($checkTC != 0) {
                return response()->json([
                    'msg' => trans('general.tcNrExists'),
                    'success' => false
                ]);
            }
        }

        $row = User::create($request->all());

        if ($row) {

            // get courses.
            $courses = Course::where('isDeleted', 0)->get();

            // save user levels for each course.
            $userCourses = [];
            foreach ($courses as $course) {
                $userCourses[] = [
                    'userId' => $row->id,
                    'courseId' => $course['id'],
                    'level' => 1
                ];
            }
            UserCourse::insert($userCourses);

            $accessToken = $row->createToken('authToken')->accessToken;

            return response()->json([
                'msg' => trans('general.registered_successfully'),
                'success' => true,
                'user' => $row,
                'accessToken' => $accessToken
            ]);
        }
        return response()->json([
            'msg' => trans('general.sorryTryAgain'),
            'success' => false
        ]);
    }


    public function index(Request $request)
    {

        $rows = User::where('isDeleted', 0);

            
            if ( ! isset(auth('api')->user()->groupId) || auth('api')->user()->groupId != 'admin') { 
                $rows =  $rows->where('status', 1); 
            }

            if (isset($request->groupId) ) { 
                $rows =  $rows->where('groupId', $request->groupId); 
            }

            if (isset($request->offset) ) { 
                $rows =  $rows->offset($request->offset)->limit(10); 
            }
            // Get by sort.
            if (isset($request->sortBy) and isset($request->sortSign)) {
                $rows =  $rows->orderBy($request->sortBy, $request->sortSign);
            }
            $rows =  $rows->get(['id', 'firstName', 'lastName']);

        return response()->json(['rows' => $rows]);
    }

    public function store(UserRequest $request)
    {

        $checkPhone = User::where('phone', $request->phone)
            ->where('isDeleted', 0)
            ->count();

        if ($checkPhone != 0) {
            return response()->json([
                'msg' => trans('general.phoneUsed'),
                'success' => false
            ]);
        }

        //$countryId = Country::where('countryCode', $request->countryCode)->first()->id;
        //$request->merge(['countryId' => $countryId]);

        $row = User::create($request->all());
        if ($row) {
            // save user roles.
            $row->roles()->sync((array)$request->roles);

            // get courses.
            $courses = Course::where('isDeleted', 0)->get();

            // save user levels for each course.
            $userCourses = [];
            foreach ($courses as $course) {
                $userCourses[] = [
                    'userId' => $row->id,
                    'courseId' => $course['id'],
                    'level' => 1
                ];
            }
            UserCourse::insert($userCourses);

            // return response.
            return response()->json([
                'msg' => trans('general.savedSuccessfully'),
                'rowId' => $row->id,
                'success' => true
            ]);
        }
        return response()->json(['msg' => trans('general.sorryTryAgain')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = User::with('files')
            ->with('roles')
            ->with('user_courses')
            ->find($id);
        return response()->json($row);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request)
    {
        //print_r($request->user_courses);die;
        /* return response()->json([
            'msg' => $request->user_courses[0]['pivot']['level'], 
            'success' => false
        ]);
        die; */
        $row = User::findOrFail($request->id);

        if ($row->phone != $request->phone) {
            $checkPhone = User::where('phone', $request->phone)
                ->where('id', '!=', $request->id)
                ->count();

            if ($checkPhone != 0) {
                return response()->json([
                    'msg' => trans('general.phoneUsed'),
                    'success' => false
                ]);
            }
        }



        // firstly, update just user without roles.
        if (trim($request->password) == '') {
            $row->update($request->except(['password']));
        } else {
            $row->update($request->all());
        }

        // then, update roles.
        $row->roles()->sync((array)$request->roles);

        // save user courses.

        if (isset($request['user_courses'])) {

            UserCourse::where('userId', $request->id)->delete();

            $userCourses = [];
            foreach ($request['user_courses'] as $item) {

                $userCourses[] = [
                    'userId' => $row->id,
                    'courseId' => $item['id'],
                    'level' => isset($item['pivot']['level']) ? $item['pivot']['level'] : 1
                ];
            }
            UserCourse::insert($userCourses);
        }
        //.......


        $accessToken = $row->createToken('authToken')->accessToken;

        return response()->json([
            'msg' => trans('general.updatedSuccessfully'),
            'success' => true,
            'user' => $row,
            'accessToken' => $accessToken
        ]);

        //$password = (trim($request->password) == "")? $request->current_password  : bcrypt($request->password);

        //$request->merge( array( 'password' => $password));

    }

    public function destroy($id)
    {
        User::where('id', $id)->update(['isDeleted' => 1]);
        //UserCourse::where('userId', $id)->delete();
    }

    public function outstandingUsers($branchId = 0)
    {
        $rows = User::where('isDeleted', 0)
            ->where('status', 1)
            ->where('branchId', $branchId)
            ->where('isOutstanding', 1)
            ->where('groupId', 'student')
            ->inRandomOrder()
            ->limit(10)
            ->get(['id', 'firstName', 'lastName', 'photo']);

        return response()->json(['rows' => $rows]);
    }

    public function usersByGroup($branchId, $groupId = 'teacher')
    {

        $rows = User::where('isDeleted', 0)
            ->where('status', 1)
            ->where('groupId', $groupId)
            ->where('branchId', $branchId)
            ->orderBy('id', 'desc')
            ->get(['id', 'firstName', 'lastName']);

        return response()->json(['rows' => $rows]);
    }

    public function usersByTeacher($teacherId = 0)
    {

        $rows = User::where('isDeleted', 0)
            ->where('status', 1)
            ->where('teacherId', $teacherId)
            ->orderBy('id', 'desc')
            ->get();

        return response()->json(['rows' => $rows]);
    }

    public function resendActivation(Request $request)
    {
        // check if the activation code exists.
        $row = User::find($request->userId);

        $activationCodeMsg = trans('general.activationCodeIs') . $row->activationCode;

        $this->sendSmsMsg($activationCodeMsg, $row->phone);
        //$this->sendEmail('sun.stronghold@gmail.com', $title, $activationCodeMsg);

        return response()->json([
            'msg' => trans('general.sentSuccessfully'),
            'success' => true
        ]);
    }

    public function activation(Request $request)
    {

        // check if the activation code exists.
        $row = User::where('activationCode', $request->code)->where('status', 0)->get()->first();

        // return back if it's wrong.
        if (is_null($row)) {
            return response()->json([
                'msg' => trans('general.wrongCode'),
                'success' => false
            ]);
        }
        // if it's ok, then activate a user.
        $row->status = 1;
        $row->activationCode = 0;
        $row->save();

        // notify a user to activation process.
        // firstly get register msg to send via sms or email or both.
        $registerMsg = Custommsg::find(1);

        // secondly preapre the msg content to be sent.
        $msg = ($request->langCode == 'AR') ? $registerMsg->contentAR : $registerMsg->contentEN;
        $title = ($request->langCode == 'AR') ? $registerMsg->titleAR : $registerMsg->titleEN;

        $msg = str_replace('customer_name', $row->full_name, $msg);

        // send sms.
        $this->sendSmsMsg($msg, $row->phone);
        //$this->sendEmail('sun.stronghold@gmail.com', $title, $msg);

        $accessToken = $row->createToken('authToken')->accessToken;

        return response()->json([
            'msg' => trans('general.activatedSuccessfully'),
            'success' => true,
            'user' => $row,
            'accessToken' => $accessToken
        ]);
    }
}
