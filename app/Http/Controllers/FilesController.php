<?php

namespace App\Http\Controllers;


use App\Http\Models\Album;
use App\Http\Models\ProductFile;
use App\Http\Models\Product;
use App\Http\Models\Setting;
use App\Http\Models\OrderFile;
use App\Http\Models\Role;
use App\Http\Models\User;
use App\Http\Models\Language;
use App\Http\Models\ProductCategoryFile;
use App\Http\Models\UserFile;
use App\Http\Models\CourseFile;
use App\Http\Models\Gift;
use Illuminate\Contracts\Auth\Guard;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use Intervention\Image\Facades\Image;


class FilesController extends Controller
{



    public function uploadFiles(Request $request)
    {

        if ($request->hasFile('files')) {

            $files = $request->file('files');

            $destinationPath = public_path() . "/uploads/files";
            $destinationThumbPath = public_path() . "/uploads/thumbnails";

            //echo $files[0]->getClientOriginalName();die;
            foreach ($files as $index => $file) {
                //echo $file->getClientOriginalName().'<br/>';
                if ($file->isValid()) {

                    $newFileName = bin2hex(openssl_random_pseudo_bytes(8)) . '.' . $file->extension();

                    $img = Image::make($file->path());

                    $img->resize(96, 96, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationThumbPath . '/' . '96px'.$newFileName);

                    $img->resize(512, 512, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationThumbPath . '/' . '512px'.$newFileName);

                    $file->move($destinationPath, $newFileName);

                    if ($request->get('type') == 'product') {
                        ProductFile::create([
                            'rowId' => $request->get('rowId'),
                            'fileName' => $newFileName,
                            'mainImg' => ($index == 0) ? 1 : 0
                        ]);

                    } elseif ($request->get('type') == 'user') {

                        return UserFile::create([

                            'rowId' => $request->get('rowId'),
                            'fileName' => $newFileName
                        ]);
                    } elseif ($request->get('type') == 'course') {

                        return CourseFile::create([

                            'rowId' => $request->get('rowId'),
                            'fileName' => $newFileName
                        ]);
                    } elseif ($request->get('type') == 'gift') {

                        return Gift::where('id', $request->get('rowId'))->update(['photo' => $newFileName]);
                    
                    }elseif ($request->get('type') == 'album') {

                        return Album::where('id', $request->get('rowId'))->update(['photo' => $newFileName]);
                    
                    }elseif ($request->get('type') == 'setting') {

                        if ($request->get('mainPhoto') == 1) {
                            return Setting::where('id', $request->get('rowId'))->update(['photo' => $newFileName]);
                        }
                    } elseif ($request->get('type') == 'language') {

                        if ($request->get('mainPhoto') == 1) {
                            return Language::where('id', $request->get('rowId'))->update(['photo' => $newFileName]);
                        }
                    } elseif ($request->get('type') == 'order') {

                        OrderFile::create([
                            'orderId' => $request->get('rowId'),
                            'fileName' => $newFileName

                        ]);
                    }
                }
            }
            return response()->json([
                'success' => true,
                'msg' => trans('general.savedSuccessfully')
            ]);
        }
    }


    public function removeFile($id, $dep)
    {
        /* if( $dep == 'product') {
            $row = ProductFile::findOrFail($id);
        }else if( $dep == 'productCategory') {
            $row = CategoryFile::findOrFail($id);
        }else */
        if ($dep == 'course') {
            $row = CourseFile::findOrFail($id);
        } else if ($dep == 'user') {
            $row = UserFile::findOrFail($id);
        }else if ($dep == 'product') {
            $row = ProductFile::findOrFail($id);
        }
        //$this->deleteFromHost($row->fileName);
        $row->delete();
        return response()->json(['msg' => trans('general.deletedSuccessfully')]);
    }


    public function downloadFile($tableName, $id)
    {


        //PDF file is stored under project/public/uploads/files

        if (preg_match('/^\d+$/', $id) and trim($tableName) != '') {



            $file = DB::table($tableName)->where('id', $id)->first();

            $fileName = $file->fileName;

            $mimeType = $file->mimeType;

            //echo $fileName;die;

            $filePath = public_path() . "/uploads/files/" . $fileName;



            if (file_exists($filePath) == true) {

                $headers = array(

                    'Content-Type: ' . $mimeType,

                );

                return response()->download($filePath, $fileName, $headers);
            } else {

                return redirect()->back()->with('msg', trans('general.fileNotFound'));
            }
        } else {

            return redirect()->back()->with('msg', trans('general.fileNotFound'));
        }
    }

    public function readFile($tableName, $id)
    {



        //PDF file is stored under project/public/uploads/files

        if (preg_match('/^\d+$/', $id) and in_array($tableName, $this->filesTables) == true) {



            try {

                $file = DB::table($tableName)->where('id', $id)->first();

                $fileName = $file->fileName;

                $mimeType = $file->mimeType;



                $filePath = public_path() . "/uploads/files/" . $fileName;



                if (file_exists($filePath) == true) {



                    $content = file_get_contents($filePath);

                    return response()->make($content, 200, array('content-type' => $mimeType));



                    //return response()->file($filePath, $fileName, $headers);



                } else {

                    return 'File Not Found!';
                }
            } // catch(Exception $e) catch any exception

            catch (ModelNotFoundException $e) {

                return redirect()->back()->with('msg', trans('general.fileNotFound'));
            }
        } else {

            return redirect()->back()->with('msg', trans('general.fileNotFound'));
        }
    }

    /*public function deleteFile(){



    }*/
}
