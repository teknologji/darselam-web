<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /* 'userId' => 'required',
            'shipmentAdrsId' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'adrs' => 'required', */
        ];
    }

   public function messages()
   {
       return [
          /* 'userId.required' => __('general.loginRequired'), 
           'shipmentAdrsId.required' => __('general.shipmentAdrsIdRequired'),
           
           'phone.required' => __('general.phoneRequired'),
           'password.required' => __('general.passwordRequired'),
           'adrs.required' => __('general.adrsRequired'), */

       ];
   }
}
