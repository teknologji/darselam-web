<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titleAR' => 'required',/* 
            'lastName' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'adrs' => 'required', */
        ];
    }

   public function messages()
   {
       
       return [
           'titleAR.required' => __('general.titleARRequired'),/* 
           'lastName.required' => __('general.last_name_required'),
           'phone.required' => __('general.phoneRequired'),
           'password.required' => __('general.passwordRequired'),
           'adrs.required' => __('general.adrsRequired'), */

       ];
   }
}
