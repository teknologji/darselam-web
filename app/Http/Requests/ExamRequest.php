<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'courseId' => 'required',
            'title' => 'required',
        ];
    }

   public function messages()
   {
       return [
        'courseId.required' => __('general.courseRequired'),
           'title.required' => __('general.titleRequired'),

       ];
   }
}
