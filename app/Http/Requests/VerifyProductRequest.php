<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VerifyProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'serialNum' => 'required',
            'guaranteeNum' => 'required',
        ];
    }

   public function messages()
   {
       
       return [
           'serialNum.required' => trans('general.serialNumRequired'),
           'guaranteeNum.required' => trans('general.guaranteeNumRequired'),
           

       ];
   }
}
