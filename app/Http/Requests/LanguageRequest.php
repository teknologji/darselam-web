<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LanguageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'languageCode' => 'required', 
        ];
    }

   public function messages()
   {
       return [
           'title.required' => __('general.titleRequired'),
           'languageCode.required' => __('general.langCodeRequired')

       ];
   }
}
