<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|numeric|digits_between:9,12', 
        ];
    }

   public function messages()
   {
       
       return [
        'phone.required' => __('general.phoneRequired'),
        'phone.numeric' => __('general.phoneNumeric'),
        'phone.digits_between' => __('general.phoneDigits_between'),

       ];
   }
}
