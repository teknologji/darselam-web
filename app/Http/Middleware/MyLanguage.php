<?php namespace App\Http\Middleware;


use Closure;

class MyLanguage {


	public function __construct()
	{
		
	}

	public function handle($request, Closure $next)
	{
		//echo \Session::get('myLanguage');die;
		//echo $request->get('langCode');
        \App::setLocale(strtolower($request->get('langCode')));

        return $next($request); 
	}

} 
