<?php namespace App\Http\Middleware;


use Closure;

class KToken {


	public function __construct()
	{ 
		
	}

	public function handle($request, Closure $next)
	{
        if( ! $request->kToken || $request->kToken != date('dh', strtotime('-3'))) {
            /* return response()->json([
                'rows' => ['k' => date('dg', strtotime('-3'))]
                ]); */
        }       
        
        return $next($request); 
	}

}
