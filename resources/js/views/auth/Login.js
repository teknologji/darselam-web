import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../frontend/partials/BreadCrumb';
import {toast} from "react-toastify";
import postData from '../../helpers/postData';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';



class Login extends Component {
    constructor(props){
        super(props);
        this.state = {phone: '', password: '', errorMessage: '', isDisabled: false};

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChangeValue = this.onChangeValue.bind(this);
    }

    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }

    handleSubmit(e){
        e.preventDefault();
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        this.setState({isDisabled: true});
        postData(this.props.defaultSets.apiUrl+'login', {
            phone: this.state.phone,
            password: this.state.password,
            langCode: this.props.selectedLanguage.langCode
        })
            .then((response) => {
                //console.log(response.data);
                if(response.data.success === true) {

                    let theUser = response.data.user;
                    theUser.userPermissions = response.data.userPermissions;
                    theUser.accessToken = response.data.accessToken;

                    //return console.log(theUser)
                    this.props.saveUserInfo(response.data.user);
                    this.props.addToFavourites(response.data.favs);

                    toast.dismiss(ldToast);

                    if(response.data.user.groupId == 1) {
                        return this.props.history.push('/admin');
                    }
                    return this.props.history.push('/');
                }  
                toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });

                //
            })
            .catch((error) => {
                //console.log(error);

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {

        const { phoneTxt, passwordTxt, registerMsgTxt,
            crtNewAccntTxt, loginTxt,langRow, newMemberTxt,forgotPasswordTxt} = this.props.selectedLanguage;

        return (
            <Fragment>
                <Helmet><title>{loginTxt}</title></Helmet>

<BreadCrumb title={loginTxt} />

                  
 
<section className="login-page section-b-space">
        <div className="container">
            <div className="row">
                <div className="col-lg-6">
                    <h3>{loginTxt}</h3>
                    <div className="theme-card">
                        <form className="theme-form" onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="email">{phoneTxt}</label>
                                <input 
                                placeholder={phoneTxt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="phone" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="review">{passwordTxt}</label>
                                <input 
                                placeholder={passwordTxt}
                                type="password" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="password"
                                /*id="review"*/
                                 />
                            </div>
                            
                        <button 
                        type="submit" 
                        disabled={this.state.isDisabled}
                        className="btn btn-solid">{loginTxt}</button>
                        </form>
                    </div>
                </div>
                <div className="col-lg-6 right-login">
                    <h3>{newMemberTxt}</h3>
                    <div className="theme-card authentication-right">
                        <h6 className="title-font">{crtNewAccntTxt}</h6>
                        <p>{registerMsgTxt}</p>
                            <Link to="/register"
                            className="btn btn-solid">{crtNewAccntTxt}</Link>
                    </div>
                </div>
            </div>
        </div>
    </section>


</Fragment>


        )
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInfo: (userInfo) => dispatch({ type: 'SAVE_USER', payload: userInfo }),

        addToFavourites: (item) => {
            dispatch({ type: 'ADD_TO_FAV', payload: item });
        }
    }
};

const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
