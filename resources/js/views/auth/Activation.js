import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../frontend/partials/BreadCrumb';
import {toast} from "react-toastify";
import { CountdownCircleTimer } from 'react-countdown-circle-timer'
import postData from '../../helpers/postData';
import { Helmet } from 'react-helmet';

class Activation extends Component {
    constructor(props){
        super(props);
        this.state = {
            code: '', 
            errorMessage: '', 
            isDisabled: false, 
            resendDisabled: true,
            showCounter: true,
            akey: 1,
            playing: true
            //verifyDisabled: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChangeValue = this.onChangeValue.bind(this);
        this.renderTime = this.renderTime.bind(this);
        
    }


    resendCode() {

        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);
        this.setState({resendDisabled: true});

        postData(this.props.defaultSets.apiUrl+'resend-activation', {
            userId: this.props.userInfo.id,
            langCode: this.props.selectedLanguage.langCode
        })
            .then((response) => {
                //console.log(response.data);
                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
                    this.setState({akey: this.state.akey + 1});
            })
            .catch((error) => {
                console.log(error);
            });
    }
    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit(e){
        e.preventDefault();
        
        this.setState({playing: true, showCounter: true})
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'activation', {
            code: this.state.code,
            langCode: this.props.selectedLanguage.langCode
        })
            .then((response) => {
                //console.log(response.data);
                if(response.data.success === true) {

                    let theUser = response.data.user;
                    theUser.accessToken = response.data.accessToken;
                    this.props.saveUserInfo(response.data.user);
                    
                    toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });
                    return this.props.history.push('/');
                }
                toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });

                //
            })
            .catch((error) => {
                //console.log(error);

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


     renderTime({ remainingTime }){
        if (remainingTime === 0) {
          return <div className="timer">Too lale...</div>;
        }
      
        return (
          <div className="timer">
            <div className="text">Remaining</div>
            <div className="value">{remainingTime}</div>
            <div className="text">seconds</div>
          </div>
        );
      };
   

    render() {

        const lng = this.props.selectedLanguage;

        return (
            <Fragment>

<Helmet><title>{lng.activationTxt}</title></Helmet>

<BreadCrumb title={lng.activationTxt} />


              <section className="login-page section-b-space">
        <div className="container">
            <div className="row">
                <div className="col-lg-6">
                    <h3>{lng.activationTxt}</h3>
                    <div className="theme-card">
                        <form className="theme-form" onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <label htmlFor="email">{lng.verificationCodeTxt}</label>
                                <input 
                                placeholder={lng.verificationCodeTxt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="code" />
                            </div>
                           
                            
                        <button 
                        type="submit" 
                        className="btn btn-solid">{lng.verifyTxt}</button>

<button 
                        style={{margin: '0 1em 0 1em'}}
                        type="button" 
                        disabled={this.state.resendDisabled}
                        onClick={() =>{ this.resendCode(); } } 
                        className="btn btn-solid">
                            {lng.resendTxt}
                        </button>


                        </form>
                    </div>
                </div>
                <div className="col-lg-6 right-login">
                    <h3></h3>
                    <div className="theme-card authentication-right">
                        
                        
                        <div className="time-wrapper">
                        {this.state.showCounter &&
                    <CountdownCircleTimer        
                    isPlaying={this.state.playing}
                    duration={60}
                    colors={[["#004777", 0.33], ["#F7B801", 0.33], ["#A30000"]]}
                    key={this.state.akey}
                    onComplete={() => {
                        this.setState({resendDisabled: false})
                        // do your stuff here
                        //return [true, 1500] // repeat animation in 1.5 seconds
                    }}> 
                    {this.renderTime}
                     </CountdownCircleTimer>}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   


</Fragment>


        )
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInfo: (userInfo) => dispatch({ type: 'SAVE_USER', payload: userInfo }),

        addToFavourites: (item) => {
            dispatch({ type: 'ADD_TO_FAV', payload: item });
        }
    }
};

const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,        
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Activation);
