import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import ImgToBeUploaded from '../../../components/ImageToUpload';
import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import {checkMimeType} from '../../../helpers/checkMimeType';
import postData from '../../../helpers/postData';

 class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            titleAR: '',
            //titleTR: '',
            titleEN: '',  
            status: 1,
            errorMessage: '',
            isDisabled: false,
            photo: '',         
            mainImg: null,
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

           //the next functions handle images on selected, change and upload.
           this.onMainImageChange = this.onMainImageChange.bind(this);
           this.rmvSlctdImg = this.rmvSlctdImg.bind(this);
           this.handleUploadMainPhoto = this.handleUploadMainPhoto.bind(this);
           //.
    }
  
    
    

    onMainImageChange(event) {
      if (event.target.files && event.target.files[0]) {

        let img = event.target.files[0];

        let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
        if(mimeTypeCheck == true){

          return this.setState({
            mainImg: img 
          }); 
        } 
        toast.error(mimeTypeCheck);
          
      }
    };

    
    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
    handleUploadMainPhoto(rowId) {
      if (this.state.mainImg == null ) {
        return false;
      }

      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', rowId);
      formData.append('type', 'category');
      formData.append('mainPhoto', '1');
      //.

      
      // append the main photo to formData.
      formData.append(
        'files[]',
        this.state.mainImg,
        this.state.mainImg.name
      )
      //.

      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({
          mainImg: null,
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }

    rmvSlctdImg() {
      this.setState({
        mainImg: null
      })
    }
    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }
    
    handleSubmit(e){
//console.log('k'+this.state.status)
        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'categories', {

            titleAR: this.state.titleAR, 
            //titleTR: this.state.titleTR,
            titleEN: this.state.titleEN,
            status: this.state.status,
            photo: this.state.mainImg ? this.state.mainImg.name :  null,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {
              this.handleUploadMainPhoto(response.data.rowId);

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

               /*  setTimeout( () => {
                    this.props.history.push('/categories');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
        const {errorMessage, isDisabled} = this.state;
        const { titleTxt, prcBfrDscnt, prcAftrDscnt, crtNewCatTxt, categoriesTxt, theCountTxt,
          skuTxt, statusTxt, mainPhotoTxt, saveTxt, descriptionTxt, activeTxt, inActiveTxt
         } = this.props.selectedLanguage;

        return (
                           
        
        <div className="row">

<Helmet><title>{crtNewCatTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/categories">{categoriesTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{crtNewCatTxt}</li>
          </ol>
        </nav>
      </div>


            <div className="col-md-8">
              <div className="card">
                <div className="card-header card-header-primary">
                  <h5 className="card-title mb-3">{crtNewCatTxt}</h5>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{titleTxt} AR</label>                          
                          <input 
                          className="form-control"
                          type="text"
                          name="titleAR"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>

                      {/* <div className="col-md-4">
                        <div className="form-group">
                          <label className="bmd-label-floating">{titleTxt} TR</label>                            
                          <input 
                          className="form-control"
                          type="text"
                          name="titleTR"
                          onChange={this.onChangeValue} />
                        </div>
                      </div> */}

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{titleTxt} EN</label>                            
                          <input 
                          className="form-control"
                          type="text"
                          name="titleEN"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>
                    </div>
                 

                    <div className="row">

                      <div className="col-md-12">
                        <div className="form-group">
                          <label className="bmd-label-floating">{statusTxt}</label>                     
                          <select 
                          name="status"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="1">{activeTxt}</option>
                            <option value="0">{inActiveTxt}</option>
                          </select>
                        </div>
                      </div>

                    </div>

                    <button 
                    type="submit" 
                    onClick={this.handleSubmit}
                    disabled={isDisabled}
                    className="btn btn-primary pull-right">{saveTxt}
                    </button>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            

            
        <div className="col-md-4">
         

         <div className="col-md-12">
            
         <div className="card card-profile">
               <div className="card-avatar">.</div>
               <div className="card-body">
                 <h6 className="card-category text-gray"></h6>
                 <h5 className="card-title mb-3">{mainPhotoTxt}</h5>
                 
                 <div className="card-description">
                   {this.state.mainImg != null && 
                   <ImgToBeUploaded
                   image={this.state.mainImg} 
                   rmvSlctdImg={this.rmvSlctdImg} />

                   }
                   <input type="file" 
                   accept=".gif,.jpg,.jpeg,.png"
                   onChange={this.onMainImageChange} />
                 </div>
                 
                 
               </div>
             </div>
          
         </div>
       
       
       </div>

          </div>
        
        

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Create);
