import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import SizeRow from '../../../components/SizeRow';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import TitleFilter from '../../../components/filters/TitleFilter';
import StatusFilter from '../../../components/filters/StatusFilter';
import getData from '../../../helpers/getData';

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: '',
            rowsBfrFltr: [],
            title: '',
            status: ''
        };
        this.onValueChange = this.onValueChange.bind(this);

      }
  
    
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      getData(this.props.defaultSets.apiUrl+'sizes')
      //.then(response => response.json())
          .then(response => {
            this.setState({ rows: response.data.rows, rowsBfrFltr: response.data.rows });
            toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    onValueChange(e) {
      //console.log(e.target.value)
      this.setState({
        [e.target.name]: e.target.value
    }, () => {
        this.tblFilter();
    });
    }

    tblFilter() {
              
       let newRows = this.state.rowsBfrFltr;      
       
        if(this.state.title != '') {
          newRows = newRows.filter(row => row.titleAR.search(this.state.title) !== -1 || (row.titleEN && row.titleEN.toLowerCase().search(this.state.title.toLowerCase()) !== -1));
        }
 
        if(this.state.status != '') {
          newRows = newRows.filter(row => row.status == this.state.status);
        }       
       
       this.setState({rows: newRows});
    }
 

    render() {
        
        //const {isLoading} = this.state;
        const { 
           titleTxt, 
           crtNewTxt, 
           sizesTxt,
           statusTxt, 
           photoTxt, 
           actionTxt} = this.props.selectedLanguage;

        return (
           
         <Fragment>
                <Helmet><title>{sizesTxt}</title></Helmet>

          <div className="row">
                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item active" aria-current="page">{sizesTxt}</li>
          </ol>
        </nav>
      </div>


            
      <div className="col-md-12">

<div className="row">
    <div className="col-md-6">
    <Link to='/admin/create-size' className="btn btn-primary">
        {crtNewTxt}
    </Link>
    </div>

      <TitleFilter
      selectedLanguage={this.props.selectedLanguage}
      onKeyUp={this.onValueChange}
      />                  
    
      <StatusFilter                     
      selectedLanguage={this.props.selectedLanguage}
      onChange={this.onValueChange} 
      />

</div>
  
</div>

          </div>

          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{sizesTxt}</h4>
                <p className="card-size"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    <thead className=" text-primary">                    
                      <tr>
                      <th>{'#'}</th>
                      <th>{titleTxt}</th>
                      <th>{statusTxt}</th>
                      <th>{photoTxt}</th>
                      <th>{actionTxt}</th>
                      </tr>                      
                    </thead>
                    <tbody>
                            {this.state.rows ? 
                            this.state.rows.map((item, index) => {
                              return(<SizeRow obj={item} num={index + 1} key={index}/>)
                            }) : null
                            }
                            
                    </tbody>
                  </table>
                </div>
              </div>
            
            </div>
          </div>

          </div>
      
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Index);
