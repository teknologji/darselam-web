
import React, {Component, Fragment} from 'react'
import {HashRouter as Router, Route} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.min.css';
import Sidebar from './partials/Sidebar';
import { connect } from 'react-redux';

import Nav from './partials/Nav';
import Footer from './partials/Footer';

import {appendScript, appendCss} from '../../helpers/appendScript';

class Layout extends Component {

    loadJs(){
        appendScript("/assets/admin/js/core/jquery.min.js");
        appendScript("/assets/admin/js/core/bootstrap-material-design.min.js");        
        appendScript("/assets/admin/js/plugins/perfect-scrollbar.jquery.min.js");
        appendScript("/assets/admin/js/material-dashboard.js?v=2.1.2");
        appendScript("/assets/admin/js/scripts.js");

    }

    loadCss() {   
    
        appendCss("/assets/admin/css/material-dashboard.css").then(res => {
            //this.setState({loading: false})
        }).catch(err => {
            console.log(err)
        });              
    }

    componentDidMount() {
        this.loadCss()

        this.loadJs();              
    }

    componentWillUnmount(){
        // remove css links.
        var element = document.getElementsByTagName("link"), index;

        for (index = element.length - 1; index >= 0; index--) {
            element[index].parentNode.removeChild(element[index]);
        }

        // remove scripts.
        var element = document.getElementsByTagName("script"), index;

        for (index = element.length - 1; index >= 0; index--) {
            element[index].parentNode.removeChild(element[index]);
        }
    }
    componentDidUpdate(){
        this.loadJs();              
    }
    render() {
        
        return (

                <Fragment>
                    <ToastContainer
                        rtl={true}
                        autoClose={5000}
                        position="top-center"
                        pauseOnHover={true}
                        hideProgressBar={true}
                    />
                    <div className="wrapper">
                        <Sidebar 
                        userInfo={this.props.userInfo}
                        selectedLanguage={this.props.selectedLanguage} />

                        <div className="main-panel"> 
                            <Nav 
                            selectedLanguage={this.props.selectedLanguage}                             userInfo={this.props.userInfo}
                            userInfo={this.props.userInfo}
                            logout={this.props.logout} 
        /*selectLanguage={this.selectLanguage}*/ />

                            <div className="content">
                                <div className="container-fluid">
                                {this.props.children}
                                </div>      
                            </div>

                            <Footer selectedLanguage={this.props.selectedLanguage} />
                        </div>

                    </div>  
                
                
                </Fragment>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => dispatch({ type: 'LOGOUT', payload: null }),

    }
}; 
export default connect(mapStateToProps, mapDispatchToProps)(Layout);