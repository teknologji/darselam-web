import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import Popup from "reactjs-popup";

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';

import DatePicker from "react-datepicker";
import Moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";
import getData from '../../../helpers/getData';
import deleteData from '../../../helpers/deleteData';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import RowThumbnail from '../../../components/RowThumbnail';

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: '',
            orderStatus: '',
            paymentStatus: '',
            orderId: '',
            full_name: '',
            phone: '',
            finalTotal: '',
            dtFrom: new Date(),
            dtTo: new Date()
        };
        this.handleOrderStatus = this.handleOrderStatus.bind(this);
        this.handleActions = this.handleActions.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
    }
  
    
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      getData(this.props.defaultSets.apiUrl+'orders')
      //.then(response => response.json())
          .then(response => {
              this.setState({ rows: response.data.rows, rowsBfrFltr: response.data.rows });
              toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    handleOrderStatus(orderId, newStatus){
      //console.log('k'+this.state.status)
      // get the value of the column to be modified.
      let columnValue = newStatus.target.value;
      
      //get current rows to update it with new modification.
      let currentRows = this.state.rows;
      let modifiedItem = currentRows.find(item => item.id == orderId);
      let newRows = currentRows.filter(item => item.id != orderId);
      modifiedItem[newStatus.target.name] = columnValue;
      newRows.push(modifiedItem);
      // sort it descently.
      newRows.sort(function (a, b) {
        return b.id - a.id;
      });
      //save new rows in state.
      this.setState({rows: newRows})

      // start updating it in DB.
      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      axios.put(this.props.defaultSets.apiUrl+'orders/'+orderId, {      
          id: orderId,
          [newStatus.target.name]: columnValue,
          langCode: this.props.selectedLanguage.langCode
      })
          .then( (response) => {

              toast.update(ldToast, {
                  type: toast.TYPE.SUCCESS,
                  render: response.data.msg
              });
          })
          .catch( (error) => {});
      
          }
  

          handleActions(orderId, action) {
            if(action == 'remove') {
              this.deleteRow(orderId);
            } else if (action == 'print'){
              alert('print!')
            } else {
              return false;
            }
          }
 
 

          deleteRow(id) {

            confirmAlert({
                  
              customUI: ({ onClose }) => {
                  return (
                      <div id="react-confirm-alert">
                          <div className="react-confirm-alert-overlay">
                              <div className="react-confirm-alert">
                                  <div className="react-confirm-alert-body">
                                      <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                      <div 
                                      style={{flexDirection: this.props.selectedLanguage.langCode == "AR" ? 'row-reverse': 'row'}}
                                      className="react-confirm-alert-button-group">
                                      <button
                                      className="yes"
                                      onClick={() => {
                                          deleteData(this.props.defaultSets.apiUrl+'orders/'+id)
                                          .then(res => {
                                              document.getElementById(id).remove();            
                                          })
                                          .catch(err => {
                                              console.log(err);
                                          });
                                          onClose();
                                      }}
                                      >
                                              {this.props.selectedLanguage.yesTxt}
                                          </button>
      
                                          <button className="no" onClick={onClose}>
                                          {this.props.selectedLanguage.noTxt}
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
      
                  );
                }
            });
        
      
          }

hndlDtFrom(dtFrom) {
  this.setState({
    dtFrom
  }, () => {
    this.tblFilter();
});
};

hndlDtTo(dtTo) {
  this.setState({
    dtTo
  }, () => {
    this.tblFilter();
});
};

onValueChange(e) {
  //console.log(e.target.value)
  this.setState({
    [e.target.name]: e.target.value
}, () => {
    this.tblFilter();
});
}

tblFilter() {
          //console.log(this.state.orderId)
   let newRows = this.state.rowsBfrFltr;      
      
   if(this.state.dtFrom != '' && this.state.dtTo != '') {
     let dtFromT = new Date(this.state.dtFrom);
     let dtToT = new Date(this.state.dtTo);
     //console.log(dtFromT+'kkkkkkkk'+dtToT)
     newRows = newRows.filter(row => row.created_at && new Date(row.created_at) >= dtFromT && new Date(row.created_at) <= dtToT);
    }

   if(this.state.finalTotal != '') {
    newRows = newRows.filter(row => row.finalTotal && row.finalTotal.toString().search(this.state.finalTotal) !== -1);
    }

    if(this.state.phone != '') {
    newRows = newRows.filter(row => row.user.phone && row.user.phone.search(this.state.phone) !== -1);
    }

    if(this.state.full_name != '') {
      newRows = newRows.filter(row => row.user.full_name && row.user.full_name.toLowerCase().search(this.state.full_name.toLowerCase()) !== -1);
    }

    if(this.state.orderId != '') {
      newRows = newRows.filter(row => row.orderId && row.orderId.search(this.state.orderId) !== -1);
    }

    if(this.state.orderStatus != '') {
      newRows = newRows.filter(row => row.orderStatus == this.state.orderStatus);
    }       
   
   this.setState({rows: newRows});
}
    render() {
        
        //const {isLoading} = this.state;
        const { dtFromTxt, dtToTxt, dateTxt, countryTxt, cityTxt, adrsTxt, closeTxt,
          paymentStatusTxt, orderStatusTxt, ordersTxt, orderDetailsTxt,customerNameTxt, doneTxt,
          cancelledTxt, returnedTxt, sentToCargoTxt, pendingTxt, waitingVerificationTxt,
          verifiedTxt, failedTxt, printTxt,chooseTxt, orderIdTxt, phoneTxt
      , langCode, deleteTxt, totalTxt, productTitleTxt, colourTxt, sizeTxt,receiptTxt,
      prcBfrDscnt, prcAftrDscnt, qtyTxt, subTotalTxt} = this.props.selectedLanguage;

        return (
           
         <Fragment>
                
          <div className="row">

                        
<Helmet><title>{ordersTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item active" aria-current="page">{ordersTxt}</li>
          </ol>
        </nav>
      </div>


            <div className="col-md-12">
              <div className="row">
      
    <div className="col-md-2">
      <input 
      type="text" 
      className="form-control" 
      name="finalTotal"
      placeholder={totalTxt}
      onKeyUp={this.onValueChange}
        />
    </div>
  
    <div className="col-md-2">
      <input 
      type="text" 
      className="form-control" 
      name="phone"
      placeholder={phoneTxt}
      onKeyUp={this.onValueChange}
        />
    </div>

    <div className="col-md-2">
      <input 
      type="text" 
      className="form-control" 
      name="full_name"
      placeholder={customerNameTxt}
      onKeyUp={this.onValueChange}
        />
    </div>

    <div className="col-md-2">
      <input 
      type="text" 
      className="form-control" 
      name="orderId"
      placeholder={orderIdTxt}
      onKeyUp={this.onValueChange}
        />
    </div>

    <div className="col-md-2">
            <select 
            name="paymentStatus"              
            onChange={this.onValueChange}
            className="form-control">
              <option value="">{paymentStatusTxt}</option>
              <option value="3">{waitingVerificationTxt}</option>
              <option value="1">{verifiedTxt}</option>
              <option value="2">{failedTxt}</option>
            </select>
    </div>

    <div className="col-md-2">
            <select 
            name="orderStatus"              
            onChange={this.onValueChange}
            className="form-control">
              <option value="">{orderStatusTxt}</option>
              <option value="3">{doneTxt}</option>
              <option value="4">{cancelledTxt}</option>
              <option value="6">{sentToCargoTxt}</option>
              <option value="7">{returnedTxt}</option>
              <option value="8">{pendingTxt}</option>
            </select>
    </div>
             
              </div>

              <div className="row">
                     
    <div className="col-md-2">
    <DatePicker
    selected={this.state.dtFrom}
    onChange={(dtFrom) => this.hndlDtFrom(dtFrom)}
    dateFormat="yyyy-MM-dd"                        
    className="form-control"
    value={this.state.dtFrom}
    />
      
    </div>
  
                       
    <div className="col-md-2">
    <DatePicker
    selected={this.state.dtTo}
    onChange={(dtTo) => this.hndlDtTo(dtTo)}
    dateFormat="yyyy-MM-dd"                        
    className="form-control"
    value={this.state.dtTo}
    />
    </div>
              </div>


            </div>
          </div>

          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{ordersTxt}</h4>
                <p className="card-order"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    
                  <thead className="text-primary">
                        <tr>
                            <th className="shoping__product">{'#'}</th>
                            <th>{orderDetailsTxt}</th>
                            <th>{customerNameTxt}</th>
                            <th>{totalTxt}</th>
                            <th>{orderStatusTxt}</th>
                            <th>{paymentStatusTxt}</th>
                            <th>{adrsTxt}</th>
                            <th>{receiptTxt}</th>
                            
                            <th>{dateTxt}</th>
                            <th>{deleteTxt}</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.rows && this.state.rows.map((item, i) => {
                                            return(
                                <tr id={item.id} key={i}>

                                    <td>{item.id}</td>

                                    <td>                                                                        
                                  <Popup 
                                  trigger={
                                  <button className="site-btn-small">
                                    <i className="fa fa-table"></i>
                                  </button>
                                  } 
                                  modal position="right center">                                  
                                  { close => (
                                      <div className="kmodal">
                                      <a className="close" onClick={close}>
                                        &times;
                                      </a>
                                      <div className="header">{orderDetailsTxt} {item.orderId}</div>
                                      <div className="content">
                                      
                                  <table>
                                      <thead>
                                          <tr>
                                              <th className="shoping__product">{'#'}</th>
                                              <th>{productTitleTxt}</th>
                                              <th>{prcBfrDscnt}</th>
                                              <th>{prcAftrDscnt}</th>
                                              <th>{colourTxt}</th>
                                              <th>{sizeTxt}</th>
                                              <th>{qtyTxt}</th>
                                              <th>{subTotalTxt}</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                      {item.order_items && item.order_items.map((row, k) => {
                                                      return(
                                                          
                                          <tr key={'oi'+k}>
                                            <td className="shoping__cart__item">
                                                  {k + 1}
                                              </td>                    
                                            <td className="shoping__cart__item">
                                              {eval("row.product.title"+langCode)}
                                            </td>
                                                                                    
                                            <td className="shoping__cart__item">
                                                {row.priceBeforeDiscount}
                                            </td>
                                                                                    
                                            <td className="shoping__cart__item">
                                                {row.priceAfterDiscount}
                                            </td>
                                                                                    
                                            <td className="shoping__cart__item">
                                            {row.size && eval("row.colour.title"+langCode)}
                                            </td>
                                                                                    
                                            <td className="shoping__cart__item">
                                            {row.size && eval("row.size.title"+langCode)}
                                            </td>
                                                                                    
                                            <td className="shoping__cart__item">
                                                {row.quantity}
                                            </td>
                                                                                    
                                            <td className="shoping__cart__item">
                                                {row.quantity * row.priceAfterDiscount}
                                            </td>
                                          </tr>
                                          
                                          );
                                      }
                                      )}                    
                                      </tbody>
                                        <tfoot>
                                          <tr>
                                              <th colSpan="3"></th>
                                              <th>{totalTxt} : {item.finalTotal}</th>
                                              <th colSpan="3"></th>
                                          </tr>
                                      </tfoot> 
                                  </table>
                                                      </div>
                              
                                                      <div className="actions">
                                                      <button
                                          className="site-btn-small"
                                          onClick={close}
                                        >{closeTxt}</button>
                                                      </div>
                              
                                  </div>
                                  )}
                                </Popup>
                                    </td>              
                                  <td>
                                    <Link 
                                    className="simple-text"
                                    to={"/admin/edit-user/"+item.user.id}>
                                    {item.user.full_name}
                                    </Link>
                                  </td>
                                  <td className="shoping__cart__item">
                                      {item.finalTotal} {item.transaction_currency}
                                  </td>
                                                                         
                                  <td className="shoping__cart__item">
                                    <select 
                                    name="orderStatus" 
                                    value={this.state.orderStatus || item.orderStatus}
                                    className="form-control"
                                    onChange={(newStatus) => {
                                      this.handleOrderStatus(item.id, newStatus);
                                    }} 
                                    >
                                      <option value="3">{doneTxt}</option>
                                      <option value="4">{cancelledTxt}</option>
                                      <option value="6">{sentToCargoTxt}</option>
                                      <option value="7">{returnedTxt}</option>
                                      <option value="8">{pendingTxt}</option>
                                    </select>
                                  </td>
                                                                         
                                  <td className="shoping__cart__item">
                                  <select 
                                    name="paymentStatus" 
                                    value={this.state.paymentStatus || item.paymentStatus}
                                    className="form-control"
                                    onChange={(newStatus) => {
                                      this.handleOrderStatus(item.id, newStatus);
                                    }} 
                                    >
                                      <option value="1">{verifiedTxt}</option>
                                      <option value="2">{failedTxt}</option>
                                      <option value="3">{waitingVerificationTxt}</option>
                                      
                                    </select>
                                  </td>
                                        
                                                                         
                                  <td className="shoping__cart__item">
                                                                                                            
                                  <Popup 
                                  trigger={
                                  <button className="site-btn-small">
                                    <i className="fa fa-map-marker"></i>
                                  </button>
                                  } 
                                  modal position="right center">                                  
                                  { close => (
                                      <div className="kmodal">
                                      <a className="close" onClick={close}>
                                        &times;
                                      </a>
                                      <div className="header">{adrsTxt}</div>
                                      <div className="content">
                                      
                                  <table>
                                      <thead>
                                          <tr>
                                              <th>{countryTxt}</th>
                                              <th>{cityTxt}</th>
                                              <th>{adrsTxt}</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                                          
                                          <tr>                   
                                            <td className="shoping__cart__item">
                                            {eval("item.shipmentadrs.country.title"+langCode)}
                                            </td>

                                            <td className="shoping__cart__item">
                                            {eval("item.shipmentadrs.city.title"+langCode)}
                                            </td>
                                                                                
                                            <td className="shoping__cart__item">
                                            {item.shipmentadrs.adrs}
                                            </td>
                                          </tr>  
                                                        
                                      </tbody>
                                        
                                  </table>
                                                      
                                                      </div>
                              
                                                      <div className="actions">
                                                      <button
                                          className="site-btn-small"
                                          onClick={close}
                                        >{closeTxt}</button>
                                                      </div>
                              
                                  </div>
                                  )}
                                </Popup>
                                
                                </td>
                                                                           
                                  <td className="shoping__cart__item">

                                                                                                               
                                  <Popup 
                                  repositionOnResize={true}
                                  lockScroll={false}
                                  overlayStyle={{zIndex: '9991'}}
                                  trigger={ item.files && item.files.length > 0 ?
                                  <button className="site-btn-small">
                                    <i className="fa fa-image"></i>
                                  </button>: null
                                  } 
                                  modal position="right center">                                  
                                  { close => (
                                      <div className="kmodal">
                                      <a className="close" onClick={close}>
                                        &times;
                                      </a>
                                      <div className="header">{receiptTxt}</div>
                                      <div className="content">
                                      
                                  <table style={{width: '100%', justifyContent: 'center'}}>
                                      
                                  <tbody>
                                            
                                
                                  {item.files.map((file, i) => {
                                    return( 
                                      
                                          <tr key={'orderFile'+i}>                   
                                            <td>
                                            <RowThumbnail imgName={file.fileName} width='300px' height="400px" />
                                            </td>
                                          
                                          </tr>  
                                                  )
                                                })}        
                                      </tbody>
                                        
                                  </table>
                                                      
                                                      </div>
                              
                                                      <div className="actions">
                                                      <button
                                          className="site-btn-small"
                                          onClick={close}
                                        >{closeTxt}</button>
                                                      </div>
                              
                                  </div>
                                  )}
                                </Popup>
                                
                                  </td>                               
                                
                                  <td className="shoping__cart__item">
                                  {Moment(item.created_at).format("YYYY-MM-DD  H:m")}
                                  </td>
                                   
                                  <td className="row shoping__cart__item__close">
                                  <select 
                                  onChange={(action) => this.handleActions(item.id, action.target.value)}
                                    className="form-control">
                                      <option value=''>{chooseTxt}</option>
                                      <option value='remove'>{deleteTxt}</option>
                                      <option value="print">{printTxt}</option>
                                    </select>

                                  </td>
                                </tr>
                               )
                            })}
                            
                    </tbody>
                  </table>
                </div>
              </div>
            
            </div>
          </div>

          </div>
      
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Index);
