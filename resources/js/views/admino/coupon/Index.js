import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import CouponRow from '../../../components/CouponRow';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import TitleFilter from '../../../components/filters/TitleFilter';
import StatusFilter from '../../../components/filters/StatusFilter';
import getData from '../../../helpers/getData';

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            rows: '' ,
            rowsBfrFltr: [],
            couponCode: '',
            status: '',
            type: ''
        };
        this.onValueChange = this.onValueChange.bind(this);

    }
  
   
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      getData(this.props.defaultSets.apiUrl+'coupons')
          //.then(response => response.json())
          .then(response => {
              this.setState({ rows: response.data.rows, rowsBfrFltr: response.data.rows });
              toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    
    onValueChange(e) {
      //console.log(e.target.value)
      this.setState({
        [e.target.name]: e.target.value
    }, () => {
        this.tblFilter();
    });
    }

    tblFilter() {
              //console.log(this.state.type)
       let newRows = this.state.rowsBfrFltr;      
       
        if(this.state.couponCode != '') {
          newRows = newRows.filter(row => row.couponCode.search(this.state.couponCode) !== -1);
        }
    
        if(this.state.type != '') {
          newRows = newRows.filter(row => row.type == this.state.type);
        }
 
        if(this.state.status != '') {
          newRows = newRows.filter(row => row.status == this.state.status);
        }       
       
       this.setState({rows: newRows});
    }
    render() {
        
        //const {isLoading} = this.state;
        const { 
           crtNewTxt, 
           couponsTxt,
           statusTxt, 
           photoTxt,
           typeTxt, expiryDateTxt,
           couponValueTxt, couponCodeTxt, fixedTxt, percentageTxt,
          originalQuantityTxt, usedQuantityTxt, minimumTxt,
           actionTxt} = this.props.selectedLanguage;

        return (
           
         <Fragment>
                
          <div className="row">

                        
<Helmet><title>{couponsTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item active" aria-current="page">{couponsTxt}</li>
          </ol>
        </nav>
      </div>


          <div className="col-md-12">
            <div className="row">
                    
            <div className="col-md-3">
      
              <Link to='/admin/create-coupon' className="btn btn-primary">
                  {crtNewTxt}
              </Link>
            </div>

    <div className="col-md-3">
        <input 
        type="text" 
        className="form-control" 
        name="couponCode"
        placeholder={couponCodeTxt}
        onKeyUp={this.onValueChange}
        />
    </div>

    <div className="col-md-3">
            <select 
            name="type"              
            onChange={this.onValueChange}
            className="form-control">
              <option value="">{typeTxt}</option>
              <option value="1">{fixedTxt}</option>
              <option value="0">{percentageTxt}</option>
            </select>
    </div>

      <StatusFilter                 
      selectedLanguage={this.props.selectedLanguage}
      onChange={this.onValueChange} 
      />
            </div>
          </div>

      

          </div>

          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{couponsTxt}</h4>
                <p className="card-coupon"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    <thead className=" text-primary">                    
                      <tr>
                      <th>{'#'}</th>
                      <th>{couponCodeTxt}</th>
                      <th>{couponValueTxt}</th>
                      <th>{typeTxt}</th>
                      <th>{minimumTxt}</th>
                      <th>{expiryDateTxt}</th>
                      <th>{originalQuantityTxt}</th>
                      <th>{usedQuantityTxt}</th>
                      <th>{statusTxt}</th>
                      <th>{photoTxt}</th>
                      <th>{actionTxt}</th>
                      </tr>                      
                    </thead>
                    <tbody>
                            {this.state.rows ? 
                            this.state.rows.map((item, index) => {
                              return(<CouponRow obj={item} num={index + 1} key={index}/>)
                            }) : null
                            }
                            
                    </tbody>
                  </table>
                </div>
              </div>
            
            </div>
          </div>

          </div>
      
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Index);
