import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ServiceRow from '../../../components/ServiceRow';
import Axios from 'axios';

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: ''
        };
    }
  
   
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      Axios.get(this.props.defaultSets.apiUrl+'services')
          //.then(response => response.json())
          .then(response => {
              this.setState({ rows: response.data.rows });
              toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    render() {
        
        //const {isLoading} = this.state;
        const { titleTxt, crtNewTxt, servicesTxt, statusTxt, mainPhotoTxt, actionTxt} = this.props.selectedLanguage;

        return (
           
         <Fragment>
                
          <div className="row">
            <div className="col-md-12">
                <Link to='create-service' className="btn btn-primary">
                    {crtNewTxt}
                </Link>
            </div>
          </div>

          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{servicesTxt}</h4>
                <p className="card-category"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    <thead className=" text-primary">                    
                      <tr>
                      <th>{'#'}</th>
                      <th>{titleTxt}</th>
                      <th>{statusTxt}</th>
                      <th>{mainPhotoTxt}</th>
                      <th>{actionTxt}</th>
                      </tr>                      
                    </thead>
                    <tbody>
                            {this.state.rows ? 
                            this.state.rows.map((item, index) => {
                              return(<ServiceRow obj={item} key={index} num={index+1} />)
                            }) : null
                            }
                            
                    </tbody>
                  </table>
                </div>
              </div>
            
            </div>
          </div>

          </div>
      
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Index);
