import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';

import BreadCrumb from '../partials/BreadCrumb';


import { Helmet } from 'react-helmet';


class Show extends Component {

    constructor(props) {
        super(props);
        this.state = {
            row: '',
        };
        
    }

    componentDidMount(){
        this.fetchService(this.props.match.params.id);
    }

    fetchService(id) {
        
        axios.get(this.props.defaultSets.apiUrl+'services/'+id)
            .then(response => {  
              this.setState({row: response.data});
                //console.log(response.data)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

    render() {
 
        const {row} = this.state;
        const lng = this.props.selectedLanguage;
        return (
    
            <Fragment>
 <BreadCrumb title={lng.servicesTxt} />
 <Helmet><title>{lng.servicesTxt}</title></Helmet>
   
 <div className="services">

<section className="p-0">
  <div className="full-banner parallax-banner4 parallax text-center p-left">
    <div className="bx-3">
      <div className="">
        <div className="top">
          <h2>Purchase & Payment</h2>
          <h2>E-shopping</h2>
          <h2>Shipping & maintenance</h2>
          <h2>quality service do as your wish</h2>
        </div>
      </div>
    </div>
  </div>
</section>

<div className="title4 pt-5">
  <h2 className="title-inner4">Our Services</h2>
  <div className="line">
    <span />
  </div>
</div>

<div className="container">
  <div className="service section-b-space  border-section border-top-0">
    <div className="row partition4 ">
      <div className="col-lg-3 col-md-6 service-block1">
        <i className="fa fa-bar-chart-o" ></i>
        <h4>Strategy</h4>
        <p>
          MWF Group follow basic continuous development of all detectors
          types, and after a lot of operations polls with a lot of
          customers, so that our customers get the best results, apply
          these updates also on devices sold to customers.
        </p>
      </div>
      <div className="col-lg-3 col-md-6 service-block1 ">
        <i className="fa fa-cogs" ></i>
        <h4>Technology</h4>
        <p>
          The technology used in our detectors machines are considered
          unique detection technology systems of its kind, and
          excellence in their results and performance, our detectors
          supported by best advanced electronic processors and software,
          in addition to the speed of response and targets analysis.
        </p>
      </div>
      <div className="col-lg-3 col-md-6 service-block1 border border-0">
        <i className="fa fa-lightbulb-o px-3" ></i>
        <h4>Creativity</h4>
        <p>
          Metal and groundwater detection devices on the market have
          multiplied in a very large, differ from each other in terms of
          work and brands, names and industries systems, but All that
          glitters is not gold , our devices maintained on creativity
          and fine results over 10 years all over the world ,It will
          also remain.
        </p>
      </div>
      <div className="col-lg-3 col-md-6 service-block1 border border-0">
        <i className="fa fa-area-chart" ></i>
        <h4>Success</h4>
        <p>
          MWF has achieved many successes, to provide the best services
          and facilities to customers, in addition to the successes
          achieved by our devices in many countries, in determining many
          treasures locations, and to identify many ground water wells.
        </p>
      </div>
    </div>
  </div>
</div>

<section className="p-0">
  <div className="full-banner parallax-banner5 parallax text-center p-left">
    <div className="bx-3">
      <div className="container service-faq">
        <div className="row justify-content-around">
          <div className="col-5 py-5">
            <div className="accordion" id="accordionExample">
              <div className="card">
                <div className="card-header" id="headingOne">
                  <h2 className="mb-0">
                    <button
                      className="btn btn-link btn-block text-left"
                      type="button"
                      data-toggle="collapse"
                      data-target="#collapseOne"
                      aria-expanded="true"
                      aria-controls="collapseOne"
                    >
                      Collapsible Group Item #1
                    </button>
                  </h2>
                </div>

                <div
                  id="collapseOne"
                  className="collapse show"
                  aria-labelledby="headingOne"
                  data-parent="#accordionExample"
                >
                  <div className="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod
                    high life accusamus terry richardson ad squid. 3
                    wolf moon officia aute, non cupidatat skateboard
                    dolor brunch. Food truck quinoa nesciunt laborum
                    eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put
                    a bird on it squid single-origin coffee nulla
                    assumenda shoreditch et. Nihil anim keffiyeh
                    helvetica, craft beer labore wes anderson cred
                    nesciunt sapiente ea proident. Ad vegan excepteur
                    butcher vice lomo. Leggings occaecat craft beer
                    farm-to-table, raw denim aesthetic synth nesciunt
                    you probably haven't heard of them accusamus labore
                    sustainable VHS.
                  </div>
                </div>
              </div>
              <div className="card">
                <div className="card-header" id="headingTwo">
                  <h2 className="mb-0">
                    <button
                      className="btn btn-link btn-block text-left collapsed"
                      type="button"
                      data-toggle="collapse"
                      data-target="#collapseTwo"
                      aria-expanded="false"
                      aria-controls="collapseTwo"
                    >
                      Collapsible Group Item #2
                    </button>
                  </h2>
                </div>
                <div
                  id="collapseTwo"
                  className="collapse"
                  aria-labelledby="headingTwo"
                  data-parent="#accordionExample"
                >
                  <div className="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod
                    high life accusamus terry richardson ad squid. 3
                    wolf moon officia aute, non cupidatat skateboard
                    dolor brunch. Food truck quinoa nesciunt laborum
                    eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put
                    a bird on it squid single-origin coffee nulla
                    assumenda shoreditch et. Nihil anim keffiyeh
                    helvetica, craft beer labore wes anderson cred
                    nesciunt sapiente ea proident. Ad vegan excepteur
                    butcher vice lomo. Leggings occaecat craft beer
                    farm-to-table, raw denim aesthetic synth nesciunt
                    you probably haven't heard of them accusamus labore
                    sustainable VHS.
                  </div>
                </div>
              </div>
              <div className="card">
                <div className="card-header" id="headingThree">
                  <h2 className="mb-0">
                    <button
                      className="btn btn-link btn-block text-left collapsed"
                      type="button"
                      data-toggle="collapse"
                      data-target="#collapseThree"
                      aria-expanded="false"
                      aria-controls="collapseThree"
                    >
                      Collapsible Group Item #3
                    </button>
                  </h2>
                </div>
                <div
                  id="collapseThree"
                  className="collapse"
                  aria-labelledby="headingThree"
                  data-parent="#accordionExample"
                >
                  <div className="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod
                    high life accusamus terry richardson ad squid. 3
                    wolf moon officia aute, non cupidatat skateboard
                    dolor brunch. Food truck quinoa nesciunt laborum
                    eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put
                    a bird on it squid single-origin coffee nulla
                    assumenda shoreditch et. Nihil anim keffiyeh
                    helvetica, craft beer labore wes anderson cred
                    nesciunt sapiente ea proident. Ad vegan excepteur
                    butcher vice lomo. Leggings occaecat craft beer
                    farm-to-table, raw denim aesthetic synth nesciunt
                    you probably haven't heard of them accusamus labore
                    sustainable VHS.
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-5 py-5">
            <img
              src="/assets/frontend/images/mwf-services.jpg"
              alt=""
              className="img-fluid blur-up lazyload"
              style={{width: '55%'}}
            />
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section className="px-5">
  <div className="title1 title5 pt-5">
    <h2 className="title-inner1">
      Purchase And Payment Services & Sales Department
    </h2>
    <hr role="tournament6" />
  </div>
  <div className="container p-5">
    <div className="row p-3">
      <div className="col">
        <h4>Support & Sales Department Services:</h4>
        <ul className="py-3">
          <li>
            Facilitate the sales Department to purchase the product on
            the Customer, as well as training on the product and the
            exact method used in cases where the client we have in the
            headquarters of the Company.
          </li>
          <li>
            Helping the customer accurately to choose the right product
            that suits with his research.
          </li>
          <li>
            We offer a warranty on all products for a period of 3 years.
          </li>
          <li>
            Provide the customer with user manuals and educational
            disks.
          </li>
          <li>
            We provide our clients from abroad with a quick and easy
            purchase process with strict purchase guarantee and purchase
            contracts guaranteeing the customer the right before and
            after purchasing the product.
          </li>
          <li>
            Facilitate the product price transfer of either by
            transferring to our bank account or through international
            money transfer companies or by paying online through our
            website.
          </li>
          <li>
            Continuous communication with the customer after the sale
            and confirmation of ensuring the work of the product and
            help him with any query, in addition to continuous
            communication with customers and inform them about any new,
            or any updates of the products.
          </li>
        </ul>
        <hr />
        <hr />
      </div>
    </div>
    <div className="row p-3">
      <div className="col">
        <h4>Payment Services and E-shopping:</h4>
        <ul className="py-3">
          <li>
            E-Payment services by credit card, through the (Checkout)
            service:
            <br />
            1. Master Card&nbsp;&nbsp; 2. Visa Card&nbsp;&nbsp; 3.
            Discover&nbsp;&nbsp; 4. American Express&nbsp;&nbsp; 5.
            Debit Card&nbsp;&nbsp; 6. Credit Card
          </li>
          <li>
            Now you can through our certified website which was supplied
            with the strongest protection systems
            (mwf-metaldetectors.com) You can purchase any product wish
            you want to buy,
          </li>
          <li>
            Just sign up and add the product you want to buy to cart,
            and then complete the payment process, fill in the shipping
            address and confirm your order,
          </li>
          <li>
            The follow-up team will be check and confirm your order, and
            send to you the electronic invoice,
          </li>
          <li>
            The shipping team will be send the device to your address
            directly in a period not exceeding a week from the payment
            date.
          </li>
        </ul>
        <hr />
        <hr />
      </div>
    </div>
    <div className="row p-3">
      <div className="col">
        <h4>Our Company Bank Accounts Information’s:</h4>
        <ul className="py-3">
          <li className="d-inline-block">
            <b>COMPANY NAME / BENEFICIARY :</b> &nbsp;&nbsp; MWF
            DEDEKTOR ELEKTRONİK TEKNOLOJİLERİ SAN VE TİC LTD STİ
            <hr />
          </li>
          <li className="d-inline-block">
            <b>BENEFICIARY ADDRESS:</b> &nbsp;&nbsp; Gürsel Mah.Nemzet
            Sok (Fulya Sokak) No.15/03 Ekşioğlu Plaza – Kağıthane /
            Istanbul –Turkey
            <hr />
          </li>
          <li className="d-inline-block">
            <b>BANK`S NAME / NO / BRANCH NAME:</b> &nbsp;&nbsp; TÜRKİYE
            İŞ BANKASI / Branch No. 1240 / CEVAHİR ALIŞVERİŞ MERKEZİ
            Bank`s Name / No / Branch Name:
            <hr />
          </li>
          <li className="d-inline-block">
            <b>BANK ADDRESS :</b> &nbsp;&nbsp; 19 Mayıs Mah, Büyükdere
            Cad, Cevahir Avm No:22/23 Şişli – Istanbul / Turkey
            <hr />
          </li>
          <li className="d-inline-block">
            <b>SWIFT CODE : </b> &nbsp;&nbsp; ISBKTRISXXX
            <hr />
          </li>
          <li className="d-inline-block">
            <b>DOLLAR ACCOUNT NO. IBAN: </b> &nbsp;&nbsp; TR46 0006 4000
            0021 2400 0404 94 ( To Transfer in Dollar )
            <hr />
          </li>
          <li className="d-inline-block">
            <b>EURO ACCOUNT NO. IBAN: </b> &nbsp;&nbsp; TR13 0006 4000
            0021 2400 0405 06 ( To Transfer in Euro )
            <hr />
          </li>
          <li className="d-inline-block">
            <b>TURKISH LIRA ACCOUNT NO. IBAN: </b> &nbsp;&nbsp; TR30
            0006 4000 0011 2400 1203 27 ( To Transfer in Turkish Lira )
          </li>
        </ul>
        <hr />
        <hr />
      </div>
    </div>
  </div>
</section>

</div>


 </Fragment>

    )
    }
}





const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        //userInfo: state.User.info
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Show);