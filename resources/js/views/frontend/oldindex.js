import React, { Component, createContext } from 'react';
import ReactDOM from 'react-dom';
 
//import {Route, HashRouter as Router } from 'react-router-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';

//import { ScrollContext } from 'react-router-scroll-4';

import AdminHome from "./views/admin/HomeScreen";
import FrontHome from "./views/frontend/HomeScreen";

import AdminRoute from './routes/AdminRoute';


import AdminLayout from './views/admin/Layout';
import FrontLayout from './views/frontend/Layout';

import AllUsers from './views/admin/user/Index';
import CreateUser from './views/admin/user/Create';
import editUser from './views/admin/user/Edit';

import AllCategories from './views/admin/category/Index';
import CreateCategory from './views/admin/category/Create';
import EditCategory from './views/admin/category/Edit';

import AllProducts from './views/admin/product/Index';
import CreateProduct from './views/admin/product/Create';
import EditProduct from './views/admin/product/Edit';

import AllArticles from './views/admin/article/Index';
import CreateArticle from './views/admin/article/Create';
import EditArticle from './views/admin/article/Edit';

import AllServices from './views/admin/service/Index';
import CreateService from './views/admin/service/Create';
import EditService from './views/admin/service/Edit';

import AllCpages from './views/admin/cpage/Index';
import CreateCpage from './views/admin/cpage/Create';
import EditCpage from './views/admin/cpage/Edit';


import { Provider } from 'react-redux';
import {persistor, store} from './store';
import { PersistGate } from 'redux-persist/integration/react'

export default class Index extends Component {
    render() {
        return ( 
            <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>

            <Router>
                <Switch>
                    
                    <AdminLayout>
                        <AdminRoute path="/users" component={AllUsers} />
                        <AdminRoute path="/create-user" component={CreateUser} />
                        <AdminRoute path="/edit-user/:id" component={editUser} />
                        
                        <AdminRoute exact path="/products" component={AllProducts} />
                        <AdminRoute path="/create-product" component={CreateProduct} />
                        <AdminRoute exact path="/edit-product/:id" component={EditProduct} />

                        <AdminRoute path="/categories" component={AllCategories} />
                        <AdminRoute path="/create-category" component={CreateCategory} />
                        <AdminRoute path="/edit-category/:id" component={EditCategory} />

                        <AdminRoute path="/articles" component={AllArticles} />
                        <AdminRoute path="/create-article" component={CreateArticle} />
                        <AdminRoute path="/edit-article/:id" component={EditArticle} />

                        <AdminRoute path="/services" component={AllServices} />
                        <AdminRoute path="/create-service" component={CreateService} />
                        <AdminRoute path="/edit-service/:id" component={EditService} />

                        <AdminRoute path="/cpages" component={AllCpages} />
                        <AdminRoute path="/create-cpage" component={CreateCpage} />
                        <AdminRoute path="/edit-cpage/:id" component={EditCpage} />

                        <AdminRoute path="/admin" component={AdminHome} />

                        </AdminLayout>

                    <FrontLayout>
                    <Route exact path="/" component={FrontHome} />
                    </FrontLayout>
                
                </Switch>
  
            </Router>
            </PersistGate>

            </Provider>

        );
    }
}

if (document.getElementById('home')) {
    ReactDOM.render(<Index />, document.getElementById('home'));
}
