import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import {toast} from "react-toastify";
//import ProductBox from '../../../components/ProductBox';

import {Helmet} from "react-helmet";


class Index extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
        this.addToFav = this.addToFav.bind(this); 

    }

    componentDidMount(){
        console.log('kkk', this.props.compareList)
    }
    addToFav(item) {
        
        if(this.props.userInfo.id == 0) {
            return toast.info(this.props.selectedLanguage.plsLgnFrst);
        }
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'favourites', {
            userId: this.props.userInfo.id,
            itemId: item.id,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

                if(response.data.success == true) {

                    item.favId = response.data.favId;

                    toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });
                    
                    return this.props.addToFavourites(item);
                }
                return toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
            })
            .catch( (error) => {
               
            });

    }

    render() {
 
        const lng = this.props.selectedLanguage;
        const rows = this.props.compareList;
        const slctdCurr = this.props.slctdCurr;

        return (
    
            <Fragment>
            <Helmet><title>{lng.compareTxt}</title></Helmet>

 <BreadCrumb title={lng.compareTxt} />
 

 <section className="compare-padding">
        <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <div className="compare-page">
                        <div className="table-wrapper table-responsive">
                            <table className="table">
                                <thead>
                                    <tr className="th-compare">
                                        <td>{lng.actionTxt}</td>

                                        {rows && rows.map((item, i) => {
                                    return(
                                        <th className="item-row" key={"rmv"+i}>
                                        <button type="button" className="remove-compare"
                                        onClick={(e) => {e.preventDefault(); this.props.removeCompare(item);}}
                                        >{lng.deleteTxt}</button>
                                    </th>
                                        );
                                    })
                                }
                                        
                                       
                                       
                                    </tr>
                                </thead>
                                <tbody id="table-compare">
                                    <tr>
                                        <th className="product-name">{lng.productTitleTxt} </th>
                                        
                                {rows && rows.map((item, i) => {
                                const elmntTrns = item.elment_trans && item.elment_trans.find(prdct => prdct.languageCode == lng.langCode);

                                    return(
                                        <td key={'titleTD'+i} className="grid-link__title">
                                            {elmntTrns.title}

                                        </td>);
                                    })
                                }
                                    </tr>
                                    <tr>
                                        <th className="product-name">{lng.photoTxt} </th>
                                       
                                        {rows && rows.map((item, i) => {
                                const elmntTrns = item.elment_trans && item.elment_trans.find(prdct => prdct.languageCode == lng.langCode);

                                    return(
                                        <td className="item-row" key={'img'+i}>
                                            {elmntTrns.files[0] && 
                                            <img src={"/uploads/files/"+elmntTrns.files[0].fileName} 
                                            alt={elmntTrns.title}
                                            className="featured-image" />}

                                            <div className="product-price product_price">
                                            {item.priceAfterDiscount != item.priceBeforeDiscount && <strong>{lng.onSaleTxt}: </strong>}
                                            {item.priceAfterDiscount == item.priceBeforeDiscount || item.priceAfterDiscount == 0 ?
                                                

                    <span>{(item.priceBeforeDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}</span>
                :<span>{(item.priceAfterDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}<del className="text-danger">{(item.priceBeforeDiscount/slctdCurr.exchangeRate).toFixed(2)}</del></span>}
                

                                            </div>
                                            <form className="variants clearfix">

                                                <button title={lng.addToCartTxt}
                                                 onClick={(e) => {
                                                    e.preventDefault();
                                                    this.props.addToCart(item, lng.addedToCartTxt)
                                                    }
                                                }
                                                className="add-to-cart btn btn-solid">
                                                    {lng.addToCartTxt}
                                                </button>
                                            </form>
                                            <p className="grid-link__title hidden"></p>
                                        </td>
                                        );
                                    })
                                }
                                        

                                    </tr>
                                    <tr>
                                        <th className="product-name">{lng.descriptionTxt}</th>
                                        {rows && rows.map((item, i) => {
                                         const elmntTrns = item.elment_trans && item.elment_trans.find(prdct => prdct.languageCode == lng.langCode);

                                    return(
                                        <td className="item-row" key={"desc"+i}>
                                            <p className="description-compare">
                                                {elmntTrns.description && elmntTrns.description.replace(/(&nbsp;|<([^>]+)>)/ig, "").slice(0, 100)}
                                            </p>
                                        </td>);
                                    })
                                }
                                    </tr>
                                    <tr>
                                        <th className="product-name">{lng.availabilityTxt}</th>
                                        {rows && rows.map((item, i) => {
                                    return(
                                        <td className="available-stock" key={"av"+i}>
                                            <p>{lng.yesTxt}</p>
                                        </td>);
                                         })
                                        }
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
                            
                           
   

</Fragment>  
    
    )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (product, msg) => {
            toast.success(msg);
            dispatch({ type: 'ADD_TO_CART', payload: product })
        },
        addToFavourites: (item) => {
            dispatch({ type: 'ADD_TO_FAV', payload: item });
        },
        removeCompare: (product) => dispatch({ type: 'REMOVE_FROM_COMPARE', payload: product }),

    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        compareList: state.CompareList.cart,
        userInfo: state.User.info,
        favedItems: state.Favourites.rows,
        slctdCurr: state.Currs.currencyInfo,
        defaultSets: state.DfltSts,
        cats: state.Cats.rows,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Index);