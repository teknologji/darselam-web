import React, { Component, Fragment} from 'react'

import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import {Helmet} from "react-helmet";
import {Link} from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import deleteData from '../../../helpers/deleteData';
import getData from '../../../helpers/getData';
import {toast} from "react-toastify";

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: []
        };
    }
  
   
    componentDidMount(){
        this.fetchData();
    }

    
   fetchData(){
    //const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

    getData(this.props.defaultSets.apiUrl+'user-favourites/'+this.props.userInfo.id, this.props.userInfo.accessToken)
        //.then(response => response.json())
        .then(response => {
            this.setState({ rows: response.data.rows });
            //toast.dismiss(ldToast)
            //console.log(response.data)
        })
        .catch(function (error) {
            console.log(error);
        })
   }

    deleteRow(id) {

        confirmAlert({
              
          customUI: ({ onClose }) => {
              return (
                  <div id="react-confirm-alert">
                      <div className="react-confirm-alert-overlay">
                          <div className="react-confirm-alert">
                              <div className="react-confirm-alert-body">
                                  <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                  <div 
                                  style={{flexDirection: this.props.selectedLanguage.langCode == "AR" ? 'row-reverse': 'row'}}
                                  className="react-confirm-alert-button-group">
                                  <button
                                  className="yes"
                                  onClick={() => {
                                    this.props.removeFromFavourites(id)

                                      deleteData(this.props.defaultSets.apiUrl+'favourites/'+id, this.props.userInfo.accessToken)
                                      .then(res => {
                                          document.getElementById(id).remove(); 
           
                                      })
                                      .catch(err => {
                                          console.log(err);
                                      });
                                      onClose();
                                  }}
                                  >
                                          {this.props.selectedLanguage.yesTxt}
                                      </button>
  
                                      <button className="no" onClick={onClose}>
                                      {this.props.selectedLanguage.noTxt}
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
  
              );
            }
        });
    
  
    }

   
  
    render() {
        
        
        const {rows} = this.state;
        const lng = this.props.selectedLanguage;
        const slctdCurr = this.props.slctdCurr;

        return (
           
         <Fragment>

<Helmet><title>{lng.myFavsTxt}</title></Helmet>

         <BreadCrumb title={lng.myFavsTxt} />



         <section className="wishlist-section section-b-space">
        <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <table className="table cart-table table-responsive-xs">
                        <thead>
                            <tr className="table-head">
                                <th scope="col">#</th>
                                <th scope="col">{lng.photoTxt}</th>
                                <th scope="col">{lng.productTitleTxt}</th>
                                <th scope="col">{lng.priceTxt}</th>
                                <th scope="col">{lng.availabilityTxt}</th>
                                <th scope="col">{lng.actionTxt}</th>
                            </tr>
                        </thead>
                        <tbody>

                        {rows && rows.map((item, i) => {
                                    const elmntTrns = item.product && item.product.elment_trans.find(row => row.languageCode == lng.langCode);

                                            return(
                                <tr key={'fav'+i} id={item.id}>
                                    <td>{i+1}</td>
                                    <td>
                                    {elmntTrns.files[0] && 
                                    <Link to={"/products/"+item.product.id}>                                        
                                    <img src={"/uploads/files/"+elmntTrns.files[0]['fileName']} 
                                    alt={elmntTrns.title} />
                                    </Link>}
                                    </td>
                                    <td>
                                    <Link to={"/products/"+item.product.id}>
                                        {elmntTrns.title}
                                    </Link>
                                    <div className="mobile-cart-content row">
                                        <div className="col-xs-3">
                                            <p>{lng.availableTxt}</p>
                                        </div>
                                        <div className="col-xs-3">
                                            <h2 className="td-color">{(item.product.priceAfterDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}</h2>
                                        </div>
                                        <div className="col-xs-3">
                                            <h2 className="td-color">
                                                <a href="#" className="icon mr-1"><i className="ti-close"></i>
                                                </a>
                                                <a href="#" className="cart"><i className="ti-shopping-cart"></i>
                                                </a></h2>
                                        </div>
                                    </div>
                                    </td>
                           
                            
                                    <td><h2>{(item.product.priceAfterDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}</h2></td>
                          
                                    <td><p>{lng.availableTxt}</p></td>


                                    <td>

                                    <a href="#"
                                        className="icon mr-3"
                                        onClick={(e) => {e.preventDefault(); this.deleteRow(item.id)}}>   
                                        <i className="ti-close"></i>                                     
                                        </a>

                                    <a href="#"
                                    onClick={(e) => { 
                                        e.preventDefault(); 
                                        this.props.addToCart(item.product, lng.addedToCartTxt);
                                    }}
                                    className="cart"><i className="ti-shopping-cart"></i>
                                    </a>
                                   
                                    </td>
                               
                            </tr>
                               )
                            })}


                            
                        
                        </tbody>
                    </table>
                </div>
            </div>
            <div className="row wishlist-buttons">
                <div className="col-12"><Link to="/" className="btn btn-solid">
                    {lng.continueShoppingTxt}</Link> 
                    <Link to="/cart"
                        className="btn btn-solid">{lng.checkoutTxt}</Link></div>
            </div>
        </div>
    </section>
   
              

         </Fragment>

        )
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (product, msg) => {
            toast.success(msg);
            dispatch({ type: 'ADD_TO_CART', payload: product })
        },
        removeFromFavourites: (itemId) => {
            dispatch({ type: 'REMOVE_FROM_FAV', payload: itemId });
        }
    }
}

const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,
        slctdCurr: state.Currs.currencyInfo,
        favedItems: state.Favourites.rows
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Index);
