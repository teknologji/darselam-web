import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import {toast} from "react-toastify";
//import ProductBox from '../../../components/ProductBox';
import { Link } from 'react-router-dom';

import {Helmet} from "react-helmet";
import postData from '../../../helpers/postData';
import ProductBoxInCat from '../../../components/ProductBoxInCat';


class Show extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            catInfo: '',
            rowsBfrFltr: [],
            lastAdded: [],
            
            //offers: [],
            categoryId: 0,
            //isLoaded: false
        };
        this.addToFav = this.addToFav.bind(this); 
        this.onFilterChange = this.onFilterChange.bind(this);
        this.tblFilter = this.tblFilter.bind(this);
    }

    componentDidMount(){
        //this.setState({catId: this.props.match.params.id, isLoaded: true})
        this.fetchProducts(this.props.match.params.id);
        this.fetchLastAdded();

    }

    componentDidUpdate(prevProps, prevState) {

        if (prevProps.match.params.id != this.props.match.params.id ) {
            this.fetchProducts(this.props.match.params.id);
        }
      }
    
      addToFav(item) {
        
        if(this.props.userInfo.id == 0) {
            return toast.info(this.props.selectedLanguage.plsLgnFrst);
        }
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'favourites', {
            userId: this.props.userInfo.id,
            itemId: item.id,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

                if(response.data.success == true) {
                    toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });
                    item.favId = response.data.favId;

                    return this.props.addToFavourites(item);
                }
                return toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
            })
            .catch( (error) => {
               
            });

    }
    fetchProducts(categoryId) {
        
        axios.get(this.props.defaultSets.apiUrl+'prdcts-by-catId/'+categoryId)
            .then(response => {  
              this.setState({rows: response.data.rows, rowsBfrFltr: response.data.rows,catInfo: response.data.catInfo});
                //console.log(response.data)
                
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

  

   /*  
    fetchOffers() {
        
        axios.get(this.props.defaultSets.apiUrl+'offer-products')
            .then(response => {  
              this.setState({offers: response.data});
                
            })
            .catch(function (error) {
                console.log(error);
            });
      
    } */

    onFilterChange(e) {
        //console.log(e.target.value)
        this.setState({
          [e.target.name]: e.target.value
      }, () => {
          this.tblFilter();
      });
    }

    tblFilter() {
//console.log(categoryId)

        let newRows = this.state.rowsBfrFltr;  
        let categoryId = this.state.categoryId;

        if(categoryId != '') {
            newRows = this.state.rowsBfrFltr.filter(function(item) {
                //applying filter for the inserted text in search bar
                //console.log(item.cats)
                const itemData = item.cats ? item.cats : []; //item.categoryId.toString();
                let prdctCats = [];
                itemData.map((prdctCat, k) => {
                prdctCats.push(prdctCat.id);
                });
                //console.log(prdctCats)
                //console.log(categoryId)

                return prdctCats.indexOf(parseInt(categoryId)) > -1;
            });
        }

         /* if(this.state.categoryId != '') {
           newRows = newRows.filter(row => row.categoryId == this.state.categoryId);
         }  */   
        
        this.setState({rows: newRows});
     }

     fetchLastAdded() {
        
        axios.get(this.props.defaultSets.apiUrl+'last-products/5')
            .then(response => {  
              this.setState({lastAdded: response.data});
                
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

    render() {
 
        const {rows, catInfo, lastAdded} = this.state;
        const lng = this.props.selectedLanguage;
        const slctdCurr = this.props.slctdCurr;
        let catTrns = catInfo.elment_trans && catInfo.elment_trans.find(row => row.languageCode == lng.langCode);

        return (
    
            <Fragment>
            <Helmet><title>{catTrns ? catTrns.title: lng.allProductsTxt}</title></Helmet>

 <BreadCrumb title={catTrns ? catTrns.title : lng.allProductsTxt} />


 <section className="section-b-space ratio_asos">
        <div className="collection-wrapper">
            <div className="container">
                <div className="row">
                    <div className="col-sm-3 collection-filter">
                        
                        <div className="collection-filter-block">
                            
                            <div className="collection-mobile-back"><span className="filter-back"><i className="fa fa-angle-left"
                                        aria-hidden="true"></i> </span></div>
                            <div className="collection-collapse-block open">
                                <h3 className="collapse-block-title">{lng.categoriesTxt}</h3>
                                <div className="collection-collapse-block-content">
                                    
        {this.props.cats && this.props.cats.map((item, i) => {
                                    let elmntTrns = item.elment_trans && item.elment_trans.find(prdct => prdct.languageCode == lng.langCode);

            return(
                                    <div className="collection-brand-filter" key={'cat'+item.id}>
                                        <div className="custom-control custom-checkbox collection-filter-checkbox">
                                            <input 
                                            name="categoryId"
                                            onChange={this.onFilterChange}
                                            value={item.id}
                                            type="radio" 
                                            className="custom-control-input" 
                                            id={"zara"+i} />
                                            <label className="custom-control-label" htmlFor={"zara"+i}>
                                            {elmntTrns && elmntTrns.title}
                                            </label>
                                        </div>
                                    </div>
            )})}
                                </div>
                            </div>
                            
                           {/*  <div className="collection-collapse-block border-0 open">
                                <h3 className="collapse-block-title">price</h3>
                                <div className="collection-collapse-block-content">
                                    <div className="collection-brand-filter">
                                        <div className="custom-control custom-checkbox collection-filter-checkbox">
                                            <input type="checkbox" className="custom-control-input" id="hundred" />
                                            <label className="custom-control-label" htmlFor="hundred">$10 - $100</label>
                                        </div>
                                        <div className="custom-control custom-checkbox collection-filter-checkbox">
                                            <input type="checkbox" className="custom-control-input" id="twohundred" />
                                            <label className="custom-control-label" htmlFor="twohundred">$100 - $200</label>
                                        </div>
                                        <div className="custom-control custom-checkbox collection-filter-checkbox">
                                            <input type="checkbox" className="custom-control-input" id="threehundred" />
                                            <label className="custom-control-label" htmlFor="threehundred">$200 - $300</label>
                                        </div>
                                        <div className="custom-control custom-checkbox collection-filter-checkbox">
                                            <input type="checkbox" className="custom-control-input" id="fourhundred" />
                                            <label className="custom-control-label" htmlFor="fourhundred">$300 - $400</label>
                                        </div>
                                        <div className="custom-control custom-checkbox collection-filter-checkbox">
                                            <input type="checkbox" className="custom-control-input" id="fourhundredabove" />
                                            <label className="custom-control-label" htmlFor="fourhundredabove">$400
                                                above</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        */}
                       
                       </div> 
                        
                        <div className="theme-card">
                            <h5 className="title-border">{lng.newProductsTxt}</h5>
                            <div className="offer-slider slide-1">
                                <div>
                                    
                        {lastAdded && lastAdded.map((item, i) => {
            let elmntTrns = item.elment_trans && item.elment_trans.find(row => row.languageCode == lng.langCode);

                        return(
                                    <div className="media" key={'prdct'+item.id}>
                                        <Link to={"/products/"+item.id}>
                                        {elmntTrns && elmntTrns.files[0] && 
                                        <img 
                                        src={"/uploads/files/"+elmntTrns.files[0].fileName}
                                        alt={elmntTrns.title}
                                        className="bg-img blur-up lazyload"
                                        />}
                                        </Link>
                                        <div className="media-body align-self-center">
                                            
                                            <Link to={"/products/"+item.id}>
                                                <h6>{elmntTrns && elmntTrns.title}</h6>
                                            </Link>

                                            {item.priceAfterDiscount == item.priceBeforeDiscount || item.priceAfterDiscount == 0 ?
                    <h4>{(item.priceBeforeDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}</h4>
                :<h4>{(item.priceAfterDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}<del className="text-danger">{(item.priceBeforeDiscount/slctdCurr.exchangeRate).toFixed(2)}</del></h4>}
                
                
                                        </div>
                                    </div>
                                   );
                                })
                            }
                                
                                </div>
                                
                            </div>
                        </div>
                        
                        <div className="collection-sidebar-banner">
                            <a href="#"><img src="/assets/frontend/images/side-banner.png" className="img-fluid blur-up lazyload" alt="" /></a>
                        </div>
                        
                    </div>

                    <div className="collection-content col">
                        <div className="page-main-content">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="top-banner-wrapper">
                                    {catTrns && catTrns.files[1] && 
                                    <img 
                                    src={"/uploads/files/"+catTrns.files[1].fileName}
                                    alt={catTrns && catTrns.title}
                                    className="img-fluid blur-up lazyload"
                                    />}
                                        <div className="top-banner-content small-section"> 
                                            
                                        </div>
                                    </div>
                                    <div className="collection-product-wrapper">
                                        <div className="product-top-filter">
                                            <div className="row">
                                                <div className="col-xl-12">
                                                    <div className="filter-main-btn"><span className="filter-btn btn btn-theme"><i className="fa fa-filter"
                                                                aria-hidden="true"></i> </span></div>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-12">
                                                    <div className="product-filter-content">
                                                        <div className="search-count">
                                                            <h5>{lng.productsCountTxt} : {rows ? rows.length: '0'}</h5>
                                                        </div>
                                                        <div className="collection-view">
                                                            <ul>
                                                                <li><i className="fa fa-th grid-layout-view"></i></li>
                                                                <li><i className="fa fa-list-ul list-layout-view"></i></li>
                                                            </ul>
                                                        </div>
                                                        <div className="collection-grid-view">
                                                            <ul>
                                                                <li><img src="/assets/frontend/images/icon/2.png" alt="" className="product-2-layout-view" /></li>
                                                                <li><img src="/assets/frontend/images/icon/3.png" alt="" className="product-3-layout-view" /></li>
                                                                <li><img src="/assets/frontend/images/icon/4.png" alt="" className="product-4-layout-view" /></li>
                                                                <li><img src="/assets/frontend/images/icon/6.png" alt="" className="product-6-layout-view" /></li>
                                                            </ul>
                                                        </div>
                                                        <div className="product-page-per-view">
                                                            <select>
                                                                <option value="High to low">24 Products Par Page
                                                                </option>
                                                                <option value="Low to High">50 Products Par Page
                                                                </option>
                                                                <option value="Low to High">100 Products Par Page
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <div className="product-page-filter">
                                                            <select>
                                                                <option value="High to low">Sorting items</option>
                                                                <option value="Low to High">50 Products</option>
                                                                <option value="Low to High">100 Products</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="product-wrapper-grid"> {/* product-load-more */}
                                            <div className="row margin-res">
                                                 
                        {rows && rows.map((item, i) => {
                            return(<ProductBoxInCat
                                obj={item} 
                                selectedLanguage={this.props.selectedLanguage}  
                                slctdCurr={this.props.slctdCurr}                          
                                key={'prdctBox'+i}                
                                addToCart={this.props.addToCart} 
                                addToFav={this.addToFav}          
                                favedItems={this.props.favedItems} 
                                />);
                            })
                        }

                                                
                                            </div>
                                        </div>
                                        <div className="load-more-sec">
                                            <a href="#" className="loadMore">
                                                {lng.showMoreTxt}</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   

</Fragment>  
    
    )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (product, msg) => {
            toast.success(msg);
            dispatch({ type: 'ADD_TO_CART', payload: product })
        },
        addToFavourites: (item) => {
            dispatch({ type: 'ADD_TO_FAV', payload: item });
        }
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        userInfo: state.User.info,
        favedItems: state.Favourites.rows,
        slctdCurr: state.Currs.currencyInfo,
        defaultSets: state.DfltSts,
        cats: state.Cats.rows,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Show);