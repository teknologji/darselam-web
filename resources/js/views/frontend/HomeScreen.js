import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import ProductBox from "../../components/ProductBox";
import ArticleBox from "../../components/ArticleBox";
import { Link } from "react-router-dom";
import postData from "../../helpers/postData";

import { Helmet } from "react-helmet";
import Carousel, { consts } from "react-elastic-carousel"; 

class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            galleryProducts: [],
            filteredCatId: 0,
            filteredPrdcts: [],

            lastAdded: [],
            offers: [],
            articles: [],
            aboutUs: []
        };

        /*        this.fetchLastAdded = this.fetchLastAdded.bind(this);
        this.fetchOffers = this.fetchOffers.bind(this);
        this.fetchArticles = this.fetchArticles.bind(this); */
        this.addToFav = this.addToFav.bind(this);
    }

    componentDidMount() {
        this.fetchLastAdded();
        this.fetchOffers();
        this.fetchArticles();
        this.fetchGalleryProducts();
        this.fetchCPage();
    }

    componentWillUnmount() {
        //document.getElementById("mishu").remove;
    }

    fetchCPage(id) {
        axios
            .get(this.props.defaultSets.apiUrl + "cpages/3")
            .then(response => {
                this.setState({ aboutUs: response.data });
                //console.log(response.data)
            })
            .catch(function(error) {
                console.log(error);
            });
    }
    fetchGalleryProducts() {
        axios
            .get(this.props.defaultSets.apiUrl + "gallery-products")
            .then(response => {
                this.setState({ galleryProducts: response.data });
                console.log(response.data);
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    fetchLastAdded() {
        axios
            .get(this.props.defaultSets.apiUrl + "last-products")
            .then(response => {
                this.setState({
                    lastAdded: response.data,
                    filteredPrdcts: response.data
                });
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    fetchOffers() {
        axios
            .get(this.props.defaultSets.apiUrl + "offer-products")
            .then(response => {
                this.setState({ offers: response.data });
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    fetchArticles() {
        axios
            .get(this.props.defaultSets.apiUrl + "last-articles")
            .then(response => {
                this.setState({ articles: response.data });
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    showPrdctsByCat(catId) {
        //passing the inserted text in textinput
        //console.log(catId)
        if (catId == 0) {
            return this.setState({
                //setting the filtered newData on datasource
                filteredPrdcts: this.state.lastAdded,
                filteredCatId: 0
            });
        }
        const newData = this.state.lastAdded.filter(function(item) {
            //applying filter for the inserted text in search bar
            //console.log(item.cats)
            const itemData = item.cats ? item.cats : []; //item.categoryId.toString();
            let prdctCats = [];
            itemData.map((prdctCat, k) => {
                prdctCats.push(prdctCat.id);
            });
            const textData = catId;
            return prdctCats.indexOf(textData) > -1;
        });
        //console.log(newData)
        this.setState({
            //setting the filtered newData on datasource
            filteredPrdcts: newData,
            filteredCatId: catId
        });
    }

    addToFav(item) {
        if (this.props.userInfo.id == 0) {
            return toast.info(this.props.selectedLanguage.plsLgnFrst);
        }
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(
            this.props.defaultSets.apiUrl + "favourites",
            {
                userId: this.props.userInfo.id,
                itemId: item.id,
                langCode: this.props.selectedLanguage.langCode
            },
            this.props.userInfo.accessToken
        )
            .then(response => {
                if (response.data.success == true) {
                    item.favId = response.data.favId;

                    toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });

                    return this.props.addToFavourites(item);
                }
                return toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
            })
            .catch(error => {});
    }

    render() {
        const breakPoints = [
            { width: 1, itemsToShow: 1 },
            { width: 550, itemsToShow: 2, itemsToScroll: 2 },
            { width: 850, itemsToShow: 4 },
            { width: 1150, itemsToShow: 4, itemsToScroll: 2 },
            { width: 1450, itemsToShow: 5 },
            { width: 1750, itemsToShow: 6 }
        ];
        const articlesPoints = [
            { width: 1, itemsToShow: 1 },
            { width: 550, itemsToShow: 2, itemsToScroll: 2 },
            { width: 850, itemsToShow: 3 },
            { width: 1150, itemsToShow: 3, itemsToScroll: 1 },
            { width: 1450, itemsToShow: 3 },
            { width: 1750, itemsToShow: 3 }
        ];
        const mainSliderPoints = [
            { width: 1, itemsToShow: 1 },
            { width: 550, itemsToShow: 1, itemsToScroll: 1 },
            { width: 850, itemsToShow: 1 },
            { width: 1150, itemsToShow: 1, itemsToScroll: 1 },
            { width: 1450, itemsToShow: 1 },
            { width: 1750, itemsToShow: 1 }
        ];

        const {
            galleryProducts,
            filteredPrdcts,
            offers,
            lastAdded,
            articles,
            filteredCatId,
            aboutUs
        } = this.state;

        const lng = this.props.selectedLanguage;
        const { cats, features } = this.props;
        let aboutUsTrans =
            aboutUs.elment_trans &&
            aboutUs.elment_trans.find(row => row.languageCode == lng.langCode);

        return (
            <Fragment>
                <Helmet>
                    <title>{lng.homeTxt}</title>
                </Helmet>

                <section className="p-0">
                    <div className="slide-1 home-slider">
                        <Carousel
                            enableAutoPlay={true}
                            autoPlaySpeed={5000}
                            breakPoints={
                                mainSliderPoints
                            } /*
                isRTL={lng.langCode == 'AR'? true : false} */
                            pagination={false}
                            renderArrow={({ type, onClick }) => (
                                <div onClick={onClick}>
                                    {type === consts.PREV ? (
                                        <div className="slick-prev">
                                            <i className="fa fa-long-arrow-alt-left"></i>
                                        </div>
                                    ) : (
                                        <div className="slick-next">
                                            <i className="fa fa-long-arrow-alt-right"></i>
                                        </div>
                                    )}
                                </div>
                            )}
                            itemPadding={[0, 0]}
                            itemsToShow={1}
                            itemPosition={consts.CENTER}
                        >
                            {galleryProducts &&
                                galleryProducts.map((item, index) => {
                                    let elmntTrns =
                                        item.elment_trans &&
                                        item.elment_trans.find(
                                            row =>
                                                row.languageCode == lng.langCode
                                        );

                                    return (
                                        <div key={"mnSlider" + index}>
                                            <div className="home">
                                                {elmntTrns &&
                                                    elmntTrns.files[0] && (
                                                        <img
                                                            src={
                                                                "/uploads/files/" +
                                                                elmntTrns
                                                                    .files[0]
                                                                    .fileName
                                                            }
                                                            alt={
                                                                elmntTrns &&
                                                                elmntTrns.title
                                                            }
                                                            className="bg-img blur-up lazyload"
                                                        />
                                                    )}
                                                <div className="container">
                                                    <div className="row">
                                                        <div className="col">
                                                            <div className="slider-contain">
                                                                <div>
                                                                    <h4>
                                                                        {elmntTrns &&
                                                                            elmntTrns.title}
                                                                    </h4>
                                                                    {/* <h1>{elmntTrns.title}</h1> */}
                                                                    <Link
                                                                        to={
                                                                            "/products/" +
                                                                            item.id
                                                                        }
                                                                        className="btn btn-solid"
                                                                    >
                                                                        {
                                                                            lng.shopifyNowTxt
                                                                        }
                                                                    </Link>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })}
                        </Carousel>
                    </div>
                </section>

                <section className="ratio_square section-b-space">
                    <div className="title1">
                        <h4>{lng.specialOffersTxt}</h4>
                        <h2 className="title-inner1">{lng.newProductsTxt}</h2>
                    </div>

                    <div className="container-fluid">
                        <div className="row">
                            <div className="col">
                                <div className="product-m no-arrow">
                                    <Carousel
                                        enableAutoPlay={true}
                                        autoPlaySpeed={5000}
                                        disableArrowsOnEnd={false}
                                        breakPoints={breakPoints}
                                        pagination={false}
                                        isRTL={
                                            lng.langCode == "AR" ? true : false
                                        }
                                        renderArrow={({ type, onClick }) => (
                                            <div onClick={onClick}>
                                                {type === consts.PREV ? "" : ""}
                                            </div>
                                        )}
                                        itemPadding={[0, 10]}
                                        itemsToShow={4}
                                        itemPosition={consts.CENTER}
                                    >
                                        {offers &&
                                            offers.map((item, i) => {
                                                return (
                                                    <ProductBox
                                                        obj={item}
                                                        selectedLanguage={
                                                            this.props
                                                                .selectedLanguage
                                                        }
                                                        slctdCurr={
                                                            this.props.slctdCurr
                                                        }
                                                        favedItems={
                                                            this.props
                                                                .favedItems
                                                        }
                                                        key={"prdctBox" + i}
                                                        addToCart={
                                                            this.props.addToCart
                                                        }
                                                        addToCompare={
                                                            this.props
                                                                .addToCompare
                                                        }
                                                        addToFav={this.addToFav}
                                                    />
                                                );
                                            })}
                                    </Carousel>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="home-distributor mt-5">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9 col-md-12">
                                <div className="">
                                    <h4>{lng.toBeOurAgentTxt}</h4>
                                </div>
                            </div>
                            <div className="col-lg-3 col-md-12">
                                <div className="">
                                    <Link
                                        to="/be-agent"
                                        className="btn btn-solid"
                                    >
                                        {lng.clickHereTxt}
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="home-categories">
                    <div className="title1 pb-3">
                        <h4></h4>
                        <h2 className="title-inner1">{lng.categoriesTxt}</h2>
                    </div>
                    <div className="">
                        <div className="row justify-content-md-center">
                            {this.props.cats &&
                                this.props.cats.map((item, i) => {
                                    let elmntTrns =
                                        item.elment_trans &&
                                        item.elment_trans.find(
                                            row =>
                                                row.languageCode == lng.langCode
                                        );

                                    return (
                                        <div
                                            key={"cat" + i}
                                            className="col-md-4 col-lg-4 col-sm-12 text-center"
                                        >
                                            <Link
                                                to={"/categories/" + item.id}
                                                className="d-block"
                                            >
                                                {elmntTrns &&
                                                    elmntTrns.files[0] && (
                                                        <img
                                                            src={
                                                                "/uploads/files/" +
                                                                elmntTrns
                                                                    .files[0]
                                                                    .fileName
                                                            }
                                                            alt={
                                                                elmntTrns &&
                                                                elmntTrns.title
                                                            }
                                                            className="img-fluid blur-up lazyload"
                                                        />
                                                    )}
                                            </Link>
                                            <Link
                                                to={"/categories/" + item.id}
                                                className="my-3 btn btn-solid d-block btn-categories"
                                            >
                                                {elmntTrns && elmntTrns.title}
                                            </Link>
                                        </div>
                                    );
                                })}
                        </div>
                    </div>
                </section>

                <section className="detector-systems py-5">
                    <div className="title1 pb-3">
                        <h6>{lng.featuresdes}</h6>
                        <h2 className="title-inner1">{lng.featuresTxt}</h2>
                    </div>
                    <div className="container">
                        <div className="row row-cols-3">
                            {features &&
                                features.map((item, i) => {
                                    const itemTrans = item.elment_trans.find(
                                        row => row.languageCode == lng.langCode
                                    );
                                    return (
                                        <div
                                            className="col-md-6 col-sm-12 d-inline-flex mt-3"
                                            key={"feature" + item.id}
                                        >
                                            <div className="col-3">
                                                <img
                                                    src={
                                                        "/uploads/files/" +
                                                        item.photo
                                                    }
                                                    alt={
                                                        itemTrans &&
                                                        itemTrans.title
                                                    }
                                                    className="img-fluid blur-up lazyload"
                                                />
                                            </div>
                                            <div className="col-9">
                                                <h3>
                                                    <Link
                                                        to={
                                                            "/features/" +
                                                            item.id
                                                        }
                                                    >
                                                        {itemTrans &&
                                                            itemTrans.title}
                                                    </Link>
                                                </h3>
                                                <p>
                                                    {itemTrans &&
                                                    itemTrans.description1
                                                        ? itemTrans.description1
                                                              .replace(
                                                                  /(&nbsp;|<([^>]+)>)/gi,
                                                                  ""
                                                              )
                                                              .slice(0, 100)
                                                        : null}
                                                </p>
                                            </div>
                                        </div>
                                    );
                                })}
                        </div>
                    </div>
                </section>

                <div className="title1 section-t-space d-none">
                    <h2 className="title-inner1">{lng.lastAddedTxt}</h2>
                </div>
                <section className="section-b-space pt-0 ratio_asos d-none">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <div className="theme-tab">
                                    <ul className="tabs tab-title">
                                        <ul>
                                            <li
                                                className={
                                                    filteredCatId == 0
                                                        ? "current"
                                                        : ""
                                                }
                                            >
                                                <a
                                                    href="#"
                                                    onClick={e => {
                                                        e.preventDefault();
                                                        this.showPrdctsByCat(0);
                                                    }}
                                                >
                                                    {lng.allTxt}
                                                </a>
                                            </li>
                                            {cats &&
                                                cats.map((item, i) => {
                                                    let elmntTrns =
                                                        item.elment_trans &&
                                                        item.elment_trans.find(
                                                            row =>
                                                                row.languageCode ==
                                                                lng.langCode
                                                        );

                                                    return (
                                                        <li
                                                            key={"cat" + i}
                                                            className={
                                                                filteredCatId ==
                                                                item.id
                                                                    ? "current"
                                                                    : ""
                                                            }
                                                        >
                                                            <a
                                                                href="#"
                                                                onClick={e => {
                                                                    e.preventDefault();
                                                                    this.showPrdctsByCat(
                                                                        item.id
                                                                    );
                                                                }}
                                                            >
                                                                {elmntTrns &&
                                                                    elmntTrns.title}
                                                            </a>
                                                        </li>
                                                    );
                                                })}
                                        </ul>
                                    </ul>
                                    <div className="tab-content-cls">
                                        <div className="tab-content active default ratio_square">
                                            <div className="no-slider product-m no-arrow row">
                                                {filteredPrdcts &&
                                                    filteredPrdcts.map(
                                                        (item, k) => {
                                                            return (
                                                                <ProductBox
                                                                    obj={item}
                                                                    selectedLanguage={
                                                                        this
                                                                            .props
                                                                            .selectedLanguage
                                                                    }
                                                                    slctdCurr={
                                                                        this
                                                                            .props
                                                                            .slctdCurr
                                                                    }
                                                                    favedItems={
                                                                        this
                                                                            .props
                                                                            .favedItems
                                                                    }
                                                                    key={
                                                                        "newPrdctBox" +
                                                                        k
                                                                    }
                                                                    addToCart={
                                                                        this
                                                                            .props
                                                                            .addToCart
                                                                    }
                                                                    addToCompare={
                                                                        this
                                                                            .props
                                                                            .addToCompare
                                                                    }
                                                                    addToFav={
                                                                        this
                                                                            .addToFav
                                                                    }
                                                                />
                                                            );
                                                        }
                                                    )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="mwf-about video-section">
                    <div className="container">
                        <div className="row py-5">
                            <div className="col-xl-5 col-lg-6 col-md-12 offset-md-2 text-center">
                                <a
                                    href=""
                                    data-toggle="modal"
                                    data-target="#video"
                                >
                                    <div className="video-img">
                                        <img
                                            src={
                                                "/uploads/files/" +
                                                aboutUs.photo
                                            }
                                            alt={
                                                aboutUsTrans &&
                                                aboutUsTrans.title
                                            }
                                            className="img-fluid blur-up lazyload"
                                        />

                                        <div className="play-btn">
                                            <span>
                                                <i
                                                    className="fa fa-play"
                                                    aria-hidden="true"
                                                ></i>
                                            </span>
                                        </div>
                                    </div>
                                </a>
                                <div
                                    className="modal fade video-modal"
                                    id="video"
                                    role="dialog"
                                    aria-hidden="true"
                                >
                                    <div
                                        className="modal-dialog modal-lg modal-dialog-centered"
                                        role="document"
                                    >
                                        <div className="modal-content">
                                            <div className="modal-body">
                                                <iframe
                                                    width="560"
                                                    height="315"
                                                    src={
                                                        "https://www.youtube.com/embed/" +
                                                        aboutUs.video
                                                    }
                                                    frameBorder="0"
                                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                    allowFullScreen
                                                ></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-5 col-lg-6 col-md-12">
                                <div className="about-section">
                                    <div>
                                        <h2>{lng.aboutUsTxt}</h2>
                                        <h5>
                                            {aboutUsTrans && aboutUsTrans.title}
                                        </h5>
                                        <div className="about-text">
                                            <div
                                                dangerouslySetInnerHTML={{
                                                    __html:
                                                        aboutUsTrans &&
                                                        aboutUsTrans.description
                                                }}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="ratio_square section-b-space d-none">
                    <div className="title1">
                        <h4>{lng.ltstDctrsInWrldTxt}</h4>
                        <h2 className="title-inner1">{lng.newProductsTxt}</h2>
                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <div className="product-m no-arrow">
                                    <Carousel
                                        enableAutoPlay={true}
                                        autoPlaySpeed={5000}
                                        disableArrowsOnEnd={false}
                                        breakPoints={breakPoints}
                                        pagination={false}
                                        isRTL={
                                            lng.langCode == "AR" ? true : false
                                        }
                                        renderArrow={({ type, onClick }) => (
                                            <div onClick={onClick}>
                                                {type === consts.PREV ? "" : ""}
                                            </div>
                                        )}
                                        itemPadding={[0, 10]}
                                        itemsToShow={4}
                                        itemPosition={consts.CENTER}
                                    >
                                        {lastAdded &&
                                            lastAdded.map((item, i) => {
                                                return (
                                                    <ProductBox
                                                        obj={item}
                                                        selectedLanguage={
                                                            this.props
                                                                .selectedLanguage
                                                        }
                                                        slctdCurr={
                                                            this.props.slctdCurr
                                                        }
                                                        favedItems={
                                                            this.props
                                                                .favedItems
                                                        }
                                                        key={"la" + i}
                                                        addToCart={
                                                            this.props.addToCart
                                                        }
                                                        addToCompare={
                                                            this.props
                                                                .addToCompare
                                                        }
                                                        addToFav={this.addToFav}
                                                    />
                                                );
                                            })}
                                    </Carousel>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="p-0">
                    <div className="full-banner parallax-banner3 parallax text-center p-left">
                        <div className="container">
                            <div className="row partition2">
                                <div className="col-md-12">
                                    <div className="title1 title5 pb-5">
                                        <h2 className="title-inner1">
                                            {lng.ComingSoon}
                                        </h2>
                                        <hr role="tournament6" />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <a href="#">
                                        <div className="collection-banner p-right text-center">
                                            <img
                                                src="/assets/frontend/images/sub-banner1.jpg"
                                                className="img-fluid"
                                                alt=""
                                            />
                                            <div className="contain-banner">
                                                <div>
                                                    <h4>{lng.NewComingSoon}</h4>
                                                    <h3>Product Name</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div className="col-md-6">
                                    <a href="#">
                                        <div className="collection-banner p-right text-center">
                                            <img
                                                src="/assets/frontend/images/sub-banner2.jpg"
                                                className="img-fluid"
                                                alt=""
                                            />
                                            <div className="contain-banner">
                                                <div>
                                                    <h4>
                                                        {lng.ComingComingSoon}
                                                    </h4>
                                                    <h3>Product Name</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <div className="blog">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <div className="title1 section-t-space">
                                    <h4>{lng.lastArticlesTxt}</h4>
                                    <h2 className="title-inner1">
                                        {lng.fromBlogTxt}
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section className="blog p-t-0 ratio3_2">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className=" no-arrow">
                                        <Carousel
                                            enableAutoPlay={true}
                                            autoPlaySpeed={5000}
                                            disableArrowsOnEnd={false}
                                            breakPoints={articlesPoints}
                                            pagination={false}
                                            isRTL={
                                                lng.langCode == "AR"
                                                    ? true
                                                    : false
                                            }
                                            renderArrow={({
                                                type,
                                                onClick
                                            }) => (
                                                <div onClick={onClick}>
                                                    {type === consts.PREV
                                                        ? ""
                                                        : ""}
                                                </div>
                                            )}
                                            itemPadding={[0, 10]}
                                            itemsToShow={3}
                                            itemPosition={consts.CENTER}
                                        >
                                            {articles &&
                                                articles.map((item, i) => {
                                                    return (
                                                        <ArticleBox
                                                            obj={item}
                                                            key={i}
                                                            selectedLanguage={
                                                                this.props
                                                                    .selectedLanguage
                                                            }
                                                        />
                                                    );
                                                })}
                                        </Carousel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div className="container">
                    <section className="service border-section small-section">
                        <div className="row">
                            <div className="col-md-4 service-block">
                                <div className="media">
                                    <i className="fa fa-truck"></i>
                                    <div className="media-body">
                                        <h4>{lng.freeShippingTxt}</h4>
                                        <p>{lng.freeShippingdes}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 service-block">
                                <div className="media">
                                    <i className="fa fa-history"></i>
                                    <div className="media-body">
                                        <h4>{lng.service724}</h4>
                                        <p>{lng.service724des}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 service-block">
                                <div className="media">
                                    <i className="fa fa-flag"></i>
                                    <div className="media-body">
                                        <h4>{lng.festival}</h4>
                                        <p>{lng.festivaldes}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </Fragment>
        );
    }
}

const mapDispatchToProps = (dispatch, msg) => {
    return {
        addToCart: (product, msg) => {
            toast.success(msg);
            dispatch({ type: "ADD_TO_CART", payload: product });
        },
        addToCompare: (product, msg) => {
            toast.success(msg);
            dispatch({ type: "ADD_TO_COMPARE", payload: product });
        },
        addToFavourites: item => {
            dispatch({ type: "ADD_TO_FAV", payload: item });
        }
    };
};

const mapStateToProps = state => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        cartItems: state.cartItems.cart,
        userInfo: state.User.info,
        favedItems: state.Favourites.rows,
        slctdCurr: state.Currs.currencyInfo,
        cats: state.Cats.rows,
        features: state.Features.rows,
        defaultSets: state.DfltSts
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
