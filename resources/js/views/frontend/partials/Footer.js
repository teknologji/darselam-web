import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import {toast} from "react-toastify";

class Footer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      isDisabled: false
    }
    this.hndlNewsletterSub = this.hndlNewsletterSub.bind(this);
    this.onChangeValue = this.onChangeValue.bind(this);

}
 
hndlNewsletterSub(e){
  e.preventDefault();
  const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

  this.setState({isDisabled: true});
  axios.post(this.props.defaultSets.apiUrl+'newsletters', {
      email: this.state.email,
      langCode: this.props.selectedLanguage.langCode
  })
      .then((response) => {
        this.setState({isDisabled: false});

          toast.update(ldToast, {
              type: toast.TYPE.SUCCESS,
              render: response.data.msg
          });
      })
      .catch((error) => {
          //console.log(error);

          if(error.response !== undefined && error.response.status === 422){
              let errorTxt = '';
              let errorMessage = error.response.data.errors;

              Object.keys(errorMessage).map((key, i) => {
                  errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
              toast.update(ldToast, {
                  type: toast.TYPE.ERROR,
                  render: errorTxt
              });
              
          }
      });

}

onChangeValue(e) {
  this.setState({
      [e.target.name]: e.target.value, isDisabled: false
  });        

}

    render() {

      const lng = this.props.selectedLanguage;
      const sts = this.props.defaultSets;
      //const {sets} = sts;

        return (
         
            <footer className="footer-light">
            <div className="light-layout">
              <div className="container">
                <section className="small-section border-section border-top-0">
                  <div className="row">
                    <div className="col-lg-6">
                      <div className="subscribe">
                        <div>
                          <h4>{lng.sbscrbWithUsTxt}</h4>
                          <p>
                          {lng.sbscrbWithUs4OffrsTxt}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6">
                      <form
                        onSubmit={this.hndlNewsletterSub}
                        className="form-inline subscribe-form auth-form needs-validation"
                        id="mc-embedded-subscribe-form"
                        name="mc-embedded-subscribe-form"
                      >
                        <div className="form-group mx-sm-3">
                        <input 
                        className="form-control"
                        type="email"                         
                        id="mce-EMAIL"
                        name="email" 
                        id="subscribe-mail"
                        onChange={this.onChangeValue} 
                        placeholder={lng.writeYrEmailTxt} />
                         
                        </div>
                        <button disabled={this.state.isDisabled} className="btn btn-solid" id="mc-submit">{lng.subscribeTxt}</button>
                       
                      </form>
                    </div>
                  </div>
                </section>
              </div>
            </div>
            <section className="py-5 light-layout">
              <div className="container">
                <div className="row footer-theme partition-f">
                  <div className="col-lg-4 col-md-6">
                    <div className="footer-title footer-mobile-title">
                      <h4>about</h4>
                    </div>
                    <div className="footer-contant">
                      <div className="footer-logo">
                        <img src="/assets/frontend/images/icon/layout3/logo.png" alt="" />
                      </div>
                      <p>{sts.welcomeMsgAR && eval("sts.welcomeMsg"+lng.langCode).slice(0, 170)}</p>
                      {sts.fb_url && 
                      <div className="footer-social">
                        <ul>
                          <li>
                            <a href={sts.fb_url} target="_blank">
                            <i className="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                          </li>
                          <li>                            
                            <a href={sts.whatsapp} target="_blank">
                            <i className="fa fa-whatsapp" aria-hidden="true"></i>
                            </a>
                          </li>
                          <li>                            
                            <a href={sts.twitter_url} target="_blank">
                            <i className="fa fa-twitter" aria-hidden="true"></i>
                            </a>
                          </li>
                          <li>                            
                            <a href={sts.google_url} target="_blank">
                            <i className="fa fa-google-plus" aria-hidden="true"></i>
                            </a>
                          </li>
                          <li>                            
                            <a href={sts.instagram} target="_blank">
                            <i className="fa fa-instagram" aria-hidden="true"></i>
                            </a>
                          </li>
                        </ul>
                      </div>}
                    </div>
                  </div>
                  <div className="col offset-xl-1">
                    <div className="sub-title">
                      <div className="footer-title">
                        <h4>{lng.categoriesTxt}</h4>
                      </div>
                      <div className="footer-contant">
                        <ul>
                        {this.props.cats && this.props.cats.map((item, i) => {
                        let elmntTrns = item.elment_trans && item.elment_trans.find(prdct => prdct.languageCode == lng.langCode);

            return(
              <li key={"catFtr"+i}>
              <Link to={"/categories/"+item.id} replace>
              {elmntTrns && elmntTrns.title}
              </Link>
            </li>
            )})}
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="col">
                    <div className="sub-title">
                      <div className="footer-title">
                        <h4>{lng.usefulLinksTxt}</h4>
                      </div>
                      <div className="footer-contant">
                        <ul> 
                          {this.props.cpages && this.props.cpages.map((item, i) => {
                              return(
                                <li key={"catFtr"+i}>
                                <Link to={"/cpages/"+item.id} replace>
                                {eval("item.title"+lng.langCode)}
                                </Link>
                              </li>
                              )})}
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="col">
                    <div className="sub-title">
                      <div className="footer-title">
                        <h4>CONTACT INFO</h4>
                      </div>
                      <div className="footer-contant">
                        <ul className="contact-list">
                          <li>
                            <i className="fa fa-map-marker"></i>{sts.adrsAR && eval("sts.adrs"+lng.langCode)}
                          </li>
                          <li><i className="fa fa-phone"></i><a href={"tel:"+sts.phone1}>
                            {sts.phone1}
                            </a></li>
                          <li><i className="fa fa-mobile"></i>
                          <a href={"tel:"+sts.phone2}>
                          {sts.phone2}
                          </a>
                              </li>
                          <li>
                            <i className="fa fa-envelope-o"></i>
                            <a href={"mailto:"+sts.email1}>
                            {sts.email1}
                              </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <div className="sub-footer">
              <div className="container">
                <div className="row">
                  <div className="col-xl-6 col-md-6 col-sm-12">
                    <div className="footer-end">
                      <p>
                        <i className="fa fa-copyright" aria-hidden="true"></i> 
                        {sts.copyRightsAR && eval("sts.copyRights"+lng.langCode)}
                      </p>
                    </div>
                  </div>
                  <div className="col-xl-6 col-md-6 col-sm-12">
                    <div className="payment-card-bottom">
                      <ul>
                        <li>
                          <a href="#"
                            ><img src="/assets/frontend/images/icon/visa.png" alt=""
                          /></a>
                        </li>
                        <li>
                          <a href="#"
                            ><img src="/assets/frontend/images/icon/mastercard.png" alt=""
                          /></a>
                        </li>
                        <li>
                          <a href="#"
                            ><img src="/assets/frontend/images/icon/paypal.png" alt=""
                          /></a>
                        </li>
                        <li>
                          <a href="#"
                            ><img
                              src="/assets/frontend/images/icon/american-express.png"
                              alt=""
                          /></a>
                        </li>
                        <li>
                          <a href="#"
                            ><img src="/assets/frontend/images/icon/discover.png" alt=""
                          /></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </footer>
    )
    }
}



export default Footer;