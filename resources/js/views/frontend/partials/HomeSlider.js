import React, {Component, Fragment} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
//import Slider from 'react-animated-slider';
//import 'react-animated-slider/build/horizontal.css';


class HomeSlider extends Component {

    render() {

        const { home, aboutUs, shoppingCart, homeTxt, servicesTxt, articlesTxt,
        aboutUsTxt, distributorsTxt, productsTxt, shopifyNowTxt, langCode} = this.props.selectedLanguage;

        const {welcomeMsgAR, welcomeMsgTR, welcomeMsgEN, phone1} = this.props.settings;

        return (

    
            
    <Fragment>
       
       
    <section className="p-0">

    <div className="slide-1 home-slider">
    {this.props.galleryProducts && this.props.galleryProducts.map((item, index) => (

        <div key={"mnSlider"+index}>
          <div className="home">
          <img
              src={"/uploads/files/"+item.photo}
              alt=""
              className="bg-img blur-up lazyload"
            />
            <div className="container">
              <div className="row">
                <div className="col">
                  <div className="slider-contain">
                    <div>
                      <h4>{item && eval("item.title"+langCode)}</h4>
                      {/* <h1>{item && eval("item.category.title"+langCode)}</h1>          */}
                      <Link to={"/products/"+item.id} className="btn btn-solid">
                            {shopifyNowTxt}
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    ))} 

    </div>
</section>
   
    </Fragment>
    )
    }
}

const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        userInfo: state.User.info,
        cartItems: state.cartItems.cart,
        defaultSets: state.DfltSts
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectLanguage: (val) => dispatch({ type: 'language_data', payload: val }),
        //changeCurrency: (curr) => dispatch({ type: 'change_currency', payload: curr }),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeSlider);
