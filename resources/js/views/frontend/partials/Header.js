import React, {Component} from 'react';
import { Link } from 'react-router-dom';



import ListItem from '../../../components/ListItem';


class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            //cats: [],
        }

        this.openNav = this.openNav.bind(this);
        this.closeNav = this.closeNav.bind(this);
        this.hndlLanguage = this.hndlLanguage.bind(this);
        
    }

    componentDidMount(){
//console.log(this.props.slctdCurr.currCode)
//console.log('kh', this.props.ar)
//
     }

    openNav(e){
      e.preventDefault();
      document.getElementById("mySidenav").classList.add('open-side');
    }

    closeNav(e) {
      e.preventDefault();
      document.getElementById("mySidenav").classList.remove('open-side');
    }

    hndlLanguage(e, languageCode){
      e.preventDefault(); 
      let {ar, tr, en, selectLanguage} = this.props;
      switch(languageCode.toLowerCase()) {        
        case 'ar' : return this.props.selectLanguage(ar);
        case 'en' : return selectLanguage(en);
        case 'tr' : return selectLanguage(tr);
        default: return selectLanguage(ar)
      }
      
    }
 
    render() {

        const sts = this.props.defaultSets;
        const {cartItems, cats, products, cpages, features, currs, slctdCurr, languages,
        } = this.props;
        const lng = this.props.selectedLanguage;

        return (

            <header>
            <div className="mobile-fix-option"></div>
            <div className="top-header">
              <div className="container">
                <div className="row">
                  <div className="col-lg-6">
                    <div className="header-contact">
                      <ul>
                        <li>
                          <i className="fa fa-envelope-o"></i>
                          <a href={"mailto:"+sts.email1}>
                            {sts.email1} 
                          </a>
                        </li>
                        <li>
                          <i className="fa fa-phone" aria-hidden="true"></i>
                          {lng.callUsTxt}: 
                          <a href={"tel:"+sts.phone1}>{sts.phone1}</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-lg-6 text-right">
                    <ul className="header-dropdown">
                      <li className="mobile-wishlist">
                      <Link to="/compare"><i className="fa fa-random" aria-hidden="true"></i>{lng.compareTxt}</Link>
                      </li>
                      {this.props.userInfo.id != 0 &&
                      <li className="mobile-wishlist">
                        <Link to="/favourites"><i className="fa fa-heart" aria-hidden="true"></i>{lng.myFavsTxt}</Link>
                      </li>}
                      <li className="onhover-dropdown mobile-account">
                        <i className="fa fa-user" aria-hidden="true"></i> {lng.welcomeTxt+' '+this.props.userInfo.full_name}
                        
                        {this.props.userInfo.id != 0 ?
                        (
                          (this.props.userInfo.groupId == 1) ?
                                                                  
                        (<ul className="onhover-show-div">
                        <li><Link to="/admin">{lng.cpanelTxt}</Link></li>
                        <li><Link to="/profile">{lng.profileTxt}</Link></li>
                        <li><a href="#" onClick={(e) => {e.preventDefault(); this.props.logout()}}>{lng.logoutTxt}</a></li>
                      </ul>)                                  
                            :
                        (<ul className="onhover-show-div">
                        <li><Link to="/orders">{lng.myOrdersTxt}</Link></li>
                        <li><Link to="/create-shipment-adrs">{lng.addShipmentAdrsTxt}</Link></li>
                        <li><Link to="/shipment-adrs">{lng.shipmentAdrsesTxt}</Link></li>
                        <li><Link to="/favourites">{lng.myFavsTxt}</Link></li>
                        <li><Link to="/profile">{lng.profileTxt}</Link></li>   
                        <li><a href="#" onClick={(e) => {e.preventDefault(); this.props.logout()}}>{lng.logoutTxt}</a></li>
                      </ul>   ) 
                      )                   

                        

                        :
                        (
                        <ul className="onhover-show-div">
                        <li><Link to="/login">{lng.loginTxt}</Link></li>
                        <li><Link to="/register">{lng.registerTxt}</Link></li>
                        </ul>
                        )
                        }
                        
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <div className="main-menu">
                    <div className="menu-left">
                      <div className="navbar">
                          <a href="#" onClick={this.openNav}>
                              <div className="bar-style"> <i className="fa fa-bars sidebar-bar" aria-hidden="true"></i>
                              </div>
                          </a>
                          <div id="mySidenav" className="sidenav">
                              <a href="#" className="sidebar-overlay" onClick={this.closeNav}></a>
                              <nav>
                                  <div onClick={this.closeNav}>
                                      <div className="sidebar-back text-left">
                                          <i className="fa fa-angle-left pr-2" aria-hidden="true"></i>
                                          {lng.backTxt}
                                      </div>
                                  </div>

                                  <ul id="sub-menu" className="sm pixelstrap sm-vertical ">
                                      <li>
                                        <Link to="/contact" className="text-black">{lng.contactTxt}</Link>
                                      </li>
                                      <li>
                                        <a href="#" className="text-black">{lng.categoriesTxt}</a>
                                        <ul className="sup-lg">
                                            {cats && cats.map((item, i) => {
                                            let elmntTrns = item.elment_trans && item.elment_trans.find(prdct => prdct.languageCode == this.props.selectedLanguage.langCode);

                                            return(
                                            <li key={"catSide"+i}>                                            
                                                <Link to={"/categories/"+item.id}>                                            
                                                  {elmntTrns && elmntTrns.title}
                                                </Link>
                                            </li>
                                            );
                                            })
                                            }
                                        </ul>
                                      </li>
                                <li>
                                  <a href="#" className="text-black">{lng.allProductsTxt}</a>
                                  <ul>
                                  {products && products.map((item, i) => {
                                  let elmntTrns = item.elment_trans && item.elment_trans.find(prdct => prdct.languageCode == this.props.selectedLanguage.langCode);

                                        return(
                                        <li key={"prdctSide"+i}>                                            
                                            <Link to={"/products/"+item.id} replace>                                            
                                              {elmntTrns && elmntTrns.title}
                                            </Link>
                                        </li>
                                        );
                                        })
                                        }
                                  </ul>
                                </li>

                                <li>
                                  <a href="#" className="text-black">{lng.cpagesTxt}</a>
                                  <ul>    
                                    
                                      {cpages && cpages.map((item, i) => {
                                      let elmntTrns = item.elment_trans && item.elment_trans.find(prdct => prdct.languageCode == lng.langCode);

                                      return(
                                        <li key={"catFtr"+i}>
                                        <Link to={"/cpages/"+item.id} replace>
                                        {elmntTrns && elmntTrns.title}
                                        </Link>
                                        </li>
                                      )})}
                                    </ul>
                                </li>
                                <li>
                                    <Link to="/articles" className="text-black">{lng.articlesTxt}</Link>
                                </li>

                                <li>
                                    <Link to="/faqs" className="text-black">{lng.faqsTxt}</Link>
                                </li>
                                
                                <li>
                                    <Link to="/verify-product" className="text-black">{lng.verifyPrdctTxt}</Link>
                                </li>
                                  </ul>
                               
                              </nav>
                          </div>
                      </div>
                      <div className="brand-logo">
                        <Link to="/"><img
                            src="/assets/frontend/images/icon/layout3/logo.png"
                            className="img-fluid blur-up lazyload"
                            alt=""
                        /></Link>
                      </div>
                    </div>
                    <div className="menu-right pull-right">
                      <div>
                        <nav id="main-nav">
                          <div className="toggle-nav">
                            <i className="fa fa-bars sidebar-bar"></i>
                          </div>
                          <ul id="main-menu" className="sm pixelstrap sm-horizontal">
                            <li>
                              <div className="mobile-back text-right">
                                {lng.backTxt}<i
                                  className="fa fa-angle-right pl-2"
                                  aria-hidden="true"
                                ></i>
                              </div>
                            </li>
                            <li><Link to="/">{lng.homeTxt}</Link></li>   

                            <li className="mega" id="hover-cls">
                              <Link to="/categories/all"
                                >{lng.categoriesTxt}
                                <div className="lable-nav">new</div>
                              </Link>
                              <ul className="mega-menu full-mega-menu">
                                <li>
                                  <div className="container">
                                    <div className="row">
                                      
                                      {cats && cats.map((item, i) => {
                                          return(<ListItem obj={item} key={'catu'+i} />);
                                      })
                                      }
                                   </div>
                                  </div>
                                </li>
                              </ul>
                            </li>
                            <li><Link to="/services">{lng.servicesTxt}</Link></li>  
                            <li><Link to="/cpages/1">{lng.aboutUsTxt}</Link></li>       
                            <li><Link to="/agents">{lng.agentsTxt}</Link></li>

                            <li>
                        <a href="#">{lng.featuresTxt}</a>
                        <ul> 
                        {features && features.map((item, i) => {
                          const itemTras = item.elment_trans.find(item => item.languageCode == lng.langCode);
                            return(
                              <li key={"feature"+item.id}>
                              <Link to={"/features/"+item.id}>{itemTras && itemTras.title}</Link>
                            </li>
                            );
                        })
                        }

                          
                        </ul>
                      </li>

                           

                            </ul>
                        </nav>
                      </div>
                      <div>
                        <div className="icon-nav">
                          <ul>
                            <li className="onhover-div mobile-search">
                              <div>
                                <i className="ti-search" /* onClick="openSearch()" */></i>
                              </div>
                              <div id="search-overlay" className="search-overlay">
                                <div>
                                  <span
                                    className="closebtn"
                                    /*onClick="closeSearch()"*/
                                    title="Close Overlay"
                                    >×</span
                                  >
                                  <div className="overlay-content">
                                    <div className="container">
                                      <div className="row">
                                        <div className="col-xl-12">
                                          <form>
                                            <div className="form-group">
                                              <input
                                                type="text"
                                                className="form-control"
                                                id="exampleInputPassword1"
                                                placeholder="Search a Product"
                                              />
                                            </div>
      
                                            <button
                                              type="submit"
                                              className="btn btn-primary"
                                            >
                                              <i className="fa fa-search"></i>
                                            </button>
                                          </form>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li className="onhover-div mobile-setting">
                              <div>
                                <i className="ti-settings"></i>
                              </div>
                              <div className="show-div setting">
                                <h6>{lng.langTxt}</h6>
                                <ul>
                                {languages && languages.map((item, i) => {
                                return(
                                  
                                  <li key={'lng'+i}> 
                                    <a href="#" onClick={(e) => this.hndlLanguage(e, item.languageCode)}>
                                        {item.title}
                                    </a>
                                  </li>
                                )}
                              )}     
                   
                                </ul>
                                <h6>{lng.currTxt}</h6>
                                <ul className="list-inline">
                                  
                              {currs && currs.map((item, i) => {
                                return(
                                  <li key={i}>
                                    <a href="#" onClick={(e)=> {e.preventDefault(); this.props.changeCurrency(item)}}>{item.currencyCode}</a>
                                  </li>
                                )}
                              )}
                                </ul>
                              </div>
                            </li>
                            <li className="onhover-div mobile-cart">
                              <div>
                                <i className="ti-shopping-cart"></i>
                              </div>
                              <ul className="show-div shopping-cart">
                               
                              {cartItems.cart && cartItems.cart.map((item, i) => {
                             const elmntTrns = item.elment_trans && item.elment_trans.find(prdct => prdct.languageCode == lng.langCode);

                                            return(

                                  <li key={i}>

                                  <div className="media">
                                    
                                    <Link to={"/products/"+item.id}>
                                      {elmntTrns && elmntTrns.files[0] &&
                                      <img src={"/uploads/files/"+elmntTrns.files[0]['fileName']} 
                                      alt={eval("item.title"+lng.langCode)} className="mr-3"
                                      style={{height: '80px', width: '80px'}} />}
                                    </Link>                                    
      
                                    <div className="media-body">
                                      <Link to={"/products/"+item.id}>
                                        <h4>{elmntTrns && elmntTrns.title}</h4>
                                      </Link>
                                      <h4><span>{item.quantity} x {(item.priceAfterDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currCode}</span></h4>
                                    </div>
                                  </div>
                                  <div className="close-circle">
                                    <a href="#"
                                    onClick={(e) => {e.preventDefault(); this.props.removeItem(item)}}
                                      ><i className="fa fa-times" aria-hidden="true"></i
                                    ></a>
                                  </div>
                                </li>);
                              })}
                              
                                <li>
                                  <div className="total">
                                    <h5>{lng.subTotalTxt} : <span>{(cartItems.totalPrice/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currCode}</span></h5>
                                  </div>
                                </li>
                                <li>
                                  <div className="buttons">
                                    <Link to="/cart" className="view-cart">
                                      {lng.viewCartTxt}
                                    </Link>
                                    <Link to="/cart" className="checkout">
                                      {lng.checkoutTxt} 
                                    </Link>
                                  </div>
                                </li>
                              </ul>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>
          
            )
    }
}





export default Header;
