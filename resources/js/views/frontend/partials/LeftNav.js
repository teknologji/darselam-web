import React, {Component} from "react";
import { connect } from 'react-redux';
import ListItem from "../../../components/ListItem";
import Slider from 'react-animated-slider';
import 'react-animated-slider/build/horizontal.css';
import { Link } from 'react-router-dom';

class LeftNav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //galleryProducts: []
        }

    }
  
    componentDidMount(){

     }

     
   

    render() {
        const {langCode, support24Txt, shopifyNowTxt, writePrdctNmHr,
            searchTxt ,allProductsTxt, categoriesTxt, langRowRvrs} = this.props.selectedLanguage;

        return (
            
    <section className="hero">
    <div className="container">
        <div className="row">
            <div className="col-lg-3">
                <div className="hero__categories">
                    <div className="hero__categories__all">
                        <i className="fa fa-bars"></i>
                        <span>{categoriesTxt}</span>
                    </div>
                    <ul>                        
                    {this.props.cats && this.props.cats.map((item, i) => {
                        return(<ListItem obj={item} key={i} />);
                    })
                    }
                    </ul>
                </div>
            </div>
            <div className="col-lg-9">
                <div className="hero__search">
                    <div className="hero__search__form">
                        <form action="#">
                            <div className="hero__search__categories">
                                {allProductsTxt}
                                <span className="arrow_carrot-down"></span>
                            </div>
                            <input type="text" placeholder={writePrdctNmHr} />
                            <button type="submit" className="site-btn">{searchTxt}</button>
                        </form>
                    </div>
                    <div className="hero__search__phone">
                        <div className="hero__search__phone__icon">
                            <i className="fa fa-phone"></i>
                        </div>
                        <div className="hero__search__phone__text">
                            <h5></h5>
                            <span>{support24Txt}</span>
                        </div>
                    </div>
                </div>
                
                <div className="">
                <Slider>
                {this.props.galleryProducts && this.props.galleryProducts.map((item, index) => (

                <div 
                    key={index}               
					className="slider-content"
					style={{ background: "url('/uploads/files/"+item.photo+"') no-repeat center center" }}
				>
					<div className="inner">
						{/* <h1>{eval("item.title"+langCode)}</h1>
						<p>{eval("item.description"+langCode) && 
                            eval("item.description"+langCode).replace(/(&nbsp;|<([^>]+)>)/ig, "").substr(1,50)
                            }
                        </p> */}
                        <Link to={"/products/"+item.id} className="site-btn">
                            {shopifyNowTxt}
                        </Link>
					</div>
				</div>

                ))} 
              

</Slider>
                </div>
            </div>
        </div>
    </div>
</section>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        userInfo: state.User.info,
        cartItems: state.cartItems.cart,
        defaultSets: state.DfltSts,
        cats: state.Cats.rows
    }
};

export default connect(mapStateToProps, null)(LeftNav);