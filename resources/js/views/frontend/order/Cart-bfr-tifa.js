import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import BreadCrumb from '../partials/BreadCrumb';
import {toast} from "react-toastify";
import {Helmet} from "react-helmet";
import getData from '../../../helpers/getData';
import postData from '../../../helpers/postData';


class Cart extends Component {

    constructor(props) {
        super(props);
        this.state = {
            //settings: []
            userShpmntAdrs: [],
            shipmentAdrsId: 0,
            shipmentCost: 0,
            userCredit: 0,
            couponValue: 0,
            showPayPalForm: false,
            cardName: '',
            cardNum: '',
            cardExpiryDate: '',
            cvv: '',
            couponCode: '',
            isDisabled: false,
            couponInfo: [],
            paymentType: 1

        };
        this.onChangeValue = this.onChangeValue.bind(this);
        this.fetchCouponInfo = this.fetchCouponInfo.bind(this);
        
    }

    componentDidMount(){    
        this.fetchUsrShpmntAdrs();
        console.log(this.props.cartItems.cart)
    }

    componentWillUnmount() {
        //this.setState({galleryProducts: []})
        //document.getElementById("mishu").remove;
    }

    fetchUsrShpmntAdrs() {
        
        getData(this.props.defaultSets.apiUrl+'user-shpmnt-adrs/'+this.props.userInfo.id, this.props.userInfo.accessToken)

        .then( (response) => {
            this.setState({userShpmntAdrs: response.data.rows});
            //console.log(this.state.rows)
        })
        .catch( (error) => {
            //console.log(error.response);               
        });
    }

    showPayPalForm() {
       //return toast.info(this.props.selectedLanguage.soonTxt);
    }   

    onChangeValue(e) {
        if(e.target.name == 'cardExpiryDate'){
            let crdexprDt = e.target.value;
            if(crdexprDt.length == 2){
                let newVal = crdexprDt+'/'; 
                return this.setState({
                    [e.target.name]: newVal
                });               
            } 
            
        }

        return this.setState({
                [e.target.name]: e.target.value, isDisabled: false
            });
        
        //console.log(this.state.cityId)

    }

    
    handlePayment(){

        if(this.state.shipmentAdrsId == 0) {
            return toast.error(this.props.selectedLanguage.chooseShipmentAdrsTxt);
        }

        this.setState({isDisabled: true})
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'orders', {
            paymentType: this.state.paymentType,
            userId: this.props.userInfo.id,
            shipmentAdrsId: this.state.shipmentAdrsId,
            items: this.props.cartItems.cart,
            subTotal: this.props.cartItems.totalPrice,
            shipmentCost: this.state.shipmentCost,
            couponValue: this.state.couponValue,
            userCredit: this.state.userCredit,
            finalTotal: (this.props.cartItems.totalPrice + this.state.shipmentCost) - (this.state.userCredit + this.state.couponValue),
            langCode: this.props.selectedLanguage.langCode,
            cardName: this.state.cardName,
            cardNum: this.state.cardNum,
            cardExpiryDate: this.state.cardExpiryDate,
            cvv: this.state.cvv,
            transaction_currency: 'USD',
            paymentStatus: 3 // waiting verification.
        }, this.props.userInfo.accessToken)
            .then( (response) => {
                this.setState({isDisabled: false})

                if(response.data.success == false) {
                    //this.setState({isDisabled: false})

                    return toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: response.data.msg
                    });
                }
                 
                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

                //this.props.emptifyCart();
                
                /* setTimeout( () => {
                    //this.props.history.push('/');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }

    fetchCouponInfo(e) {
        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        getData(this.props.defaultSets.apiUrl+'coupon-info/'+this.state.couponCode, this.props.userInfo.accessToken)
            .then(response => {  
                if(response.data.success == true) {
                    let couponRealValue = 0;
                    if(response.data.couponInfo.type == 1) {
                        couponRealValue = response.data.couponInfo.couponValue;
                    } else {
                        couponRealValue = ((response.data.couponInfo.couponValue * this.props.cartItems.totalPrice)/100).toFixed(2);
                    }
                    this.setState({couponInfo: response.data.couponInfo, couponValue: couponRealValue });

                    toast.dismiss(ldToast);
                } else {
                    return toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: response.data.msg
                    });
                }
                console.log(response.data)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }
    render() {
 
        const {isDisabled, paymentType, couponValue} = this.state;
        const {langCode, totalTxt, qtyTxt, priceTxt,  payPalTxt, photoTxt,
            actionTxt, productTitleTxt,
        shippingChargesTxt, loginTxt, 
    payUpnRcptTxt, payMnlTrnsferTxt, payCardTxt, couponCodeTxt,
    applyTxt, addShipmentAdrsTxt,
} = this.props.selectedLanguage;
const slctdCurr = this.props.slctdCurr;

const lng = this.props.selectedLanguage;

        return (
    
            <Fragment>
<Helmet><title>{lng.shoppingCart}</title></Helmet>
                
<BreadCrumb title={lng.shoppingCart} />



<section className="cart-section section-b-space">
        <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <table className="table cart-table table-responsive-xs">
                        <thead>
                            <tr className="table-head">
                                <th scope="col">{photoTxt}</th>
                                <th scope="col">{productTitleTxt}</th>
                                <th scope="col">{priceTxt}</th>
                                <th scope="col">{qtyTxt}</th>
                                <th scope="col">{actionTxt}</th>
                                <th scope="col">{totalTxt}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.cartItems && this.props.cartItems.cart.map((item, i) => {
                            let elmntTrns = item.elment_trans && item.elment_trans.find(prdct => prdct.languageCode == lng.langCode);

                                            return(
                                <tr key={i}>
                                <td>
                                <Link to={"/products/"+item.id}>
                                        {elmntTrns && elmntTrns.files[0] && 
                                    <img src={"/uploads/files/"+elmntTrns.files[0].fileName} 
                                        alt={eval("item.title"+langCode)}
                                        style={{height: '80px', width: '80px'}} />
                                    }
                                </Link>
                                </td>
                                <td>
                                <Link to={"/products/"+item.id}>
                                    {elmntTrns && elmntTrns.title}  
                                    
                                    {item.chosenPrpVl && ' - ' + eval("item.chosenPrpVl.title"+lng.langCode)}  
                                    {item.chosenPrp && ' - ' + eval("item.chosenPrp.title"+lng.langCode)}  

                                </Link>
                                

                                    <div className="mobile-cart-content row">
                                        <div className="col-xs-3">
                                            <div className="qty-box">
                                                <div className="input-group">
                                                    <input type="text" 
                                                    name="quantity" 
                                                    onChange={(qty) =>
                                                        {
                                                            if(qty.target.value > 0) {
                                                                this.props.updateQuantity(item.id, qty.target.value);
                                                            }                   
                                                            
                                                        }}
                                                    className="form-control input-number"
                                                        defaultValue={item.quantity} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-3">
                                            <h2 className="td-color">
                                            {(item.priceAfterDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}
                                            </h2>
                                        </div>
                                        <div className="col-xs-3">
                                            <h2 className="td-color"><a href="#" className="icon"><i className="ti-close"></i></a>
                                            </h2>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <h2>{(item.priceAfterDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}</h2>
                                </td>
                                <td>
                                    <div className="qty-box">
                                        <div className="input-group">
                                            <input 
                                            type="number" 
                                            
                                            onChange={(qty) =>
                                                {
                                                    if(qty.target.value > 0) {
                                                        this.props.updateQuantity(item.id, qty.target.value);
                                                    }                   
                                                    
                                                }}
                                            name="quantity" 
                                            className="form-control input-number"
                                            defaultValue={item.quantity} />
                                        </div>
                                    </div>
                                </td>
                                <td><a 
                                 href="#" className="icon"
                                onClick={(e) => {e.preventDefault(); this.props.removeItem(item);}}
                                ><i className="ti-close"></i></a></td>
                                <td>
                                    <h2 className="td-color">
                                    {((item.priceAfterDiscount * item.quantity)/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}
                                    </h2>
                                </td>
                            </tr>
                               )
                            })}
                        </tbody>    
                    </table>
                </div>
            
            
            
                <div className="checkout-page col-lg-6 col-sm-12 col-xs-12 ml-5 mt-3">
            <div className="checkout-form">

                                

                <form onSubmit={this.fetchCouponInfo}>
                <div className="form-group d-inline-flex">
                  <div className="field-label mr-3" style={{lineHeight: "40px"}}>
                    Coupon
                  </div>
                  
                  <input 
                                type="text" 
                                name="couponCode" 
                                onChange={this.onChangeValue}
                                placeholder={couponCodeTxt} />
                </div>

                <button 
                type="submit"                                 
                style={{padding: "10px 29px"}}
                disabled={isDisabled}                                
                className="btn-solid btn ml-3">
                    {applyTxt}
                </button>
              </form>
            </div>
          
          </div>
            </div>

        <div className="checkout-page">
          <div className="checkout-form">
              <div className="row">
                {this.props.userInfo.id != 0 && 

                <div className="col-lg-6 col-sm-12 col-xs-12">
                  <div className="checkout-details">
                    <div className="order-box">
                      <div className="title-box">
                        <div>{lng.adrsTxt}</div>
                      </div>
                      <ul className="sub-total">
                        <li>
                          {lng.shipmentAdrsTxt}
                          <div
                            style={{width: "100%", float: 'right', marginTop: "5px"}}
                          >
                              {this.state.userShpmntAdrs && this.state.userShpmntAdrs.map((item, i) => {
                        return(
                            <div key={i} className="shopping-option">
                              <input 
                                        type="radio"
                                        value={item.id}
                                        name="shipmentAdrsId" 
                                        onChange={this.onChangeValue}
                                        />
                              <label htmlFor="free-shipping">
                              {eval("item.country.title"+langCode)}, 
                                    {eval("item.city.title"+langCode)},
                                    {item.adrs}
                              </label>
                            </div>
                        )

                        })}

                            
                            
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div className="text-right">
                      <Link 
                        to={{
                            pathname: "/create-shipment-adrs",
                            data: {
                                plcToBck: '/Cart'
                            } // your data array of objects
                          }}
                        className="btn btn-solid">
                            {addShipmentAdrsTxt}
                        </Link>
                    </div>
                  </div>
                </div>
                }
                <div className="col-lg-6 col-sm-12 col-xs-12">
                  <div className="checkout-details">
                    <div className="order-box">
                      <div className="title-box">
                        <div>Product <span>Total</span></div>
                      </div>
                      <ul className="qty">
                        <li>{lng.subTotalTxt}<span>{(this.props.cartItems.totalPrice/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}</span></li>
                        <li>{shippingChargesTxt}<span>0.00</span></li>
                      </ul>
                      <ul className="total">
                        <li>{lng.couponValueTxt} <span className="count">{(couponValue/slctdCurr.exchangeRate).toFixed(2)}</span></li>
                      </ul>
                      <ul className="sub-total">
                        <li>{lng.totalTxt} <span className="count">{((this.props.cartItems.totalPrice - couponValue)/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}</span></li>
                      </ul>
                    </div>

                  {this.props.userInfo.id != 0 ? 
                  
                  (this.props.cartItems.cart.length > 0) &&
                    (<div className="payment-box">
                    <div className="upper-box">
                      <div className="">
                        <div className="card ">
                          <div className="card-header">
                              <div className="bg-white shadow-sm pt-3 pl-2 pr-2 pb-1">
                                  
                                  <ul role="tablist" className="nav bg-light nav-pills rounded nav-fill mb-3">
                                      <li className="nav-item"> <a onClick={() => this.setState({paymentType: 1})} data-toggle="pill" href="#credit-card" className="nav-link  active "> <i className="fas fa-credit-card mr-2"></i> {payCardTxt} </a> </li>
                                      <li className="nav-item"> <a  data-toggle="pill" href="#paypal" className="nav-link "> <i className="fab fa-paypal mr-2"></i> {payPalTxt} </a> </li>
                                      <li className="nav-item"> <a onClick={() => this.setState({paymentType: 3})} data-toggle="pill" href="#net-banking" className="nav-link "> <i className="fas fa-mobile-alt mr-2"></i> {payUpnRcptTxt} </a> </li>
                                      <li className="nav-item"> <a onClick={() => this.setState({paymentType: 2})} data-toggle="pill" href="#net-banking2" className="nav-link "> <i className="fas fa-mobile-alt mr-2"></i> {payMnlTrnsferTxt} </a> </li>
                                  </ul>
                              </div>
                              
                              <div className="tab-content">

                                  <div id="credit-card" className="tab-pane fade show active pt-3">
      
                                      <form role="form">
                                          <div className="form-group"> <label htmlFor="username">
                                                  <h6>{lng.cardHolderTxt}</h6>
                                              </label> 
                                              <input type="text" 
                                              className="form-control"
                                              name="cardName"
                                              placeholder={lng.cardHolderTxt}
                                              onChange={this.onChangeValue} /> 
                                          </div>
                                          <div className="form-group"> <label htmlFor="cardNumber">
                                                  <h6>{lng.cardNumberTxt}</h6>
                                              </label>
                                              <div className="input-group"> 
                                              
                                                                               
                                                  <input 
                                                  type="text" 
                                                  className="form-control"
                                                  name="cardNum"
                                                  placeholder={lng.cardNumberTxt}
                                                  maxLength="18"
                                                  onChange={this.onChangeValue} />
                                                  <div className="input-group-append"> 
                                                  <span className="input-group-text text-muted"> 
                                                  <i className="fab fa-cc-visa mx-1"></i> 
                                                  <i className="fab fa-cc-mastercard mx-1"></i> 
                                                  <i className="fab fa-cc-amex mx-1"></i> </span> </div>
                                              </div>
                                          </div>
                                          <div className="row">
                                              <div className="col-sm-8">
                                                  <div className="form-group"> <label><span className="hidden-xs">
                                                              <h6>{lng.expiryDateTxt}</h6>
                                                          </span></label>
                                                      <div className="input-group"> 
                                                      <input 
                      placeholder={lng.expiryDateTxt}
                      type="text" 
                      placeholder="MM/YY"
                      maxLength="5"
                      onChange={this.onChangeValue}
                      className="form-control" 
                      value={this.state.cardExpiryDate}
                      name="cardExpiryDate" /> 
                                                      
                                                      </div>
                                                  </div>
                                              </div>
                                              <div className="col-sm-4">
                                                  <div className="form-group mb-4"> <label data-toggle="tooltip" title="Three digit CV code on the back of your card">
                                                          <h6>{lng.cvvTxt} <i className="fa fa-question-circle d-inline"></i></h6>
                                                      </label> 
                                                      <input 
                      placeholder={lng.cvvTxt}
                      type="text" 
                      onChange={this.onChangeValue}
                      className="form-control" 
                      maxLength="4"
                      name="cvv" />
                                                  </div>
                                              </div>
                                          </div>
                                          <div className=""> 
                                          <button 
                      onClick={() => this.handlePayment()}
                      type="button" 
                      className="btn-solid btn"
                      >
                          {lng.completeTxt}
                      </button>
                                          
                                          </div>
                                      </form>
                                  </div>
                               
                              
                              <div id="paypal" className="tab-pane fade pt-3">
                                  <h6 className="pb-2">Select your paypal account type</h6>
                                  <div className="form-group "> <label className="radio-inline"> <input type="radio" name="optradio" defaultChecked /> Domestic </label> <label className="radio-inline"> <input type="radio" name="optradio" className="ml-5" />International </label></div>
                                  <p> <button type="button" className="btn-solid btn"><i className="fab fa-paypal mr-2"></i> Log into my Paypal</button> </p>
                                  <p className="text-muted"> Note: After clicking on the button, you will be directed to a secure gateway for payment. After completing the payment process, you will be redirected back to the website to view details of your order. </p>
                              </div> 
                              
                              <div id="net-banking" className="tab-pane fade pt-3">

                                  <div className=""> 
                                          <button 
                      onClick={() => this.handlePayment()}
                      type="button" 
                      className="btn-solid btn"
                      >
                          {lng.completeTxt}
                      </button>
                                          
                                          </div>
                              </div> 
                              
                              <div id="net-banking2" className="tab-pane fade pt-3">

                                  <div className=""> 
                                          <button 
                      onClick={() => this.handlePayment()}
                      type="button" 
                      className="btn-solid btn"
                      >
                          {lng.completeTxt}
                      </button>
                                          
                                          </div>
                              </div>

                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                
                  </div>)

          : 
          <Link to="/login" className="btn-solid btn">
              {loginTxt}
          </Link>}
                  </div>
                </div>
              </div>
          </div>
        
        </div>
            

        </div>
    </section>
     

        
        
          </Fragment>

    )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        //changeCurrency: (curr) => dispatch({ type: 'change_currency', payload: curr }),
        removeItem: (product) => dispatch({ type: 'REMOVE_FROM_CART', payload: product }),
        updateQuantity: (productId, quantity) => dispatch({ type: 'ChANGE_PRODUCT_QUANTITY', productId: productId, newQuantity: quantity }),
        emptifyCart: () => dispatch({ type: 'EMPTIFY_CART', payload: null }),
    }
};
const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        cartItems: state.cartItems,
        userInfo: state.User.info,
        slctdCurr: state.Currs.currencyInfo,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Cart);