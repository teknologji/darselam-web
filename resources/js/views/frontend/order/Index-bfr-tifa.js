import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import BreadCrumb from '../partials/BreadCrumb';
//import ShipmentadrsRow from '../../../components/ShipmentadrsRow';
import Moment from 'moment';
import Popup from "reactjs-popup";
//import '../../../../../public/assets/frontend/css/modal.css';
import {Helmet} from "react-helmet";
import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import { checkMimeType } from '../../../helpers/checkMimeType';
import RowThumbnail from '../../../components/RowThumbnail';
//import UserSideNav from '../partials/UserSideNav';


import DatePicker from "react-datepicker";
//import Moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            status: null,
            modalIsOpen: false,
            photo: '',         
            mainImg: null,
            isDisabled: false,
            rowsBfrFltr: [],
            
            orderStatus: '',
            paymentStatus: '',
            orderId: '',
            finalTotal: '',            
            dtFrom: new Date().setMonth(new Date().getMonth() - 1),
            dtTo: new Date()
        };
        this.returnOrderStatus = this.returnOrderStatus.bind(this);

        this.onValueChange = this.onValueChange.bind(this);
    }
  
    componentDidMount(){

      this.fetchData();
    }

   fetchData(){
    //const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

    getData(this.props.defaultSets.apiUrl+'user-orders/'+this.props.userInfo.id, this.props.userInfo.accessToken)
        //.then(response => response.json())
        .then(response => {
          //console.log(response.data.rows)
            this.setState({ rows: response.data.rows, rowsBfrFltr: response.data.rows });
            //toast.dismiss(ldToast)
        })
        .catch(function (error) {
            console.log(error);
        })
   }
    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }



    
    cancelOrder(orderId) {

        confirmAlert({
              
          customUI: ({ onClose }) => {
              return (
                  <div id="react-confirm-alert">
                      <div className="react-confirm-alert-overlay">
                          <div className="react-confirm-alert">
                              <div className="react-confirm-alert-body">

                                  <h1>{this.props.selectedLanguage.rUSureTxt}</h1>

                                  <div 
                                  style={{flexDirection: this.props.selectedLanguage.langCode == "AR" ? 'row-reverse': 'row'}}
                                  className="react-confirm-alert-button-group">
                                  <button
                                  disabled={this.state.isDisabled}
                                  className="yes"
                                  onClick={() => {
                                      
                                    this.setState({isDisabled: true});
                                    const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

                                    putData(this.props.defaultSets.apiUrl+'orders/'+orderId, {
                                        id: orderId,
                                        orderStatus: 4, // cancelled
                                        langCode: this.props.selectedLanguage.langCode
                                    }, this.props.userInfo.accessToken)
                                    .then( (response) => {
                                        this.fetchData();

                                        toast.update(ldToast, {
                                            type: toast.TYPE.SUCCESS,
                                            render: response.data.msg
                                        });
                                    })
                                    .catch( (error) => {return null});
                                  }}
                                  >
                                          {this.props.selectedLanguage.yesTxt}
                                      </button>
  
                                      <button className="no" onClick={onClose}>
                                      {this.props.selectedLanguage.noTxt}
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
  
              );
            }
        });
    
  
      }

    
      
    returnOrderStatus(orderStatus){
        //console.log(orderStatus)
        switch (orderStatus){
            case 3 : return <span className="text-success">{this.props.selectedLanguage.doneTxt}</span>;
            case 4 : return <span className="text-danger">{this.props.selectedLanguage.cancelledTxt}</span>;
            case 8 : return <span className="text-info">{this.props.selectedLanguage.pendingTxt}</span>;
            case 7 : return <span className="text-danger">{this.props.selectedLanguage.returnedTxt}</span>;
            case 6 : return <span className="text-warning">{this.props.selectedLanguage.sentToCargoTxt}</span>;
            default: return 'Null'
        }
    }

 
    returnPaymentStatus(paymentStatus){
        switch (paymentStatus){
            case 1 : return <h4><span className="badge badge-success p2">{this.props.selectedLanguage.verifiedTxt}</span></h4>;
            case 2 : return <h4><span className="badge badge-danger p2">{this.props.selectedLanguage.failedTxt}</span></h4>;
            case 3 : return <h4><span className="badge badge-info p2">{this.props.selectedLanguage.waitingVerificationTxt}</span></h4>;
            default : return <h4><span className="badge badge-info p2">{this.props.selectedLanguage.loadingTxt}</span></h4>;
        }
    }

    onMainImageChange(event) {
        if (event.target.files && event.target.files[0]) {
  
          let img = event.target.files[0];
  
          let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
          if(mimeTypeCheck == true){
  
            return this.setState({
              mainImg: img 
            }); 
          } 
          toast.error(mimeTypeCheck);
            
        }
      };

          // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
    handleUploadMainPhoto(rowId) {
        if (this.state.mainImg == null ) {
          return false;
        }
  
        // then prepare a form to send files.
        const formData = new FormData()
        
        // The next two lines go to FilesController to specify the row and tbl where inserted.
        formData.append('rowId', rowId);
        formData.append('type', 'order');
        formData.append('mainPhoto', '0');
        //.
  
        
        // append the main photo to formData.
        formData.append(
          'files[]',
          this.state.mainImg,
          this.state.mainImg.name
        )
        //.
  
        fetch(this.props.defaultSets.apiUrl+'upload-files', {
          method: 'POST',
          body: formData
        })
        .then(response => response.json())
        .then(response => {
            toast.success(this.props.selectedLanguage.uploadedSuccessfullyTxt);
          this.setState({
            mainImg: null,
          });
          /* onUploadProgress: progressEvent => {
            console.log(progressEvent.loaded / progressEvent.total)
          } */
        })
      }
  
      rmvSlctdImg() {
        this.setState({
          mainImg: null
        })
      }

      onValueChange(e) {
        //console.log(e.target.value)
        this.setState({
          [e.target.name]: e.target.value
      }, () => {
          this.tblFilter();
      });
      }
      
      tblFilter() {

        let newRows = this.state.rowsBfrFltr;      

         if(this.state.dtFrom != '' && this.state.dtTo != '') {
           let dtFromT = new Date(this.state.dtFrom);
           let dtToT = new Date(this.state.dtTo);
           //console.log(dtFromT+'kkkkkkkk'+dtToT)
           newRows = newRows.filter(row => row.created_at && new Date(row.created_at) >= dtFromT && new Date(row.created_at) <= dtToT);
          }
      
         if(this.state.finalTotal != '') {
          newRows = newRows.filter(row => row.finalTotal && row.finalTotal.toString().search(this.state.finalTotal) !== -1);
          }
      /* 
          if(this.state.phone != '') {
          newRows = newRows.filter(row => row.user.phone && row.user.phone.search(this.state.phone) !== -1);
          }
      
          if(this.state.full_name != '') {
            newRows = newRows.filter(row => row.user.full_name && row.user.full_name.toLowerCase().search(this.state.full_name.toLowerCase()) !== -1);
          } */
      
          if(this.state.orderId != '') {
            newRows = newRows.filter(row => row.orderId && row.orderId.search(this.state.orderId) !== -1);
          }
      
          if(this.state.orderStatus != '') {
            newRows = newRows.filter(row => row.orderStatus == this.state.orderStatus);
          }       

          
    if(this.state.paymentStatus != '') {
      newRows = newRows.filter(row => row.paymentStatus == this.state.paymentStatus);
    }
         
         this.setState({rows: newRows});
      }
      
      hndlDtFrom(dtFrom) {
        this.setState({
          dtFrom
        }, () => {
          this.tblFilter();
      });
      };
      
      hndlDtTo(dtTo) {
        this.setState({
          dtTo
        }, () => {
          this.tblFilter();
      });
      };
      
 
    render() {
        
        //const {isLoading} = this.state;
        const lng = this.props.selectedLanguage;
        const { cancelTxt, dateTxt, countryTxt, cityTxt, adrsTxt, closeTxt,
            paymentStatusTxt, orderStatusTxt, myOrdersTxt, orderDetailsTxt, receiptTxt
        , langCode, uploadTxt, totalTxt, productTitleTxt, colourTxt, sizeTxt,
        prcBfrDscnt, prcAftrDscnt, qtyTxt, subTotalTxt} = this.props.selectedLanguage;
        const slctdCurr = this.props.slctdCurr;
        return (
            
         <Fragment>

<Helmet><title>{myOrdersTxt}</title></Helmet>
         <BreadCrumb title={myOrdersTxt} />


         <section className="wishlist-section section-b-space">
        <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <table className="table cart-table table-responsive-xs">
                        <thead>
                            <tr className="table-head">
                                <th>#</th>
                                <th scope="col">{lng.orderDetailsTxt}</th>
                                <th scope="col">{lng.totalTxt}</th>
                                <th scope="col">{lng.orderStatusTxt}</th>
                                <th scope="col">{lng.paymentStatusTxt}</th>
                                <th scope="col">{lng.receiptTxt}</th>
                                <th scope="col">{lng.adrsTxt}</th>
                                <th scope="col">{lng.dateTxt}</th>
                                <th scope="col">{lng.cancelTxt}</th>
                            </tr>
                        </thead>
                        <tbody>
                        {this.state.rows && this.state.rows.map((item, i) => {
                                            return(
                            <tr id={item.id} key={i}>
                                    <td>
                                        {i + 1}
                                    </td>
                                <td>
                                <Popup 
     trigger={<button className="site-btn-small"><i className="fa fa-table"></i></button>} 
     modal position="right center">
    
     { close => (
         <div className="kmodal">
         <a className="close" onClick={close}>
           &times;
         </a>
         <div className="header">{orderDetailsTxt} - {item.orderId}</div>
         <div className="content">
         
     <table>
                             <thead>
                                 <tr>
                                     <th>{'#'}</th>
                                     <th>{productTitleTxt}</th>
                                     <th>{prcBfrDscnt}</th>
                                     <th>{prcAftrDscnt}</th>
                                     <th>{colourTxt}</th>
                                     <th>{sizeTxt}</th>
                                     <th>{qtyTxt}</th>
                                     <th>{subTotalTxt}</th>
                                 </tr>
                             </thead>
                             <tbody>
                             {item.order_items && item.order_items.map((row, k) => {
                                             return(
                                                 
                                 <tr key={'oi'+k}>
                                   <td>
                                         {k + 1}
                                     </td>                    
                                   <td>
                                    {eval("row.product.title"+langCode)}
                                   </td>
                                                                          
                                   <td>
                                       {(row.priceBeforeDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}
                                   </td>
                                                                          
                                   <td>
                                       {(row.priceAfterDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}
                                   </td>
                                                                          
                                   <td>
                                   {row.size && eval("row.colour.title"+langCode)}
                                   </td>
                                                                          
                                   <td>
                                   {row.size && eval("row.size.title"+langCode)}
                                   </td>
                                                                          
                                   <td>
                                       {row.quantity}
                                   </td>
                                                                          
                                   <td>
                                       {row.quantity * row.priceAfterDiscount}
                                   </td>
                                 </tr>
                                
                                 );
                             }
                             )}                    
                             </tbody>
                              <tfoot>
                                 <tr>
                                     <th colSpan="3"></th>
                                     <th>{totalTxt} : {item.finalTotal}</th>
                                     <th colSpan="3"></th>
                                 </tr>
                             </tfoot> 
                         </table>
                         </div>
 
                         <div className="actions">
                         <button
             className="site-btn-small"
             onClick={close}
           >{closeTxt}</button>
                         </div>
 
     </div>
   
     )}
  </Popup>
                                </td>
                                <td>
                                    <h2>{item.finalTotal} {item.transaction_currency}</h2>
                                </td>
                                <td>
                                    <p>{this.returnOrderStatus(item.orderStatus)}</p>
                                </td>
                                <td>
                                { this.returnPaymentStatus(item.paymentStatus)}
                                </td>
                                <td>
                                {item.paymentType == 2 && item.orderStatus == 8 &&
                                      
                                      <div>
                                          <input type="file"     
                                           onChange={(e) => this.onMainImageChange(e)}              
                                           accept=".gif,.jpg,.jpeg,.png" />
                   
                                           <button 
                                           type="button" 
                                           onClick={() => this.handleUploadMainPhoto(item.id)}
                                           disabled={this.state.isDisabled}
                                           className="btn btn-primary pull-left">{uploadTxt}
                                           </button>
                                      </div>
                                                         }
                                </td>
                                


                                <td>
                                <Popup 
                                  trigger={
                                  <button className="site-btn-small">
                                    <i className="fa fa-map-marker"></i>
                                  </button>
                                  } 
                                  modal position="right center">                                  
                                  { close => (
                                      <div className="kmodal">
                                      <a className="close" onClick={close}>
                                        &times;
                                      </a>
                                      <div className="header">{adrsTxt}</div>
                                      <div className="content">
                                      
                                  <table>
                                      <thead>
                                          <tr>
                                              <th>{countryTxt}</th>
                                              <th>{cityTxt}</th>
                                              <th>{adrsTxt}</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                                          
                                          <tr>                   
                                            <td>
                                            {eval("item.shipmentadrs.country.title"+langCode)}
                                            </td>

                                            <td>
                                            {eval("item.shipmentadrs.city.title"+langCode)}
                                            </td>
                                                                                
                                            <td>
                                            {item.shipmentadrs.adrs}
                                            </td>
                                          </tr>  
                                                        
                                      </tbody>
                                        
                                  </table>
                                                      
                                                      </div>
                              
                                    <div className="actions">
                                                      <button
                                          className="site-btn-small"
                                          onClick={close}
                                        >{closeTxt}</button>
                                                      </div>
                              
                                  </div>
                                  )}
                                </Popup>
                                </td>

                                <td>
                                {Moment(item.created_at).format("YYYY-MM-DD")}
                                </td>
                                <td>
                                  <a href="#" className="icon mr-3" onClick={(e) => {e.preventDefault(); this.cancelOrder(item.id)}}>
                                    <i className="ti-close"></i> 
                                  </a>
                                </td>
                            </tr>
                            )
                            })}
                        </tbody>
                    </table>
                </div>
            
        </div>
        </div>
    </section>
         
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,
        slctdCurr: state.Currs.currencyInfo,
    }
}
export default connect(mapStateToProps, null)(Index);
