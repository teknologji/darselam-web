import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import BreadCrumb from '../partials/BreadCrumb';
//import ShipmentadrsRow from '../../../components/ShipmentadrsRow';
import Moment from 'moment';
import Popup from "reactjs-popup";
//import '../../../../../public/assets/frontend/css/modal.css';
import {Helmet} from "react-helmet";
import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import { checkMimeType } from '../../../helpers/checkMimeType';
import RowThumbnail from '../../../components/RowThumbnail';

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            status: null,
            modalIsOpen: false,
            photo: '',         
            mainImg: null,
            isDisabled: false
        };
        this.returnOrderStatus = this.returnOrderStatus.bind(this);
        //this.returnPaymentStatus = this.returnPaymentStatus.bind(this);

    }
  
    componentDidMount(){

      this.fetchData();
    }

   fetchData(){
    //const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

    getData(this.props.defaultSets.apiUrl+'user-orders/'+this.props.userInfo.id, this.props.userInfo.accessToken)
        //.then(response => response.json())
        .then(response => {
            this.setState({ rows: response.data.rows });
            //toast.dismiss(ldToast)
            //console.log(response.data)
        })
        .catch(function (error) {
            console.log(error);
        })
   }
    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }



    
    cancelOrder(orderId) {

        confirmAlert({
              
          customUI: ({ onClose }) => {
              return (
                  <div id="react-confirm-alert">
                      <div className="react-confirm-alert-overlay">
                          <div className="react-confirm-alert">
                              <div className="react-confirm-alert-body">

                                  <h1>{this.props.selectedLanguage.rUSureTxt}</h1>

                                  <div 
                                  style={{flexDirection: this.props.selectedLanguage.langCode == "AR" ? 'row-reverse': 'row'}}
                                  className="react-confirm-alert-button-group">
                                  <button
                                  disabled={this.state.isDisabled}
                                  className="yes"
                                  onClick={() => {
                                      
                                    this.setState({isDisabled: true});
                                    const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

                                    putData(this.props.defaultSets.apiUrl+'orders/'+orderId, {
                                        //userId: this.props.userInfo.id,
                                        id: orderId,
                                        orderStatus: 4, // cancelled
                                        langCode: this.props.selectedLanguage.langCode
                                    }, this.props.userInfo.accessToken)
                                    .then( (response) => {
                                        this.fetchData();

                                        toast.update(ldToast, {
                                            type: toast.TYPE.SUCCESS,
                                            render: response.data.msg
                                        });
                                    })
                                    .catch( (error) => {return null});
                                  }}
                                  >
                                          {this.props.selectedLanguage.yesTxt}
                                      </button>
  
                                      <button className="no" onClick={onClose}>
                                      {this.props.selectedLanguage.noTxt}
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
  
              );
            }
        });
    
  
      }

    
      
    returnOrderStatus(orderStatus){
        //console.log(orderStatus)
        switch (orderStatus){
            case 3 : return this.props.selectedLanguage.doneTxt;
            case 4 : return this.props.selectedLanguage.cancelledTxt;
            case 8 : return this.props.selectedLanguage.pendingTxt;
            case 7 : return this.props.selectedLanguage.returnedTxt;
            case 6 : return this.props.selectedLanguage.sentToCargoTxt;
            default: return 'Null'
        }
    }

 
    returnPaymentStatus(paymentStatus){
        switch (paymentStatus){
            case 1 : return this.props.selectedLanguage.verifiedTxt;
            case 2 : return this.props.selectedLanguage.failedTxt;
            case 3 : return this.props.selectedLanguage.waitingVerificationTxt;
            default: return 'Null'
        }
    }

    onMainImageChange(event) {
        if (event.target.files && event.target.files[0]) {
  
          let img = event.target.files[0];
  
          let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
          if(mimeTypeCheck == true){
  
            return this.setState({
              mainImg: img 
            }); 
          } 
          toast.error(mimeTypeCheck);
            
        }
      };

          // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
    handleUploadMainPhoto(rowId) {
        if (this.state.mainImg == null ) {
          return false;
        }
  
        // then prepare a form to send files.
        const formData = new FormData()
        
        // The next two lines go to FilesController to specify the row and tbl where inserted.
        formData.append('rowId', rowId);
        formData.append('type', 'order');
        formData.append('mainPhoto', '0');
        //.
  
        
        // append the main photo to formData.
        formData.append(
          'files[]',
          this.state.mainImg,
          this.state.mainImg.name
        )
        //.
  
        fetch(this.props.defaultSets.apiUrl+'upload-files', {
          method: 'POST',
          body: formData
        })
        .then(response => response.json())
        .then(response => {
            toast.success(this.props.selectedLanguage.uploadedSuccessfullyTxt);
          this.setState({
            mainImg: null,
          });
          /* onUploadProgress: progressEvent => {
            console.log(progressEvent.loaded / progressEvent.total)
          } */
        })
      }
  
      rmvSlctdImg() {
        this.setState({
          mainImg: null
        })
      }


 
    render() {
        
        //const {isLoading} = this.state;
        const { cancelTxt, dateTxt, countryTxt, cityTxt, adrsTxt, closeTxt,
            paymentStatusTxt, orderStatusTxt, myOrdersTxt, orderDetailsTxt, receiptTxt
        , langCode, uploadTxt, totalTxt, productTitleTxt, colourTxt, sizeTxt,
        prcBfrDscnt, prcAftrDscnt, qtyTxt, subTotalTxt} = this.props.selectedLanguage;
        const lng = this.props.selectedLanguage;

        return (
            
         <Fragment>

<Helmet><title>{myOrdersTxt}</title></Helmet>
         <BreadCrumb title={myOrdersTxt} />




         <section className="wishlist-section section-b-space">
        <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <table className="table cart-table table-responsive-xs">
                        <thead>
                            <tr className="table-head">
                                <th>#</th>
                                <th scope="col">{langCode.orderDetailsTxt}</th>
                                <th scope="col">{lng.totalTxt}</th>
                                <th scope="col">{lng.orderStatusTxt}</th>
                                <th scope="col">{lng.paymentStatusTxt}</th>
                                <th scope="col">{lng.adrsTxt}</th>
                                <th scope="col">{lng.receiptTxt}</th>
                                <th scope="col">{lng.dateTxt}</th>
                                <th scope="col">{lng.cancelTxt}</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        {this.state.rows && this.state.rows.map((item, i) => {
                                            return(
                              
                            <tr id={item.id} key={i}>
                                    <td>
                                        {item.id}
                                    </td>
                                <td>
                                <Popup 
     trigger={<button className="site-btn-small"><i className="fa fa-table"></i></button>} 
     modal position="right center">
    
     { close => (
         <div className="kmodal">
         <a className="close" onClick={close}>
           &times;
         </a>
         <div className="header">{orderDetailsTxt} - {item.orderId}</div>
         <div className="content">
         
     <table>
                             <thead>
                                 <tr>
                                     <th className="shoping__product">{'#'}</th>
                                     <th>{productTitleTxt}</th>
                                     <th>{prcBfrDscnt}</th>
                                     <th>{prcAftrDscnt}</th>
                                     <th>{colourTxt}</th>
                                     <th>{sizeTxt}</th>
                                     <th>{qtyTxt}</th>
                                     <th>{subTotalTxt}</th>
                                 </tr>
                             </thead>
                             <tbody>
                             {item.order_items && item.order_items.map((row, k) => {
                                             return(
                                                 
                                 <tr key={'oi'+k}>
                                   <td className="shoping__cart__item">
                                         {k + 1}
                                     </td>                    
                                   <td className="shoping__cart__item">
                                    {eval("row.product.title"+langCode)}
                                   </td>
                                                                          
                                   <td className="shoping__cart__item">
                                       {row.priceBeforeDiscount}
                                   </td>
                                                                          
                                   <td className="shoping__cart__item">
                                       {row.priceAfterDiscount}
                                   </td>
                                                                          
                                   <td className="shoping__cart__item">
                                   {row.size && eval("row.colour.title"+langCode)}
                                   </td>
                                                                          
                                   <td className="shoping__cart__item">
                                   {row.size && eval("row.size.title"+langCode)}
                                   </td>
                                                                          
                                   <td className="shoping__cart__item">
                                       {row.quantity}
                                   </td>
                                                                          
                                   <td className="shoping__cart__item">
                                       {row.quantity * row.priceAfterDiscount}
                                   </td>
                                 </tr>
                                
                                 );
                             }
                             )}                    
                             </tbody>
                              <tfoot>
                                 <tr>
                                     <th colSpan="3"></th>
                                     <th>{totalTxt} : {item.finalTotal}</th>
                                     <th colSpan="3"></th>
                                 </tr>
                             </tfoot> 
                         </table>
                         </div>
 
                         <div className="actions">
                         <button
             className="site-btn-small"
             onClick={close}
           >{closeTxt}</button>
                         </div>
 
     </div>
   
     )}
  </Popup>
                                </td>
                                <td>
                                    <h2>{item.finalTotal} {item.transaction_currency}</h2>
                                </td>
                                <td>
                                    <p>{this.returnOrderStatus(item.orderStatus)}</p>
                                </td>

                                <td>
                                {item.paymentType == 2 && item.orderStatus == 8 &&
                                      
                                      <div>
                                          <input type="file"     
                                           onChange={(e) => this.onMainImageChange(e)}              
                                           accept=".gif,.jpg,.jpeg,.png" />
                   
                                           <button 
                                           type="button" 
                                           onClick={() => this.handleUploadMainPhoto(item.id)}
                                           disabled={this.state.isDisabled}
                                           className="btn btn-primary pull-left">{uploadTxt}
                                           </button>
                                      </div>
                                                         }
                                </td>
                                <td>
                                { this.returnPaymentStatus(item.paymentStatus)}
                                </td>


                                <td>
                                <Popup 
                                  trigger={
                                  <button className="site-btn-small">
                                    <i className="fa fa-map-marker"></i>
                                  </button>
                                  } 
                                  modal position="right center">                                  
                                  { close => (
                                      <div className="kmodal">
                                      <a className="close" onClick={close}>
                                        &times;
                                      </a>
                                      <div className="header">{adrsTxt}</div>
                                      <div className="content">
                                      
                                  <table>
                                      <thead>
                                          <tr>
                                              <th>{countryTxt}</th>
                                              <th>{cityTxt}</th>
                                              <th>{adrsTxt}</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                                          
                                          <tr>                   
                                            <td className="shoping__cart__item">
                                            {eval("item.shipmentadrs.country.title"+langCode)}
                                            </td>

                                            <td className="shoping__cart__item">
                                            {eval("item.shipmentadrs.city.title"+langCode)}
                                            </td>
                                                                                
                                            <td className="shoping__cart__item">
                                            {item.shipmentadrs.adrs}
                                            </td>
                                          </tr>  
                                                        
                                      </tbody>
                                        
                                  </table>
                                                      
                                                      </div>
                              
                                    <div className="actions">
                                                      <button
                                          className="site-btn-small"
                                          onClick={close}
                                        >{closeTxt}</button>
                                                      </div>
                              
                                  </div>
                                  )}
                                </Popup>
                                </td>


                                <td>
                                                                                                       
                                <Popup 
                                  repositionOnResize={true}
                                  lockScroll={false}
                                  overlayStyle={{zIndex: '9991'}}
                                  trigger={item.files && item.files.length > 0 ?
                                  <button className="site-btn-small">
                                    <i className="fa fa-image"></i>
                                  </button>: null
                                  } 
                                  modal position="right center">                                  
                                  { close => (
                                      <div className="kmodal">
                                      <a className="close" onClick={close}>
                                        &times;
                                      </a>
                                      <div className="header">{receiptTxt}</div>
                                      <div className="content">
                                      
                                  <table style={{width: '100%', justifyContent: 'center'}}>
                                      
                                      <tbody>
                                            
                                
                                  {item.files.map((file, i) => {
                                    return(                                      
                                                
                                          <tr key={'orderFile'+i}>                   
                                            <td>
                                            <RowThumbnail imgName={file.fileName} width='300px' height="400px" />
                                            </td>
                                          
                                          </tr>  
                                                  )
                                                })}        
                                      </tbody>
                                        
                                  </table>
                                                      
                                                      </div>
                              
                                                      <div className="actions">
                                                      <button
                                          className="site-btn-small"
                                          onClick={close}
                                        >{closeTxt}</button>
                                                      </div>
                              
                                  </div>
                                  )}
                                </Popup>
                                
                                </td>

                                <td>
                                {Moment(item.created_at).format("YYYY-MM-DD")}
                                </td>
                                <td>
                                  <a href="#" className="icon mr-3" onClick={(e) => {e.preventDefault(); this.cancelOrder(item.id)}}>
                                    <i className="ti-close"></i> 
                                  </a>
                                </td>
                            </tr>
                            )
                            })}
                        </tbody>
                    </table>
                </div>
            
        </div>
        </div>
    </section>
         
                                  
                               
                            
                


         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,
    }
}
export default connect(mapStateToProps, null)(Index);
