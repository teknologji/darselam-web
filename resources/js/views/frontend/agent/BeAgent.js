import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import {toast} from "react-toastify";
import { Helmet } from 'react-helmet';
import postData from '../../../helpers/postData';

class Contact extends Component {

    constructor(props) {
        super(props);
        this.state = {
            companyName: '',
            companyOwner: '',
            email: '',
            adrs: '',
            city: '',
            theState: '',
            zipCode: '',
            country: '',
            phone: '',
            fax: '',
            website: '',
            foundationYear: '',
            employeesCount: '',
            salePointsCount: '',
            licenseType: '',
            otherSourcesNames: '',
            activityCountries: '',
            notes: '',
            password: 123456,
            isDisabled: false
        };
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount(){    

    }

    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
        //console.log(this.state.cityId)

    }
    

    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'users', {
            groupId: 3, // 1:admin, 2:user, 3:agent.
            companyName: this.state.companyName,
            companyOwner: this.state.companyOwner,
            email: this.state.email,
            adrs: this.state.adrs,
            city: this.state.city,
            theState: this.state.theState,
            zipCode: this.state.zipCode,
            country: this.state.country,
            phone: this.state.phone,
            fax: this.state.fax,
            website: this.state.website,
            foundationYear: this.state.foundationYear,
            employeesCount: this.state.employeesCount,
            salePointsCount: this.state.salePointsCount,
            licenseType: this.state.licenseType,
            otherSourcesNames: this.state.otherSourcesNames,
            activityCountries: this.state.activityCountries,
            notes: this.state.notes,
            password: this.state.password,
            langCode: this.props.selectedLanguage.langCode
        })
            .then( (response) => {

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
                
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }
    render() {
 
        //const {} = this.state;
        const lng = this.props.selectedLanguage;
        const sts = this.props.defaultSets;

        return (
    
            <Fragment>
                <Helmet><title>{lng.beOurAgentTxt}</title></Helmet>

<BreadCrumb title={lng.beOurAgentTxt} />



<section className="register-page section-b-space">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <h3>{lng.beOurAgentTxt}</h3>
            <div className="theme-card">
              <h4 className="mb-4">{lng.shouldRgstrMsgTxt}:</h4>
              <form className="theme-form" onSubmit={this.handleSubmit}>

                <div className="form-row">
                  <div className="col-md-6">
                    <label htmlFor="name">{lng.companyNameTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="companyName"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                  <div className="col-md-6">
                    <label htmlFor="">{lng.companyOwnerTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="companyOwner"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-12">
                    <label htmlFor="email">{lng.emailTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="email"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <label htmlFor="">{lng.adrsTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="adrs"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                  <div className="col-md-6">
                    <label htmlFor="">{lng.cityTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="city"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <label htmlFor="">{lng.theStateTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="theState"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                  <div className="col-md-6">
                    <label htmlFor="">{lng.zipCodeTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="zipCode"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-12">
                    <label htmlFor="">{lng.countryTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="country"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <label htmlFor="">{lng.phoneTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="phone"      
                      maxLength={13}                       
                      onChange={this.onChangeValue}
                    />
                  </div>
                  <div className="col-md-6">
                    <label htmlFor="">{lng.faxTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="fax"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <label htmlFor="">{lng.websiteTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="website"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                  <div className="col-md-6">
                    <label htmlFor="">{lng.foundationYearTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="foundationYear"    
                      maxLength={4}                         
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-12">
                    <label htmlFor="">{lng.employeesCountTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="employeesCount"     
                      maxLength={5}                        
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <label htmlFor="">{lng.salePointsCountTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="salePointsCount"
                      maxLength={5}                       
                      onChange={this.onChangeValue}
                    />
                  </div>
                  <div className="col-md-6">
                    <label htmlFor="">{lng.licenseTypeTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="licenseType"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-6">
                    <label htmlFor="">{lng.otherSourcesNamesTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="otherSourcesNames"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                  <div className="col-md-6">
                    <label htmlFor="">{lng.activityCountriesTxt}</label>
                    <input
                      type="text"
                      className="form-control"
                      name="activityCountries"                             
                      onChange={this.onChangeValue}
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-12">
                    <label htmlFor="">{lng.msgTxt}</label>
                    <textarea
                      className="form-control"
                      name="notes"                             
                      onChange={this.onChangeValue}
                    ></textarea>
                  </div>
                </div>
                <button 
                className="btn btn-solid mt-4" 
                disabled={this.state.isDisabled}
                type="submit">{lng.sendTxt}</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    

          </Fragment>

    )
    }
}



const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        cartItems: state.cartItems.cart,
        //userInfo: state.User.info
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Contact);