import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import BreadCrumb from '../partials/BreadCrumb';
import {toast} from "react-toastify";
//import CountriesSelect from '../../../components/CountriesSelect';
//import CitiesSelect from '../../../components/CitiesSelect';
import {Helmet} from "react-helmet";
import postData from '../../../helpers/postData';

import ReactFlagsSelect from 'react-flags-select'; 
//import css module
import 'react-flags-select/css/react-flags-select.css';
import AsyncSelect from 'react-select/async';



class Create extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            receiverMobile: '',
            receiverMobile2: '',
            //password: '',
            countryId: 162,
            cityId: '',
            adrs: '',
            email: '',
            errorMessage: '',
            isDisabled: false,
            isLoading: false,
            cities: [],
            countryCode: 'TR',
            cityName: '',
            
        };

        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.filterCities = this.filterCities.bind(this);
        this.loadCities = this.loadCities.bind(this);

    }

    
    componentDidMount(){    
        this.fetchCities();
    }

    componentWillUnmount() {
        //this.setState({galleryProducts: []})
        //document.getElementById("mishu").remove;
    }

    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
        

    }




    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'shipmentadrs', {
            userId: this.props.userInfo.id,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            receiverMobile: this.state.receiverMobile,
            receiverMobile2: this.state.receiverMobile2,
            countryId: this.state.countryId,
            countryCode: this.state.countryCode,
            cityId: this.state.cityId,
            //password: this.state.password,
            adrs: this.state.adrs,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
                if(this.props.location.data != undefined){
                    return this.props.history.push(this.props.location.data.plcToBck);
                    //return <Redirect to="/cart" />
                }
                /* 
                setTimeout( () => {
                    //this.props.history.push('/');
                }, 3000); */
            })
            .catch( (error) => {
                
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }

    /* cities selectbox functions */
    filterCities(cityName){
        
        return this.state.cities.filter(i =>
          i.label.toLowerCase().includes(cityName.toLowerCase())
        );
      }; 

     loadCities(cityName, callback){

        setTimeout(() => {
          callback(this.filterCities(cityName));
        }, 1000);
      }; 

    fetchCities(){
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);
        this.setState({ isDisabled: true, isLoading: true, cities: [] });

        axios.get(this.props.defaultSets.apiUrl+'country-cities/'+this.state.countryCode)  
            //.then(response => response.json())
            .then(response => {
                let cities = [];
                this.setState({ cities, isDisabled: false, isLoading: false });

                {response.data.rows && response.data.rows.map((item, i) => {                    
                    cities.push({ value: item.id, label: item.titleAR+'-'+item.titleEN})
                })} 

                toast.dismiss(ldToast)
                
            })
            .catch(function (error) {
                console.log(error);
            })
        
    }


      /* cities selectbox functions end */
    render() {
 
        const {isDisabled, cities, isLoading} = this.state;
        const { receiverMobileTxt, receiverMobile2Txt,countryTxt, 
            chooseTxt, cityTxt, firstNameTxt, lastNameTxt, addShipmentAdrsTxt,
            saveTxt, emailTxt, showShipmentAdrsesTxt, langCode,
            adrsTxt} = this.props.selectedLanguage;
        return (

            <Fragment>
                <Helmet><title>{addShipmentAdrsTxt}</title></Helmet>
                 <BreadCrumb title={addShipmentAdrsTxt} />



                 <section className="register-page section-b-space">
        <div className="container">
            <div className="row">
                <div className="col-lg-12">
                    <h3>{showShipmentAdrsesTxt}</h3>
                    <div className="theme-card">
                        <form className="theme-form" onSubmit={this.handleSubmit}>                        

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label htmlFor="email">{firstNameTxt}</label>
                                            
                                    <input type="text" 
                                    className="form-control"
                                    name="firstName"
                                    placeholder={firstNameTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                                <div className="col-md-6">
                                    <label htmlFor="review">{lastNameTxt}</label>
                                    
                                    <input 
                                    type="text" 
                                    className="form-control"
                                    name="lastName"
                                    placeholder={lastNameTxt}
                                    onChange={this.onChangeValue} />
                                </div>
                            </div>

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label htmlFor="email">{receiverMobileTxt}</label>
                                    <input 
                                placeholder={receiverMobileTxt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="receiverMobile" />
                                </div>
                                <div className="col-md-6">
                                    <label htmlFor="review">{receiverMobile2Txt}</label>
                                    <input 
                                placeholder={receiverMobile2Txt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="receiverMobile2" />
                                </div>
                       
                          
                            </div>



                            <div className="form-row">
                                <div className="col-md-6">
                                    <label htmlFor="email">{countryTxt}</label>
                                    <ReactFlagsSelect
                    searchable={true}
                    defaultCountry="TR"
                    searchPlaceholder={countryTxt}    
                    onSelect={(countryCode) => {this.setState({countryCode}, () => this.fetchCities())}}
                    />
                                </div>
                                <div className="col-md-6">
                                    <label htmlFor="review">{cityTxt}</label>
                                    <AsyncSelect
                    className="form-control"
                    classNamePrefix={cityTxt}
                    defaultValue={cities[0]}
                    /* isDisabled={isDisabled} */
                    isLoading={isLoading}
                    isRtl={langCode == 'AR'? true: false}
                    isSearchable={true}
                    name="cityId"
                    onChange={(cityInfo) => this.setState({cityId: cityInfo.value})}
                    /*options={cities}*/
                    loadOptions={this.loadCities}
                    onInputChange={(cityName) => this.setState({ cityName })}

                    />
                                </div>
                       
                          
                            </div>

                            <div className="form-row">
                                <div className="col-md-6">
                                    <label htmlFor="email">{adrsTxt}</label>
                                            
                                    <input 
                                placeholder={adrsTxt}
                                type="text" 
                                onChange={this.onChangeValue}
                                className="form-control" 
                                name="adrs"
                                 />
                                </div>
                                <div className="col-md-6"></div>

                                <button type="submit" disabled={isDisabled} className="btn btn-solid">{saveTxt}</button>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


            </Fragment>



    )
    }
}



const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        userInfo: state.User.info,
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Create);