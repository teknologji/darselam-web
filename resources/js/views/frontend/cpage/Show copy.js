import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import {Helmet} from "react-helmet";

class Show extends Component {

    constructor(props) {
        super(props);
        this.state = {
            row: '',
        };
        
    }

    componentDidMount(){
        this.fetchCPage(this.props.match.params.id);
    
    }

    componentDidUpdate(prevProps, prevStates) {
        if(this.props.match.params.id != prevProps.match.params.id) {
            this.fetchCPage(this.props.match.params.id);
        }
    }

    fetchCPage(id) {
        
        axios.get(this.props.defaultSets.apiUrl+'cpages/'+id)
            .then(response => {  
              this.setState({row: response.data});
                //console.log(response.data)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

    render() {
 
        const {row} = this.state;
        const lng = this.props.selectedLanguage;
        return (
    
            <Fragment>
<Helmet><title>{row && eval('row.title'+lng.langCode)}</title></Helmet>

 <BreadCrumb title={row && eval('row.title'+lng.langCode)} />

{row.id != 1 ? 
 <section className="about-page section-b-space mb-4">
      <div className="container">
      <div className="row">
          <div className="col-lg-5 col-sm-12">
            <div className="banner-section">
              
            <img style={{width: '1370px', height: '385px'}}
                    src={"/uploads/files/"+row.photo} 
                    alt={eval("row.title"+lng.langCode)} 
                    className="img-fluid blur-up lazyload" />
            </div>
          </div>
          <div className="col-sm-12 col-lg-7">
            <h4>{eval("row.title"+lng.langCode)}</h4>
            <p>
            {eval("row.description"+lng.langCode) && 
             
             <div dangerouslySetInnerHTML={ { __html: eval("row.description"+lng.langCode) } }></div>

            }
            </p>
          </div>
        </div>
      </div>
    </section> 
:

    <div>
    
    <section className="about-page section-b-space">
      <div className="container">
        <div className="row">
          <div className="col-lg-5 col-sm-12">
            <div className="banner-section">
              <img
                src="/assets/frontend/images/about/mwf-logo.png"
                className="img-fluid"
                alt=""
              />
            </div>
          </div>
          <div className="col-sm-12 col-lg-7">
            <h4>ABOUT MWF FACTORY</h4>
            <p>
              MWF Group Of Factories and Companies, specialized in the
              Manufacturing and Trading of Metal detectors, Treasures, Gold and
              groundwater, MWF is an American and Turkish company, headquartered
              in the United States in Illinois, and in Turkey in Istanbul,
              Founded in 2008. MWF Group works on the improvement and continuous
              development of Detectors equipment’s, Since years ago we
              manufacture these detectors on scientific grounds is true and
              accurate and we are keen to offer our products with very
              carefully. MWF Group always strives to provide the best level of
              technology of underground Metal, Gold detectors, whatever their
              depths and sizes, Through our knowledge of all types of
              exploration science and geology, and through our overall
              knowledge, research and experience over many years that were
              covered by the confidence of our customers, And the vast
              experience gained through our experiences.
            </p>
          </div>
        </div>
      </div>
    </section>

    

    <section className="testimonial small-section">
      <div className="container">
        <div className="row">
            <div className="testimonial-slider no-arrow">
              <div className="py-3">
                <div className="media">
                  <div className="text-center">
                    <img src="/assets/frontend/images/about/2019.jpg" alt="#" />
                    {/* 
                        <h5>
                          What
                          <br /> we have
                          <br /> achieved
                          <br /> this year
                        </h5> */}
                  </div>
                  <div className="media-body">
                    <p>
                      MWF Group has opened its new production and development
                      branch in the European continent in Germany to produce new
                      and advanced sets of systems and detectors, through German
                      engineers responsible for this production line and
                      development.
                    </p>
                  </div>
                </div>
              </div>
              <div className="d-inline-block py-3">
                <div className="media">
                  <div className="text-center">
                    <img src="/assets/frontend/images/about/2018.jpg" alt="#" />
                  </div>
                  <div className="media-body">
                    <p>
                      Our modern equipment’s Has achieved a wide spread an all
                      over the world and has made great achievements in most of
                      our exploration operations conducted by our customers
                      around the world, – Spark, has achieved great success with
                      our customers in locating gold treasure and has been
                      widely recognized for its impressive results in locating
                      gold nuggets in Africa, South Africa and Saudi Arabia, –
                      Aqua, the world’s No. 1 device, has been named the
                      preferred device of many farmers and prospectors for
                      groundwater, with great success in many countries. – Gold
                      Radar is one of our most popular devices, which has
                      achieved tangible results in finding many treasures hidden
                      in the Arab countries and Western & Eastern of Europe.
                    </p>
                  </div>
                </div>
              </div>
              <div className="d-inline-block py-3">
                <div className="media">
                  <div className="text-center">
                    <img src="/assets/frontend/images/about/2016.jpg" alt="#" />
                  </div>
                  <div className="media-body">
                    <p>
                      After four years of research, from 2012 to 2016, the
                      company produced new and advanced sets of metal and
                      groundwater detectors, which operate on intelligent
                      detection systems to locate targets in the ground. This
                      was a remarkable success for us, These devices have been
                      the result of extensive research and development that has
                      been introduced and conducted for many previous devices as
                      well as new technologies that have never been before. This
                      production and development included several devices: Spark
                      – Aqua – Gold Line – Gold Radar – MF-1200 Active – MF-1500
                      SMART – MF-9700 QUINRAY – WF-202 PRO+ .
                    </p>
                  </div>
                </div>
              </div>
              <div className="d-inline-block py-3">
                <div className="media">
                  <div className="text-center">
                    <img src="/assets/frontend/images/about/2014.jpg" alt="#" />
                  </div>
                  <div className="media-body">
                    <p>
                      This year 2014 the company celebrated to their access to
                      first place in the best and most accurate determinants of
                      places for gold and treasures and burials, which works on
                      the principle of Long Range Locators Systems and
                      geophysical survey systems.
                      <br />
                      - Where the latest device MF-1100 A global revolution in
                      obtaining No. 1 Long Range Treasures Locator Through More
                      than 1350 client certificate user of this detector around
                      the world , and has achieved great achievements and
                      discoveries impressive.
                      <br />
                      - MF-8000 PRO device also occupied the rank of the best
                      automatic geophysical survey device to locate burials
                      blanks, available to its great features and functions and
                      accurate in identifying places of targets, depths and the
                      presence of the target point.
                      <br />
                      - Also, as we have achieved through our groundwater
                      detectors since 2011 until 2014 has achieved the following
                      devices:
                      <br />
                      1- WF-101 B<br />
                      2- WF-202 PRO
                      <br />
                      3- WF-303 PLUS
                      <br />
                      4- Were drilled more than approximately 1000 water wells
                      in different countries over 3 years
                      <br />
                      5- Pakistan: 103 wells
                      <br />
                      6- Saudi Arabia: 97 wells
                      <br />
                      7- Algeria: 102 wells
                      <br />
                      8- China: 87 wells
                      <br />
                      9- India: 68 wells
                      <br />
                      10- Turkey: 88 wells
                      <br />
                      11- United Arab Emirates: 49 wells
                      <br />
                      12- Yemen: 46 wells
                      <br />
                      13- USA: 23 wells
                      <br />
                      14- Australia: 14 wells
                      <br />
                      15- Mexico: 39 wells
                      <br />
                      16- Sudan: 71 wells
                      <br />
                      17- Zimbabwe: 82 wells
                      <br />
                      18- South Africa: 54 wells
                      <br />
                      19- Afghanistan: 20 wells
                      <br />
                      20- Syria: 46 wells
                      <br />
                      21- Jordan: 11 wells
                      <br />
                      22- Oman Authority: 19 wells
                      <br />
                      And our achievements are still seen around the world,
                      thanks to the continuous development and intensive
                      research to reach the best and the newest, and we promise
                      to each new and evolving.
                      <br />
                      And with you for the better.
                    </p>
                  </div>
                </div>
              </div>
              <div className="d-inline-block py-3">
                <div className="media">
                  <div className="text-center">
                    <img src="/assets/frontend/images/about/2013.jpg" alt="#" />
                  </div>
                  <div className="media-body">
                    <p>
                      Polls by customers buyers for our devices, where we have
                      to take the opinions of more than 1,600 user devices so as
                      to know the results of our devices and its accuracy and
                      effectiveness, where the ratio was 87% of the customers
                      fully satisfied with the work of the devices and their
                      effectiveness and accuracy in determining the goals and
                      points and depths , And a percentage of the remaining 13%
                      had some suggestions and modifications to the devices to
                      maximize the accuracy of the results and the ease of use
                      and work for hours more work.
                      <br />
                      Where he was in this year 2013 also witnessed significant
                      developments of our products in terms of accessories,
                      software, and greater quality and performance and high
                      efficiency, the development has been on most devices.
                    </p>
                  </div>
                </div>
              </div>
              <div className="d-inline-block py-3">
                <div className="media">
                  <div className="text-center">
                    <img src="/assets/frontend/images/about/2011.jpg" alt="#" />
                  </div>
                  <div className="media-body">
                    <p>
                      The company has produced a new set of determinants and
                      detection devices to search for metal, water and caves ,
                      and specialized equipment detects gold and treasures.
                      <br />
                      Three sets of advanced electronic devices:
                      <br />
                      1- METAL FINDER: Equipment and detectors specializes in
                      the research and exploration for gold and minerals and
                      troves, these devices operate within international norms
                      and standards and specifications of the results amazing.
                      <br />
                      2- WATER FINDER: equipment and detectors specializes in
                      the research, detection and exploration of groundwater and
                      know the types of water and determine the salinity of the
                      water and the depths and intensity
                      <br />
                      3- CAVE FINDER: Equipment specializing to research and
                      detection of voids and tunnels, caves and hollows in the
                      ground and determine the levels, area and depths.
                    </p>
                  </div>
                </div>
              </div>
              <div className="d-inline-block py-3">
                <div className="media">
                  <div className="text-center">
                    <img src="/assets/frontend/images/about/2009.jpg" alt="#" />
                  </div>
                  <div className="media-body">
                    <p>
                      Our engineering team began work in rigorous studies and
                      extensive land and geology, and these studies were divided
                      into several sections.
                      <br />
                      1- Mineralogy: who studies natural minerals and chemical
                      properties, and that because of its significant impact on
                      the work of the organs and systems of the tracer materials
                      underground.
                      <br />
                      2- Petrology: who studies the types of rock and mineral
                      and chemical composition, as it contains rocks on a
                      variety of different combinations, as they also have a
                      negative impact on the work of some types of metal
                      detectors.
                      <br />
                      <b>Mineral composition:</b> Each rock special metal
                      installation. If possible, we distinguish determine the
                      proportion of each metal in the rock. Fabric: a
                      description of the rock granules and size. And the rock
                      fabric large-grained crystalline, or delicate crystalline
                      granules. Rarely have a glass fabric amorphous.
                      Infrastructure: Infrastructure reflect on the appearance
                      of a whole piece of the rock, and identify parts of a
                      relationship together. In the dense structure of the rock
                      granules be contiguous and so congenial to any particular
                      direction does not appear to agglutination granules. The
                      class structure indicate the presence of certain trends
                      for agglutination metals. If the rock is rich Palmsamat
                      structure window. Dominant color: It means the color
                      mostly on the rock until the color if you found some other
                      colors.
                      <br />
                      3- Geophysics : is the science that studies the
                      underground layers of different structures and geology.
                      <br />
                      4- Geology: is the science that studies various laws and
                      conditions that govern the formation of classes.
                      <br />
                      5- Palaeontology: specialist studying the remnants of the
                      old neighbourhoods or excavations.
                      <br />
                      6- Historical Geology: competent study classes and
                      arranging types since ancient times to the present day.
                      <br />
                      7- Geochemistry: specialist study minerals and rocks
                      chemically and distribution of elements in the earth’s
                      crust, and the types of iron and the proportion of mineral
                      ores in various areas of the cortex ground.
                    </p>
                  </div>
                </div>
              </div>
              <div className="d-inline-block py-3">
                <div className="media">
                  <div className="text-center">
                    <img src="/assets/frontend/images/about/2008.jpg" alt="#" />
                  </div>
                  <div className="media-body">
                    <p>
                      MWF Group Of Factories and companies, specialized in the
                      Manufacturing and Trading of Metal detectors, Treasures,
                      Gold and groundwater, MWF is an American and Turkish
                      company, headquartered in the United States in Illinois,
                      and in Turkey in Istanbul, Founded in 2008.
                      <br />
                      MWF Group works on the improvement and continuous
                      development of Detectors equipment’s, Since Years ago we
                      manufacture these detectors on scientific grounds is true
                      and accurate and we are keen to offer our products with
                      very carefully.
                    </p>
                  </div>
                </div>
              </div>
              <div className="d-inline-block py-3">
                <div className="media">
                  <div className="text-center">
                    <img src="/assets/frontend/images/about/2006.jpg" alt="" />
                  </div>
                  <div className="media-body">
                    <p>
                      We started as experts and professionals in the treasures
                      metal detectors and locators , since 2006, where we had
                      full knowledge in this area and experience expert to work
                      on all metal detectors for the amateur as well the
                      professional detection devices, and we had many
                      expeditions all over the world and we fought many
                      experiments practical exploration for archaeological
                      locations , and also work on the geological and
                      geographical scanning devices as well , except for
                      geological surveys for research and exploration of
                      groundwater
                      <br />
                      The experience in this field is very great experience
                      where already we have a large knowledge in the detection
                      systems, in terms of scientific and electronic principle
                      for each device, and we treat the former as experts and
                      professionals, with a lot of global companies for
                      specialized industry detectors , And we bought many of
                      these equipment and devices, and we end up also to develop
                      on the same devices to get the best results
                      <br />
                      At the end of 2007, our group and experts decided to open
                      MWF companies in the United States of America and in
                      Turkey as legally licensed trading companies, as well as a
                      laboratory and a scientific centre for the development and
                      manufacture of metal and groundwater detectors and
                      research equipment for gold and treasures.
                    </p>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
    </div>
    }
     </Fragment>

    )
    }
}





const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        //userInfo: state.User.info
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Show);