import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import BreadCrumb from '../partials/BreadCrumb';
import {Helmet} from "react-helmet";

class Show extends Component {

    constructor(props) {
        super(props);
        this.state = {
            row: '',
        };
        
    }

    componentDidMount(){
        this.fetchCPage(this.props.match.params.id);
    
    }

    componentDidUpdate(prevProps, prevStates) {
        if(this.props.match.params.id != prevProps.match.params.id) {
            this.fetchCPage(this.props.match.params.id);
        }
    }

    fetchCPage(id) {
        
        axios.get(this.props.defaultSets.apiUrl+'cpages/'+id)
            .then(response => {  
              this.setState({row: response.data});
                //console.log(response.data)
            })
            .catch(function (error) {
                console.log(error);
            });
      
    }

    render() {
 
        const {row} = this.state;
        const lng = this.props.selectedLanguage;
        let elmntTrns = row.elment_trans && row.elment_trans.find(page => page.languageCode == lng.langCode);

        return (
    
            <Fragment>
<Helmet><title>{elmntTrns && elmntTrns.title}</title></Helmet>

 <BreadCrumb title={elmntTrns && elmntTrns.title} />

 {elmntTrns && elmntTrns.description ? 
             
             <div dangerouslySetInnerHTML={ { __html: elmntTrns.description } }></div>
              : null
            } 
     </Fragment>

    )
    }
}





const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        //userInfo: state.User.info
        //selectedCurr: state.Curr.slctdCurr,
        defaultSets: state.DfltSts
    }
}
export default connect(mapStateToProps, null)(Show);