import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import BreadCrumb from "../partials/BreadCrumb";

//import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
//import { Carousel } from 'react-responsive-carousel';

import { Helmet } from "react-helmet";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";
import ProductBox from "../../../components/ProductBox";
import postData from "../../../helpers/postData";
import "react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from "react-image-gallery";

class Show extends Component {
    constructor(props) {
        super(props);
        this.state = {
            row: "",
            rowBfrFilter: "",
            qty: 1,
            similarProducts: [],
            accessoryId: 0,
            prpId: 0,
            prdctImgs: [],
            orjPrcBfrDscnt: 0,
            orjPrcAftrDscnt: 0,
            isDisabled: false,
            name: this.props.userInfo.full_name || "",
            phone: this.props.userInfo.phone || "",
            email: this.props.userInfo.email || "",
            msg: ""
        };

        this.hndlAddToCart = this.hndlAddToCart.bind(this);
        this.onChangeValue = this.onChangeValue.bind(this);
        this.hndlBuyNow = this.hndlBuyNow.bind(this);
        this.addToFav = this.addToFav.bind(this);
        this.sendQuestion = this.sendQuestion.bind(this);
    }

    componentDidMount() {
        this.fetchProduct(this.props.match.params.id);
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.match.params.id != this.props.match.params.id) {
            this.fetchProduct(this.props.match.params.id);
        }

        if (
            this.props.selectedLanguage.langCode !=
            prevProps.selectedLanguage.langCode
        ) {
            let item = this.state.row;
            let elmntTrns =
                item.elment_trans &&
                item.elment_trans.find(
                    prdct =>
                        prdct.languageCode ==
                        this.props.selectedLanguage.langCode
                );

            let prdctImgs = [];
            item.isAccessory == 1
                ? elmntTrns &&
                  elmntTrns.files.map(
                      (file, i) =>
                          i == 0 &&
                          prdctImgs.push({
                              original: "/uploads/files/" + file.fileName,
                              thumbnail: "/uploads/files/" + file.fileName
                          })
                  )
                : elmntTrns &&
                  elmntTrns.files.map(
                      (file, i) =>
                          i > 2 &&
                          prdctImgs.push({
                              original: "/uploads/files/" + file.fileName,
                              thumbnail: "/uploads/files/" + file.fileName
                          })
                  );

            this.setState({ prdctImgs });
        }
    }

    componentWillUnmount() {
        //document.getElementById('mishu').innerText = '';
    }

    /* fetchSimilarProducts() {

        axios.get(this.props.defaultSets.apiUrl+'last-products')
            .then(response => {
              this.setState({similarProducts: response.data});

            })
            .catch(function (error) {
                console.log(error);
            });

    } */

    fetchProduct(id) {
        axios
            .get(this.props.defaultSets.apiUrl + "products/" + id)
            .then(response => {
                let item = response.data.row;
                let elmntTrns =
                    item.elment_trans &&
                    item.elment_trans.find(
                        prdct =>
                            prdct.languageCode ==
                            this.props.selectedLanguage.langCode
                    );

                let prdctImgs = [];
                item.isAccessory == 1
                    ? elmntTrns &&
                      elmntTrns.files.map(
                          (file, i) =>
                              i == 0 &&
                              prdctImgs.push({
                                  original: "/uploads/files/" + file.fileName,
                                  thumbnail: "/uploads/files/" + file.fileName
                              })
                      )
                    : elmntTrns &&
                      elmntTrns.files.map((file, i) =>
                          i > 2 || i == 1
                              ? prdctImgs.push({
                                    original: "/uploads/files/" + file.fileName,
                                    thumbnail: "/uploads/files/" + file.fileName
                                })
                              : null
                      );

                this.setState({
                    row: item,
                    orjPrcAftrDscnt: response.data.row.priceAfterDiscount,
                    orjPrcBfrDscnt: response.data.row.priceBeforeDiscount,
                    similarProducts: response.data.similarProducts,
                    prdctImgs
                });
                //console.log(this.state.row)
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    hndlAddToCart(e) {
        e.preventDefault();

        let prdct_prp = this.state.row.product_props.find(
            item => item.id == this.state.prpId
        );

        let newRow = this.state.row;
        newRow.chosenPrp = prdct_prp ? prdct_prp : null;

        this.props.addToCartFrmDtls(newRow, this.state.qty);

        return toast.success(this.props.selectedLanguage.addedToCartTxt);
    }

    /* hndlAddToCart(e){
        e.preventDefault();

        this.props.addToCart(this.state.row, this.state.qty);

        return toast.success(this.props.selectedLanguage.addedToCartTxt);
    } */

    hndlBuyNow(e) {
        e.preventDefault();

        this.props.addToCartFrmDtls(this.state.row, this.state.qty);

        return this.props.history.push("/cart");
    }

    onChangeValue(e) {
        this.setState(
            {
                [e.target.name]: e.target.value,
                isDisabled: false
            },
            () => this.getAccessoryInfo()
        );
    }

    getAccessoryInfo() {
        //let rowBfrFilter = this.state.rowBfrFilter;
        //console.log(this.state.row.product_accessories, ' ',this.state.accessoryId)

        let orjRow = this.state.row;
        let elmntTrns =
            orjRow.elment_trans &&
            orjRow.elment_trans.find(
                prdct =>
                    prdct.languageCode == this.props.selectedLanguage.langCode
            );
        let prdctImgs = [];

        if (this.state.accessoryId != "0") {
            // then a user choosed a product that is assigned to this accessory.

            // firsly we get the chosen prdct key to show the proper photo as the accessory's photos filled in the same order of products from admin panel.
            let accessoryIndex = orjRow.product_accessories.findIndex(
                item => item.productId == this.state.accessoryId
            );

            // secondly get the accessory info like prices according to the chosen product.
            let accessoryInfo = orjRow.product_accessories.find(
                item =>
                    item.accessoryId == orjRow.id &&
                    item.productId == this.state.accessoryId
            );

            if (accessoryInfo != undefined) {
                // set the new prices according to the chosen prdct.
                orjRow.priceBeforeDiscount = accessoryInfo.priceBeforeDiscount;
                orjRow.priceAfterDiscount = accessoryInfo.priceAfterDiscount;

                // set the new photo according to the chosen prdct.
                prdctImgs.push({
                    // don't forget that the order of accessory photos must comply with the order of products
                    original:
                        elmntTrns && elmntTrns.files[accessoryIndex + 2]
                            ? "/uploads/files/" +
                              elmntTrns.files[accessoryIndex + 2].fileName
                            : "",
                    thumbnail:
                        elmntTrns && elmntTrns.files[accessoryIndex + 2]
                            ? "/uploads/files/" +
                              elmntTrns.files[accessoryIndex + 2].fileName
                            : ""
                });
            }
        } else {
            // then the user wants the accessory only not linked with another product, so return to the original prices and photos.
            orjRow.priceBeforeDiscount = this.state.orjPrcBfrDscnt;
            orjRow.priceAfterDiscount = this.state.orjPrcAftrDscnt;

            prdctImgs.push({
                original:
                    elmntTrns && elmntTrns.files[0]
                        ? "/uploads/files/" + elmntTrns.files[0].fileName
                        : "", // Unlike products, there is only one photo for the accessory, so i set index to 0 with loops.
                thumbnail:
                    elmntTrns && elmntTrns.files[0]
                        ? "/uploads/files/" + elmntTrns.files[0].fileName
                        : ""
            });
        }
        this.setState({ row: orjRow, prdctImgs });
    }
    /* getAccessoryInfo(){ // not used
//console.log(this.state.accessoryId)
      let newRow = this.state.rowBfrFilter;
      let accessory = newRow.product_accessories.find(item => item.productId == newRow.id && item.accessoryId == this.state.accessoryId);

      if(accessory && this.state.accessoryId != '0'){
        newRow.photo = accessory.photo;
        newRow.priceBeforeDiscount = accessory.priceBeforeDiscount;
        newRow.priceAfterDiscount = accessory.priceAfterDiscount;
        this.setState({row: newRow})
      }

      //console.log(accessory)
    } */

    addToFav(item) {
        if (this.props.userInfo.id == 0) {
            return toast.info(this.props.selectedLanguage.plsLgnFrst);
        }
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(
            this.props.defaultSets.apiUrl + "favourites",
            {
                userId: this.props.userInfo.id,
                itemId: item.id,
                langCode: this.props.selectedLanguage.langCode
            },
            this.props.userInfo.accessToken
        )
            .then(response => {
                if (response.data.success == true) {
                    toast.update(ldToast, {
                        type: toast.TYPE.SUCCESS,
                        render: response.data.msg
                    });
                    item.favId = response.data.favId;

                    return this.props.addToFavourites(item);
                }
                return toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
            })
            .catch(error => {});
    }

    sendQuestion(e) {
        e.preventDefault();
        this.setState({ isDisabled: true });
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl + "contact", {
            name: this.state.name,
            phone: this.state.phone,
            email: this.state.email,
            msg: this.state.msg,
            langCode: this.props.selectedLanguage.langCode
        })
            .then(response => {
                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
            })
            .catch(error => {
                //console.log(error.response);
                this.setState({ isDisabled: false });

                if (
                    error.response !== undefined &&
                    error.response.status === 422
                ) {
                    let errorTxt = "";
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (
                            <span>
                                {" "}
                                {errorTxt} {errorMessage[key][0]} <br />
                            </span>
                        );
                    });
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });
    }

    render() {
        //const elmntTrns = this.state.rowTrans;

        const { row, similarProducts, prdctImgs, prpId } = this.state;

        const lng = this.props.selectedLanguage;
        const { slctdCurr, userInfo } = this.props;

        const elmntTrns =
            row.elment_trans &&
            row.elment_trans.find(prdct => prdct.languageCode == lng.langCode);

        const sts = this.props.defaultSets;

        return (
            <Fragment>
                <Helmet>
                    <title>{elmntTrns && elmntTrns.title}</title>
                    <meta
                        name="keywords"
                        content={elmntTrns && elmntTrns.metaTags}
                    />
                    <meta
                        name="description"
                        content={elmntTrns && elmntTrns.metaTags}
                    />
                </Helmet>

                <BreadCrumb title={elmntTrns && elmntTrns.title} />

                {!row.isAccessory &&
                elmntTrns &&
                elmntTrns.files[2] &&
                elmntTrns.files[2]["fileName"] != null ? (
                    <div className="breadcrumb-image">
                        <img
                            src={
                                "/uploads/files/" +
                                elmntTrns.files[2]["fileName"]
                            }
                            alt={elmntTrns && elmntTrns.title}
                            style={{ width: "100%", height: "300px" }}
                        />
                    </div>
                ) : null}

                <section className="section-b-space">
                    <div className="collection-wrapper">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-9 col-sm-12 col-xs-12">
                                    <div className="container-fluid">
                                        <div className="row">
                                            <div className="col-xl-12">
                                                <div className="filter-main-btn mb-2">
                                                    <span className="filter-btn">
                                                        <i
                                                            className="fa fa-filter"
                                                            aria-hidden="true"
                                                        ></i>
                                                        filter
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-5">
                                                <ImageGallery
                                                    showNav={false}
                                                    lazyLoad={true}
                                                    showPlayButton={false}
                                                    showFullscreenButton={false}
                                                    items={prdctImgs}
                                                    autoPlay={true}
                                                />

                                                {/* {elmntTrns && elmntTrns.files.map((file, i) => i > 2 &&
                                        (
                                        <div key={i}>
                                        <img
                                          src={"/uploads/files/"+file.fileName}
                                          alt={elmntTrns && elmntTrns.title}
                                          className="img-fluid blur-up lazyload image_zoom_cls-3"
                                        />
                                      </div> )


        )} */}
                                            </div>
                                            <div className="col-lg-6 rtl-text">
                                                <div className="product-right">
                                                    <h2>
                                                        {elmntTrns &&
                                                            elmntTrns.title}
                                                    </h2>

                                                    {row.priceAfterDiscount ==
                                                        row.priceBeforeDiscount ||
                                                    row.priceAfterDiscount ==
                                                        0 ? (
                                                        <h3>
                                                            {(
                                                                row.priceBeforeDiscount /
                                                                slctdCurr.exchangeRate
                                                            ).toFixed(2)}{" "}
                                                            {
                                                                slctdCurr.currencyCode
                                                            }
                                                        </h3>
                                                    ) : (
                                                        <div>
                                                            <h4>
                                                                <del>
                                                                    {(
                                                                        row.priceBeforeDiscount /
                                                                        slctdCurr.exchangeRate
                                                                    ).toFixed(
                                                                        2
                                                                    )}
                                                                </del>
                                                                <span></span>
                                                            </h4>
                                                            <h3>
                                                                {(
                                                                    row.priceAfterDiscount /
                                                                    slctdCurr.exchangeRate
                                                                ).toFixed(
                                                                    2
                                                                )}{" "}
                                                                {
                                                                    slctdCurr.currencyCode
                                                                }
                                                            </h3>
                                                        </div>
                                                    )}

                                                    <div className="border-product">
                                                        <h6 className="product-title d-none">
                                                            {
                                                                lng.productDetailsTxt
                                                            }
                                                        </h6>

                                                        {elmntTrns &&
                                                            elmntTrns.description && (
                                                                <div
                                                                    dangerouslySetInnerHTML={{
                                                                        __html:
                                                                            elmntTrns.description
                                                                    }}
                                                                ></div>
                                                            )}
                                                    </div>

                                                    {row.isAccessory == true ? (
                                                        <div className="single-product-tables border-product detail-section">
                                                            <table>
                                                                <tbody>
                                                                    {row.product_props &&
                                                                    row
                                                                        .product_props
                                                                        .length >
                                                                        0 ? (
                                                                        <tr>
                                                                            <td>
                                                                                {
                                                                                    lng.colourTxt
                                                                                }

                                                                                :
                                                                            </td>
                                                                            <td>
                                                                                <select
                                                                                    className="form-control"
                                                                                    onChange={
                                                                                        this
                                                                                            .onChangeValue
                                                                                    }
                                                                                    issearchable="true"
                                                                                    name="prpId"
                                                                                    defaultValue={
                                                                                        prpId
                                                                                    }
                                                                                >
                                                                                    <option value="0">
                                                                                        {
                                                                                            lng.allTxt
                                                                                        }
                                                                                    </option>
                                                                                    {row.product_props.map(
                                                                                        (
                                                                                            prdctPrp,
                                                                                            i
                                                                                        ) => {
                                                                                            return (
                                                                                                <option
                                                                                                    value={
                                                                                                        prdctPrp.id
                                                                                                    }
                                                                                                    key={
                                                                                                        "prdctPrp" +
                                                                                                        prdctPrp.id
                                                                                                    }
                                                                                                >
                                                                                                    {prdctPrp.titleAR &&
                                                                                                        prdctPrp.titleAR}
                                                                                                </option>
                                                                                            );
                                                                                        }
                                                                                    )}
                                                                                </select>
                                                                            </td>
                                                                        </tr>
                                                                    ) : null}
                                                                    {row.accessories &&
                                                                    row
                                                                        .accessories
                                                                        .length >
                                                                        0 ? (
                                                                        <tr>
                                                                            <td>
                                                                                {
                                                                                    lng.productsTxt
                                                                                }

                                                                                :
                                                                            </td>
                                                                            <td>
                                                                                <select
                                                                                    className="form-control"
                                                                                    onChange={
                                                                                        this
                                                                                            .onChangeValue
                                                                                    }
                                                                                    issearchable="true"
                                                                                    name="accessoryId"
                                                                                >
                                                                                    <option value="0">
                                                                                        {
                                                                                            lng.chooseTxt
                                                                                        }
                                                                                    </option>
                                                                                    {row.accessories.map(
                                                                                        (
                                                                                            accessory,
                                                                                            i
                                                                                        ) => {
                                                                                            let accessoryTrns =
                                                                                                accessory.elment_trans &&
                                                                                                accessory.elment_trans.find(
                                                                                                    prdct =>
                                                                                                        prdct.languageCode ==
                                                                                                        this
                                                                                                            .props
                                                                                                            .selectedLanguage
                                                                                                            .langCode
                                                                                                );

                                                                                            return (
                                                                                                <option
                                                                                                    value={
                                                                                                        accessory.id
                                                                                                    }
                                                                                                    key={
                                                                                                        "accessory" +
                                                                                                        i
                                                                                                    }
                                                                                                >
                                                                                                    {accessoryTrns &&
                                                                                                        accessoryTrns.title}
                                                                                                </option>
                                                                                            );
                                                                                        }
                                                                                    )}
                                                                                </select>
                                                                            </td>
                                                                        </tr>
                                                                    ) : null}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    ) : null}
                                                    <div className="product-description border-product">
                                                        <h6 className="product-title">
                                                            {lng.qtyTxt}
                                                        </h6>
                                                        <div className="qty-box">
                                                            <div className="input-group">
                                                                <span className="input-group-prepend">
                                                                    <button
                                                                        type="button"
                                                                        className="btn quantity-left-minus"
                                                                        data-type="minus"
                                                                        data-field=""
                                                                        onClick={() => {
                                                                            if (
                                                                                this
                                                                                    .state
                                                                                    .qty >
                                                                                1
                                                                            ) {
                                                                                this.setState(
                                                                                    {
                                                                                        qty:
                                                                                            this
                                                                                                .state
                                                                                                .qty -
                                                                                            1
                                                                                    }
                                                                                );
                                                                            }
                                                                        }}
                                                                    >
                                                                        <i className="ti-angle-left"></i>
                                                                    </button>
                                                                </span>
                                                                <input
                                                                    type="text"
                                                                    name="quantity"
                                                                    className="form-control input-number"
                                                                    value={
                                                                        this
                                                                            .state
                                                                            .qty
                                                                    }
                                                                    onChange={e => {
                                                                        if (
                                                                            e
                                                                                .target
                                                                                .value >
                                                                            0
                                                                        ) {
                                                                            this.setState(
                                                                                {
                                                                                    qty:
                                                                                        e
                                                                                            .target
                                                                                            .value
                                                                                }
                                                                            );
                                                                        }
                                                                    }}
                                                                />
                                                                <span className="input-group-prepend">
                                                                    <button
                                                                        type="button"
                                                                        className="btn quantity-right-plus"
                                                                        data-type="plus"
                                                                        data-field=""
                                                                        onClick={() =>
                                                                            this.setState(
                                                                                {
                                                                                    qty:
                                                                                        this
                                                                                            .state
                                                                                            .qty +
                                                                                        1
                                                                                }
                                                                            )
                                                                        }
                                                                    >
                                                                        <i className="ti-angle-right"></i>
                                                                    </button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="product-buttons">
                                                        <a
                                                            href="#"
                                                            data-toggle="modal"
                                                            data-target="#addtocart"
                                                            className="btn btn-solid"
                                                            onClick={
                                                                this
                                                                    .hndlAddToCart
                                                            }
                                                        >
                                                            {lng.addToCartTxt}
                                                        </a>
                                                        <a
                                                            href="#"
                                                            className="btn btn-solid"
                                                            onClick={
                                                                this.hndlBuyNow
                                                            }
                                                        >
                                                            {lng.buyNowTxt}
                                                        </a>
                                                    </div>

                                                    <div className="border-product">
                                                        <h6 className="product-title">
                                                            {lng.shareTxt}
                                                        </h6>
                                                        <div className="product-icon">
                                                            <ul className="product-social">
                                                                <li>
                                                                    <a
                                                                        href={
                                                                            "https://www.facebook.com/sharer/sharer.php?u=" +
                                                                            window
                                                                                .location
                                                                                .href
                                                                        }
                                                                        target="_blank"
                                                                    >
                                                                        <i className="fa fa-facebook"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a
                                                                        href={
                                                                            "https://plus.google.com/share?url=" +
                                                                            window
                                                                                .location
                                                                                .href
                                                                        }
                                                                        target="_blank"
                                                                    >
                                                                        <i className="fa fa-google-plus"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a
                                                                        href={
                                                                            "https://twitter.com/intent/tweet?url=" +
                                                                            window
                                                                                .location
                                                                                .href
                                                                        }
                                                                        target="_blank"
                                                                    >
                                                                        <i className="fa fa-twitter"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a
                                                                        href={
                                                                            sts.instagram
                                                                        }
                                                                        target="_blank"
                                                                    >
                                                                        <i className="fa fa-instagram"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a
                                                                        href={
                                                                            sts.whatsapp
                                                                        }
                                                                        target="_blank"
                                                                    >
                                                                        <i className="fa fa-whatsapp"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <form className="d-inline-block">
                                                                <button
                                                                    className="wishlist-btn"
                                                                    onClick={
                                                                        this
                                                                            .addToFav
                                                                    }
                                                                >
                                                                    <i className="fa fa-heart"></i>
                                                                    <span className="title-font">
                                                                        {
                                                                            lng.addToFavTxt
                                                                        }
                                                                    </span>
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <section className="tab-product m-0">
                                        <div className="row">
                                            <div className="col-sm-12 col-lg-12">
                                                <ul
                                                    className="nav nav-tabs nav-material"
                                                    id="top-tab"
                                                    role="tablist"
                                                >
                                                    {row.tabs &&
                                                        row.tabs.map(
                                                            (tab, i) => {
                                                                let tabTrns =
                                                                    tab.elment_trans &&
                                                                    tab.elment_trans.find(
                                                                        row =>
                                                                            row.languageCode ==
                                                                            lng.langCode
                                                                    );

                                                                return (
                                                                    <li
                                                                        key={
                                                                            "tabTitle" +
                                                                            tab.id
                                                                        }
                                                                        className="nav-item"
                                                                    >
                                                                        <a
                                                                            className={
                                                                                i ==
                                                                                0
                                                                                    ? "nav-link active"
                                                                                    : "nav-link"
                                                                            }
                                                                            id={
                                                                                "khTab" +
                                                                                i
                                                                            }
                                                                            data-toggle="tab"
                                                                            href={
                                                                                "#kh" +
                                                                                i
                                                                            }
                                                                            role="tab"
                                                                            aria-controls={
                                                                                "kh" +
                                                                                i
                                                                            }
                                                                            aria-selected={
                                                                                i ==
                                                                                0
                                                                                    ? "true"
                                                                                    : "false"
                                                                            }
                                                                        >
                                                                            {tab.photo && (
                                                                                <img
                                                                                    src={
                                                                                        "/uploads/files/" +
                                                                                        tab.photo
                                                                                    }
                                                                                    alt={
                                                                                        tabTrns &&
                                                                                        tabTrns.title
                                                                                    }
                                                                                    className=""
                                                                                />
                                                                            )}
                                                                            <h6>
                                                                                {tabTrns &&
                                                                                    tabTrns.title}
                                                                            </h6>
                                                                        </a>
                                                                    </li>
                                                                );
                                                            }
                                                        )}

                                                    <li className="nav-item">
                                                        <a
                                                            className="nav-link"
                                                            id={
                                                                "contactFormTab"
                                                            }
                                                            data-toggle="tab"
                                                            href={
                                                                "#contactForm"
                                                            }
                                                            role="tab"
                                                            aria-controls={
                                                                "contactForm"
                                                            }
                                                            aria-selected={
                                                                false
                                                            }
                                                        >
                                                            <img
                                                                src="/uploads/files/contact-product.png"
                                                                alt=""
                                                                className=""
                                                            />
                                                            <h6>
                                                                {lng.contactTxt}
                                                            </h6>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div
                                                    className="tab-content nav-material"
                                                    id="top-tabContent"
                                                >
                                                    {row.tabs &&
                                                        row.tabs.map(
                                                            (tab, i) => {
                                                                let tabTrns =
                                                                    tab.elment_trans &&
                                                                    tab.elment_trans.find(
                                                                        row =>
                                                                            row.languageCode ==
                                                                            lng.langCode
                                                                    );

                                                                return (
                                                                    <div
                                                                        key={
                                                                            "tabContent" +
                                                                            tab.id
                                                                        }
                                                                        className={
                                                                            i ==
                                                                            0
                                                                                ? "tab-pane fade show active"
                                                                                : "tab-pane fade"
                                                                        }
                                                                        id={
                                                                            "kh" +
                                                                            i
                                                                        }
                                                                        role="tabpanel"
                                                                        aria-labelledby={
                                                                            "khTab" +
                                                                            i
                                                                        }
                                                                    >
                                                                        <div
                                                                            dangerouslySetInnerHTML={{
                                                                                __html:
                                                                                    tabTrns &&
                                                                                    tabTrns.description
                                                                            }}
                                                                        />
                                                                    </div>
                                                                );
                                                            }
                                                        )}

                                                    <div
                                                        className="tab-pane fade"
                                                        id={"contactForm"}
                                                        role="tabpanel"
                                                        aria-labelledby="contactFormTab"
                                                    >
                                                        <form
                                                            className="theme-form mt-4"
                                                            onSubmit={
                                                                this
                                                                    .sendQuestion
                                                            }
                                                        >
                                                            <div className="form-row">
                                                                <div className="col-md-6">
                                                                    <label htmlFor="name">
                                                                        {
                                                                            lng.nameTxt
                                                                        }
                                                                    </label>

                                                                    <input
                                                                        type="text"
                                                                        className="form-control"
                                                                        name="name"
                                                                        defaultValue={
                                                                            userInfo.full_name
                                                                        }
                                                                        onChange={
                                                                            this
                                                                                .onChangeValue
                                                                        }
                                                                    />
                                                                </div>
                                                                <div className="col-md-6">
                                                                    <label htmlFor="review">
                                                                        {
                                                                            lng.phoneTxt
                                                                        }
                                                                    </label>
                                                                    <input
                                                                        type="text"
                                                                        className="form-control"
                                                                        name="phone"
                                                                        defaultValue={
                                                                            userInfo.phone
                                                                        }
                                                                        onChange={
                                                                            this
                                                                                .onChangeValue
                                                                        }
                                                                    />
                                                                </div>

                                                                <div className="col-md-6">
                                                                    <label htmlFor="email">
                                                                        {
                                                                            lng.titleTxt
                                                                        }
                                                                    </label>

                                                                    <input
                                                                        type="text"
                                                                        className="form-control"
                                                                        name="title"
                                                                        value={
                                                                            elmntTrns
                                                                                ? elmntTrns.title
                                                                                : lng.questionTxt
                                                                        }
                                                                        onChange={
                                                                            this
                                                                                .onChangeValue
                                                                        }
                                                                    />
                                                                </div>

                                                                <div className="col-md-6">
                                                                    <label htmlFor="email">
                                                                        {
                                                                            lng.emailTxt
                                                                        }
                                                                    </label>
                                                                    <input
                                                                        type="text"
                                                                        className="form-control"
                                                                        defaultValue={
                                                                            userInfo.email
                                                                        }
                                                                        onChange={
                                                                            this
                                                                                .onChangeValue
                                                                        }
                                                                    />
                                                                </div>
                                                                <div className="col-md-12">
                                                                    <label htmlFor="review">
                                                                        {
                                                                            lng.questionTxt
                                                                        }
                                                                    </label>
                                                                    <textarea
                                                                        className="form-control"
                                                                        name="msg"
                                                                        rows="6"
                                                                        placeholder={
                                                                            lng.msgTxt
                                                                        }
                                                                        onChange={
                                                                            this
                                                                                .onChangeValue
                                                                        }
                                                                    ></textarea>
                                                                </div>
                                                                <div className="col-md-12">
                                                                    <button
                                                                        className="btn btn-solid"
                                                                        disabled={
                                                                            this
                                                                                .state
                                                                                .isDisabled
                                                                        }
                                                                        type="submit"
                                                                    >
                                                                        {
                                                                            lng.sendTxt
                                                                        }
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div className="col-sm-3 collection-filter">
                                    <div className="collection-filter-block">
                                        <div className="collection-mobile-back">
                                            <span className="filter-back">
                                                <i
                                                    className="fa fa-angle-left"
                                                    aria-hidden="true"
                                                ></i>
                                            </span>
                                        </div>
                                        <div className="collection-collapse-block border-0 open">
                                            <h3 className="collapse-block-title">
                                                {lng.categoriesTxt}
                                            </h3>
                                            <div className="collection-collapse-block-content">
                                                <div className="collection-brand-filter">
                                                    <ul className="category-list">
                                                        {this.props.cats &&
                                                            this.props.cats.map(
                                                                (item, i) => {
                                                                    let elmntTrns =
                                                                        item.elment_trans &&
                                                                        item.elment_trans.find(
                                                                            prdct =>
                                                                                prdct.languageCode ==
                                                                                this
                                                                                    .props
                                                                                    .selectedLanguage
                                                                                    .langCode
                                                                        );

                                                                    return (
                                                                        <li
                                                                            key={
                                                                                "cat" +
                                                                                i
                                                                            }
                                                                        >
                                                                            <Link
                                                                                to={
                                                                                    "/categories/" +
                                                                                    item.id
                                                                                }
                                                                            >
                                                                                {elmntTrns &&
                                                                                    elmntTrns.title}
                                                                            </Link>
                                                                        </li>
                                                                    );
                                                                }
                                                            )}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="collection-filter-block">
                                        <div className="product-service">
                                            <div className="media">
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 -117 679.99892 679"
                                                >
                                                    <path
                                                        d="m12.347656 378.382812h37.390625c4.371094 37.714844 36.316407 66.164063 74.277344 66.164063 37.96875 0 69.90625-28.449219 74.28125-66.164063h241.789063c4.382812 37.714844 36.316406 66.164063 74.277343 66.164063 37.96875 0 69.902344-28.449219 74.285157-66.164063h78.890624c6.882813 0 12.460938-5.578124 12.460938-12.460937v-352.957031c0-6.882813-5.578125-12.464844-12.460938-12.464844h-432.476562c-6.875 0-12.457031 5.582031-12.457031 12.464844v69.914062h-105.570313c-4.074218.011719-7.890625 2.007813-10.21875 5.363282l-68.171875 97.582031-26.667969 37.390625-9.722656 13.835937c-1.457031 2.082031-2.2421872 4.558594-2.24999975 7.101563v121.398437c-.09765625 3.34375 1.15624975 6.589844 3.47656275 9.003907 2.320312 2.417968 5.519531 3.796874 8.867187 3.828124zm111.417969 37.386719c-27.527344 0-49.851563-22.320312-49.851563-49.847656 0-27.535156 22.324219-49.855469 49.851563-49.855469 27.535156 0 49.855469 22.320313 49.855469 49.855469 0 27.632813-22.21875 50.132813-49.855469 50.472656zm390.347656 0c-27.53125 0-49.855469-22.320312-49.855469-49.847656 0-27.535156 22.324219-49.855469 49.855469-49.855469 27.539063 0 49.855469 22.320313 49.855469 49.855469.003906 27.632813-22.21875 50.132813-49.855469 50.472656zm140.710938-390.34375v223.34375h-338.375c-6.882813 0-12.464844 5.578125-12.464844 12.460938 0 6.882812 5.582031 12.464843 12.464844 12.464843h338.375v79.761719h-66.421875c-4.382813-37.710937-36.320313-66.15625-74.289063-66.15625-37.960937 0-69.898437 28.445313-74.277343 66.15625h-192.308594v-271.324219h89.980468c6.882813 0 12.464844-5.582031 12.464844-12.464843 0-6.882813-5.582031-12.464844-12.464844-12.464844h-89.980468v-31.777344zm-531.304688 82.382813h99.703125v245.648437h-24.925781c-4.375-37.710937-36.3125-66.15625-74.28125-66.15625-37.960937 0-69.90625 28.445313-74.277344 66.15625h-24.929687v-105.316406l3.738281-5.359375h152.054687c6.882813 0 12.460938-5.574219 12.460938-12.457031v-92.226563c0-6.882812-5.578125-12.464844-12.460938-12.464844h-69.796874zm-30.160156 43h74.777344v67.296875h-122.265625zm0 0"
                                                        fill="#ff4c3b"
                                                    ></path>
                                                </svg>
                                                <div className="media-body">
                                                    <h4>
                                                        {lng.freeShippingTxt}
                                                    </h4>
                                                    <p>{lng.freeShippingdes}</p>
                                                </div>
                                            </div>
                                            <div className="media">
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    xmlnsXlink="http://www.w3.org/1999/xlink"
                                                    version="1.1"
                                                    id="Capa_1"
                                                    x="0px"
                                                    y="0px"
                                                    viewBox="0 0 480 480"
                                                    style={{
                                                        enableBackground:
                                                            "new 0 0 480 480"
                                                    }}
                                                    xmlSpace="preserve"
                                                    width="512px"
                                                    height="512px"
                                                >
                                                    <g>
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="M472,432h-24V280c-0.003-4.418-3.588-7.997-8.006-7.994c-2.607,0.002-5.05,1.274-6.546,3.41l-112,160     c-2.532,3.621-1.649,8.609,1.972,11.14c1.343,0.939,2.941,1.443,4.58,1.444h104v24c0,4.418,3.582,8,8,8s8-3.582,8-8v-24h24     c4.418,0,8-3.582,8-8S476.418,432,472,432z M432,432h-88.64L432,305.376V432z"
                                                                    fill="#ff4c3b"
                                                                />
                                                                <path
                                                                    d="M328,464h-94.712l88.056-103.688c0.2-0.238,0.387-0.486,0.56-0.744c16.566-24.518,11.048-57.713-12.56-75.552     c-28.705-20.625-68.695-14.074-89.319,14.631C212.204,309.532,207.998,322.597,208,336c0,4.418,3.582,8,8,8s8-3.582,8-8     c-0.003-26.51,21.486-48.002,47.995-48.005c10.048-0.001,19.843,3.151,28.005,9.013c16.537,12.671,20.388,36.007,8.8,53.32     l-98.896,116.496c-2.859,3.369-2.445,8.417,0.924,11.276c1.445,1.226,3.277,1.899,5.172,1.9h112c4.418,0,8-3.582,8-8     S332.418,464,328,464z"
                                                                    fill="#ff4c3b"
                                                                />
                                                                <path
                                                                    d="M216.176,424.152c0.167-4.415-3.278-8.129-7.693-8.296c-0.001,0-0.002,0-0.003,0     C104.11,411.982,20.341,328.363,16.28,224H48c4.418,0,8-3.582,8-8s-3.582-8-8-8H16.28C20.283,103.821,103.82,20.287,208,16.288     V40c0,4.418,3.582,8,8,8s8-3.582,8-8V16.288c102.754,3.974,185.686,85.34,191.616,188l-31.2-31.2     c-3.178-3.07-8.242-2.982-11.312,0.196c-2.994,3.1-2.994,8.015,0,11.116l44.656,44.656c0.841,1.018,1.925,1.807,3.152,2.296     c0.313,0.094,0.631,0.172,0.952,0.232c0.549,0.198,1.117,0.335,1.696,0.408c0.08,0,0.152,0,0.232,0c0.08,0,0.152,0,0.224,0     c0.609-0.046,1.211-0.164,1.792-0.352c0.329-0.04,0.655-0.101,0.976-0.184c1.083-0.385,2.069-1.002,2.888-1.808l45.264-45.248     c3.069-3.178,2.982-8.242-0.196-11.312c-3.1-2.994-8.015-2.994-11.116,0l-31.976,31.952     C425.933,90.37,331.38,0.281,216.568,0.112C216.368,0.104,216.2,0,216,0s-0.368,0.104-0.568,0.112     C96.582,0.275,0.275,96.582,0.112,215.432C0.112,215.632,0,215.8,0,216s0.104,0.368,0.112,0.568     c0.199,115.917,91.939,210.97,207.776,215.28h0.296C212.483,431.847,216.013,428.448,216.176,424.152z"
                                                                    fill="#ff4c3b"
                                                                />
                                                                <path
                                                                    d="M323.48,108.52c-3.124-3.123-8.188-3.123-11.312,0L226.2,194.48c-6.495-2.896-13.914-2.896-20.408,0l-40.704-40.704     c-3.178-3.069-8.243-2.981-11.312,0.197c-2.994,3.1-2.994,8.015,0,11.115l40.624,40.624c-5.704,11.94-0.648,26.244,11.293,31.947     c9.165,4.378,20.095,2.501,27.275-4.683c7.219-7.158,9.078-18.118,4.624-27.256l85.888-85.888     C326.603,116.708,326.603,111.644,323.48,108.52z M221.658,221.654c-0.001,0.001-0.001,0.001-0.002,0.002     c-3.164,3.025-8.148,3.025-11.312,0c-3.125-3.124-3.125-8.189-0.002-11.314c3.124-3.125,8.189-3.125,11.314-0.002     C224.781,213.464,224.781,218.53,221.658,221.654z"
                                                                    fill="#ff4c3b"
                                                                />
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <div className="media-body">
                                                    <h4>{lng.service724}</h4>
                                                    <p>{lng.service724des}</p>
                                                </div>
                                            </div>
                                            <div className="media">
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    viewBox="0 -14 512.00001 512"
                                                >
                                                    <path
                                                        d="m136.964844 308.234375c4.78125-2.757813 6.417968-8.878906 3.660156-13.660156-2.761719-4.777344-8.878906-6.417969-13.660156-3.660157-4.78125 2.761719-6.421875 8.882813-3.660156 13.660157 2.757812 4.78125 8.878906 6.421875 13.660156 3.660156zm0 0"
                                                        fill="#ff4c3b"
                                                    />
                                                    <path
                                                        d="m95.984375 377.253906 50.359375 87.230469c10.867188 18.84375 35.3125 25.820313 54.644531 14.644531 19.128907-11.054687 25.703125-35.496094 14.636719-54.640625l-30-51.96875 25.980469-15c4.78125-2.765625 6.421875-8.878906 3.660156-13.660156l-13.003906-22.523437c1.550781-.300782 11.746093-2.300782 191.539062-37.570313 22.226563-1.207031 35.542969-25.515625 24.316407-44.949219l-33.234376-57.5625 21.238282-32.167968c2.085937-3.164063 2.210937-7.230469.316406-10.511719l-20-34.640625c-1.894531-3.28125-5.492188-5.203125-9.261719-4.980469l-38.472656 2.308594-36.894531-63.90625c-5.34375-9.257813-14.917969-14.863281-25.605469-14.996094-.128906-.003906-.253906-.003906-.382813-.003906-10.328124 0-19.703124 5.140625-25.257812 13.832031l-130.632812 166.414062-84.925782 49.03125c-33.402344 19.277344-44.972656 62.128907-25.621094 95.621094 17.679688 30.625 54.953126 42.671875 86.601563 30zm102.324219 57.238282c5.523437 9.554687 2.253906 21.78125-7.328125 27.316406-9.613281 5.558594-21.855469 2.144531-27.316407-7.320313l-50-86.613281 34.640626-20c57.867187 100.242188 49.074218 85.011719 50.003906 86.617188zm-22.683594-79.296876-10-17.320312 17.320312-10 10 17.320312zm196.582031-235.910156 13.820313 23.9375-12.324219 18.664063-23.820313-41.261719zm-104.917969-72.132812c2.683594-4.390625 6.941407-4.84375 8.667969-4.796875 1.707031.019531 5.960938.550781 8.527344 4.996093l116.3125 201.464844c3.789063 6.558594-.816406 14.804688-8.414063 14.992188-1.363281.03125-1.992187.277344-5.484374.929687l-123.035157-213.105469c2.582031-3.320312 2.914063-3.640624 3.425781-4.480468zm-16.734374 21.433594 115.597656 200.222656-174.460938 34.21875-53.046875-91.878906zm-223.851563 268.667968c-4.390625-7.597656-6.710937-16.222656-6.710937-24.949218 0-17.835938 9.585937-34.445313 25.011718-43.351563l77.941406-45 50 86.601563-77.941406 45.003906c-23.878906 13.78125-54.515625 5.570312-68.300781-18.304688zm0 0"
                                                        fill="#ff4c3b"
                                                    />
                                                    <path
                                                        d="m105.984375 314.574219c-2.761719-4.78125-8.878906-6.421875-13.660156-3.660157l-17.320313 10c-4.773437 2.757813-10.902344 1.113282-13.660156-3.660156-2.761719-4.78125-8.878906-6.421875-13.660156-3.660156s-6.421875 8.878906-3.660156 13.660156c8.230468 14.257813 26.589843 19.285156 40.980468 10.980469l17.320313-10c4.78125-2.761719 6.421875-8.875 3.660156-13.660156zm0 0"
                                                        fill="#ff4c3b"
                                                    />
                                                    <path
                                                        d="m497.136719 43.746094-55.722657 31.007812c-4.824218 2.6875-6.5625 8.777344-3.875 13.601563 2.679688 4.820312 8.765626 6.566406 13.601563 3.875l55.71875-31.007813c4.828125-2.6875 6.5625-8.777344 3.875-13.601562-2.683594-4.828125-8.773437-6.5625-13.597656-3.875zm0 0"
                                                        fill="#ff4c3b"
                                                    />
                                                    <path
                                                        d="m491.292969 147.316406-38.636719-10.351562c-5.335938-1.429688-10.820312 1.734375-12.25 7.070312-1.429688 5.335938 1.738281 10.816406 7.074219 12.246094l38.640625 10.351562c5.367187 1.441407 10.824218-1.773437 12.246094-7.070312 1.429687-5.335938-1.738282-10.820312-7.074219-12.246094zm0 0"
                                                        fill="#ff4c3b"
                                                    />
                                                    <path
                                                        d="m394.199219 7.414062-10.363281 38.640626c-1.429688 5.335937 1.734374 10.816406 7.070312 12.25 5.332031 1.425781 10.816406-1.730469 12.25-7.070313l10.359375-38.640625c1.429687-5.335938-1.734375-10.820312-7.070313-12.25-5.332031-1.429688-10.816406 1.734375-12.246093 7.070312zm0 0"
                                                        fill="#ff4c3b"
                                                    />
                                                </svg>
                                                <div className="media-body">
                                                    <h4>{lng.festival}</h4>
                                                    <p>{lng.festivaldes}</p>
                                                </div>
                                            </div>
                                            <div className="media border-0 m-0">
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    xmlnsXlink="http://www.w3.org/1999/xlink"
                                                    version="1.1"
                                                    id="Layer_1"
                                                    x="0px"
                                                    y="0px"
                                                    viewBox="0 0 512 512"
                                                    style={{
                                                        enableBackground:
                                                            "new 0 0 512 512"
                                                    }}
                                                    xmlSpace="preserve"
                                                    width="512px"
                                                    height="512px"
                                                >
                                                    <g>
                                                        <g>
                                                            <g>
                                                                <path
                                                                    d="M498.409,175.706L336.283,13.582c-8.752-8.751-20.423-13.571-32.865-13.571c-12.441,0-24.113,4.818-32.865,13.569     L13.571,270.563C4.82,279.315,0,290.985,0,303.427c0,12.442,4.82,24.114,13.571,32.864l19.992,19.992     c0.002,0.001,0.003,0.003,0.005,0.005c0.002,0.002,0.004,0.004,0.006,0.006l134.36,134.36H149.33     c-5.89,0-10.666,4.775-10.666,10.666c0,5.89,4.776,10.666,10.666,10.666h59.189c0.014,0,0.027,0.001,0.041,0.001     s0.027-0.001,0.041-0.001l154.053,0.002c5.89,0,10.666-4.776,10.666-10.666c0-5.891-4.776-10.666-10.666-10.666l-113.464-0.002     L498.41,241.434C516.53,223.312,516.53,193.826,498.409,175.706z M483.325,226.35L226.341,483.334     c-4.713,4.712-11.013,7.31-17.742,7.32h-0.081c-6.727-0.011-13.025-2.608-17.736-7.32L56.195,348.746L302.99,101.949     c4.165-4.165,4.165-10.919,0-15.084c-4.166-4.165-10.918-4.165-15.085,0.001L41.11,333.663l-12.456-12.456     c-4.721-4.721-7.321-11.035-7.321-17.779c0-6.744,2.6-13.059,7.322-17.781L285.637,28.665c4.722-4.721,11.037-7.321,17.781-7.321     c6.744,0,13.059,2.6,17.781,7.322l57.703,57.702l-246.798,246.8c-4.165,4.164-4.165,10.918,0,15.085     c2.083,2.082,4.813,3.123,7.542,3.123c2.729,0,5.459-1.042,7.542-3.124l246.798-246.799l89.339,89.336     C493.128,200.593,493.127,216.546,483.325,226.35z"
                                                                    fill="#ff4c3b"
                                                                />
                                                                <path
                                                                    d="M262.801,308.064c-4.165-4.165-10.917-4.164-15.085,0l-83.934,83.933c-4.165,4.165-4.165,10.918,0,15.085     c2.083,2.083,4.813,3.124,7.542,3.124c2.729,0,5.459-1.042,7.542-3.124l83.934-83.933     C266.966,318.982,266.966,312.229,262.801,308.064z"
                                                                    fill="#ff4c3b"
                                                                />
                                                                <path
                                                                    d="M228.375,387.741l-34.425,34.425c-4.165,4.165-4.165,10.919,0,15.085c2.083,2.082,4.813,3.124,7.542,3.124     c2.731,0,5.459-1.042,7.542-3.124l34.425-34.425c4.165-4.165,4.165-10.919,0-15.085     C239.294,383.575,232.543,383.575,228.375,387.741z"
                                                                    fill="#ff4c3b"
                                                                />
                                                                <path
                                                                    d="M260.054,356.065l-4.525,4.524c-4.166,4.165-4.166,10.918-0.001,15.085c2.082,2.083,4.813,3.125,7.542,3.125     c2.729,0,5.459-1.042,7.541-3.125l4.525-4.524c4.166-4.165,4.166-10.918,0.001-15.084     C270.974,351.901,264.219,351.9,260.054,356.065z"
                                                                    fill="#ff4c3b"
                                                                />
                                                                <path
                                                                    d="M407.073,163.793c-2-2-4.713-3.124-7.542-3.124c-2.829,0-5.541,1.124-7.542,3.124l-45.255,45.254     c-2,2.001-3.124,4.713-3.124,7.542s1.124,5.542,3.124,7.542l30.17,30.167c2.083,2.083,4.813,3.124,7.542,3.124     c2.731,0,5.459-1.042,7.542-3.124l45.253-45.252c4.165-4.165,4.165-10.919,0-15.084L407.073,163.793z M384.445,231.673     l-15.085-15.084l30.17-30.169l15.084,15.085L384.445,231.673z"
                                                                    fill="#ff4c3b"
                                                                />
                                                                <path
                                                                    d="M320.339,80.186c2.731,0,5.461-1.042,7.543-3.126l4.525-4.527c4.164-4.166,4.163-10.92-0.003-15.084     c-4.165-4.164-10.92-4.163-15.084,0.003l-4.525,4.527c-4.164,4.166-4.163,10.92,0.003,15.084     C314.881,79.146,317.609,80.186,320.339,80.186z"
                                                                    fill="#ff4c3b"
                                                                />
                                                                <path
                                                                    d="M107.215,358.057l-4.525,4.525c-4.165,4.164-4.165,10.918,0,15.085c2.083,2.082,4.813,3.123,7.542,3.123     s5.459-1.041,7.542-3.123l4.525-4.525c4.165-4.166,4.165-10.92,0-15.085C118.133,353.891,111.381,353.891,107.215,358.057z"
                                                                    fill="#ff4c3b"
                                                                />
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                                <div className="media-body">
                                                    <h4>{lng.onlinePayment}</h4>
                                                    <p>
                                                        {lng.onlinePaymentTxt}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                {similarProducts.length > 0 ? (
                    <section className="section-b-space ratio_asos">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 product-related">
                                    <h2>{lng.prpsdPrdcts}</h2>
                                </div>
                            </div>
                            <div className="row search-product">
                                {similarProducts.map((similarProduct, i) => {
                                    return (
                                        <div
                                            key={"sp" + i}
                                            className="col-xl-2 col-md-4 col-sm-6"
                                        >
                                            <ProductBox
                                                obj={similarProduct}
                                                selectedLanguage={
                                                    this.props.selectedLanguage
                                                }
                                                slctdCurr={this.props.slctdCurr}
                                                favedItems={
                                                    this.props.favedItems
                                                }
                                                key={"similarPrdctBox" + i}
                                                addToCart={this.props.addToCart}
                                                addToFav={this.addToFav}
                                            />
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </section>
                ) : null}
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addToCart: (product, msg) => {
            toast.success(msg);
            dispatch({ type: "ADD_TO_CART", payload: product });
        },

        addToCartFrmDtls: (product, qty) =>
            dispatch({ type: "ADD_TO_CART", payload: product, qty: qty }),

        addToFavourites: item => {
            dispatch({ type: "ADD_TO_FAV", payload: item });
        }
    };
};

const mapStateToProps = state => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        //cartItems: state.cartItems.cart,
        userInfo: state.User.info,
        favedItems: state.Favourites.rows,
        slctdCurr: state.Currs.currencyInfo,
        cats: state.Cats.rows,
        defaultSets: state.DfltSts
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Show);
