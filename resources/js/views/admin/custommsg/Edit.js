import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import ImgToBeUploaded from '../../../components/ImageToUpload';
import RowThumbnail from '../../../components/RowThumbnail';

import { Link } from 'react-router-dom';
import {Helmet} from "react-helmet";
import putData from '../../../helpers/putData';
import { checkMimeType } from '../../../helpers/checkMimeType';
import getData from '../../../helpers/getData';

 class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            id:0,
            titleAR: '',
            //titleTR: '',
            titleEN: '',
            contentAR: '',
            //descriptionTR: '',
            contentEN: '',
            //status: 1,
            errorMessage: '',
            isDisabled: false,

            // here i load original images of an element that comes from db.
            photo: '',
            //.
            
            // here the new selected images will be loaded.
            mainImg: null,            
            //.
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchData = this.fetchData.bind(this);
        //this.hndlCntnParams = this.hndlCntnParams.bind(this);
        
        //the next functions handle images on selected, change and upload.
        this.onMainImageChange = this.onMainImageChange.bind(this);
        this.rmvSlctdMnImg = this.rmvSlctdMnImg.bind(this);        
        this.hndlUpldMnImg = this.hndlUpldMnImg.bind(this);        
        this.rmvMnImg = this.rmvMnImg.bind(this);      
        //.
    }
  
     
    hndlCntnParams(paramName, lang) {

      if(lang == "AR") {
        let contentAR = this.state.contentAR+' '+paramName;
        this.setState({contentAR});
      } else {
        let contentEN = this.state.contentEN+' '+paramName;
        this.setState({contentEN});
      }
    }
    componentDidMount(){

      this.fetchData(this.props.match.params.id);    
      //console.log('khaled', this.state.titleAR)                
 }

 fetchData(elmntId) {
  getData(this.props.defaultSets.apiUrl+'custommsgs/'+elmntId+'/edit', this.props.userInfo.accessToken)
       //.then(response => response.json())
       .then(response => {

         this.setState({ 
             id: elmntId,
             titleAR: response.data.titleAR,
             //titleTR: response.data.titleTR,
             titleEN: response.data.titleEN,
             contentAR: response.data.contentAR || '',
             //descriptionTR: response.data.descriptionTR || '',
             contentEN: response.data.contentEN || '',
             
             //status: response.data.status,
             photo: response.data.photo,
             
           });
           //console.log(response.data)
       })
       .catch(function (error) {
           console.log(error);
       });
 }



 onMainImageChange(event) {
  if (event.target.files && event.target.files[0]) {

    let img = event.target.files[0];

    let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
    if(mimeTypeCheck == true){

      return this.setState({
        mainImg: img 
      }); 
    } 
    toast.error(mimeTypeCheck);
      
  }
};
    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
    
    hndlUpldMnImg(rowId) {
      if (this.state.mainImg == null ) {
        return false;
      }

      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', rowId);
      formData.append('type', 'custommsg');
      formData.append('mainPhoto', '1');
      //.

      
      // append the main photo to formData.
      formData.append(
        'files[]',
        this.state.mainImg,
        this.state.mainImg.name
      )
      //.

      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({
          mainImg: null,
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }

 rmvSlctdMnImg() { // handle selected photo not main photo that comes from db.
  this.setState({
    mainImg: null
  })
}

rmvMnImg() { //handle main photo that comes from db not a selected one.
  this.setState({
    photo: null
  });
}

    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }
    
    handleSubmit(e){
//console.log('k'+this.state.status)
        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

          putData(this.props.defaultSets.apiUrl+'custommsgs/'+this.state.id, {
            id: this.state.id,
            titleAR: this.state.titleAR,
            //titleTR: this.state.titleTR,
            titleEN: this.state.titleEN,
            contentAR: this.state.contentAR,
            contentEN: this.state.contentEN,
            //descriptionTR: this.state.descriptionTR,
            
            //status: this.state.status,
            photo: this.state.mainImg || this.state.photo,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {
              this.hndlUpldMnImg(this.state.id);

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

               /*  setTimeout( () => {
                    this.props.history.push('/custommsgs');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
        
        const {isDisabled,titleAR, titleEN, /* titleTR, */ 
          contentAR, contentEN
          , /* descriptionTR, */ photo, 
        mainImg } = this.state;

        const { titleTxt, custommsgsTxt, updtCstmMsgTxt, mainPhotoTxt, 
          saveTxt, descriptionTxt, orderIdTxt, customerNameTxt
         } = this.props.selectedLanguage;

        return (
                   
          
          <div className="row">
            <Helmet><title>{updtCstmMsgTxt}</title></Helmet>

          <div className="col-md-12">
                  
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb">
                <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
                <li className="breadcrumb-item"><Link to="/admin/custommsgs">{custommsgsTxt}</Link></li>
                <li className="breadcrumb-item active" aria-current="page">{updtCstmMsgTxt}</li>
              </ol>
            </nav>
          </div>
          
          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{updtCstmMsgTxt}</h4>
                <p className="card-category"></p>
              </div>
              <div className="card-body">
                <form>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{titleTxt} AR</label>                          
                        <input 
                        className="form-control"
                        value={titleAR}
                        type="text"
                        name="titleAR"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    {/* <div className="col-md-4">
                      <div className="form-group">
                        <label className="bmd-label-floating">{titleTxt} TR</label>                            
                        <input 
                        value={titleTR}
                        className="form-control"
                        type="text"
                        name="titleTR"
                        onChange={this.onChangeValue} />
                      </div>
                    </div> */}

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{titleTxt} EN</label>                            
                        <input 
                        value={titleEN}
                        className="form-control"
                        type="text"
                        name="titleEN"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                  </div>
               
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                        <button 
                        type="button"
                        className="btn btn-primary"
                        onClick={() => this.hndlCntnParams('customer_name', 'AR')}>
                        {customerNameTxt}
                        </button>
                        
                        <button 
                        type="button"
                        className="btn btn-warning"
                        onClick={() => this.hndlCntnParams('orderId', 'AR')}>
                        {orderIdTxt}
                        </button>

                          <label className="bmd-label-floating">
                            {descriptionTxt} AR
                            
                          </label>
                          
                          <textarea 
                          name="contentAR" 
                          className="form-control"
                          value={contentAR || ''}
                          onChange={this.onChangeValue}>
                          </textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  {/* <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{descriptionTxt} TR</label>
                          <CKEditor
                          editor={ ClassicEditor }
                          data={descriptionTR}
                          onChange={ ( event, editor ) => {
                              const data = editor.getData();
                              this.setState({descriptionTR: data})
                              //console.log( { event, editor, data } );
                          }}
                          />
                        </div>
                      </div>
                    </div>
                  </div> */}
                  
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                      <button 
                        type="button"
                        className="btn btn-primary"
                        onClick={() => this.hndlCntnParams('customer_name', 'EN')}>
                        {customerNameTxt}
                        </button>
                        
                        <button 
                        type="button"
                        className="btn btn-warning"
                        onClick={() => this.hndlCntnParams('orderId', 'EN')}>
                        {orderIdTxt}
                        </button>

                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{descriptionTxt} EN</label>
                          
                          <textarea 
                          name="contentEN" 
                          className="form-control"
                          value={contentEN || ''}
                          onChange={this.onChangeValue}>
                          </textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  disabled={isDisabled}
                  className="btn btn-primary pull-right">{saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
          

              
        <div className="col-md-4">
         

         <div className="col-md-12">
            
         <div className="card card-profile">
               <div className="card-avatar">.</div>
               <div className="card-body">
                 <h6 className="card-category text-gray"></h6>
                 { <h5 className="card-title mb-3">{mainPhotoTxt}</h5>}
                 
                 <div className="card-description">
                   <RowThumbnail imgName={photo} rmvMnImg={this.rmvMnImg} />
                    
                   <ImgToBeUploaded 
                   image={mainImg} 
                   rmvSlctdMnImg={this.rmvSlctdMnImg} />
                   
                   <input type="file" 
                   accept=".gif,.jpg,.jpeg,.png"
                   onChange={this.onMainImageChange} />
                 </div>
                 
                 {/* <button 
                 onClick={this.handleUpload}
                 className="btn btn-primary btn-round">
                   Upload
                 </button> */}
               </div>
             </div>
          
         </div>
       
      
       </div>


        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Edit);
