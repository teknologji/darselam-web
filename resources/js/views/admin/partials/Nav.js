import React, { Component} from 'react'

import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { ar } from '../../../lang/ar';
import { en } from '../../../lang/en';

 class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isNotificationOpened: false,
            isUserOpened: false
        };
        this.hndlLogout = this.hndlLogout.bind(this);
    }
 
    hndlLogout(e){
      
      e.preventDefault(); 
      this.props.logout();
      this.props.history.push("/");
    }
    render() {
        
        const lng = this.props.selectedLanguage;
        const {isNotificationOpened, isUserOpened} = this.state;

        return (
            
  
            <nav className="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div className="container-fluid">
              <div className="navbar-wrapper">
                
                <Link to="/admin" className="navbar-brand">{lng.cpanelTxt}</Link>
              </div>
              <button className="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span className="sr-only">Toggle navigation</span>
                <span className="navbar-toggler-icon icon-bar"></span>
                <span className="navbar-toggler-icon icon-bar"></span>
                <span className="navbar-toggler-icon icon-bar"></span>
              </button>
              <div className="collapse navbar-collapse justify-content-end">
                <form className="navbar-form">
                  <div className="input-group no-border">
                   
                  </div>
                </form>
                <ul className="navbar-nav">
                        
              
                  <li className="nav-item">
                    <a className="nav-link" onClick={()=> this.props.selectLanguage(ar)}>
                    <img src={'/assets/custom-img/ye-24.png'} alt="Arabic" />
                      <p className="d-lg-none d-md-block">
                        
                      </p>
                    </a>
                  </li>
        
                  <li className="nav-item">
                    <a className="nav-link" onClick={()=> this.props.selectLanguage(en)}>
                    <img src={'/assets/custom-img/uk-24.png'} alt="English" />
                      <p className="d-lg-none d-md-block">
                        
                      </p>
                    </a>
                  </li>
        
        <li className="nav-item dropdown"
                    onMouseOver={() => {this.setState({isUserOpened: true})}}
                    onMouseLeave={() => this.setState({isUserOpened: false})}>
                    <a className="nav-link" href="#" 
                    onClick={() => this.props.logout()}
                    id="navbarDropdownProfile" 
                    data-toggle="dropdown" 
                    aria-haspopup="true" aria-expanded="false">
                      <i className="fa fa-user"></i>
                      <p className="d-lg-none d-md-block">
                        Account
                      </p>
                    </a>
                    <div className={isUserOpened == true ? "dropdown-menu dropdown-menu-right show": "dropdown-menu dropdown-menu-right hiding"} aria-labelledby="navbarDropdownProfile">
                      {/* <a className="dropdown-item" href="#">Profile</a> */}
                      <Link to="/admin/settings" className="dropdown-item">{lng.settingsTxt}</Link>
                      <div className="dropdown-divider"></div>
                      <a className="dropdown-item" href="#" onClick={this.hndlLogout}>{lng.logoutTxt}</a>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        

        )
    }
}

const mapDispatchToProps = (dispatch) => {
  return {
      selectLanguage: (val) => 
      {
        
        document.getElementsByTagName("html")[0].setAttribute("dir", val.langDir);
        document.getElementsByTagName("body")[0].setAttribute("class", val.langDir);
        dispatch({ type: 'language_data', payload: val })
      }
      ,
      //changeCurrency: (curr) => dispatch({ type: 'change_currency', payload: curr }),
  }
};

export default connect(null, mapDispatchToProps)(Nav);
