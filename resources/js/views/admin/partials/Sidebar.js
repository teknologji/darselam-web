import React, { Component} from 'react'

import { Link } from 'react-router-dom';

 class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };
    }
 
    render() { 
        
        const {userInfo}= this.props;

        const lng = this.props.selectedLanguage;

        return (
            
                           
    <div className="sidebar" data-color="purple" data-background-color="white" 
    data-image="/assets/admin/img/sidebar-1.jpg">
      
      
      <div className="logo"><Link to="/" className="simple-text logo-normal">
          {lng.derasaTxt}
        </Link></div>
      <div className="sidebar-wrapper">
        <ul className="nav">
          <li className="nav-item active  ">
            <Link to="/admin" className="nav-link">                                
              <i className="fa fa-laptop"></i>
              <p className="text-white">{lng.homeTxt}</p>                            
            </Link>            
          </li>

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllOrders")) || (userInfo.super == 1) ?
          <li className="nav-item ">
            <Link to="/admin/orders" className="nav-link">                                
              <i className="fa fa-shopping-cart"></i>
              <p>{lng.ordersTxt}</p>                            
            </Link>
          </li>
          : null}

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllUsers")) || (userInfo.super == 1) ?
          <li className="nav-item ">
            <Link to="/admin/users" className="nav-link">                                
              <i className="fa fa-user"></i>
              <p>{lng.usersTxt}</p>                            
            </Link>
          </li>
          : null}

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllProducts")) || (userInfo.super == 1) ?
          <li className="nav-item ">
            <Link to="/admin/products" className="nav-link">                                
              <i className="fa fa-window-restore"></i>
              <p>{lng.productsTxt}</p>                            
            </Link>
          </li>
          : null}

                {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllTabs")) || (userInfo.super == 1) ?
                  <li className="nav-item ">
                    <Link to="/admin/tabs" className="nav-link">
                      <i className="fa fa-window-restore"></i>
                      <p>{lng.tabsTxt}</p>
                    </Link>
                  </li>
                  : null}

                {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllGuarantees")) || (userInfo.super == 1) ?
                  <li className="nav-item ">
                    <Link to="/admin/guarantees" className="nav-link">
                      <i className="fa fa-window-restore"></i>
                      <p>{lng.guaranteesTxt}</p>
                    </Link>
                  </li>
                  : null}

                  {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllProductRegister")) || (userInfo.super == 1) ?
                  <li className="nav-item ">
                    <Link to="/admin/register-product" className="nav-link">
                      <i className="fa fa-window-restore"></i>
                      <p>{lng.rgstrdPrdctsTxt}</p>
                    </Link>
                  </li>
                  : null}

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllCategories")) || (userInfo.super == 1) ?
          <li className="nav-item ">
            <Link to="/admin/categories" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{lng.categoriesTxt}</p>                            
            </Link>
          </li>
          : null}


          {/* {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllSubcats")) || (userInfo.super == 1) ?
          
          <li className="nav-item ">
            <Link to="/admin/subcats" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{lng.subcatsTxt}</p>                            
            </Link>
          </li>
          : null} */}

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllRoles")) || (userInfo.super == 1) ?
          <li className="nav-item ">
            <Link to="/admin/roles" className="nav-link">                                
              <i className="fa fa-cogs"></i>
              <p>{lng.rolesTxt}</p>                            
            </Link>
          </li>
          : null}


          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllColours")) || (userInfo.super == 1) ?
          

          <li className="nav-item ">
            <Link to="/admin/colours" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{lng.coloursTxt}</p>                            
            </Link>
          </li>
          : null}

{(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllFeatures")) || (userInfo.super == 1) ?
          

          <li className="nav-item ">
            <Link to="/admin/features" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{lng.featuresTxt}</p>                            
            </Link>
          </li>
          : null}

          {/* {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllSizes")) || (userInfo.super == 1) ?

          <li className="nav-item ">
            <Link to="/admin/sizes" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{lng.sizesTxt}</p>                            
            </Link>
          </li>
          : null} */}

          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllCoupons")) || (userInfo.super == 1) ?

          <li className="nav-item ">
            <Link to="/admin/coupons" className="nav-link">                                
              <i className="fa fa-file"></i>
              <p>{lng.couponsTxt}</p>                            
            </Link>
          </li>
          : null}

          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllArticles")) || (userInfo.super == 1) ?

          <li className="nav-item ">
            <Link to="/admin/articles" className="nav-link">                                
              <i className="fa fa-file"></i>
              <p>{lng.articlesTxt}</p>                            
            </Link>
          </li>
          : null}
          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllServices")) || (userInfo.super == 1) ?
          

          <li className="nav-item ">
            <Link to="/admin/services" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{lng.servicesTxt}</p>                            
            </Link>
          </li>
          : null}
          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllCpages")) || (userInfo.super == 1) ?

          <li className="nav-item ">
            <Link to="/admin/cpages" className="nav-link">                                
              <i className="fa fa-file"></i>
              <p>{lng.cpagesTxt}</p>                            
            </Link>
          </li>
          : null}
          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllFaqs")) || (userInfo.super == 1) ?

          <li className="nav-item ">
          <Link to="/admin/faqs" className="nav-link">                                
              <i className="fa fa-envelope"></i>
              <p>{lng.faqsTxt}</p>                            
            </Link>
          </li>
          : null}

          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllCustommsgs")) || (userInfo.super == 1) ?

          <li className="nav-item ">
            <Link to="/admin/custommsgs" className="nav-link">                                
              <i className="fa fa-user"></i>
              <p>{lng.custommsgsTxt}</p>                            
            </Link>
          </li>
          : null}

          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllCargos")) || (userInfo.super == 1) ?
          
          <li className="nav-item ">
            <Link to="/admin/cargos" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{lng.cargosTxt}</p>                            
            </Link>
          </li>
          : null}


{(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllCurrencies")) || (userInfo.super == 1) ?
          
          <li className="nav-item ">
            <Link to="/admin/currencies" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{lng.currenciesTxt}</p>                            
            </Link>
          </li>
          : null}

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllLanguages")) || (userInfo.super == 1) ?
          
          <li className="nav-item ">
            <Link to="/admin/languages" className="nav-link">                                
              <i className="fa fa-book"></i>
              <p>{lng.languagesTxt}</p>                            
            </Link>
          </li>
          : null}

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("EditSettings")) || (userInfo.super == 1) ?

          <li className="nav-item ">
          <Link to="/admin/settings" className="nav-link">                                
              <i className="fa fa-cog"></i>
              <p>{lng.settingsTxt}</p>                            
            </Link>
          </li>
          : null}
          
          
          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("Newsletters")) || (userInfo.super == 1) ?

          <li className="nav-item ">
          <Link to="/admin/newsletters" className="nav-link">                                
              <i className="fa fa-envelope"></i>
              <p>{lng.newsletterTxt}</p>                            
            </Link>
          </li>
          : null}

          {(userInfo.userPermissions.length > 0 && userInfo.userPermissions.includes("AllContacts")) || (userInfo.super == 1) ?

          <li className="nav-item ">
          <Link to="/admin/contact" className="nav-link">                                
              <i className="fa fa-envelope"></i>
              <p>{lng.inboxTxt}</p>                            
            </Link>
          </li>
          : null}
        </ul>
      </div>
    
    
    </div>


        )
    }
}



export default Sidebar;
