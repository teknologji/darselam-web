import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import ImgToBeUploaded from '../../../components/ImageToUpload';
import RowThumbnail from '../../../components/RowThumbnail';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';

import DatePicker from "react-datepicker";
import Moment from 'moment';
import "react-datepicker/dist/react-datepicker.css";
import getData from '../../../helpers/getData';
import { checkMimeType } from '../../../helpers/checkMimeType';

 class Edit extends Component {
    constructor(props) { 
        super(props);
        this.state = {
            id:0,             
            couponCode: '',
            couponValue: '',
            originalQuantity: '',  
            type: 1,  
            validFrom: '',  
            validTo: '',  
            minimumValueToBeApplied: '',  
            status: 1,
            errorMessage: '',
            isDisabled: false,

            // here i load original images of an element that comes from db.
            photo: '',
            //.
            
            // here the new selected images will be loaded.
            mainImg: null,            
            //.
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchData = this.fetchData.bind(this);

        //the next functions handle images on selected, change and upload.
        this.onMainImageChange = this.onMainImageChange.bind(this);
        this.rmvSlctdMnImg = this.rmvSlctdMnImg.bind(this);        
        this.hndlUpldMnImg = this.hndlUpldMnImg.bind(this);        
        this.rmvMnImg = this.rmvMnImg.bind(this);      
        //.
    }
  
     
  
    componentDidMount(){

      this.fetchData(this.props.match.params.id);    
      //console.log('khaled', this.state.titleAR)                
 }

 fetchData(elmntId) {

  getData(this.props.defaultSets.apiUrl+'coupons/'+elmntId+'/edit', this.props.userInfo.accessToken)

       //.then(response => response.json())
       .then(response => {

         this.setState({ 
             id: elmntId,
             couponValue: response.data.couponValue,
             couponCode: response.data.couponCode,
             minimumValueToBeApplied: response.data.minimumValueToBeApplied,
             validFrom: response.data.validFrom,
             validTo: response.data.validTo,
             originalQuantity: response.data.originalQuantity,
             type: response.data.type,
             status: response.data.status,
             photo: response.data.photo,
             
           });
           //console.log(response.data)
       })
       .catch(function (error) {
           console.log(error);
       });
 }



 onMainImageChange(event) {
  if (event.target.files && event.target.files[0]) {

    let img = event.target.files[0];

    let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
    if(mimeTypeCheck == true){

      return this.setState({
        mainImg: img 
      }); 
    } 
    toast.error(mimeTypeCheck);
      
  }
};
    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
    
    hndlUpldMnImg(rowId) {
      if (this.state.mainImg == null ) {
        return false;
      }

      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', rowId);
      formData.append('type', 'coupon');
      formData.append('mainPhoto', '1');
      //.

      
      // append the main photo to formData.
      formData.append(
        'files[]',
        this.state.mainImg,
        this.state.mainImg.name
      )
      //.

      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({
          mainImg: null,
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }

 rmvSlctdMnImg() { // handle selected photo not main photo that comes from db.
  this.setState({
    mainImg: null
  })
}

rmvMnImg() { //handle main photo that comes from db not a selected one.
  this.setState({
    photo: null
  });
}

    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }
    
    handleSubmit(e){
//console.log('k'+this.state.status)
        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

          putData(this.props.defaultSets.apiUrl+'coupons/'+this.state.id, {
            id: this.state.id,
            couponCode: this.state.couponCode,
            couponValue: this.state.couponValue,
            minimumValueToBeApplied: this.state.minimumValueToBeApplied,            
            validFrom: Moment(this.state.validFrom).format("YYYY-MM-DD"),
            validTo: Moment(this.state.validTo).format("YYYY-MM-DD"),
            type: this.state.type,
            originalQuantity: this.state.originalQuantity,
            status: this.state.status,
            photo: this.state.mainImg || this.state.photo,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {
              this.hndlUpldMnImg(this.state.id);

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

               /*  setTimeout( () => {
                    this.props.history.push('/coupons');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }
    hndlValidFrom(validFrom) {
      this.setState({
        validFrom
      });
    };
    
    hndlValidTo(validTo) {
      this.setState({
        validTo
      });
    };

    render() {
        
        
        const {errorMessage, isDisabled, /*titleTR,*/ couponValue, minimumValueToBeApplied
          , couponCode, type, validFrom, validTo, originalQuantity, photo, status, 
        mainImg } = this.state;

        const { updtCouponTxt,  couponCodeTxt, couponValueTxt, typeTxt, crtNewTxt, couponsTxt, percentageTxt,
          minimumValueToBeAppliedTxt, fixedTxt, activeTxt, inActiveTxt,
          mainPhotoTxt, saveTxt, qtyTxt, validFromTxt, validToTxt, statusTxt
         } = this.props.selectedLanguage;

        return (
                   

          <div className="row">
            
<Helmet><title>{updtCouponTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/coupons">{couponsTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{updtCouponTxt}</li>
          </ol>
        </nav>
      </div>


         
      <div className="col-md-8">
              <div className="card">
                <div className="card-header card-header-primary">
                  <h5 className="card-title mb-3">{crtNewTxt}</h5>
                  <p className="card-coupon"></p>
                </div>
                <div className="card-body">
                  <form>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{couponCodeTxt}</label>                          
                          <input 
                          className="form-control"
                          type="text"
                          name="couponCode"
                          value={couponCode}
                          onChange={this.onChangeValue} />
                        </div>
                      </div>


                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{minimumValueToBeAppliedTxt}</label>                     
                          <input 
                          className="form-control"
                          type="text"
                          name="minimumValueToBeApplied"
                          value={minimumValueToBeApplied}
                          onChange={this.onChangeValue} />
                        </div>
                      </div>


                    </div>
                 

                  <div className="row">
                      

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{validFromTxt}</label>                     
                        
                        <DatePicker
                        value={validFrom}
                        selected={new Date(validFrom)}
                        onChange={(validFrom) => this.hndlValidFrom(validFrom)}
                        dateFormat="yyyy-MM-dd"                        
                        className="form-control"
                        minTime={new Date()}
                        />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{validToTxt}</label>                     
                        
                        <DatePicker
                        value={validTo}
                        selected={new Date(validTo)}
                        onChange={(validTo) => this.hndlValidTo(validTo)}
                        dateFormat="yyyy-MM-dd"                        
                        className="form-control"
                        minTime={new Date()}
                        />
                      </div>
                    </div>

                  </div>
                                        
                    
                    <div className="row">


                    <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{couponValueTxt}</label>                            
                          <input 
                          className="form-control"
                          type="text"
                          name="couponValue"
                          value={couponValue}
                          onChange={this.onChangeValue} />
                        </div>
                      </div>
                      

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{qtyTxt}</label>                     
                          <input 
                          className="form-control"
                          type="text"
                          name="originalQuantity"
                          value={originalQuantity}
                          onChange={this.onChangeValue} />
                        </div>
                      </div>
                    </div>
                    

                    <div className="row">                      
                      
                      <div className="col-md-6">
                          <div className="form-group">
                            <label className="bmd-label-floating">{typeTxt}</label>                     
                            <select 
                            name="type"
                            className="form-control"
                            value={type}
                            onChange={this.onChangeValue}>
                              <option value="1">{fixedTxt}</option>
                              <option value="0">{percentageTxt}</option>
                            </select>
                          </div>
                      </div>

                      
                      <div className="col-md-6">
                          <div className="form-group">
                            <label className="bmd-label-floating">{statusTxt}</label>                     
                            <select 
                            name="status"
                            className="form-control"
                            value={validTo}
                            onChange={this.onChangeValue}>
                              <option value="1">{activeTxt}</option>
                              <option value="0">{inActiveTxt}</option>
                            </select>
                          </div>
                      </div>

                    </div>

                    <button 
                    type="submit" 
                    onClick={this.handleSubmit}
                    disabled={isDisabled}
                    className="btn btn-primary pull-right">{saveTxt}
                    </button>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
           

              
        <div className="col-md-4">
         

         <div className="col-md-12">
            
         <div className="card card-profile">
               <div className="card-avatar">.</div>
               <div className="card-body">
                 <h6 className="card-coupon text-gray"></h6>
                 { <h5 className="card-title mb-3">{mainPhotoTxt}</h5>}
                 
                 <div className="card-description">
                   <RowThumbnail imgName={photo} rmvMnImg={this.rmvMnImg} />
                    
                   <ImgToBeUploaded 
                   image={mainImg} 
                   rmvSlctdMnImg={this.rmvSlctdMnImg} />
                   
                   <input type="file" 
                   accept=".gif,.jpg,.jpeg,.png"
                   onChange={this.onMainImageChange} />
                 </div>
                 
                 {/* <button 
                 onClick={this.handleUpload}
                 className="btn btn-primary btn-round">
                   Upload
                 </button> */}
               </div>
             </div>
          
         </div>
       
      
       </div>


        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Edit);
