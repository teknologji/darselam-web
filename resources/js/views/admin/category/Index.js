import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import CategoryRow from '../../../components/CategoryRow';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import TitleFilter from '../../../components/filters/TitleFilter';
import StatusFilter from '../../../components/filters/StatusFilter';
import getData from '../../../helpers/getData';

 class Index extends Component { 
    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            rowsBfrFltr: [],
            title: '',
            status: '' 
        };

        this.onValueChange = this.onValueChange.bind(this);

    }
   
   
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      getData(this.props.defaultSets.apiUrl+'categories', this.props.userInfo.accessToken)
       .then(response => {
        this.setState({ rows: response.data.rows, rowsBfrFltr: response.data.rows });
        toast.dismiss(ldToast);      
      })
      .catch(function (error) { 
          console.log(error);
      });
//this.setState({ rows: items, rowsBfrFltr: items });
    }

    onValueChange(e) {
      //console.log(e.target.value)
      this.setState({
        [e.target.name]: e.target.value
    }, () => {
        this.tblFilter();
    });
    }

    tblFilter() {
              
       let newRows = this.state.rowsBfrFltr;      
       
        if(this.state.title != '') {
          newRows = newRows.filter(row => row.titleAR.search(this.state.title) !== -1 || (row.titleEN && row.titleEN.toLowerCase().search(this.state.title.toLowerCase()) !== -1));
        }
 
        if(this.state.status != '') {
          newRows = newRows.filter(row => row.status == this.state.status);
        }       
       
       this.setState({rows: newRows});
    }
    
    render() {
        
        //const {isLoading} = this.state;
        const lng = this.props.selectedLanguage;

        return (
           
         <Fragment>
              <Helmet><title>{lng.categoriesTxt}</title></Helmet>
  
          <div className="row">
            
      <div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.categoriesTxt}</li>
          </ol>
        </nav>
      </div>


      <div className="col-md-12">
        <div className="row">
                <div className="col-md-6">
                    <Link to='/admin/create-category' className="btn btn-primary">
                        {lng.crtNewTxt}
                    </Link>
                </div>

                
                <TitleFilter 
                selectedLanguage={this.props.selectedLanguage}
                onKeyUp={this.onValueChange}
                />                  
              
                <StatusFilter                   
                selectedLanguage={this.props.selectedLanguage}
                onChange={this.onValueChange} 
                />
        </div>
      </div>

      </div>

          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{lng.categoriesTxt}</h4>
                <p className="card-category"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    <thead className=" text-primary">                    
                      <tr>
                      <th>{'#'}</th>
                      <th>{lng.titleTxt}</th>
                      <th>{lng.languageTxt}</th>
                      <th>{lng.statusTxt}</th>
                      {/* <th>{lng.photoTxt}</th> */}
                      <th>{lng.actionTxt}</th>
                      </tr>                      
                    </thead>
                    <tbody>
                            {this.state.rows ? 
                            this.state.rows.map((item, index) => {
                              return(<CategoryRow obj={item} num={index + 1} key={index}
                                defaultSets = {this.props.defaultSets}
                                selectedLanguage = {this.props.selectedLanguage}
                                userInfo = {this.props.userInfo}
                                />)
                            }) : null
                            }
                            
                    </tbody>
                  </table>
                </div>
              </div>
            
            </div>
          </div>

          </div>
      
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Index);
