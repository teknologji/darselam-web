import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';


import CKEditor from 'ckeditor4-react';

import postData from '../../../helpers/postData';
import LangCodesSelect from '../../../components/LangCodesSelect';

 class EditTrans extends Component {
    constructor(props) { 
        super(props); 
        this.state = {
            id:this.props.match.params.id,
            
            languageCode: '',
            description1: '',
            description2: '',
            errorMessage: '',
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
  
     
  
    componentDidMount(){

      //console.log('khaled', this.state.titleAR)                
 }

    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    
    handleSubmit(e){
//console.log('k'+this.state.status)
        e.preventDefault();
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

          postData(this.props.defaultSets.apiUrl+'faqs-trans', {
            rowId: this.state.id,
            languageCode: this.state.languageCode,
            
            description1: this.state.description1,
            description2: this.state.description2,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

               /*  setTimeout( () => {
                    this.props.history.push('/faqs');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        

        const lng = this.props.selectedLanguage;

        return (
                   

          <div className="row">
            
<Helmet><title>{lng.crtNewTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/faqs">{lng.faqsTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.crtNewTxt}</li>
          </ol>
        </nav>
      </div>


          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{lng.crtNewTxt}</h4>
                <p className="card-faq"></p>
              </div>
              <div className="card-body">
                <form>
                  <div className="row">
                    
                    
                    <div className="col-md-12">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.languageTxt}</label>                          
                          <LangCodesSelect
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage}
                          onChangeValue={this.onChangeValue} />
                        </div>
                    </div>

                  </div>
               

                  
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.qTxt}</label>
                          <input 
                          className="form-control"
                          type="text"
                          name="description1"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>
                    </div>
                  </div>


                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.aTxt}</label>
                          <CKEditor
                                onChange={evt => {
                                  this.setState({description2: evt.editor.getData()})

                                }}
                                onKey={evt => {
                                  this.setState({description2: evt.editor.getData()})

                                }}                               
        
                            />
                        </div>
                      </div>
                    </div>
                  </div>


                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  className="btn btn-primary pull-right">{lng.saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
          

              
       {/*  <div className="col-md-4">
         

         <div className="col-md-12">
            
         <div className="card card-profile">
               <div className="card-avatar">.</div>
               <div className="card-body">
                 <h6 className="card-faq text-gray"></h6>
                 { <h5 className="card-title mb-3">{mainPhotoTxt}</h5>}
                 
                 <div className="card-description">
                   <RowThumbnail imgName={photo} rmvMnImg={this.rmvMnImg} />
                    
                   <ImgToBeUploaded 
                   image={mainImg} 
                   rmvSlctdMnImg={this.rmvSlctdMnImg} />
                   
                   <input type="file" 
                   accept=".gif,.jpg,.jpeg,.png"
                   onChange={this.onMainImageChange} />
                 </div>
               </div>
             </div>
          
         </div>
       
      
       </div> */}


        
        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(EditTrans);
