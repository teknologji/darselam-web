import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
 
import ImgToBeUploaded from '../../../components/ImageToUpload';
import RowThumbnail from '../../../components/RowThumbnail';
import RmvFileFrmDB from '../../../helpers/RmvFileFrmDB';
import CKEditor from 'ckeditor4-react';
import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';
import { checkMimeType } from '../../../helpers/checkMimeType';
//import SubcatSelect from '../../../components/SubcatSelect';

 class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:0,
            title: '',
            description: null,
            metaTags: '',
            languageCode: '',
            errorMessage: '',
            isDisabled: false,
            // here i load original images of an element that comes from db.
            otherImages: [], 
           
            //.
            // here the new selected images will be loaded.
      
            images: [],
            //.
        };
        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchData = this.fetchData.bind(this);

        //the next functions handle images on selected, change and upload.

        this.onOtherImagesChange = this.onOtherImagesChange.bind(this);

        this.rmvSlctdOthrImgs = this.rmvSlctdOthrImgs.bind(this);
        
        this.hndlUpldOthrImgs = this.hndlUpldOthrImgs.bind(this);
        
        this.rmvOthrImgs = this.rmvOthrImgs.bind(this);
        
    }
  

    
    
    /* onSizesSelectChange(selectedSizes) {
      this.setState({selectedSizes})
    } */
    componentDidMount(){

         this.fetchData(this.props.match.params.id);     

    }

    fetchData(elmntId) {

      getData(this.props.defaultSets.apiUrl+'products-trans/'+elmntId+'/edit', this.props.userInfo.accessToken)

          //.then(response => response.json())
          .then(response => {       

            this.setState({ 
                id: elmntId,
                title: response.data.title,
                description: response.data.description,
                metaTags: response.data.metaTags,
                otherImages: response.data.files, 
                languageCode: response.data.languageCode
              
                //features: response.data.features
              });
              //console.log(response.data.files)
          })
          .catch(function (error) {
              console.log(error);
          });
    }


    onOtherImagesChange(event) {
      if (event.target.files && event.target.files[0]) {

        let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
        if(mimeTypeCheck == true){
    
          return this.setState({ images: [...this.state.images, ...event.target.files] })

        } 
        return toast.error(mimeTypeCheck);


      }
    };


    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
   

    hndlUpldOthrImgs() { // Id of the row that created in DB to be inserted in files db table.
      
      if (this.state.images.length == 0) {
        return false;
      }
      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', this.state.id);
      formData.append('type', 'product'); 
      formData.append('languageCode', this.state.languageCode);

      //.

      // iterate over multiple additional images and append them to formData.
      this.state.images.forEach((image, i) => { 
        formData.append(
          'files[]',
          image,
          image.name
        )
      })
  
      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({          
          images: []
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }
    
    
    rmvSlctdOthrImgs(name) { // handle selected photos not that comes from db.
      this.setState({
        images: this.state.images.filter(image => image.name !== name)
      })
    }

    rmvOthrImgs(id) { // handle other photos that comes from db not a selected ones.
      
      this.setState({
        otherImages: this.state.otherImages.filter(image => image.id !== id)        
      })
      RmvFileFrmDB(this.props.defaultSets.apiUrl, id, 'product', this.props.selectedLanguage.loadingTxt);
    }

  

    onChangeValue(e) {

      this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }
    
    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);


          putData(this.props.defaultSets.apiUrl+'products-trans/'+this.state.id, {
            id: this.state.id,
            title: this.state.title,            
            description: this.state.description,
            metaTags: this.state.metaTags,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

              this.hndlUpldOthrImgs();

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
                this.setState({isDisabled: false});
 
               /*  setTimeout( () => {
                    this.props.history.push('/products');
                }, 3000); */
            })
            .catch( (error) => {
                
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }

    render() {
        
        
        const {isDisabled, title, description, images, otherImages,  metaTags} = this.state;

         const lng = this.props.selectedLanguage;

        return (
                   
          
          <div className="row">

                        
<Helmet><title>{lng.updtPrdctTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/products">{lng.productsTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.updtPrdctTxt}</li>
          </ol>
        </nav>
      </div>


          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{lng.updtPrdctTxt}</h4>
                <p className="card-category"></p>
              </div>
              <div className="card-body">
                <form>

                  

                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.titleTxt}</label>                          
                        <input 
                        className="form-control"
                        defaultValue={title}
                        type="text"
                        name="title"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    </div>

                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.descriptionTxt}</label>

                          <CKEditor
                          data={description}
                                onChange={evt => {
                                  this.setState({description: evt.editor.getData()})

                                }}
                                onKey={evt => {
                                  this.setState({description: evt.editor.getData()})

                                }}                                
        
                            />

                
                        </div>
                      </div>
                    </div>
                  </div>
                 
                  <div className="row">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label></label>
                          <div className="form-group">
                            <label className="bmd-label-floating">{lng.metaTagsTxt}</label>
                                             
                          <textarea 
                          className="form-control"
                          name="metaTags"
                          defaultValue={metaTags}
                          onChange={this.onChangeValue}
                          >
                          </textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                   
                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  disabled={isDisabled}
                  className="btn btn-primary pull-right">{lng.saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          
          
          

          </div>
          

          
        <div className="col-md-4">
       
       
         <div className="col-md-12">
             
         <div className="card card-profile">
               <div className="card-avatar">.</div>
               <div className="card-body">
                 <h6 className="card-category text-gray"></h6>
                 <h5 className="card-title mb-3">{lng.otherPhotosTxt}</h5>
                 
                 <div className="card-description">
                 {otherImages ? otherImages.map((img, index) => {
                                 return(
                                  <RowThumbnail imgName={img.fileName} imgId={img.id} rmvOthrImgs={this.rmvOthrImgs} key={'otherImg'+index} />
                                 )
                     }) : null
                   }

                 
                   {images? images.map((img, index) => {
                                 return(
                                   <ImgToBeUploaded image={img} key={'img'+index} rmvSlctdOthrImgs={this.rmvSlctdOthrImgs} />
                                 )
                     }) : null
                   }
                 
                   <input type="file" 
                   accept=".gif,.jpg,.jpeg,.png"
                   multiple onChange={this.onOtherImagesChange} />
                 </div>
                 
                 {/* <button 
                 onClick={this.handleUpload}
                 className="btn btn-primary btn-round">
                   Upload
                 </button> */}
               </div>
             </div>
          
         </div>            
       
       </div>

       
        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Edit);
