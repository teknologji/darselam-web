import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import ImgToBeUploaded from '../../../components/ImageToUpload';
//import CategoriesSelect from '../../../components/CategoriesSelect';
import ColoursSelect from '../../../components/ColoursSelect';
import FeaturesSelect from '../../../components/FeaturesSelect';
import CatsSelect from '../../../components/CatsSelect';

//import SizesSelect from '../../../components/SizesSelect';

import CKEditor from 'ckeditor4-react';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';

import postData from '../../../helpers/postData';
//import SubcatSelect from '../../../components/SubcatSelect';
import ProductsSelect from '../../../components/ProductsSelect';
import LangCodesSelect from '../../../components/LangCodesSelect';

 class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
          productId:0,
            title: '',
            description: '',
            metaTags: '',            
            languageCode: '',
            priceBeforeDiscount: 0,
            priceAfterDiscount: 0,
            sku: '',
            photo: '',
            status: 1,
            isOffer: 0,
            isAccessory: 0,
            isGallery: 0,
            isNew: 0,
            theCount: 1,
            errorMessage: '',
            isDisabled: false,
            
            images: [],
            //categoryId: '',
            //subcatId: '',
            selectedColours: [],
            selectedFeatures: [],
            selectedCats: [],
            /* selectedSizes: [], */
            prdctPrps: [],
            propValueId: 0,
            propId: 0,
            isSvPrpsDisabled: true,
            //isSvVlsDisabled: true
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        //this.hndlSlctVlu = this.hndlSlctVlu.bind(this);

        //the next functions handle images on selected, change and upload.
        
        this.onOtherImagesChange = this.onOtherImagesChange.bind(this);
        this.rmvSlctdOthrImgs = this.rmvSlctdOthrImgs.bind(this);
        
        this.handleUploadOtherPhoto = this.handleUploadOtherPhoto.bind(this);
        
        //.
        this.onColoursSelectChange = this.onColoursSelectChange.bind(this);
        this.onFeaturesSelectChange = this.onFeaturesSelectChange.bind(this);
        this.onCatsSelectChange = this.onCatsSelectChange.bind(this);
        /* this.onSizesSelectChange = this.onSizesSelectChange.bind(this); */
        this.handleSvPrps = this.handleSvPrps.bind(this);
        //this.handleSvValues = this.handleSvValues.bind(this);
         
    }
    onColoursSelectChange(selectedColours) {
      this.setState({selectedColours})
    }
    onFeaturesSelectChange(selectedFeatures) {
      this.setState({selectedFeatures})
    }
    onCatsSelectChange(selectedCats) {
      this.setState({selectedCats})
    }
    
    /* onSizesSelectChange(selectedSizes) {
      this.setState({selectedSizes})
    } */
    componentDidMount() {  
    }

    

    onOtherImagesChange(event) {
      if (event.target.files && event.target.files[0]) {

        this.setState({ images: [...this.state.images, ...event.target.files] })

      }
    };

    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
   
    
    handleUploadOtherPhoto(transId) { // Id of the row that created in DB to be inserted in files db table.
      
      if (this.state.images.length == 0) {
        return false;
      }
      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', transId);
      formData.append('type', 'product');
      formData.append('languageCode', this.state.languageCode);

      //.

      // iterate over multiple additional images and append them to formData.
      this.state.images.forEach((image, i) => { 
        //console.log(image.name)
        formData.append(
          'files[]',
          image,
          image.name
        )
      })
  
      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({          
          images: []
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }
    
    
    rmvSlctdOthrImgs(name) {
      this.setState({
        images: this.state.images.filter(image => image.name !== name)
      })
    }

    onChangeValue(e) {
      console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value, isDisabled: false
        });
    }

    /*handleSvValues(e){

      //console.log('halid', this.state.propId)
      e.preventDefault();
      this.setState({isSvVlsDisabled: true});
      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      // prepare selected roles to be sent.
      let sizes = [];
      {this.state.selectedSizes && this.state.selectedSizes.map((item, i) => {
        sizes.push(item.value);
      })} 
      //.
 
       postData(this.props.defaultSets.apiUrl+'sv-prdct-prps-vls', {
        propId: this.state.propId,
        productId: this.state.productId,
        //propValues: sizes,
        sku: this.state.sku,
        theCount: this.state.theCount,
        langCode: this.props.selectedLanguage.langCode
      }, this.props.userInfo.accessToken)
          .then( (response) => {
            if(response.data.success == true) {
              toast.update(ldToast, {
                type: toast.TYPE.SUCCESS,
                render: response.data.msg
            });
            } else { 
              toast.update(ldToast, {
                type: toast.TYPE.ERROR,
                render: response.data.msg
            });
            }
              
              this.setState({ isSvVlsDisabled: false})

          })
          .catch( (error) => {
              console.log(error);
          });

   
    } */

    handleSvPrps(e){

      e.preventDefault();
      this.setState({isSvPrpsDisabled: true});
      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      // prepare selected roles to be sent.
      let colours = [];
      {this.state.selectedColours && this.state.selectedColours.map((item, i) => {
        colours.push(item.value);
      })}
      //.

      postData(this.props.defaultSets.apiUrl+'sv-prdct-prps', {
        productId: this.state.productId,
        props: colours,
        langCode: this.props.selectedLanguage.langCode
      }, this.props.userInfo.accessToken)
          .then( (response) => {
            
              if(response.data.success == true) {
                toast.update(ldToast, {
                  type: toast.TYPE.SUCCESS,
                  render: response.data.msg
              });
              } else {
                toast.update(ldToast, {
                  type: toast.TYPE.ERROR,
                  render: response.data.msg
              });
              }
              this.setState({ /*isSvVlsDisabled: false,*/ isSvPrpsDisabled: false, prdctPrps: response.data.prps})

          })
          .catch( (error) => {
              console.log(error);
          });

  }


    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        // prepare selected features to be sent.
      let features = [];
      {this.state.selectedFeatures && this.state.selectedFeatures.map((item, i) => {
        features.push(item.value);
      })}
      //.

      // prepare selected cats to be sent.
      let cats = [];
      {this.state.selectedCats && this.state.selectedCats.map((item, i) => {
        cats.push(item.value);
      })}
      //.

        postData(this.props.defaultSets.apiUrl+'products', {
          
          languageCode: this.state.languageCode,
            title: this.state.title,
            description: this.state.description,
            metaTags: this.state.metaTags,
            priceBeforeDiscount: this.state.priceBeforeDiscount,
            priceAfterDiscount: this.state.priceAfterDiscount,            
            status: this.state.status,
            isGallery: this.state.isGallery,
            isNew: this.state.isNew,
            isOffer: this.state.isOffer,            
            isAccessory: this.state.isAccessory,
            features: features,
            cats:cats,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {
              //console.log(response.data.transId)
              this.handleUploadOtherPhoto(response.data.transId);

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
                this.setState({ isSvPrpsDisabled: false, productId: response.data.rowId})

                /* setTimeout( () => {
                    this.props.history.push('/products');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
       
        const {isSvPrpsDisabled, isDisabled, images, selectedColours, 
          prdctPrps, selectedFeatures, selectedCats /* selectedSizes */} = this.state;
          
        const lng = this.props.selectedLanguage;

        return (
                           
        
      <div className="row">
          <Helmet><title>{lng.crtPrdctTxt}</title></Helmet>

                  
      <div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/products">{lng.productsTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.crtPrdctTxt}</li>
          </ol>
        </nav>
      </div>


        <div className="col-md-8">
            
                     
            <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">{lng.crtPrdctTxt}</h4>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>

                    <div className="row mb-3">
                      <div className="col-md-6">
                      <label className="bmd-label-floating">{lng.categoryTxt}</label>                     

                      <CatsSelect 
                          selected={selectedCats}
                          onCatsSelectChange={this.onCatsSelectChange}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />
                          {/* <CategoriesSelect 
                          categoryId={this.state.categoryId} 
                          onChangeValue={this.onChangeValue}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} /> */}
                      </div>
                      
                      <div className="col-md-6">
                      <label className="bmd-label-floating">{lng.featuresTxt}</label>

                      
                      <FeaturesSelect 
                          selected={selectedFeatures}
                          onFeaturesSelectChange={this.onFeaturesSelectChange}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />
                      </div>

                      {/* <div className="col-md-6">
                      <label className="bmd-label-floating">{lng.subcatTxt}</label>

                          <SubcatSelect 
                          onChangeValue={this.onChangeValue}  
                          categoryId={this.state.categoryId}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />
                      </div> */}
                    </div>  


                       
                    <div className="row mb-4">
                      
                      
                      <div className="col-md-12">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.isAccessoryTxt}</label>                     
                          <select 
                          name="isAccessory"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="0">{lng.noTxt}</option>
                            <option value="1">{lng.yesTxt}</option>
                          </select>
                        </div>
                      </div>

                    </div> 
                    

                    <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.languageTxt}</label>                          
                          <LangCodesSelect
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage}
                          onChangeValue={this.onChangeValue} />
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.titleTxt}</label>                          
                          <input 
                          className="form-control"
                          type="text"
                          name="title"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>

                      </div>

                      <div className="row">

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.prcBfrDscnt}</label>                            
                          <input 
                          className="form-control"
                          type="text"
                          name="priceBeforeDiscount"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>


                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.prcAftrDscnt}</label>                            
                          <input 
                          className="form-control"
                          type="text"
                          name="priceAfterDiscount"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>

                    </div>
                 

                    <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.statusTxt}</label>                     
                          <select 
                          name="status"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="1">{lng.activeTxt}</option>
                            <option value="0">{lng.inActiveTxt}</option>
                          </select>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.newTxt}</label>                     
                          <select 
                          name="isNew"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="0">{lng.noTxt}</option>
                            <option value="1">{lng.yesTxt}</option>
                          </select>
                        </div>
                      </div>

                    </div>


                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.isGalleryTxt}</label>                     
                          <select 
                          name="isGallery"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="0">{lng.noTxt}</option>
                            <option value="1">{lng.yesTxt}</option>
                          </select>
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.isOfferTxt}</label>                     
                          <select 
                          name="isOffer"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="0">{lng.noTxt}</option>
                            <option value="1">{lng.yesTxt}</option>
                          </select>
                        </div>
                      </div>

                    </div>

                    <div className="row">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label></label>
                          <div className="form-group">
                            <label className="bmd-label-floating">{lng.descriptionTxt}</label>

                            
                          <CKEditor
                                onChange={evt => {
                                  this.setState({description: evt.editor.getData()})

                                }}
                                onKey={evt => {
                                  this.setState({description: evt.editor.getData()})

                                }}                                
        
                            />
    
                          
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div className="row">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label></label>
                          <div className="form-group">
                            <label className="bmd-label-floating">{lng.metaTagsTxt}</label>
                                             
                          <textarea 
                          defaultValue=""
                          className="form-control"
                          name="metaTags"
                          onChange={this.onChangeValue}
                          >
                          </textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                   
                    <button 
                    type="submit" 
                    onClick={this.handleSubmit}
                    disabled={isDisabled}
                    className="btn btn-primary pull-right">{lng.addProductTxt}
                    </button>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
        
        
              <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">{lng.coloursTxt}</h4>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>

                  <div className="row"> 
                      
                      <div className="col-md-12">
                        <label className="bmd-label-floating">{lng.colourTxt}</label>  
                        
                          
      
                          <ColoursSelect 
                          selected={selectedColours}
                          onColoursSelectChange={this.onColoursSelectChange}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />
                      </div>
                    
                  </div>                  
                  
 
                  <button 
                  type="submit" 
                  onClick={this.handleSvPrps}
                  disabled={isSvPrpsDisabled}
                  className="btn btn-primary pull-right">
                    {isSvPrpsDisabled? lng.svPrdctFrstTxt: lng.saveTxt}
                  </button>
                  <div className="clearfix"></div>
                  </form>
                </div>
              </div>
        

              {/* <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">{sizesTxt}</h4>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>

                  <div className="row">

                      <div className="col-md-6">
                        <label className="bmd-label-floating">{colourTxt}</label>                     

                          <select 
                          className="form-control" 
                          name="propId"                  
                          onChange={this.onChangeValue}>
                            
                            <option>{prdctPrps && prdctPrps.length == 0 ? lng.svPrpsFrstTxt : lng.chooseTxt }</option>
                            
                            {prdctPrps && prdctPrps.map((item, i) => {
                              return(<option
                                key={i}
                                value={item.propId}>
                                  {eval("item.colour.title"+langCode)}
                                  </option>)
                            })}
                          </select>
                      </div> 

                      <div className="col-md-6">
                        <label className="bmd-label-floating">{sizeTxt}</label>                     

                          <SizesSelect 
                          selected={selectedSizes}
                          onSizesSelectChange={this.onSizesSelectChange}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />  
                      </div> 

                      
                      
                    
                  </div>

                  
                  <div className="row">
                  
                        <div className="col-md-6">
                          <div className="form-group">
                            <label className="bmd-label-floating">{lng.theCountTxt}</label>                       
                            <input 
                            className="form-control"
                            type="text"
                            name="theCount"
                            onChange={this.onChangeValue} />
                          </div>
                        </div>


                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.skuTxt}</label>                           
                          <input 
                          className="form-control"
                          type="text"
                          name="sku"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>

                    </div>
                 
 
                  <button 
                  type="submit" 
                  onClick={this.handleSvValues}
                  disabled={isSvVlsDisabled}
                  className="btn btn-primary pull-right">
                    {isSvVlsDisabled? lng.svPrpsFrstTxt: lng.savePropsTxt}
                  </button>
                  <div className="clearfix"></div>
                  </form>
                </div>
              </div>
         */}
        </div>



        <div className="col-md-4">
         
        
          <div className="col-md-12">
              
          <div className="card card-profile">
                <div className="card-avatar">.</div>
                <div className="card-body">
                  <h6 className="card-category text-gray"></h6>
                  <h5 className="card-title mb-3">{lng.photosTxt}</h5>
                  
                  <div className="card-description">

                    {images? images.map((img, index) => {
                                  return(
                                    <ImgToBeUploaded image={img} key={'img'+index} rmvSlctdOthrImgs={this.rmvSlctdOthrImgs} />
                                  )
                      }) : null
                    }
                  
                    <input type="file" multiple onChange={this.onOtherImagesChange} />
                  </div>
                  
                </div>
              </div>
           
          </div>            
        
        </div>


      </div>
        
        

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Create);
