import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import getData from '../../../helpers/getData';
import deleteData from '../../../helpers/deleteData';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

 class ProductRegister extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            rows: ''
        };
    }
  
   
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      getData(this.props.defaultSets.apiUrl+'register-product', this.props.userInfo.accessToken)
          //.then(response => response.json())
          .then(response => {
              this.setState({ rows: response.data.rows });
              toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

  
    
    deleteRow(id) {

      confirmAlert({
            
        customUI: ({ onClose }) => {
            return (
                <div id="react-confirm-alert">
                    <div className="react-confirm-alert-overlay">
                        <div className="react-confirm-alert">
                            <div className="react-confirm-alert-body">
                                <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                <div 
                                className="react-confirm-alert-button-group">
                                <button
                                className="yes"
                                onClick={() => {
                                    deleteData(this.props.defaultSets.apiUrl+'register-product/'+id, this.props.userInfo.accessToken)
                                    .then(res => {
                                        document.getElementById(id).remove();            
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    });
                                    onClose();
                                }}
                                >
                                        {this.props.selectedLanguage.yesTxt}
                                    </button>

                                    <button className="no" onClick={onClose}>
                                    {this.props.selectedLanguage.noTxt}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            );
          }
      });
  

    }
    render() {
        
        //const {isLoading} = this.state;
        const lng = this.props.selectedLanguage;

        return (
           
         <Fragment>
                
          <div className="row">

                        
<Helmet><title>{lng.rgstrdPrdctsTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.rgstrdPrdctsTxt}</li>
          </ol>
        </nav>
      </div>


      </div>

          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{lng.rgstrdPrdctsTxt}</h4>
                <p className="card-colour"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    <thead className=" text-primary">                    
                      <tr>
                      <th>{'#'}</th>
                      <th>{lng.firstNameTxt}</th>
                      <th>{lng.lastNameTxt}</th>
                      <th>{lng.emailTxt}</th>
                      <th>{lng.phoneTxt}</th>
                      <th>{lng.serialNumTxt}</th>
                      <th>{lng.guaranteeNumTxt}</th>
                      <th>{lng.countryTxt}</th>
                      <th>{lng.adrsTxt}</th>
                      <th>{lng.actionTxt}</th>
                      </tr>                      
                    </thead>
                    <tbody>
                            {this.state.rows ? 
                            this.state.rows.map((item, index) => {
                              return (
                                <tr id={item.id} key={index}>
                                    <td>{index + 1}</td>
                                    <td>{item.firstName}</td>
                                    <td>{item.lastName}</td>
                                    <td>{item.email}</td>
                                    <td>{item.phone}</td>
                                    <td>{item.serialNum}</td>
                                    <td>{item.guaranteeNum}</td>
                                    <td>{item.countryCode}</td>
                                    <td>{item.adrs}</td>
                                    <td>
                                            <input type="button" 
                                            value={lng.deleteTxt} 
                                            onClick={() => this.deleteRow(item.id)}
                                            className="btn btn-danger"/>
                                        
                                    </td>
                                </tr>
                            )
                            }) : null
                            }
                            
                    </tbody>
                  </table>
                </div>
              </div>
            
            </div>
          </div>

          </div>
      
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(ProductRegister);
