import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import postData from '../../../helpers/postData';
 
import ProductsSelect from '../../../components/ProductsSelect';

 class Create extends Component {  
    constructor(props) {
        super(props);
        this.state = {
          productId: '',
          serialNum: '',
          guaranteeNum: '', 
          status: 1,
          errorMessage: '',
          isDisabled: false
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
  
    
    onChangeValue(e) {
      //console.log(e.target.value)
        this.setState({
            [e.target.name]: e.target.value,  isDisabled: false
        });
    }
    handleSubmit(e){
        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        postData(this.props.defaultSets.apiUrl+'guarantees', {
            productId: this.state.productId,            
            serialNum: this.state.serialNum,
            guaranteeNum: this.state.guaranteeNum,
            status: this.state.status,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

              if(response.data.success == false) {
                return toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
              }
              return  toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

               /*  setTimeout( () => {
                    this.props.history.push('/guarantees');
                }, 3000); */
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
        const {isDisabled} = this.state;
        const lng = this.props.selectedLanguage;

        return (
                           
        
      <div className="row">
            <Helmet><title>{lng.crtNewTxt}</title></Helmet>

                  
          <div className="col-md-12">
                  
                  <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
                      <li className="breadcrumb-item"><Link to="/admin/guarantees">{lng.guaranteesTxt}</Link></li>
                      <li className="breadcrumb-item active" aria-current="page">{lng.crtNewTxt}</li>
                    </ol>
                  </nav>
                </div>


            <div className="col-md-8">
              <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title mb-3">{lng.crtNewTxt}</h4>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>

                  <div className="row mb-3">
             
                      <div className="col-md-6">
                      <label className="bmd-label-floating">{lng.productTitleTxt}</label>                     

                      <ProductsSelect
                          productId={this.state.productId} 
                          onChangeValue={this.onChangeValue}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />
                      </div>

                      
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.statusTxt}</label>                     
                          <select 
                          name="status"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="1">{lng.activeTxt}</option>
                            <option value="0">{lng.inActiveTxt}</option>
                          </select>
                        </div>
                      </div>
                    

                    </div> 
                    
                    <div className="row">

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.serialNumTxt}</label>                          
                          <input 
                          className="form-control"
                          type="text"
                          name="serialNum"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.guaranteeNumTxt}</label>                          
                          <input 
                          className="form-control"
                          type="text"
                          name="guaranteeNum"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>
                    </div>
                 

                    <button 
                    type="submit" 
                    onClick={this.handleSubmit}
                    disabled={isDisabled}
                    className="btn btn-primary pull-right">{lng.saveTxt}
                    </button>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            

          </div>
        
        

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,        
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Create);
