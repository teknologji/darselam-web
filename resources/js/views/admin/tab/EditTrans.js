import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';

import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
 
import ImgToBeUploaded from '../../../components/ImageToUpload';
import RowThumbnail from '../../../components/RowThumbnail';
import RmvFileFrmDB from '../../../helpers/RmvFileFrmDB';
import CKEditor from 'ckeditor4-react';
import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';
import { checkMimeType } from '../../../helpers/checkMimeType';
//import SubcatSelect from '../../../components/SubcatSelect';
import postData from '../../../helpers/postData';
import FeaturesSelect from '../../../components/FeaturesSelect';
import CatsSelect from '../../../components/CatsSelect';

 class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:0,
            title: '',
            description: null,
            errorMessage: '',
            
        };
        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchData = this.fetchData.bind(this);
        
    }
  

    
    
    /* onSizesSelectChange(selectedSizes) {
      this.setState({selectedSizes})
    } */
    componentDidMount(){

         this.fetchData(this.props.match.params.id);     

    }

    fetchData(elmntId) {

      getData(this.props.defaultSets.apiUrl+'tabs-trans/'+elmntId+'/edit', this.props.userInfo.accessToken)

          //.then(response => response.json())
          .then(response => {       

            this.setState({ 
                id: elmntId,
                title: response.data.title,
                description: response.data.description
              
                //features: response.data.features
              });
              console.log(response.data.files)
          })
          .catch(function (error) {
              console.log(error);
          });
    } 

    onChangeValue(e) {

      this.setState({
            [e.target.name]: e.target.value
        });
    }
    
    handleSubmit(e){

        e.preventDefault();
        
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);


          putData(this.props.defaultSets.apiUrl+'tabs-trans/'+this.state.id, {
            id: this.state.id,
            title: this.state.title,            
            description: this.state.description,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
 
               /*  setTimeout( () => {
                    this.props.history.push('/tabs');
                }, 3000); */
            })
            .catch( (error) => {

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }

    render() {
        
        
        const {title, description} = this.state;

         const lng = this.props.selectedLanguage;

        return (
                   
          
          <div className="row">

                        
<Helmet><title>{lng.updtTabTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/tabs">{lng.tabsTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{lng.updtTabTxt}</li>
          </ol>
        </nav>
      </div>


          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{lng.updtTabTxt}</h4>
                <p className="card-category"></p>
              </div>
              <div className="card-body">
                <form>

                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.titleTxt}</label>                          
                        <input 
                        className="form-control"
                        defaultValue={title}
                        type="text"
                        name="title"
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    </div>

                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{lng.descriptionTxt}</label>

                          <CKEditor
                          data={description}
                                onChange={evt => {
                                  this.setState({description: evt.editor.getData()})

                                }}
                                onKey={evt => {
                                  this.setState({description: evt.editor.getData()})

                                }}                                
        
                            />
                        </div>
                      </div>
                    </div>
                  </div>
                 
                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  className="btn btn-primary pull-right">{lng.saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          

          </div>
          

          

        </div>
      
        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Edit);
