import React, { Component} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';

import RolesSelect from '../../../components/RolesSelect';
import postData from '../../../helpers/postData';
import { checkMimeType } from '../../../helpers/checkMimeType';
import ImgToBeUploaded from '../../../components/ImageToUpload';

 class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            phone: '',
            password: '',
            email: '',
            website: '',
            adrs: '',
            notes: '',
            region: 1,
            //status:1, it's default is active from admin page
            groupId: 1,
            selectedRoles: [],
            errorMessage: '',
            isDisabled: false,
            rolesDisabled: false,
            
            photo: '',         
            mainImg: null,
        };

         //the next functions handle images on selected, change and upload.
         this.onMainImageChange = this.onMainImageChange.bind(this);
         this.rmvSlctdImg = this.rmvSlctdImg.bind(this);
         this.handleUploadMainPhoto = this.handleUploadMainPhoto.bind(this);
         //.

        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onRolesSelectChange = this.onRolesSelectChange.bind(this);
    }
  

    
    onMainImageChange(event) {
      if (event.target.files && event.target.files[0]) {

        let img = event.target.files[0];

        let mimeTypeCheck = checkMimeType(event, this.props.selectedLanguage.notAllowedTxt);
        if(mimeTypeCheck == true){

          return this.setState({
            mainImg: img 
          }); 
        } 
        toast.error(mimeTypeCheck);
          
      }
    };

    
    // The next function is used inside handleSubmit function not used directrly anywhere 
    // so after row have been inserted it uploads its photo.
    handleUploadMainPhoto(rowId) {
      if (this.state.mainImg == null ) {
        return false;
      }

      // then prepare a form to send files.
      const formData = new FormData()
      
      // The next two lines go to FilesController to specify the row and tbl where inserted.
      formData.append('rowId', rowId);
      formData.append('type', 'user');
      formData.append('mainPhoto', '1');
      //.

      
      // append the main photo to formData.
      formData.append(
        'files[]',
        this.state.mainImg,
        this.state.mainImg.name
      )
      //.

      fetch(this.props.defaultSets.apiUrl+'upload-files', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        this.setState({
          mainImg: null,
        });
        /* onUploadProgress: progressEvent => {
          console.log(progressEvent.loaded / progressEvent.total)
        } */
      })
    }

    rmvSlctdImg() {
      this.setState({
        mainImg: null
      })
    }

    onRolesSelectChange(selectedRoles) {
      this.setState({selectedRoles})
    }
    onChangeValue(e) {

      this.setState({disabled: false})
      if([e.target.name] == 'groupId') {
        if(e.target.value != 1){
          this.setState({rolesDisabled: true})
        } else {
          this.setState({rolesDisabled: false})
        }
      }

        this.setState({
            [e.target.name]: e.target.value
        });
    }
    
    handleSubmit(e){ 

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        // prepare selected roles to be sent.
        let roles = [];
        {this.state.selectedRoles && this.state.selectedRoles.map((item, i) => {
          roles.push(item.value);
        })}
        //.

        postData(this.props.defaultSets.apiUrl+'users', {

            firstName: this.state.firstName,
            lastName: this.state.lastName,
            phone: this.state.phone,
            email: this.state.email,
            website: this.state.website,
            region: this.state.region,
            password: this.state.password,
            adrs: this.state.adrs,
            notes: this.state.notes,
            roles: roles,
            groupId: this.state.groupId,
            photo: this.state.mainImg ? this.state.mainImg.name :  null,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {
              if(response.data.success == true) {
                this.handleUploadMainPhoto(response.data.rowId);

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });
                setTimeout( () => {
                  this.props.history.push('/admin/users');
              }, 3000);

              } else {
                this.setState({isDisabled: false});

                toast.update(ldToast, {
                    type: toast.TYPE.ERROR,
                    render: response.data.msg
                });
              }
              
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
        const {selectedRoles, isDisabled, rolesDisabled} = this.state;
        const { phoneTxt, passwordTxt, firstNameTxt, lastNameTxt, crtNewAccntTxt,
          notesTxt, adrsTxt, saveTxt, emailTxt, usersTxt, rolesTxt,
        typeTxt, clientTxt, adminTxt} = this.props.selectedLanguage;
        const lng = this.props.selectedLanguage;
        

        return (
                           
        
                <div className="row">

<Helmet><title>{crtNewAccntTxt}</title></Helmet>

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/users">{usersTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{crtNewAccntTxt}</li>
          </ol>
        </nav>
      </div>

            <div className="col-md-8">
              <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">{crtNewAccntTxt}</h4>
                  <p className="card-category"></p>
                </div>
                <div className="card-body">
                  <form>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{firstNameTxt}</label>
                          
                          <input 
                          className="form-control"
                          type="text"
                          name="firstName"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>

                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{lastNameTxt}</label>                            
                          <input 
                          className="form-control"
                          type="text"
                          name="lastName"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{phoneTxt}</label>                           
                          <input 
                          className="form-control"
                          type="text"
                          name="phone"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{passwordTxt}</label>                       
                          <input 
                          className="form-control"
                          type="password"
                          name="password"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>
                    </div>

                    <div className="row">                      

                      <div className="col-md-6">
                          <div className="form-group">
                            <label className="bmd-label-floating">{emailTxt}</label>                            
                            <input 
                            className="form-control"
                            type="text"
                            name="email"
                            onChange={this.onChangeValue} />
                          </div>
                      </div>

                      <div className="col-md-6">
                          <div className="form-group">
                            <label className="bmd-label-floating">{lng.websiteTxt}</label>                            
                            <input 
                            className="form-control"
                            type="text"
                            name="website"
                            onChange={this.onChangeValue} />
                          </div>
                      </div>                      

                    </div>

                    <div className="row">
                    { !rolesDisabled &&
                      <div className="col-md-6">
                      <label className="bmd-label-floating">{rolesTxt}</label>                     

      
                          {<RolesSelect 
                          selected={selectedRoles}
                          onRolesSelectChange={this.onRolesSelectChange}
                          defaultSets={this.props.defaultSets}
                          userInfo={this.props.userInfo}
                          selectedLanguage={this.props.selectedLanguage} />}
                      </div>
                    }
                      
                      <div className="col-md-6">
                        <div className="form-group">
                        <label className="bmd-label-floating">{typeTxt}</label>                            

                          <select 
                          name="groupId"
                          className="form-control"
                          onChange={this.onChangeValue}>
                            <option value="1">{adminTxt}</option>
                            <option value="2">{clientTxt}</option>
                            <option value="3">{lng.agentTxt}</option>
                          </select>
                        </div>
                      </div>
                    </div>  

                    <div className="row">
                      
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lng.regionTxt}</label>                     
                        <select 
                        name="region"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{lng.americaTxt}</option>
                          <option value="2">{lng.russiaTxt}</option>
                          <option value="3">{lng.middleEastTxt}</option>
                          <option value="4">{lng.latinAmercaTxt}</option>
                          <option value="5">{lng.europeTxt}</option>
                          <option value="6">{lng.asiaPacificTxt}</option>
                          <option value="7">{lng.australiaTxt}</option>
                        </select>
                      </div>
                    </div>
                  
                      <div className="col-md-6">
                        <div className="form-group">
                          <label className="bmd-label-floating">{adrsTxt}</label>                     
                          <input 
                          className="form-control"
                          type="text"
                          name="adrs"
                          onChange={this.onChangeValue} />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-12">
                        <div className="form-group">
                          <label></label>
                          <div className="form-group">
                            <label className="bmd-label-floating">{notesTxt}</label>
                            <textarea 
                            name="notes"
                            className="form-control" 
                            rows="5" onChange={this.onChangeValue}></textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                    <button 
                    type="submit" 
                    onClick={this.handleSubmit}
                    disabled={isDisabled}
                    className="btn btn-primary pull-right">{saveTxt}
                    </button>
                    <div className="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>


   
            <div className="col-md-4">
         

         <div className="col-md-12">
            
         <div className="card card-profile">
               <div className="card-avatar">.</div>
               <div className="card-body">
                 <h6 className="card-size text-gray"></h6>
                 <h5 className="card-title mb-3">{lng.mainPhotoTxt}</h5>
                 
                 <div className="card-description">
                   {this.state.mainImg != null && 
                   <ImgToBeUploaded
                   image={this.state.mainImg} 
                   rmvSlctdImg={this.rmvSlctdImg} />

                   }
                   <input type="file"                   
                   accept=".gif,.jpg,.jpeg,.png" 
                   onChange={this.onMainImageChange} />
                 </div>
                 
                 
               </div>
             </div>
          
         </div>
       
       
       </div>

          </div>
        
        

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Create);
