import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
          usrsCnt: 0,
          prdctsCnt: 0,
          catsCnt: 0,
          ordrsCnt: 0,
      };
    }

    componentDidMount(){
      
      //const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      axios.get(this.props.defaultSets.apiUrl+'counts')
          //.then(response => response.json())
          .then(response => {
              this.setState({
                usrsCnt: response.data.usrsCnt,
                ordrsCnt: response.data.ordrsCnt,
                prdctsCnt: response.data.prdctsCnt,
                catsCnt: response.data.catsCnt
              });
              //toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    componentWillUnmount () {
      //removeScript("/assets/admin/js/core/k.js");
    }

    render() {

        const { clientsCountTxt, productsCountTxt, ordersCountTxt, categoriesCountTxt
        , detailsTxt} = 
        this.props.selectedLanguage;

        const {usrsCnt, ordrsCnt, prdctsCnt, catsCnt} = this.state;
        return (
          
          <Fragment>

            <div className="row">
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="card card-stats">
                <div className="card-header card-header-warning card-header-icon">
                  <div className="card-icon">
                    <i className="fa fa-user"></i>
                  </div>
                  <p className="card-category">{clientsCountTxt}</p>
                  <h3 className="card-title">{usrsCnt}
                    <small></small>
                  </h3>
                </div>
                <div className="card-footer">
                  <div className="stats">
                  <Link to="/admin/users">{detailsTxt}</Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="card card-stats">
                <div className="card-header card-header-success card-header-icon">
                  <div className="card-icon">
                    <i className="fa fa-calendar"></i>
                  </div>
                  <p className="card-category">{ordersCountTxt}</p>
                  <h3 className="card-title">{ordrsCnt}</h3>
                </div>
                <div className="card-footer">
                  <div className="stats">
                  <Link to="/admin/orders">{detailsTxt}</Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="card card-stats">
                <div className="card-header card-header-danger card-header-icon">
                  <div className="card-icon">
                    <i className="fa fa-file"></i>
                  </div>
                  <p className="card-category">{productsCountTxt}</p>
                  <h3 className="card-title">{prdctsCnt}</h3>
                </div>
                <div className="card-footer">
                  <div className="stats">
                  <Link to="/admin/products">{detailsTxt}</Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="card card-stats">
                <div className="card-header card-header-info card-header-icon">
                  <div className="card-icon">
                    <i className="fa fa-briefcase"></i>
                  </div>
                  <p className="card-category">{categoriesCountTxt}</p>
                  <h3 className="card-title">{catsCnt}</h3>
                </div>
                <div className="card-footer">
                  <div className="stats">
                  <Link to="/admin/categories">{detailsTxt}</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
           
        
          </Fragment>

    )
    }
}




const mapStateToProps = (state) => {
  return {
      selectedLanguage: state.LanguageReducer.slctdLng,
      defaultSets: state.DfltSts
  }
}
export default connect(mapStateToProps)(HomeScreen);
