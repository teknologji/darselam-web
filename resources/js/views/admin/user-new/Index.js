import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import UserRow from '../../../components/UserRow';
import { Helmet } from 'react-helmet';
import StatusFilter from '../../../components/filters/StatusFilter';
import getData from '../../../helpers/getData';

 class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rows: '',
            rowsBfrFltr: [],
            full_name: '',
            phone: '',
            status: ''
        };
        this.onValueChange = this.onValueChange.bind(this);

    }
  
   
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

      getData(this.props.defaultSets.apiUrl+'users', this.props.userInfo.accessToken)
          .then(response => {
            this.setState({ rows: response.data.rows, rowsBfrFltr: response.data.rows });
            toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }
    onValueChange(e) {
      //console.log(e.target.value)
      this.setState({
        [e.target.name]: e.target.value
    }, () => {
        this.tblFilter();
    });
    }

    tblFilter() {
              
       let newRows = this.state.rowsBfrFltr;      
       

       if(this.state.phone != '') {
        newRows = newRows.filter(row => row.phone && row.phone.search(this.state.phone) !== -1);
        }
    
        if(this.state.full_name != '') {
          newRows = newRows.filter(row => row.full_name && row.full_name.toLowerCase().search(this.state.full_name.toLowerCase()) !== -1);
        }
 
        if(this.state.status != '') {
          newRows = newRows.filter(row => row.status == this.state.status);
        }       
       
       this.setState({rows: newRows});
    }
    
    render() {
        
        //const {isLoading} = this.state;
        const { phoneTxt, nameTxt, adrsTxt,
          emailTxt, crtNewAccntTxt, actionTxt, usersTxt} = this.props.selectedLanguage;

        return (
           
         <Fragment>
          <Helmet><title>{usersTxt}</title></Helmet>

          <div className="row">
                        
      <div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item active" aria-current="page">{usersTxt}</li>
          </ol>
        </nav>
      </div>


      <div className="col-md-12">
        <div className="row">
                <div className="col-md-5">
                    <Link to='/admin/create-user' className="btn btn-primary">
                        {crtNewAccntTxt}
                    </Link>
                </div>

                 
    <div className="col-md-2">
      <input 
      type="text" 
      className="form-control" 
      name="phone"
      placeholder={phoneTxt}
      onKeyUp={this.onValueChange}
        />
    </div>

    <div className="col-md-2">
      <input 
      type="text" 
      className="form-control" 
      name="full_name"
      placeholder={nameTxt}
      onKeyUp={this.onValueChange}
        />
    </div>

              
    <StatusFilter                  
                selectedLanguage={this.props.selectedLanguage}
                onChange={this.onValueChange} 
                />

        </div>
      </div>

          </div>

          <div className="row">
          <div className="col-md-12">
            <div className="card">
              
              <div className="card-header card-header-primary">
                <h4 className="card-title ">{usersTxt}</h4>
                <p className="card-category"></p>
              </div>

              <div className="card-body">
                <div className="table-responsive">
                  <table className="table">
                    <thead className=" text-primary">                    
                      <tr>
                      <th>{'#'}</th>
                      <th>{nameTxt}</th>
                      <th>{phoneTxt}</th>
                      <th>{emailTxt}</th>
                      <th>{adrsTxt}</th>
                      <th>{actionTxt}</th>
                      </tr>                      
                    </thead>
                    <tbody>
                            {this.state.rows ? 
                            this.state.rows.map((item, index) => {
                              return(<UserRow 
                                obj={item} 
                                key={index}
                                num={index + 1}
                                defaultSets = {this.props.defaultSets}
                                selectedLanguage = {this.props.selectedLanguage}
                                userInfo = {this.props.userInfo} />)
                            }) : null
                            }
                            
                    </tbody>
                  </table>
                </div>
              </div> 
            
            </div>
          </div>

          </div>
      
         </Fragment>

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Index);
