import React, { Component, Fragment} from 'react'

import {toast} from 'react-toastify';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import RolesSelect from '../../../components/RolesSelect';
import getData from '../../../helpers/getData';
import putData from '../../../helpers/putData';

 class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: 0,
            firstName: '',
            lastName: '',
            phone: '',
            email: '',
            password: '', 
            adrs: '',
            status:1,
            groupId: 1,
            errorMessage: '',
            selectedRoles: [],
            isDisabled: false,
            
        };

        
        this.onChangeValue = this.onChangeValue.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onRolesSelectChange = this.onRolesSelectChange.bind(this);

    }
  
    componentDidMount(){

      const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);
 
      getData(this.props.defaultSets.apiUrl+'users/'+this.props.match.params.id+'/edit', this.props.userInfo.accessToken)

          //.then(response => response.json())
          .then(response => {

            let roles = [];
            {response.data.roles && response.data.roles.map((item, i) => {
              roles.push({
                "label": eval("item.title"+this.props.selectedLanguage.langCode), 
                "value": item.id
              })
            })};

            this.setState({ 
                id: response.data.id,
                firstName: response.data.firstName,
                lastName: response.data.lastName,
                phone: response.data.phone,
                email: response.data.email,
                adrs: response.data.adrs,
                notes: response.data.notes,
                selectedRoles: roles,                
                status: response.data.status,        
                groupId: response.data.groupId,
              });
              toast.dismiss(ldToast)
              //console.log(response.data)
          })
          .catch(function (error) {
              console.log(error);
          })
    }

    onRolesSelectChange(selectedRoles) {
      this.setState({selectedRoles})
    }

    onChangeValue(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }
    
    handleSubmit(e){

        e.preventDefault();
        this.setState({isDisabled: true});
        const ldToast = toast.info(this.props.selectedLanguage.loadingTxt);

        // prepare selected roles to be sent.
        let roles = [];
        {this.state.selectedRoles && this.state.selectedRoles.map((item, i) => {
          roles.push(item.value);
        })}
        //.

          putData(this.props.defaultSets.apiUrl+'users/'+this.state.id, {
            id: this.props.match.params.id,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            phone: this.state.phone,
            email: this.state.email,
            password: this.state.password,
            adrs: this.state.adrs,
            notes: this.state.notes,
            roles: roles,
            status: this.state.status,
            groupId: this.state.groupId,
            langCode: this.props.selectedLanguage.langCode
        }, this.props.userInfo.accessToken)
            .then( (response) => {

                toast.update(ldToast, {
                    type: toast.TYPE.SUCCESS,
                    render: response.data.msg
                });

                setTimeout( () => {
                    this.props.history.push('/admin/users');
                }, 3000);
            })
            .catch( (error) => {
                //console.log(error.response);
                this.setState({  isDisabled: false})

                if(error.response !== undefined && error.response.status === 422){
                    let errorTxt = '';
                    let errorMessage = error.response.data.errors;

                    Object.keys(errorMessage).map((key, i) => {
                        errorTxt = (<span> {errorTxt} {errorMessage[key][0]} <br /></span>);});
                    toast.update(ldToast, {
                        type: toast.TYPE.ERROR,
                        render: errorTxt
                    });
                }
            });

    }


    render() {
        
        const {isDisabled,firstName, lastName, phone, email, adrs, notes, 
          selectedRoles, groupId,
          status} = this.state;
        
        const { phoneTxt, passwordTxt, firstNameTxt, lastNameTxt, updtAccntTxt,
          notesTxt, adrsTxt, saveTxt, emailTxt,statusTxt, activeTxt, typeTxt,
          inActiveTxt , usersTxt, rolesTxt, adminTxt, clientTxt} = this.props.selectedLanguage;

        return (
          <div className="row">

<Helmet><title>{updtAccntTxt}</title></Helmet> 

                  
<div className="col-md-12">
        
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><Link to="/admin"><i className="fa fa-home"></i></Link></li>
            <li className="breadcrumb-item"><Link to="/admin/users">{usersTxt}</Link></li>
            <li className="breadcrumb-item active" aria-current="page">{updtAccntTxt}</li>
          </ol>
        </nav>
      </div>

          <div className="col-md-8">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">{updtAccntTxt}</h4>
                <p className="card-category"></p>
              </div>
              <div className="card-body">
                <form>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{firstNameTxt}</label>
                         
                        <input 
                        className="form-control"
                        type="text"
                        name="firstName"
                        value={firstName}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{lastNameTxt}</label>                            
                        <input 
                        className="form-control"
                        type="text"
                        name="lastName"
                        value={lastName}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>

                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{phoneTxt}</label>                           
                        <input 
                        className="form-control"
                        type="text"
                        name="phone"
                        value={phone}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <label className="bmd-label-floating">{passwordTxt}</label>                       
                        <input 
                        className="form-control"
                        type="password"
                        name="password"
                        /*autoComplete={false}*/
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                  </div>
                  
                  
                  <div className="row">                      

<div className="col-md-12">
    <div className="form-group">
      <label className="bmd-label-floating">{emailTxt}</label>                            
      <input 
      className="form-control"
      type="text"
      name="email"
      value={email}
      onChange={this.onChangeValue} />
    </div>
</div>

</div>

                  <div className="row">
                      <div className="col-md-6">
                      <label className="bmd-label-floating">{rolesTxt}</label>                     

      
                          {<RolesSelect
                          selected={selectedRoles}
                          onRolesSelectChange={this.onRolesSelectChange}
                          defaultSets={this.props.defaultSets}
                          selectedLanguage={this.props.selectedLanguage} />}
                      </div>

                      
<div className="col-md-6">
  <div className="form-group">
  <label className="bmd-label-floating">{typeTxt}</label>                     

    <select 
    value={groupId}
    name="groupId"
    className="form-control"
    onChange={this.onChangeValue}>
      <option value="1">{adminTxt}</option>
      <option value="2">{clientTxt}</option>
    </select>
  </div>
</div>

                  </div>  

                  <div className="row">

                    <div className="col-md-12">
                      <div className="form-group">
                        <label className="bmd-label-floating">{statusTxt}</label>                     
                        <select 
                        value={status}
                        name="status"
                        className="form-control"
                        onChange={this.onChangeValue}>
                          <option value="1">{activeTxt}</option>
                          <option value="0">{inActiveTxt}</option>
                        </select>
                      </div>
                    </div>

                  </div>
                  <div className="row mt-3">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label className="bmd-label-floating">{adrsTxt}</label>                     
                        <input 
                        className="form-control"
                        type="text"
                        name="adrs"
                        value={adrs}
                        onChange={this.onChangeValue} />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        <label></label>
                        <div className="form-group">
                          <label className="bmd-label-floating">{notesTxt}</label>
                          <textarea className="form-control" 
                          rows="5" name="notes"
                          value={notes}
                          onChange={this.onChangeValue}>
                          </textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button 
                  type="submit" 
                  onClick={this.handleSubmit}
                  disabled={isDisabled}
                  className="btn btn-primary pull-right">{saveTxt}
                  </button>
                  <div className="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
          
        </div>
      
        

        )
    }
}


const mapStateToProps = (state) => {
    return {
        selectedLanguage: state.LanguageReducer.slctdLng,
        defaultSets: state.DfltSts,
        userInfo: state.User.info
    }
}
export default connect(mapStateToProps, null)(Edit);
