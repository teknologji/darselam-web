const INITIAL_STATE = {
    info: {
        id: 0,
        group_id: 2,
        full_name: '',
        phone: '',
        contactLang: 'ar',
        super:0,
        countryPhoneCode: '',
        countryCode: 'SA',
        userPermissions: []
    }

};

export const User  = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case 'SAVE_USER':
      //return console.log(action.payload)
      return {
        info: action.payload
      };
    case 'LOGOUT':
    return INITIAL_STATE;

    default:
     return state;
  }
};

export default User;
