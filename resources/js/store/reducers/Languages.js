
const INITIAL_STATE = {rows: []};

export const Languages  = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'SAVE_LANGUAGES':
            return {
                rows: action.payload
            };
        default:
            return state;
    }
};

export default Languages;
