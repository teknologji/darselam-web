import { combineReducers } from 'redux'

import LanguageReducer from './LanguageReducer';
import cartItems from './cartItems';
import User from './User';
import DfltSts from './DfltSts';
import Cats from './Cats';
import Favourites from './Favourites';
import Products from './Products';
import Features from './Features';
import Currs from './Currs';
import Languages from './Languages';
import CompareList from './CompareList';

export default combineReducers({
    cartItems,
    LanguageReducer,
    User,
    DfltSts,
    Cats,
    Favourites,
    Products,
    Features,
    Currs,
    Languages,
    CompareList

});