
const INITIAL_STATE = {rows: []};

export const Features  = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'SAVE_FEATURES':
            return {
                rows: action.payload
            };
        default:
            return state;
    }
};

export default Features;
