
import React from 'react'
import {toast} from 'react-toastify';

import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

 export default function RmvFileFrmDB(apiUrl, id, dep, loadingTxt) {
  const ldToast = toast.info(<FontAwesomeIcon icon={faSpinner} spin  />);

   axios.get(apiUrl+'remove-file/'+id+'/'+dep) 
      .then( (response) => {
          toast.update(ldToast, {
              type: toast.TYPE.SUCCESS,
              render: response.data.msg
          });  
      })
      .catch( (error) => {
          //console.log(error.response);               
      });
}

