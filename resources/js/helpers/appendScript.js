/* export const appendScript = (scriptToAppend) => {
    
    const script = document.createElement("script");
    script.src = scriptToAppend;
    script.async = false;
    //script.defer=true;
    document.body.appendChild(script);
} */

export const  appendScript = (src) => {
    return new Promise(function(resolve, reject) {
      let script = document.createElement('script');
      script.src = src;
  
      script.onload = () => resolve(script);
      script.onerror = () => reject(new Error(`Script load error for ${src}`));
  
      document.body.appendChild(script);
    });
  }
  
export const appendCss = (cssToAppend) => {

    return new Promise((resolve, reject) => {       
              
    var head = document.getElementsByTagName('head')[0];  

    var link = document.createElement("link");
    link.rel = 'stylesheet';  
    link.type = 'text/css'; 
    link.href = cssToAppend;     
    document.head.appendChild(link);
    resolve(true);
      });

}