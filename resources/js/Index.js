import React, { Component, createContext } from 'react';
import ReactDOM from 'react-dom';
 
//import {Route, HashRouter as Router } from 'react-router-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';

//import { ScrollContext } from 'react-router-scroll-4';

import AdminHome from "./views/admin/HomeScreen";
import FrontHome from "./views/frontend/HomeScreen";

import AppRoute from './routes/AppRoute';

import AgentsIndex from './views/frontend/agent/Index';
import AgentsList from './views/frontend/agent/Agents';
import BeAgent from './views/frontend/agent/BeAgent';

import AdminLayout from './views/admin/Layout';
import FrontLayout from './views/frontend/Layout';

import AllUsers from './views/admin/user/Index';
import CreateUser from './views/admin/user/Create';
import editUser from './views/admin/user/Edit';

import AllRoles from './views/admin/role/Index';
import CreateRole from './views/admin/role/Create';
import EditRole from './views/admin/role/Edit';

import Newsletters from './views/admin/newsletter/Index';

import AllColours from './views/admin/colour/Index';
import CreateColour from './views/admin/colour/Create';
import EditColour from './views/admin/colour/Edit';

import AllLanguages from './views/admin/language/Index';
import CreateLanguage from './views/admin/language/Create';
import EditLanguage from './views/admin/language/Edit';

import AllCurrencies from './views/admin/currency/Index';
import CreateCurrency from './views/admin/currency/Create';
import EditCurrency from './views/admin/currency/Edit';

import AllFeatures from './views/admin/feature/Index';
import CreateFeature from './views/admin/feature/Create';
import EditFeature from './views/admin/feature/Edit';
import EditFeatureTrans from './views/admin/feature/EditTrans';
import CreateFeatureTrans from './views/admin/feature/CreateTrans';

import ShowFeature from './views/frontend/feature/Show';

import AllSubcats from './views/admin/subcat/Index';
import CreateSubcat from './views/admin/subcat/Create';
import EditSubcat from './views/admin/subcat/Edit';

import AllCargos from './views/admin/cargo/Index';
import CreateCargo from './views/admin/cargo/Create';
import EditCargo from './views/admin/cargo/Edit';

import AllCoupons from './views/admin/coupon/Index';
import CreateCoupon from './views/admin/coupon/Create';
import EditCoupon from './views/admin/coupon/Edit';

import AllSizes from './views/admin/size/Index';
import CreateSize from './views/admin/size/Create';
import EditSize from './views/admin/size/Edit';

import PrintOrder from './views/admin/order/Print';

import Contact from './views/frontend/Contact';
import ContactIndex from './views/admin/contact/Index';

import AllCategories from './views/admin/category/Index';
import CreateCategory from './views/admin/category/Create';
import EditCategory from './views/admin/category/Edit';
import EditCategoryTrans from './views/admin/category/EditTrans';
import CreateCategoryTrans from './views/admin/category/CreateTrans';
import ShowCategory from './views/frontend/category/Show';


import AllProductRegister from './views/admin/product/ProductRegister';
import AllProducts from './views/admin/product/Index';
import CreateProduct from './views/admin/product/Create';
import EditProduct from './views/admin/product/Edit';
import EditProductTrans from './views/admin/product/EditTrans';
import CreateProductTrans from './views/admin/product/CreateTrans';
import CreateProductAccessory from './views/admin/product/CreateAccessory';
import ShowProduct from './views/frontend/product/Show';
import VerifyProduct from './views/frontend/product/Verify';
//import ShowProducts from './views/frontend/product/Index';
import AllArticles from './views/admin/article/Index';
import CreateArticle from './views/admin/article/Create';
import EditArticle from './views/admin/article/Edit';
import ShowArticle from './views/frontend/article/Show';

import EditArticleTrans from './views/admin/article/EditTrans';
import CreateArticleTrans from './views/admin/article/CreateTrans';

import AllGuarantees from './views/admin/guarantee/Index';
import CreateGuarantee from './views/admin/guarantee/Create';
import EditGuarantee from './views/admin/guarantee/Edit';

import AllTabs from './views/admin/tab/Index';
import CreateTab from './views/admin/tab/Create';
import EditTab from './views/admin/tab/Edit';

import EditTabTrans from './views/admin/tab/EditTrans';
import CreateTabTrans from './views/admin/tab/CreateTrans';


import AllCustommsg     from './views/admin/custommsg/Index';
import CreateCustommsg  from './views/admin/custommsg/Create';
import EditCustommsg    from './views/admin/custommsg/Edit';


import AllServices from './views/admin/service/Index';
import CreateService from './views/admin/service/Create';
import EditService from './views/admin/service/Edit';
import ShowService from './views/frontend/service/Show';

import AllFaqs from './views/admin/faq/Index';
import CreateFaq from './views/admin/faq/Create';
import EditFaq from './views/admin/faq/Edit';
import FaqsIndex from './views/frontend/faq/Index';

import EditFaqTrans from './views/admin/faq/EditTrans';
import CreateFaqTrans from './views/admin/faq/CreateTrans';

import AllCpages from './views/admin/cpage/Index';
import CreateCpage from './views/admin/cpage/Create';
import EditCpage from './views/admin/cpage/Edit';
import ShowCpage from './views/frontend/cpage/Show';
import EditCpageTrans from './views/admin/cpage/EditTrans';
import CreateCpageTrans from './views/admin/cpage/CreateTrans';

import EditSettings from './views/admin/setting/Edit';

import Cart from './views/frontend/order/Cart';
import Compare from './views/frontend/compare/Index';
//import Checkout from './views/frontend/order/Checkout';
import CreateShipmentAdrs from './views/frontend/shipmentadrs/Create';
import UpdateShipmentAdrs from './views/frontend/shipmentadrs/Edit';
import ShipmentAdrs from './views/frontend/shipmentadrs/Index';
import UsersOrders from './views/frontend/order/Index';
import AllOrders from './views/admin/order/Index';
import UpdateProfile from './views/auth/Profile';

import { Provider } from 'react-redux';
import {persistor, store} from './store';
import { PersistGate } from 'redux-persist/integration/react';
import Login from './views/auth/Login';
import Register from './views/auth/Register';
import Activation from './views/auth/Activation';
import AllFavourites from './views/frontend/favourite/Index';

export default class Index extends Component {
    render() {
        return ( 
            <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>

            <Router>
                <Switch>
                    
                <AppRoute layout={FrontLayout} exact path="/" component={FrontHome} />
                <AppRoute layout={FrontLayout} exact path="/login" component={Login} />
                <AppRoute layout={FrontLayout} exact path="/register" component={Register} />
                <AppRoute layout={FrontLayout} exact path="/activation" component={Activation} />
                <AppRoute layout={FrontLayout} userGroup="client" privateArea exact path="/profile" component={UpdateProfile} />

                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="EditSettings" exact path="/admin/settings" component={EditSettings} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AdminHome" exact path="/admin" component={AdminHome} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllUsers" exact path="/admin/users" component={AllUsers} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="CreateUser" exact path="/admin/create-user" component={CreateUser} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="EditUser" exact path="/admin/edit-user/:id" component={editUser} />
                
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllProducts" exact path="/admin/products" component={AllProducts} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="CreateProduct" exact path="/admin/create-product" component={CreateProduct} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="EditProduct" exact path="/admin/edit-product/:id" component={EditProduct} />
                
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditProductTrans" path="/admin/edit-product-trans/:id" component={EditProductTrans} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateProductTrans" path="/admin/create-product-trans/:id" component={CreateProductTrans} />
                
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateProductAccessory" path="/admin/create-product-accessory/:id" component={CreateProductAccessory} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="AllProductRegister" path="/admin/register-product" component={AllProductRegister} />

                <AppRoute layout={FrontLayout} exact path="/products/:id" component={ShowProduct} />
                {/* <AppRoute layout={FrontLayout} exact path="/products" component={ShowProducts} /> */}
                <AppRoute layout={FrontLayout} exact path="/verify-product" component={VerifyProduct} />


                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllCategories" exact path="/admin/categories" component={AllCategories} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="CreateCategory" exact path="/admin/create-category" component={CreateCategory} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="EditCategory" exact path="/admin/edit-category/:id" component={EditCategory} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditCategoryTrans" path="/admin/edit-category-trans/:id" component={EditCategoryTrans} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateCategoryTrans" path="/admin/create-category-trans/:id" component={CreateCategoryTrans} />
                <AppRoute layout={FrontLayout} exact path="/categories/:id" component={ShowCategory} />

                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllRoles" exact path="/admin/roles" component={AllRoles} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="CreateRole" exact path="/admin/create-role" component={CreateRole} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditRole" path="/admin/edit-role/:id" component={EditRole} />

                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="Newsletters" path="/admin/newsletters" component={Newsletters} />

                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="AllColours" path="/admin/colours" component={AllColours} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateColour" path="/admin/create-colour" component={CreateColour} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditColour" path="/admin/edit-colour/:id" component={EditColour} />
                
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="AllCurrencies" path="/admin/currencies" component={AllCurrencies} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateCurrency" path="/admin/create-currency" component={CreateCurrency} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditCurrency" path="/admin/edit-currency/:id" component={EditCurrency} />
                  
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="AllLanguages" path="/admin/languages" component={AllLanguages} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateLanguage" path="/admin/create-language" component={CreateLanguage} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditLanguage" path="/admin/edit-language/:id" component={EditLanguage} />
                  
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="AllFeatures" path="/admin/features" component={AllFeatures} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateFeature" path="/admin/create-feature" component={CreateFeature} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditFeature" path="/admin/edit-feature/:id" component={EditFeature} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditFeatureTrans" path="/admin/edit-feature-trans/:id" component={EditFeatureTrans} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateFeatureTrans" path="/admin/create-feature-trans/:id" component={CreateFeatureTrans} />
                <AppRoute layout={FrontLayout} exact path="/features/:id" component={ShowFeature} />

                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="AllSubcats" path="/admin/subcats" component={AllSubcats} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateSubcat" path="/admin/create-subcat" component={CreateSubcat} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditSubcat" path="/admin/edit-subcat/:id" component={EditSubcat} />
                 
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="AllCargos" path="/admin/cargos" component={AllCargos} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateCargo" path="/admin/create-cargo" component={CreateCargo} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditCargo" path="/admin/edit-cargo/:id" component={EditCargo} />
                 
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="AllCoupons" path="/admin/coupons" component={AllCoupons} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateCoupon" path="/admin/create-coupon" component={CreateCoupon} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditCoupon" path="/admin/edit-coupon/:id" component={EditCoupon} />
                
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllSizes" exact path="/admin/sizes" component={AllSizes} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="CreateSize" exact path="/admin/create-size" component={CreateSize} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="EditSize" exact path="/admin/edit-size/:id" component={EditSize} />
                
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllGuarantees" exact path="/admin/guarantees" component={AllGuarantees} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="CreateGuarantee" exact path="/admin/create-guarantee" component={CreateGuarantee} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="EditGuarantee" exact path="/admin/edit-guarantee/:id" component={EditGuarantee} />
                
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllTabs" exact path="/admin/tabs" component={AllTabs} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="CreateTab" exact path="/admin/create-tab" component={CreateTab} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="EditTab" exact path="/admin/edit-tab/:id" component={EditTab} />
                
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditTabTrans" path="/admin/edit-tab-trans/:id" component={EditTabTrans} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateTabTrans" path="/admin/create-tab-trans/:id" component={CreateTabTrans} />
                
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllArticles" exact path="/admin/articles" component={AllArticles} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="CreateArticle" exact path="/admin/create-article" component={CreateArticle} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="EditArticle" exact path="/admin/edit-article/:id" component={EditArticle} />
                <AppRoute layout={FrontLayout} exact path="/articles/:id" component={ShowArticle} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditArticleTrans" path="/admin/edit-article-trans/:id" component={EditArticleTrans} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateArticleTrans" path="/admin/create-article-trans/:id" component={CreateArticleTrans} />
                

                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllCustommsgs" exact path="/admin/custommsgs" component={AllCustommsg} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="CreateCustommsg" exact path="/admin/create-custommsg" component={CreateCustommsg} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="EditCustommsg" exact path="/admin/edit-custommsg/:id" component={EditCustommsg} />
                
         
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea  exact path="/admin/services" component={AllServices} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact path="/admin/create-service" component={CreateService} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact path="/admin/edit-service/:id" component={EditService} />
                <AppRoute layout={FrontLayout} exact path="/services" component={ShowService} />

                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllCpages" exact path="/admin/cpages" component={AllCpages} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="CreateCpage" exact path="/admin/create-cpage" component={CreateCpage} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="EditCpage" exact path="/admin/edit-cpage/:id" component={EditCpage} />
                <AppRoute layout={FrontLayout} exact path="/cpages/:id" component={ShowCpage} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditCpageTrans" path="/admin/edit-cpage-trans/:id" component={EditCpageTrans} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateCpageTrans" path="/admin/create-cpage-trans/:id" component={CreateCpageTrans} />
                

                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllFaqs" exact path="/admin/faqs" component={AllFaqs} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="CreateFaq" exact path="/admin/create-faq" component={CreateFaq} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="EditFaq" exact path="/admin/edit-faq/:id" component={EditFaq} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="EditFaqTrans" path="/admin/edit-faq-trans/:id" component={EditFaqTrans} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact permissionName="CreateFaqTrans" path="/admin/create-faq-trans/:id" component={CreateFaqTrans} />
                

                <AppRoute layout={FrontLayout} exact path="/contact" component={Contact} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllContacts" exact path="/admin/contact" component={ContactIndex} />

                <AppRoute layout={FrontLayout} exact path="/cart" component={Cart} />
                <AppRoute layout={FrontLayout} exact path="/compare" component={Compare} />
                {/* <AppRoute layout={FrontLayout} exact path="/checkout" component={Checkout} /> */}

                <AppRoute layout={FrontLayout} userGroup="client" privateArea  exact path="/create-shipment-adrs" component={CreateShipmentAdrs} />                    
                <AppRoute layout={FrontLayout} userGroup="client" privateArea  exact path="/shipment-adrs" component={ShipmentAdrs} />                    
                <AppRoute layout={FrontLayout} userGroup="client" privateArea  exact path="/edit-shipment-adrs/:id" component={UpdateShipmentAdrs} />                    
                <AppRoute layout={FrontLayout} userGroup="client" privateArea exact path="/favourites" component={AllFavourites} />
                
                <AppRoute layout={FrontLayout} exact path="/faqs" component={FaqsIndex} />
                <AppRoute layout={FrontLayout} exact path="/agents" component={AgentsIndex} />
                <AppRoute layout={FrontLayout} exact path="/be-agent" component={BeAgent} />
                <AppRoute layout={FrontLayout} exact path="/agents/:regionId" component={AgentsList} />

                <AppRoute layout={FrontLayout} userGroup="client" privateArea  exact path="/orders" component={UsersOrders} />                    
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="AllOrders"  exact path="/admin/orders" component={AllOrders} />
                <AppRoute layout={AdminLayout} userGroup="admin" privateArea permissionName="printOrder" exact path="/admin/print-order/:id" component={PrintOrder} />

                <AppRoute layout={AdminLayout} userGroup="admin" privateArea exact path="/admin" component={AdminHome} />                    
                <Route render={() => <div>404 Not Found</div>} />

                </Switch>
  
            </Router>
            </PersistGate>

            </Provider>

        );
    }
}

if (document.getElementById('home')) {
    ReactDOM.render(<Index />, document.getElementById('home'));
}
