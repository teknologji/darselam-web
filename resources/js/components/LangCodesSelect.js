import React, { Component } from 'react';

class LangCodesSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
          rows: []
        }
        //this.deleteRow = this.deleteRow.bind(this);
    }

    componentDidMount() {
      
      axios.get(this.props.defaultSets.apiUrl+'languages/status/1')
      .then( (response) => {
          this.setState({rows: response.data.rows});
          //console.log(response.data.rows)
      })
      .catch( (error) => {
          //console.log(error.response);               
      });
    }

    render() {
        const {rows} = this.state;
        const lng = this.props.selectedLanguage;
        const {languageCode, onChangeValue} = this.props;
        
        return (
 
          <select 
          name="languageCode"
          defaultValue={languageCode}
          className="form-control"
          onChange={onChangeValue}>
            <option value={''}>{lng.chooseTxt}</option>

            {rows && rows.map((item, i) => {
              return(<option key={i} value={item.languageCode}>{item.title}</option>)

            })}
            
          </select>
        );
    }
}


export default LangCodesSelect;
