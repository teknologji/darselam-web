import React from 'react'
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
//import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux';

 const RowThumbnail = (props) => {
  
    return(<div className='fadein'> 
    {
      <div 
      className='delete'  
      onClick={
          () => {
              props.rmvMnImg ?
              props.rmvMnImg() : props.rmvOthrImgs(props.imgId)
            }
    }  
    > 
    { (props.rmvMnImg || props.rmvOthrImgs) && props.imgName != null ? 'X': ''}
    </div>
    } 
    { props.imgName &&
      <img src={props.defaultSets.mainUrl+'uploads/files/'+props.imgName} 
      style={{width: props.width ? props.width : '50px', height: props.height ? props.height : '50px'}} />
    }
  </div>)
  }


const mapStateToProps = (state) => {
  return {
      defaultSets: state.DfltSts
  }
}
export default connect(mapStateToProps, null)(RowThumbnail);