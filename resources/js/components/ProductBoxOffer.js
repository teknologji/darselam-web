import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class ProductBoxOffer extends Component {
    constructor(props) {
        super(props);
        //this.deleteRow = this.deleteRow.bind(this);
    }

    render() {
        const {titleAR, titleTR, titleEN, status, photo, id,
             priceBeforeDiscount, priceAfterDiscount, category
        } = this.props.obj;
        const {langCode, addToCartTxt, addToFavTxt, addedToCartTxt} = this.props.selectedLanguage;
        
        return (

            
            <div className="col-md-12">
            
            <div className="product__discount__item">
            <div className="product__discount__item__pic set-bg" 
                style={{backgroundImage: "url(/uploads/files/"+photo+")"}}
                data-setbg={"/uploads/files/"+photo}>
                    <div className="product__discount__percent">-20%</div>
                    <ul className="product__item__pic__hover">
                        <li><a href="#" onClick={(e) => {
                            e.preventDefault();
                            this.props.addToFav(this.props.obj)
                            }
                        }>
                        <i className={(this.props.favedItems.some(item => item.id === id))? "fa fa-heart text-danger": "fa fa-heart"}></i>
                        </a></li>
                        <li><a href="#"><i className="fa fa-retweet"></i></a></li>
                        <li><a href="#"
                        onClick={(e) => { 
                            e.preventDefault(); this.props.addToCart(this.props.obj, addedToCartTxt);
                        }}><i className="fa fa-shopping-cart"></i></a></li>
                    </ul>
                </div>
                <div className="product__discount__item__text">
                    <span>{category && eval("category.title"+langCode)}</span>
                    <h5>
                    <Link to={"/products/"+id}>{eval("title"+langCode)}</Link>
                    </h5>
                    <div className="product__item__price">
                    {priceAfterDiscount == priceBeforeDiscount || priceAfterDiscount == 0 ?
                            <h5>{priceBeforeDiscount}</h5>
                        :<h5>{priceAfterDiscount} <del className="text-danger">{priceBeforeDiscount}</del></h5>}
                          

                    </div>
                </div>
            </div>
        
        </div>


            );
    }
}


export default ProductBoxOffer;
