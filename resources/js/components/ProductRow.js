import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import RowThumbnail from './RowThumbnail';
import deleteData from '../helpers/deleteData';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import getData from '../helpers/getData';

class ProductRow extends Component {
    constructor(props) {
        super(props);
        //this.deleteRow = this.deleteRow.bind(this);
    }

   
    deleteRow(id) {

      confirmAlert({
            
        customUI: ({ onClose }) => {
            return (
                <div id="react-confirm-alert">
                    <div className="react-confirm-alert-overlay">
                        <div className="react-confirm-alert">
                            <div className="react-confirm-alert-body">
                                <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                <div 
                                className="react-confirm-alert-button-group">
                                <button
                                className="yes"
                                onClick={() => {
                                    deleteData(this.props.defaultSets.apiUrl+'products/'+id, this.props.userInfo.accessToken)
                                    .then(res => {
                                        document.getElementById(id).remove();            
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    });
                                    onClose();
                                }}
                                >
                                        {this.props.selectedLanguage.yesTxt}
                                    </button>

                                    <button className="no" onClick={onClose}>
                                    {this.props.selectedLanguage.noTxt}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            );
          }
      });
  

    }

    
    deleteAccessory(productId, accessoryId) {

        confirmAlert({
              
          customUI: ({ onClose }) => {
              return (
                  <div id="react-confirm-alert">
                      <div className="react-confirm-alert-overlay">
                          <div className="react-confirm-alert">
                              <div className="react-confirm-alert-body">
                                  <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                  <div                                   
                                  className="react-confirm-alert-button-group">
                                  <button
                                  className="yes"
                                  onClick={() => {
                                      getData(this.props.defaultSets.apiUrl+'product-accessories/'+productId+'/'+accessoryId, this.props.userInfo.accessToken)
                                      .then(res => {
                                          document.getElementById(productId+'k'+accessoryId).remove();            
                                      })
                                      .catch(err => {
                                          console.log(err);
                                      });
                                      onClose();
                                  }}
                                  >
                                          {this.props.selectedLanguage.yesTxt}
                                      </button>
  
                                      <button className="no" onClick={onClose}>
                                      {this.props.selectedLanguage.noTxt}
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
  
              );
            }
        });
    
  
      }
  

      
    deleteLang(id) {

        confirmAlert({
              
          customUI: ({ onClose }) => {
              return (
                  <div id="react-confirm-alert">
                      <div className="react-confirm-alert-overlay">
                          <div className="react-confirm-alert">
                              <div className="react-confirm-alert-body">
                                  <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                  <div 
                                  className="react-confirm-alert-button-group">
                                  <button
                                  className="yes"
                                  onClick={() => {
                                      deleteData(this.props.defaultSets.apiUrl+'products-trans/'+id, this.props.userInfo.accessToken)
                                      .then(res => {
                                          document.getElementById(id).remove();            
                                      })
                                      .catch(err => {
                                          console.log(err);
                                      });
                                      onClose();
                                  }}
                                  >
                                          {this.props.selectedLanguage.yesTxt}
                                      </button>
  
                                      <button className="no" onClick={onClose}>
                                      {this.props.selectedLanguage.noTxt}
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
  
              );
            }
        });
    
  
      }
  
    render() {
        const item = this.props.obj;
        //console.log(this.props.obj)
        const lng = this.props.selectedLanguage;
        const itemTrans = item.elment_trans.find(item => item.languageCode == lng.langCode);

        return (
            <tr id={item.id} key={'prdct'+item.id}>
                <td>{this.props.keyNum}</td>
                <td>{itemTrans && itemTrans.title}</td>
                
                <td>
                    {item.elment_trans && item.elment_trans.map((aLang, k) => {
                        return(
                            <div className="row" id={aLang.id} key={'aLang'+aLang.id}>
                                <div className="col-4">
                                    {aLang.languageCode}
                                </div>
                                
                                <div className="col-4">
                                <input type="button" 
                                value={lng.deleteTxt} 
                                onClick={() => this.deleteLang(aLang.id)}
                                className="btn btn-danger btn-sm"/>
                                </div>
                                
                                <div className="col-4">
                                <Link 
                                to={"/admin/edit-product-trans/"+aLang.id}
                                className="btn btn-sm btn-primary">{lng.updateTxt}</Link>
                                </div>
                            </div>
                        );
                    })}
                    <div className="row">
                    <Link 
                        to={"/admin/create-product-trans/"+item.id}
                        className="btn btn-sm btn-success">{lng.crtNewTxt}
                    </Link>
                    </div>
                </td>
                

                <td>
                    {item.cats && item.cats.map((cat, k) => {
                    return(<p key={'cat'+cat.id}>{eval("cat.title"+lng.langCode)}</p>)
                    })}
                </td>
                
                <td>
                    {item.accessories && item.accessories.map((accessory, k) => {
    let accessoryTrns = accessory.elment_trans && accessory.elment_trans.find(prdct => prdct.languageCode == this.props.selectedLanguage.langCode);

                        return(
                            <div className="row" id={item.id+'k'+accessory.id} key={'accessory'+accessory.id}>
                                <div className="col-6">
                                    {accessoryTrns && accessoryTrns.title}
                                </div>
                                
                                <div className="col-3">
                                <input type="button" 
                                value={lng.deleteTxt} 
                                onClick={() => this.deleteAccessory(item.id, accessory.id)}
                                className="btn btn-danger btn-sm"/>
                                </div>
                            </div>
                        );
                    })}
                    
                    <div className="row">
                    <Link 
                        to={"/admin/create-product-accessory/"+item.id}
                        className="btn btn-sm btn-success">{lng.addAsAccessoryTxt}
                    </Link>
                    </div>
                    
                </td>
                
                <td>{item.priceBeforeDiscount}</td>
                <td>{item.priceAfterDiscount}</td>               
                <td>{item.product_props && item.product_props.map((colour, clrId) => {
                  return(<span key={'clr'+clrId}>{eval("colour.title"+lng.langCode)} - </span>)
                })}</td>
                
                {/* <td>{size && eval("size.title"+lng.langCode)}</td> */} 
                <td>{item.sku}</td>
                <td>{item.status == 1 ? lng.activeTxt : inlng.activeTxt}</td>
                {/* 
                <td><RowThumbnail imgName={item.photo} /></td>
                <td><RowThumbnail imgName={item.photo2} /></td> */}
                <td>

                        <Link 
                        to={{
                            pathname: "/admin/edit-product/"+item.id,
                            //data: this.props.obj // your data array of objects
                          }}
                        className="btn btn-primary">{lng.updateTxt}</Link>
                        <input type="button" 
                        value={lng.deleteTxt} 
                        onClick={() => this.deleteRow(item.id)}
                        className="btn btn-danger"/>
                    
                </td>
            </tr>
        );
    }
}


export default ProductRow;
