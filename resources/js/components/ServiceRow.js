import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import RowThumbnail from './RowThumbnail';
import deleteData from '../helpers/deleteData';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
 
class ServiceRow extends Component {
    constructor(props) {
        super(props);
        //this.deleteRow = this.deleteRow.bind(this);
    }

    
    deleteRow(id) {

      confirmAlert({
            
        customUI: ({ onClose }) => {
            return (
                <div id="react-confirm-alert">
                    <div className="react-confirm-alert-overlay">
                        <div className="react-confirm-alert">
                            <div className="react-confirm-alert-body">
                                <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                <div 
                                className="react-confirm-alert-button-group">
                                <button
                                className="yes"
                                onClick={() => {
                                    deleteData(this.props.defaultSets.apiUrl+'services/'+id, this.props.userInfo.accessToken)
                                    .then(res => {
                                        document.getElementById(id).remove();            
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    });
                                    onClose();
                                }}
                                >
                                        {this.props.selectedLanguage.yesTxt}
                                    </button>

                                    <button className="no" onClick={onClose}>
                                    {this.props.selectedLanguage.noTxt}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            );
          }
      });
  

    }
    render() {
        const {titleAR, titleTR, titleEN, status, photo, id} = this.props.obj;
        const {updateTxt, deleteTxt, activeTxt, inActiveTxt, langCode} = this.props.selectedLanguage;
        
        return (
            <tr id={id}>
                <td>{this.props.num}</td>
                <td>{eval("title"+langCode)}</td> 
                <td>{status == 1 ? activeTxt : inActiveTxt}</td>
                <td>{<RowThumbnail imgName={photo} />}</td>
                <td>

                        <Link 
                        to={{
                            pathname: "/edit-service/"+id,
                            //data: this.props.obj // your data array of objects
                          }}
                        className="btn btn-primary">{updateTxt}</Link>
                        <input type="button" 
                        value={deleteTxt} 
                        onClick={() => this.deleteRow(id)}
                        className="btn btn-danger"/>
                    
                </td>
            </tr>
        );
    }
}

export default ServiceRow;
