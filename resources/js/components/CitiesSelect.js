import React, { Component } from 'react';

class CitiesSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
          rows: []
        }
        //this.deleteRow = this.deleteRow.bind(this);
    }

    componentDidMount() {
      
      this.fetchCities()

    }

    fetchCities(countryId) {
      axios.get(this.props.defaultSets.apiUrl+'country-cities/'+countryId)
      .then( (response) => {
          this.setState({rows: response.data.rows});
          //console.log(this.state.rows)
      })
      .catch( (error) => {
          //console.log(error.response);               
      });
    }

    componentDidUpdate(prevProps, prevState) {

      if (prevProps.countryId != this.props.countryId ) {
          this.fetchCities(this.props.countryId);
      }
    }

    render() {
        const {rows} = this.state;
        const { langCode, cityTxt} = this.props.selectedLanguage;
        const {dfltValue, onChangeValue, chosenOne, dfltCity} = this.props;
        
        return (
 
          <select 
          name="cityId"
          className="form-control"
          onChange={onChangeValue}>
            <option value={dfltValue? dfltValue: '0'}>{dfltCity? dfltCity : cityTxt}</option>
            {rows && rows.map((item, i) => {
              return(<option key={i} value={item.id}>{eval("item.title"+langCode)}</option>)

            })}
            
          </select>
        );
    }
}


export default CitiesSelect;
