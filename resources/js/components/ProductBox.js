import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class ProductBox extends Component {
    constructor(props) {
        super(props);
        //this.deleteRow = this.deleteRow.bind(this);
    }
 
    render() {
        const item = this.props.obj;
        const slctdCurr = this.props.slctdCurr;
        const lng = this.props.selectedLanguage;
        const elmntTrns = item && item.elment_trans.find(row => row.languageCode == lng.langCode);

        return (

            
            <div className="product-box">{/*  product-wrap */}
            <div className="img-wrapper">
            <div className="lable-block">
        {item.isNew == 1 && <span className="lable3">{lng.newTxt}</span>} 
        {item.priceBeforeDiscount != item.priceAfterDiscount && <span className="lable4">{lng.onSaleTxt}</span>}
                     
                
            </div>

                <div className="front">
                    {elmntTrns && elmntTrns.files[0] ?
                <Link to={"/products/"+item.id}>
                        <img src={"/uploads/files/"+elmntTrns.files[0]['fileName']} 
                        className="img-fluid blur-up lazyload" /*bg-img*/ 
                        alt={elmntTrns.title}  />
                </Link>: null}
                </div>
                <div className="back">
                { elmntTrns && elmntTrns.files[1] ?

                    <Link to={"/products/"+item.id}>
                    <img src={"/uploads/files/"+elmntTrns.files[1]['fileName']} 
                        className="img-fluid blur-up lazyload" /*bg-img*/ 
                        alt={elmntTrns.title}
                        />
                    </Link>: null}
                </div>
                <div className="cart-box">
                    <button 
                    onClick={(e) => {
                        e.preventDefault();
                        this.props.addToCart(this.props.obj, lng.addedToCartTxt)
                        }
                    }/*onclick="openCart()" */
                    title={lng.addToCartTxt}>
                        <i className="ti-shopping-cart"></i>
                    </button>
                    <a href="#" title={lng.addToFavTxt}
                    onClick={(e) => {
                        e.preventDefault();
                        this.props.addToFav(this.props.obj)
                        }
                    }>
                        <i 
                        className={(this.props.favedItems.some(row => row.id === item.id))? 
                            "ti-heart text-danger": "ti-heart"}
                        aria-hidden="true">                            
                        </i>
                    </a>
                    <a href="#" data-toggle="modal" data-target="#quick-view" 
                    title="Quick View">
                        <i className="ti-search" aria-hidden="true"></i>
                    </a>
                    <a href="#" title="Compare" onClick={(e) => {
                        e.preventDefault();
                        this.props.addToCompare(this.props.obj, lng.addedToCompareTxt)
                        }
                    }>
                        <i className="ti-reload" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div className="product-detail text-center">
            <Link to={"/products/"+item.id}>
            <h6 className="pt-3">{elmntTrns && elmntTrns.title}</h6>
            </Link>
                <p className="pt-1">
                {elmntTrns && elmntTrns.description && 
                elmntTrns.description.replace(/(&nbsp;|<([^>]+)>)/ig, "").slice(0,50)
                }
                </p>
                {item.priceAfterDiscount == item.priceBeforeDiscount || item.priceAfterDiscount == 0 ?
                    <h4>{(item.priceBeforeDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}</h4>
                :<h4>{(item.priceAfterDiscount/slctdCurr.exchangeRate).toFixed(2)} {slctdCurr.currencyCode}<del className="text-danger">{(item.priceBeforeDiscount/slctdCurr.exchangeRate).toFixed(2)}</del></h4>}
                
                
            </div>
        
            </div>



            );
    }
}


export default ProductBox;