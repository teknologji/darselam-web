import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import RowThumbnail from './RowThumbnail';
import deleteData from '../helpers/deleteData';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

class CpageRow extends Component {
    constructor(props) {
        super(props);
        //this.deleteRow = this.deleteRow.bind(this);
    }

    deleteLang(id) {

        confirmAlert({
              
          customUI: ({ onClose }) => {
              return (
                  <div id="react-confirm-alert">
                      <div className="react-confirm-alert-overlay">
                          <div className="react-confirm-alert">
                              <div className="react-confirm-alert-body">
                                  <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                  <div                                   
                                  className="react-confirm-alert-button-group">
                                  <button
                                  className="yes"
                                  onClick={() => {
                                      deleteData(this.props.defaultSets.apiUrl+'cpages-trans/'+id, this.props.userInfo.accessToken)
                                      .then(res => {
                                          document.getElementById(id).remove();            
                                      })
                                      .catch(err => {
                                          console.log(err);
                                      });
                                      onClose();
                                  }}
                                  >
                                          {this.props.selectedLanguage.yesTxt}
                                      </button>
  
                                      <button className="no" onClick={onClose}>
                                      {this.props.selectedLanguage.noTxt}
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
  
              );
            }
        });
    
  
      }
  
    deleteRow(id) {

      confirmAlert({
            
        customUI: ({ onClose }) => {
            return (
                <div id="react-confirm-alert">
                    <div className="react-confirm-alert-overlay">
                        <div className="react-confirm-alert">
                            <div className="react-confirm-alert-body">
                                <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                <div 
                                className="react-confirm-alert-button-group">
                                <button
                                className="yes"
                                onClick={() => {
                                    deleteData(this.props.defaultSets.apiUrl+'cpages/'+id, this.props.userInfo.accessToken)
                                    .then(res => {
                                        document.getElementById(id).remove();            
                                    })
                                    .catch(err => {
                                        console.log(err);
                                    });
                                    onClose();
                                }}
                                >
                                        {this.props.selectedLanguage.yesTxt}
                                    </button>

                                    <button className="no" onClick={onClose}>
                                    {this.props.selectedLanguage.noTxt}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            );
          }
      });
  

    }
    render() {
        const item = this.props.obj;
        const lng = this.props.selectedLanguage;
        const elmntTrans = item.elment_trans.find(item => item.languageCode == lng.langCode);

        return (
            <tr id={item.id}>
                <td>{this.props.num}</td>
                <td>{elmntTrans && elmntTrans.title}</td>
                <td>
                    {item.elment_trans && item.elment_trans.map((aLang, k) => {
                        return(
                            <div className="row" id={aLang.id} key={'elmnt'+aLang.id}>
                                <div className="col-2">
                                    {aLang.languageCode}
                                </div>
                                
                                <div className="col-2">
                                <input type="button" 
                                value={lng.deleteTxt} 
                                onClick={() => this.deleteLang(aLang.id)}
                                className="btn btn-danger btn-sm"/>
                                </div>
                                
                                <div className="col-2">
                                <Link 
                                to={"/admin/edit-cpage-trans/"+aLang.id}
                                className="btn btn-sm btn-primary">{lng.updateTxt}</Link>
                                </div>
                            </div>
                        );
                    })}
                    <div className="row">
                    <Link 
                        to={"/admin/create-cpage-trans/"+item.id}
                        className="btn btn-sm btn-success">{lng.crtNewTxt}
                    </Link>
                    </div>
                </td>
                
                <td>{item.status == 1 ? lng.activeTxt : lng.inActiveTxt}</td>
                <td>{<RowThumbnail imgName={item.photo} />}</td>
                <td>

                        <Link 
                        to={{
                            pathname: "/admin/edit-cpage/"+item.id,
                            //data: this.props.obj // your data array of objects
                          }}
                        className="btn btn-primary">{lng.updateTxt}</Link>
                       {[1,2,3].indexOf(item.id) == -1 && 
                        <input type="button" 
                        value={lng.deleteTxt} 
                        onClick={() => this.deleteRow(item.id)}
                        className="btn btn-danger"/>
                        }
                    
                </td>
            </tr>
        );
    }
}

export default CpageRow;
