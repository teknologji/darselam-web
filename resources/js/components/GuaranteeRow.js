import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import deleteData from '../helpers/deleteData';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

class GuaranteeRow extends Component {
    constructor(props) {
        super(props);
        //this.deleteRow = this.deleteRow.bind(this);
    }

    deleteRow(id) {

        confirmAlert({
            
            customUI: ({ onClose }) => {
                return (
                    <div id="react-confirm-alert">
                        <div className="react-confirm-alert-overlay">
                            <div className="react-confirm-alert">
                                <div className="react-confirm-alert-body">
                                    <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                    <div 
                                    className="react-confirm-alert-button-group">
                                    <button
                                    className="yes"
                                    onClick={() => {
                                        deleteData(this.props.defaultSets.apiUrl+'guarantees/'+id, this.props.userInfo.accessToken)
                                        .then(res => {
                                            document.getElementById(id).remove();            
                                        })
                                        .catch(err => {
                                            console.log(err);
                                        });
                                        onClose();
                                    }}
                                    >
                                            {this.props.selectedLanguage.yesTxt}
                                        </button>

                                        <button className="no" onClick={onClose}>
                                        {this.props.selectedLanguage.noTxt}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                );
              }
          });
          

    }

    
    deleteLang(id) {

        confirmAlert({
              
          customUI: ({ onClose }) => {
              return (
                  <div id="react-confirm-alert">
                      <div className="react-confirm-alert-overlay">
                          <div className="react-confirm-alert">
                              <div className="react-confirm-alert-body">
                                  <h1>{this.props.selectedLanguage.rUSureTxt}</h1>
                                  <div 
                                  className="react-confirm-alert-button-group">
                                  <button
                                  className="yes"
                                  onClick={() => {
                                      deleteData(this.props.defaultSets.apiUrl+'guarantees-trans/'+id, this.props.userInfo.accessToken)
                                      .then(res => {
                                          document.getElementById(id).remove();            
                                      })
                                      .catch(err => {
                                          console.log(err);
                                      });
                                      onClose();
                                  }}
                                  >
                                          {this.props.selectedLanguage.yesTxt}
                                      </button>
  
                                      <button className="no" onClick={onClose}>
                                      {this.props.selectedLanguage.noTxt}
                                      </button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
  
              );
            }
        });
    
  
      }
  
    render() {
        const item = this.props.obj;
        const lng = this.props.selectedLanguage;
        const prdctTrans = item.product.elment_trans.find(prdct => prdct.languageCode == lng.langCode);

        return (
            <tr id={item.id}>   
            <td>{prdctTrans && prdctTrans.title}</td> 
                <td>{this.props.num}</td>
                <td>{item.serialNum}</td>
                <td>{item.guaranteeNum}</td>             
                <td>{item.status == 1 ? lng.activeTxt : lng.inActiveTxt}</td>

                <td>

                        <Link 
                        to={{
                            pathname: "/admin/edit-guarantee/"+item.id,
                            //data: this.props.obj // your data array of objects
                          }}
                        className="btn btn-primary">{lng.updateTxt}</Link>
                        <input type="button" 
                        value={lng.deleteTxt} 
                        onClick={() => this.deleteRow(item.id)}
                        className="btn btn-danger"/>
                    
                </td>
            </tr>
        );
    }
}

export default GuaranteeRow;
