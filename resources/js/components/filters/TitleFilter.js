import React, { Component } from 'react';

class TitleFilter extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="col-md-3">
                <input 
                type="text" 
                className="form-control" 
                name="title"
                placeholder={this.props.selectedLanguage.searchByTitleTxt}
                onKeyUp={this.props.onKeyUp}
                />
            </div>

            );
    }
}


export default TitleFilter;
