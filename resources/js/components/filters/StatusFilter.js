import React, { Component } from 'react';

class StatusFilter extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
             
            <div className="col-md-3">
            <select 
            name="status"              
            onChange={this.props.onChange}
            className="form-control">
              <option value="">{this.props.selectedLanguage.searchByStatusTxt}</option>
              <option value="1">{this.props.selectedLanguage.activeTxt}</option>
              <option value="0">{this.props.selectedLanguage.inActiveTxt}</option>
            </select>
            </div>    

            );
    }
}


export default StatusFilter;
