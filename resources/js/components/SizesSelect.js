import React, { Component } from 'react';
import MultiSelect from "react-multi-select-component";
import getData from '../helpers/getData';


class SizesSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {   
          sizes: []
        }
        //this.deleteRow = this.deleteRow.bind(this);
    }

    componentDidMount() {

      getData(this.props.defaultSets.apiUrl+'sizes')
      .then( (response) => {
        let sizes = [];
        {response.data.rows && response.data.rows.map((item, i) => {
          sizes.push({
            "label": eval("item.title"+this.props.selectedLanguage.langCode), 
            "value": item.id
          })
        })};
          this.setState({sizes});

          //console.log(this.state.rows)
      })
      .catch( (error) => {
          //console.log(error.response);               
      });

    }

    render() {
        const {sizes} = this.state;
        const { langCode, chooseTxt, AllSelectedTxt, sizesTxt, searchTxt, selectAllTxt} = this.props.selectedLanguage;
        const {onSizesSelectChange, selected} = this.props;
        
        return (

          <div>
            {/* <pre>{JSON.stringify(selected)}</pre> */}
 
<MultiSelect
options={sizes}
value={selected}
onChange={(selected) => onSizesSelectChange(selected)}
labelledBy={chooseTxt}

overrideStrings={{
  selectSomeItems: chooseTxt,
  allItemsAreSelected: AllSelectedTxt,
  selectAll: selectAllTxt,
  search: searchTxt,
}}

filterOptions={(sizes, filter) => {
  if (!filter) {
    return sizes;
  }
  
  const re = new RegExp(filter, "i");
  return sizes.filter(({ label }) => label && label.match(re));
}}
/>
          </div>
        
        );
    }
}


export default SizesSelect;
