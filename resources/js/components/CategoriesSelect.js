import React, { Component } from 'react';
import getData from '../helpers/getData';

class CategoriesSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
          rows: []
        }
        //this.deleteRow = this.deleteRow.bind(this);
    }

    componentDidMount() {
 
      getData(this.props.defaultSets.apiUrl+'categories/status/1')
      .then( (response) => {
          this.setState({rows: response.data.rows});
          //console.log(this.state.rows)
      })
      .catch( (error) => {
          //console.log(error.response);               
      });

    }

    render() {
        const {rows} = this.state;
        const { langCode, chooseTxt} = this.props.selectedLanguage;
        const {categoryId, onChangeValue} = this.props;
        
        return (
 
          <select 
          value={categoryId}
          name="categoryId"
          className="form-control"
          onChange={onChangeValue}>
            <option value="">{chooseTxt}</option>
            {rows && rows.map((item, i) => {
              let catInfo = item.elment_trans.find(trns => trns.languageCode == this.props.selectedLanguage.langCode);

              return(<option key={i} value={item.id}>{catInfo && catInfo.title}</option>)

            })}
            
          </select>
        );
    }
}


export default CategoriesSelect;
