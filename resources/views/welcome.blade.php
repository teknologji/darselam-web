<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo url('/assets/admin'); ?>/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?php echo url('/assets/admin'); ?>/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Derasa Group
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />

  <link
      rel="icon"
      href="/assets/frontend/images/favicon/3.png"
      type="image/x-icon" 
    />
    <link
      rel="shortcut icon"
      href="/assets/frontend/images/favicon/3.png"
      type="image/x-icon"
    />
    <!--Google font-->
    <link
      href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
      rel="stylesheet"
    />

    </head>
    <body>
                <div id="home">
                </div>

    </body>

    <script type="text/javascript" src="js/app.js?v=11"> </script>


</html>
