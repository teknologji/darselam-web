-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 07, 2020 at 11:48 AM
-- Server version: 5.7.31-0ubuntu0.18.04.1
-- PHP Version: 7.3.21-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `derasa`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0: inactive, 1:active',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `photo` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `status`, `isDeleted`, `photo`, `created_at`, `updated_at`) VALUES
(9, 1, 0, 'e9a6f3dfb558ec74.jpg', '2020-09-10 18:36:05', '2020-09-10 18:36:05'),
(10, 1, 0, '71d9dcad0be61680.jpg', '2020-09-10 18:37:52', '2020-09-10 18:37:52');

-- --------------------------------------------------------

--
-- Table structure for table `articles_trans`
--

CREATE TABLE `articles_trans` (
  `id` int(11) NOT NULL,
  `rowId` int(11) NOT NULL,
  `languageCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'AR',
  `title` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles_trans`
--

INSERT INTO `articles_trans` (`id`, `rowId`, `languageCode`, `title`, `description`) VALUES
(7, 9, 'AR', 'كل بطيخ وانسى المريخ', '<p>كل بطيخ وانسى المريخ&nbsp;كل بطيخ وانسى المريخ&nbsp;كل بطيخ وانسى المريخ&nbsp;كل بطيخ وانسى المريخ&nbsp;&nbsp;</p>'),
(8, 10, 'AR', 'كل مشمش', '<p>كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;كل مشمش&nbsp;</p>'),
(9, 11, 'EN', 'eat watermelon', '<p>eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;</p>'),
(10, 12, 'EN', 'eat watermelon', '<p>eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;</p>'),
(11, 13, 'TR', 'eat watermelon', '<p>eat watermelon&nbsp;eat watermelon&nbsp;</p>'),
(12, 9, 'EN', 'eat watermelon', '<p>eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;eat watermelon&nbsp;</p>'),
(14, 1, 'EN', 'About us', '<p>about us</p>');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleEn` varchar(100) DEFAULT NULL,
  `photo` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `place` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `title`, `titleEn`, `photo`, `place`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'c2605d5c8978c409.jpeg', 'logout', '2019-08-28 17:18:38', '2019-08-28 17:18:38'),
(3, 'اتصل بنا', 'Contact Us', 'effcf6a1a6e0393c.jpg', 'contact', '2019-09-19 11:18:44', '2019-09-19 11:18:44'),
(4, 'الباكيجات', 'Packages', 'effcf6a1a6e0393c.jpg', 'packages', '2019-09-21 05:01:19', '2019-09-21 05:01:19'),
(5, 'الأسئلة الشائعة', 'Faq', 'effcf6a1a6e0393c.jpg', 'faqs', '2019-09-22 11:25:29', '2019-09-22 11:25:29'),
(6, 'FAQ FAQ ', 'FAQ FAQ FAQ', 'b77b31f87de42831.jpg', 'faqs', '2019-11-09 12:37:45', '2019-11-09 12:37:45'),
(7, '11', '11', '2868f446fc5a6e02.jpeg', 'login', '2019-11-09 12:38:27', '2019-11-09 12:38:27'),
(10, '', 'ww', 'fc1e6b9a4e6038d6.jpg', 'login', '2019-11-16 15:37:57', '2019-11-16 15:37:57');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '2020-09-07 11:45:58', '2020-09-07 11:46:02'),
(2, 1, 0, '2020-09-07 11:46:16', '2020-09-07 11:46:22'),
(3, 1, 0, '2020-09-07 15:08:43', '2020-09-07 15:08:43'),
(4, 1, 1, '2020-09-27 11:50:14', '2020-09-27 11:52:50'),
(5, 1, 0, '2020-09-27 11:51:32', '2020-09-27 11:52:56');

-- --------------------------------------------------------

--
-- Table structure for table `categories_trans`
--

CREATE TABLE `categories_trans` (
  `id` int(11) NOT NULL,
  `rowId` int(11) NOT NULL,
  `languageCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories_trans`
--

INSERT INTO `categories_trans` (`id`, `rowId`, `languageCode`, `title`, `description`) VALUES
(1, 1, 'AR', 'معادن', 'وصفم عادن'),
(2, 2, 'AR', 'ذهب', 'وصف ذهب'),
(3, 2, 'EN', 'Gold', 'Gold Description'),
(4, 1, 'EN', 'Metal', 'Metal Description'),
(5, 3, 'AR', 'تصنيف1', NULL),
(6, 9, 'EN', 'eat watermelon', 'eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon eat watermelon'),
(8, 3, 'TR', 'sinif', 'sinif'),
(9, 4, NULL, NULL, ''),
(10, 5, 'AR', 'تصنيف بصور متعددة  02', 'تصنيف بصور متعددة  02');

-- --------------------------------------------------------

--
-- Table structure for table `category_file`
--

CREATE TABLE `category_file` (
  `id` int(11) NOT NULL,
  `rowId` int(11) DEFAULT NULL,
  `fileName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mimeType` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `languageCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category_file`
--

INSERT INTO `category_file` (`id`, `rowId`, `fileName`, `mimeType`, `status`, `languageCode`, `created_at`, `updated_at`) VALUES
(3, 8, 'a7bf28225968d338.jpg', 'image/jpeg', 1, 'TR', '2020-09-27 08:37:32', '2020-09-27 08:37:32'),
(4, 8, 'bc71481639769a44.png', 'image/png', 1, 'TR', '2020-09-27 08:37:32', '2020-09-27 08:37:32'),
(5, 10, 'ba177feeea6916df.jpg', 'image/jpeg', 1, 'AR', '2020-09-27 08:51:32', '2020-09-27 08:51:32'),
(6, 10, '605f2a5398ba2a0f.png', 'image/png', 1, 'AR', '2020-09-27 08:51:32', '2020-09-27 08:51:32');

-- --------------------------------------------------------

--
-- Table structure for table `colours`
--

CREATE TABLE `colours` (
  `id` int(11) NOT NULL,
  `titleAR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleEN` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `colours`
--

INSERT INTO `colours` (`id`, `titleAR`, `titleEN`, `status`, `photo`, `isDeleted`) VALUES
(1, '2016', '2016', 1, NULL, 0),
(2, '2015', '2015', 1, NULL, 0),
(3, '2017', '2017', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `the_content` mediumtext COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `productId`, `userId`, `the_content`, `status`, `created_at`, `updated_at`) VALUES
(1, 406, 8365, 'سبورة جميلة', 1, '2019-05-13 09:36:03', '2019-05-13 09:36:03'),
(3, 406, 8365, 'تعليق ثان تعليق ثان تعليق ثان تعليق ثان تعليق ثان تعليق ثان تعليق ثان تعليق ثان تعليق ثان تعليق ثان تعليق ثان ', 1, '2019-05-13 09:42:04', '2019-05-13 09:42:04'),
(20, 12, 1, '9', 0, '2019-11-12 03:02:04', '2019-11-12 03:02:04'),
(21, 32, 1, 'ب', 0, '2019-11-12 12:13:35', '2019-11-12 12:13:35'),
(25, 0, 999, 'meshmesh', 0, '2019-11-20 06:18:26', '2019-11-20 06:18:26'),
(23, 17, 17035, '22', 0, '2019-11-12 18:59:27', '2019-11-12 18:59:27'),
(26, 0, 999, '', 0, '2019-11-20 06:21:39', '2019-11-20 06:21:39'),
(27, 0, 999, '', 0, '2019-11-20 06:32:16', '2019-11-20 06:32:16'),
(28, 0, 999, '', 0, '2019-11-20 06:32:20', '2019-11-20 06:32:20'),
(29, 0, 999, 'الحمد لله ', 0, '2019-11-20 06:34:09', '2019-11-20 06:34:09'),
(17, 5, 1, '99', 0, '2019-11-12 02:49:07', '2019-11-14 06:11:59'),
(24, 8, 1, 'd', 0, '2019-11-15 09:01:51', '2019-11-15 09:01:51'),
(16, 5, 1, '222', 0, '2019-11-12 02:43:38', '2019-11-12 02:43:38'),
(11, 406, 13432, 'تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به تعليق لتجربة طول النص المسموح به', 0, '2019-05-14 10:04:14', '2019-11-14 06:11:35'),
(15, 91, 1, 'تعليق اختبار\r\nنص نص نص نص نص نص نص نص نص نص نص نص نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص  نص ', 0, '2019-09-19 08:04:36', '2019-09-19 08:04:36'),
(13, 406, 16240, 'تعليق جديد للتجربة', 0, '2019-06-20 08:46:00', '2019-06-20 08:46:00'),
(14, 406, 1, 'rawan test ', 0, '2019-06-30 08:21:05', '2019-06-30 08:21:05'),
(30, 0, 999, 'yygggggg', 0, '2019-11-20 06:45:06', '2019-11-20 06:45:06'),
(31, 0, 999, 'gffvvgh\n', 0, '2019-11-20 06:50:31', '2019-11-20 06:50:31'),
(32, 0, 999, 'نتتتاااااا', 0, '2019-11-20 06:50:44', '2019-11-20 06:50:44'),
(33, 0, 999, 'تعليق جديد', 0, '2019-11-28 15:15:46', '2019-11-28 15:15:46'),
(34, 0, 999, 'تعليق', 0, '2019-11-28 16:06:02', '2019-11-28 16:06:02'),
(35, 0, 999, 'تعليق', 0, '2019-12-03 12:53:21', '2019-12-03 12:53:21'),
(36, 0, 999, 'تستتتت', 0, '2019-12-14 19:15:26', '2019-12-14 19:15:26'),
(37, 0, 999, 'راااااىع', 0, '2019-12-15 07:13:48', '2019-12-15 07:13:48'),
(38, 0, 999, 'راااااىع', 0, '2019-12-15 10:26:16', '2019-12-15 10:26:16'),
(39, 0, 999, 'راااىع', 0, '2019-12-15 10:32:37', '2019-12-15 10:32:37'),
(40, 0, 999, 'ها\nتوعةغةغة', 0, '2019-12-18 05:47:27', '2019-12-18 05:47:27'),
(41, 0, 999, 'السلام عليكم\n', 0, '2019-12-18 05:47:49', '2019-12-18 05:47:49'),
(42, 7, 1, 'فعلا الزيت ده رائع', 0, '2019-12-18 06:25:13', '2019-12-18 06:25:13'),
(43, 7, 1, 'أول مرة أشتريه وكانت تجربة ممتازة', 0, '2019-12-18 06:27:29', '2019-12-18 06:27:29'),
(44, 7, 1, 'أول مرة أشتريه وكانت تجربة ممتازة', 0, '2019-12-18 06:28:02', '2019-12-18 06:28:02'),
(45, 7, 1, 'أول مرة أشتريه وكانت تجربة ممتازة', 0, '2019-12-18 06:28:08', '2019-12-18 06:28:08'),
(46, 0, 999, 'ااااااا', 0, '2019-12-19 04:33:50', '2019-12-19 04:33:50'),
(47, 0, 999, 'االالاال', 0, '2019-12-19 04:34:09', '2019-12-19 04:34:09'),
(48, 0, 999, 'الوووووو', 0, '2019-12-19 04:34:33', '2019-12-19 04:34:33'),
(49, 0, 999, 'جميل جداً\n\n\n', 0, '2019-12-19 05:45:09', '2019-12-19 05:45:09'),
(50, 0, 999, 'جميل جداً\n\n\n', 0, '2019-12-19 05:45:52', '2019-12-19 05:45:52'),
(51, 0, 999, 'تجربظ', 0, '2020-01-01 05:49:06', '2020-01-01 05:49:06'),
(52, 18, 999, 'بسم الله نجرب', 1, '2020-01-01 05:50:41', '2020-01-01 05:50:41'),
(53, 18, 19779, 'تجربة التعليق على منتج معين', 1, '2020-01-20 14:59:22', '2020-01-20 14:59:22'),
(54, 444, 19779, 'نمشسي شسي بشسيم صخثغف صث ؤرىءؤتا سيهفض سيب ', 1, '2020-01-20 14:59:22', '2020-01-20 14:59:22'),
(55, 38, 19779, 'تيتري  زيزط  طر', 1, '2020-01-20 14:04:58', '2020-01-20 14:04:58'),
(56, 18, 19779, '', 0, '2020-02-01 18:19:50', '2020-02-01 18:19:50'),
(57, 18, 19779, 'ميشو بيشو', 0, '2020-02-01 18:19:59', '2020-02-01 18:19:59'),
(58, 432, 19779, 'مشمشمشمش', 0, '2020-02-01 18:48:50', '2020-02-01 18:48:50'),
(59, 18, 19806, 'مشمش هنا', 0, '2020-02-09 13:20:42', '2020-02-09 13:20:42'),
(60, 18, 19779, 'مشمش', 0, '2020-02-09 13:26:28', '2020-02-09 13:26:28'),
(61, 433, 19807, 'Test product', 0, '2020-02-10 06:35:24', '2020-02-10 06:35:24'),
(62, 436, 19814, 'رااااىعة', 0, '2020-02-12 17:27:21', '2020-02-12 17:27:21'),
(63, 433, 16240, 'تيست', 0, '2020-02-13 12:08:26', '2020-02-13 12:08:26'),
(64, 433, 16240, 'تعليق تعليق', 0, '2020-02-13 12:08:39', '2020-02-13 12:08:39'),
(65, 432, 16240, 'تجربة تعليق', 0, '2020-02-16 11:30:28', '2020-02-16 11:30:28'),
(66, 5, 19815, 'رااااااائع', 0, '2020-02-19 12:40:55', '2020-02-19 12:40:55'),
(67, 17, 16240, 'd', 0, '2020-03-14 14:38:33', '2020-03-14 14:38:33'),
(68, 17, 16240, 'd', 0, '2020-03-14 14:38:51', '2020-03-14 14:38:51'),
(69, 430, 16240, '9', 0, '2020-03-14 14:42:47', '2020-03-14 14:42:47'),
(70, 430, 16240, '9', 0, '2020-03-14 14:42:56', '2020-03-14 14:42:56'),
(71, 8, 16240, '9', 0, '2020-03-15 10:07:28', '2020-03-15 10:07:28'),
(72, 424, 19834, '999', 0, '2020-03-21 09:48:14', '2020-03-21 09:48:14'),
(73, 7, 19834, 'Leave', 0, '2020-03-21 09:50:24', '2020-03-21 09:50:24');

-- --------------------------------------------------------

--
-- Table structure for table `comment_file`
--

CREATE TABLE `comment_file` (
  `id` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `fileName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mimeType` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comment_file`
--

INSERT INTO `comment_file` (`id`, `elementId`, `fileName`, `mimeType`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '811694d29feb1ffd.jpg', 'image/jpeg', 1, '2019-05-13 09:36:03', '2019-05-13 09:36:03'),
(2, 2, 'eaf3437889c35e5d.jpg', 'image/jpeg', 1, '2019-05-13 09:40:25', '2019-05-13 09:40:25'),
(3, 3, '68f0021008e9ed54.jpg', 'image/jpeg', 1, '2019-05-13 09:42:06', '2019-05-13 09:42:06'),
(4, 4, '924d2f1581f6fe89.mp4', 'video/mp4', 1, '2019-05-13 10:07:00', '2019-05-13 10:07:00'),
(5, 7, '16a2855a66d1d7f6.jpg', 'image/jpeg', 1, '2019-05-13 10:36:49', '2019-05-13 10:36:49'),
(6, 10, '369c203b8d3118a2.jpg', 'image/jpeg', 1, '2019-05-14 09:47:55', '2019-05-14 09:47:55'),
(7, 11, '585fb80251bedd1d.jpg', 'image/jpeg', 1, '2019-05-14 10:04:15', '2019-05-14 10:04:15'),
(8, 12, '29893f1f49e2f404.jpg', 'image/jpeg', 1, '2019-05-16 20:23:25', '2019-05-16 20:23:25'),
(9, 12, '737ea32876caa8c7.jpg', 'image/jpeg', 1, '2019-05-16 20:23:25', '2019-05-16 20:23:25'),
(10, 13, 'f714ee66e1ee4c2d.jpg', 'image/jpeg', 1, '2019-06-20 08:46:03', '2019-06-20 08:46:03'),
(11, 15, '923b9f8811d88fa7.PNG', 'image/png', 1, '2019-09-19 08:04:50', '2019-09-19 08:04:50'),
(12, 15, 'd69fb0a67bd031ba.png', 'image/png', 1, '2019-09-19 08:04:50', '2019-09-19 08:04:50'),
(13, 15, '0fad020c6bd2cd9a.jpg', 'image/jpeg', 1, '2019-09-19 08:04:51', '2019-09-19 08:04:51'),
(14, 16, '2b1646cbca8ff858.png', 'image/png', 1, '2019-11-12 02:43:43', '2019-11-12 02:43:43'),
(15, 70, '414c6e9343c7e80e.png', 'image/png', 1, '2020-03-14 14:42:57', '2020-03-14 14:42:57');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reason` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `msg` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `reason`, `status`, `country`, `title`, `msg`, `created_at`, `updated_at`) VALUES
(1, 'هشك بشك', NULL, '4444444', NULL, 0, NULL, NULL, 'تجربة رسائل', '2020-07-01 13:20:13', '2020-07-01 13:20:13'),
(3, 'علي', NULL, 'عليون', NULL, 0, NULL, NULL, 'عايز أحلق', '2020-07-19 08:04:13', '2020-07-19 08:04:13'),
(4, 'مشمش', NULL, 'يجرب', NULL, 0, NULL, NULL, 'test ali', '2020-09-28 05:50:36', '2020-09-28 05:50:36'),
(5, 'Super Admin', '905541711737', '905541711737', NULL, 0, NULL, NULL, 'after login', '2020-09-28 05:55:08', '2020-09-28 05:55:08');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `titleAR` varchar(70) DEFAULT NULL,
  `titleEN` varchar(70) DEFAULT NULL,
  `countryCode` varchar(3) DEFAULT NULL,
  `timeZone` varchar(4) DEFAULT NULL,
  `phoneCode` varchar(5) DEFAULT NULL,
  `long` varchar(125) DEFAULT NULL,
  `latt` varchar(125) DEFAULT NULL,
  `img` varchar(500) NOT NULL,
  `flag` varchar(500) NOT NULL,
  `shipmentCalculationType` tinyint(4) NOT NULL DEFAULT '1',
  `fixedValue` float NOT NULL DEFAULT '0',
  `firstKiloOfferProduct` float NOT NULL DEFAULT '0',
  `nextKiloOfferProduct` float NOT NULL DEFAULT '0',
  `firstKiloNotOfferProduct` float NOT NULL DEFAULT '0',
  `nextKiloNotOfferProduct` float NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `theOrder` tinyint(4) NOT NULL,
  `vat` float NOT NULL DEFAULT '0',
  `cargoPeriodMsg` varchar(250) DEFAULT NULL,
  `currencyId` mediumint(9) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `titleAR`, `titleEN`, `countryCode`, `timeZone`, `phoneCode`, `long`, `latt`, `img`, `flag`, `shipmentCalculationType`, `fixedValue`, `firstKiloOfferProduct`, `nextKiloOfferProduct`, `firstKiloNotOfferProduct`, `nextKiloNotOfferProduct`, `status`, `theOrder`, `vat`, `cargoPeriodMsg`, `currencyId`, `isDeleted`) VALUES
(162, 'اليمن', 'Yemen\n', 'YE', NULL, '967', '48.516388', '15.552727', '', 'yemen.png', 1, 40, 0, 0, 0, 0, 1, 100, 0, NULL, 0, 0),
(163, 'المملكة العربية السعودية', 'Saudi Arabia', 'SA', '+180', '966', '45.079162', '23.885942', '', 'saoudi.png', 1, 0, 0, 0, 0, 0, 1, 1, 5, 'مدة الشحن للسعودية تترواح بين 3 و 7 أيام', 120, 0),
(166, 'مصر', 'Egypt', 'EG', '+120', '20', '30.802498', '26.820553', 'egypt.jpg', 'egypt.png', 2, 40, 51, 11, 51, 11, 1, 7, 7, NULL, 0, 0),
(167, 'تونس', 'Tunisia', 'TN', '+60', '216', '9.537499', '33.886917', 'tunisia.jpg', 'tunisia.png', 2, 40, 60, 20, 60, 20, 1, 100, 0, NULL, 0, 0),
(168, 'فرنسا', 'France', 'FR', NULL, '33', '2.213749', '46.227638', 'french.jpg', 'french.png', 2, 40, 138, 19, 138, 19, 1, 100, 0, NULL, 0, 0),
(169, 'الفلبين', 'Philippines', 'PH', NULL, '63', '121.774017', '12.879721', '', 'philippine.png', 2, 40, 109.7, 17, 109.7, 17, 1, 100, 0, NULL, 0, 0),
(170, 'أندورا', 'Andorra', 'AD', NULL, '376', '1.601554', '42.546245', '', 'andorra.png', 2, 40, 307, 93, 307, 93, 1, 100, 0, NULL, 0, 0),
(172, 'الإمارات العربية المتحدة', 'United Arab Emirates', 'AE', '+180', '971', '53.847818', '23.424076', '', 'united_arab_emirates.png', 2, 0, 0, 0, 0, 11, 1, 3, 0, '', 0, 0),
(173, 'أفغانستان', 'Afghanistan', 'AF', NULL, '93', '67.709953', '33.93911', '', 'afghanistan.png', 2, 40, 227, 39.9, 227, 39.9, 1, 100, 0, NULL, 0, 0),
(175, 'أنجويلا', 'Angola', 'AO', NULL, '244', '-63.068615', '18.220554', '', 'anguilla.png', 2, 40, 175.7, 43.6, 175.7, 43.6, 1, 100, 0, NULL, 0, 0),
(176, 'ألبانيا', 'Albania', 'AL', NULL, '355', '20.168331', '41.153332', '', 'albania.png', 2, 40, 625, 43.6, 62.5, 43.6, 1, 100, 0, NULL, 0, 0),
(177, 'أرمينيا', 'Armenia', 'AM', NULL, '374', '45.038189', '40.069099', '', 'armenia.png', 2, 40, 351.5, 60, 351.5, 60, 1, 100, 0, NULL, 0, 0),
(179, 'أنغولا', 'Angola', 'AO', NULL, '244', '17.873887', '-11.202692', '', 'angola.png', 2, 40, 221.5, 43.6, 221.5, 43.6, 1, 100, 0, NULL, 0, 0),
(180, 'الأرجنتين', 'Argentina', 'AR', NULL, '54', '-63.616672', '-38.416097', '', 'argentina.png', 2, 40, 203.3, 44.6, 203.3, 44.6, 1, 100, 0, NULL, 0, 0),
(182, 'النمسا', 'Austria', 'AT', NULL, '43', '14.550072', '47.516231', '', 'austria.png', 2, 40, 217.1, 21.9, 217.1, 21.9, 1, 100, 0, NULL, 0, 0),
(183, 'أستراليا', 'Australia', 'AU', NULL, '61', '133.775136', '-25.274398', '', 'australia.png', 2, 40, 107, 16, 107, 16, 0, 100, 0, NULL, 0, 0),
(184, 'أذربيجان', 'Azerbaijan', 'AZ', NULL, '994', '47.576927', '40.143105', '', 'azerbaijan.png', 2, 40, 351.5, 60, 351.5, 60, 1, 100, 0, NULL, 0, 0),
(185, 'البوسنة/الهرسك', 'Bosnia and Herzegovina', 'BA', NULL, '387', '17.679076', ' 43.915886', '', 'bosnia_and_herzegovina.png', 2, 40, 62.5, 43, 62.5, 43, 1, 100, 0, NULL, 0, 0),
(186, 'بنغلاديش', 'Bangladesh', 'BD', NULL, '880', '90.356331', ' 23.684994', '', 'bangladesh.png', 2, 0, 89, 21, 89, 21, 1, 100, 0, NULL, 0, 0),
(187, 'بلجيكا', 'Belgium', 'BE', NULL, '32', '4.469936', '50.503887', '', 'belgium.png', 2, 40, 116, 18, 0, 0, 1, 100, 0, NULL, 0, 0),
(188, 'بوركينا فاسو', 'Burkina Faso', 'BF', NULL, '226', '-1.561593', '12.238333', '', 'burkina_faso.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(189, 'بلغاريا', 'Bulgaria', 'BG', NULL, '359', '25.48583', ' 	42.733883', '', 'bulgaria.png', 2, 40, 239, 35, 239, 35, 1, 100, 0, NULL, 0, 0),
(190, 'البحرين', 'Bahrain', 'BH', NULL, '973', '50.637772', '25.930414', '', 'bahrain.png', 2, 40, 0, 0, 30, 0, 1, 2, 0, NULL, 0, 0),
(191, 'بنين', 'Benin', 'BJ', NULL, '229', '2.315834', '9.30769', '', 'benin.png', 2, 40, 169, 40, 169, 40, 1, 100, 0, NULL, 0, 0),
(192, 'بوليفيا', 'Bolivia', 'BO', NULL, '591', '-63.588653', '-16.290154', '', 'bolivia.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(193, 'البرازيل', 'Brazil', 'BR', NULL, '55', '-51.92528', '-14.235004', '', 'brazil.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(194, 'الباهاماس', 'Bahamas', 'BS', NULL, '1242', '-77.39628', '25.03428', '', 'bahamas.png', 2, 40, 221.5, 43, 221.5, 43, 1, 100, 0, NULL, 0, 0),
(195, 'بوتسوانا', 'Botswana', 'BW', NULL, '267', '24.684866', '-22.328474', '', 'botswana.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(196, 'روسيا البيضاء', 'Belarus', 'BY', NULL, '375', '27.953389', '53.709807', '', 'belarus.png', 2, 40, 351, 60, 351, 60, 1, 100, 0, NULL, 0, 0),
(197, 'كندا', 'Canada', 'CA', NULL, '1', '-106.346771', '56.130366', '', 'canada.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(198, 'سويسرا', 'Switzerland', 'CH', NULL, '41', '8.227512', '46.818188', '', 'switzerland.png', 2, 40, 140, 22, 140, 22, 1, 100, 0, NULL, 0, 0),
(199, 'ساحل العاج', 'Ivory Coast', 'CI', NULL, '225', '-5.54708', '7.539989', '', 'cote_d_Ivoire.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(200, 'شيلي', 'Republic of Chile', 'CL', NULL, '56', '-71.542969', '-35.675147', '', 'chile.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(201, 'كاميرون', 'Republic of Cameroon', 'CM', NULL, '237', '12.354722', '7.369722', '', 'cameroon.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(202, 'الصين', 'China', 'CN', NULL, '86', '104.195397', '35.86166', '', 'china.png', 2, 40, 107, 16, 107, 16, 1, 100, 0, NULL, 0, 0),
(203, 'كولومبيا', 'Colombia', 'CO', NULL, '57', '-74.297333', '4.570868', '', 'colombia.png', 1, 40, 203, 44, 0, 0, 1, 100, 0, NULL, 0, 0),
(204, 'كوستاريكا', 'Costa Rica', 'CR', NULL, '506', '-83.753428', '9.748917', '', 'costa_rica.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(205, 'كوبا', 'Cuba', 'CU', NULL, '53', '-77.781167', '21.521757', '', 'costa_rica.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(206, 'الرأس الأخضر', 'Cape Verde', 'CV', NULL, '238', '-24.013197', '16.002082', '', 'cape_verde.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(207, 'قبرص', 'Cyprus', 'CY', NULL, '357', '33.429859', '35.126413', '', 'cyprus.png', 2, 40, 60, 8, 60, 8, 1, 100, 0, NULL, 0, 0),
(208, 'الجمهورية التشيكية', 'Czech Republic', 'CZ', NULL, '420', '15.472962', '49.817492', '', 'czech_republic.png', 2, 40, 213, 37, 213, 37, 1, 100, 0, NULL, 0, 0),
(209, 'ألمانيا', 'Germany', 'DE', NULL, '49', '10.451526', '51.165691', '', 'germany.png', 2, 40, 145.9, 18, 145.9, 18, 1, 100, 0, NULL, 0, 0),
(210, 'جيبوتي', 'Djibouti', 'DJ', NULL, '253', '42.590275', '11.825138', '', 'djibouti.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(211, 'الدانمارك', 'Denmark', 'DK', NULL, '45', '9.501785', '56.26392', '', 'denmark.png', 2, 40, 129, 24, 129, 24, 1, 100, 0, NULL, 0, 0),
(212, 'الجزائر', 'Algeria', 'DZ', NULL, '213', '1.659626', '28.033886', '', 'algeria.png', 2, 40, 266, 75, 266, 75, 1, 100, 0, NULL, 0, 0),
(213, 'إكوادور', 'Ecuador', 'EC', NULL, '593', '-78.183406', '-1.831239', '', 'ecuador.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(214, 'استونيا', 'Estonia', 'EE', NULL, '372', '25.013607', '58.595272', '', 'estonia.png', 2, 40, 388, 35, 388, 35, 1, 100, 0, NULL, 0, 0),
(215, 'إريتريا', 'Eritrea', 'ER', NULL, '291', '39.782334', '15.179384', '', 'eritrea.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(216, 'إسبانيا', 'Spain', 'ES', NULL, '34', '-3.74922', '40.463667', '', 'spain.png', 2, 40, 108, 25, 108, 25, 1, 100, 0, NULL, 0, 0),
(217, 'أثيوبيا', 'Ethiopia', 'ET', NULL, '251', '40.489673', '9.145', '', 'ethiopia.png', 2, 40, 169, 40, 169, 40, 1, 100, 0, NULL, 0, 0),
(218, 'فنلندا', 'Finland', 'FI', NULL, '358', '25.748151', '61.92411', '', 'finland.png', 2, 40, 209, 43, 209, 43, 1, 100, 0, NULL, 0, 0),
(219, 'المالديف', 'Maldives', 'MV', NULL, '960', '73.22068', '3.202778', '', 'maldives.png', 2, 40, 320, 56, 320, 56, 1, 100, 0, NULL, 0, 0),
(220, 'الغابون', 'Gabon', 'GA', NULL, '241', '11.609444', '-0.803689', '', 'gabon.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(221, 'المملكة المتحدة', 'United kingdom', 'GB', '+180', '44', '-3.435973', '55.378051', '', 'united_kingdom.png', 2, 40, 57, 0, 57, 9, 1, 100, 0, NULL, 120, 0),
(222, 'جيورجيا', 'Georgia', 'GE', NULL, '995', '43.356892', '42.315407', '', 'georgia.png', 2, 40, 351, 60, 351, 60, 1, 100, 0, NULL, 0, 0),
(223, 'غانا', 'Ghana', 'GH', NULL, '233', '-1.023194', '7.946527', '', 'ghana.png', 2, 40, 169, 40, 169, 40, 1, 100, 0, NULL, 0, 0),
(224, 'غامبيا', 'Gambia', 'GM', NULL, '220', '-15.310139', '13.443182', '', 'gambia.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(225, 'غينيا', 'Guinea', 'GN', NULL, '224', '-9.696645', '9.945587', '', 'guinea.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(226, 'غينيا الاستوائي', 'Equatorial Guinea', 'GQ', NULL, '224', '10.267895', '1.650801', '', 'equatorial_guinea.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(227, 'اليونان', 'Greece', 'GR', NULL, '30', '21.824312', '39.074208', '', 'greece.png', 2, 40, 127, 25, 127, 25, 1, 100, 0, NULL, 0, 0),
(228, 'غواتيمال', 'Guatemala', 'GT', NULL, '502', '-90.230759', '15.783471', '', 'guatemala.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(229, 'هونغ كونغ', 'Hong Kong', 'HK', NULL, '852', '114.109497', '22.396428', '', 'hong_kong.png', 2, 40, 98.3, 16, 98.3, 16, 1, 100, 0, NULL, 0, 0),
(230, 'هندوراس', 'Honduras', 'HN', NULL, '504', '-86.241905', '15.199999', '', 'honduras.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(231, 'كرواتيا', 'Croatia', 'HR', NULL, '385', '15.2', '45.1', '', 'croatia.png', 2, 40, 388, 35, 388, 35, 1, 100, 0, NULL, 0, 0),
(232, 'هايتي', 'Haiti', 'HT', NULL, '509', '-72.285215', '18.971187', '', 'haiti.png', 2, 40, 221, 43, 221, 43, 1, 100, 0, NULL, 0, 0),
(233, 'هنغاريا', 'Hungary', 'HU', NULL, '36', '19.503304', '47.162494', '', 'hungary.png', 2, 40, 213.6, 37, 231.6, 37, 1, 100, 0, NULL, 0, 0),
(234, 'أندونيسيا', 'Indonesia', 'ID', NULL, '62', '113.921327', '-0.789275', '', 'indonesia.png', 2, 40, 109, 17, 109, 17, 1, 100, 0, NULL, 0, 0),
(235, 'أيرلندا', 'Ireland', 'IE', NULL, '353', '-8.24389', '53.41291', '', 'ireland.png', 2, 40, 159, 24, 159, 25, 1, 100, 0, NULL, 0, 0),
(236, 'الهند', 'India', 'IN', NULL, '91', '78.96288', '20.593684', '', 'india.png', 2, 40, 43, 16, 43, 16, 1, 100, 0, NULL, 0, 0),
(237, 'العراق', 'Iraq', 'IQ', NULL, '964', '43.679291', '33.223191', '', 'iraq.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(238, 'إيران', 'Iran', 'IR', NULL, '98', '53.688046', '32.427908', '', 'iran.png', 2, 40, 71, 7, 71, 7, 1, 100, 0, NULL, 0, 0),
(239, 'آيسلندا', 'Iceland', 'IS', NULL, '354', '-19.020835', '64.963051', '', 'iceland.png', 2, 40, 272, 43, 272, 43, 1, 100, 0, NULL, 0, 0),
(240, 'إيطاليا', 'Italy', 'IT', NULL, '39', '12.56738', '41.87194', '', 'italy.png', 2, 40, 139, 24, 139, 24, 1, 100, 0, NULL, 0, 0),
(241, 'جمايكا', 'Jamaica', 'JM', NULL, '1876', '-77.297508', '18.109581', '', 'jamaica.png', 2, 40, 221, 43, 221, 43, 1, 100, 0, NULL, 0, 0),
(242, 'الأردن', 'Jordan', 'JO', NULL, '962', '36.238414', '30.585164', '', 'jordan.png', 2, 40, 51, 0, 51, 3, 1, 8, 0, NULL, 0, 0),
(243, 'اليابان', 'Japan', 'JP', NULL, '81', '138.252924', '36.204824', '', 'japan.png', 2, 40, 183, 32, 183, 32, 1, 100, 0, NULL, 0, 0),
(244, 'كينيا', 'Kenya', 'KE', NULL, '254', '37.906193', '-0.023559', '', 'kenya.png', 2, 40, 169, 40, 169, 40, 1, 100, 0, NULL, 0, 0),
(245, 'قيرغيزستان', 'Kyrgyzstan', 'KG', NULL, '996', '74.766098', '41.20438', '', 'kyrgyzstan.png', 2, 40, 351, 60, 351, 60, 1, 100, 0, NULL, 0, 0),
(246, 'كوريا الشمالية', 'Korea North', 'KP', NULL, '850', '127.510093', '40.339852', '', 'korea_north.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(247, 'كوريا الجنوبية', 'Korea South', 'KR', NULL, '82', '127.766922', '35.907757', '', 'korea_south.png', 2, 40, 107, 16, 107, 16, 1, 100, 0, NULL, 0, 0),
(248, 'الكويت', 'Kuwait', 'KW', NULL, '965', '47.481766', '29.31166', '', 'kuwait.png', 2, 40, 0, 0, 30, 0, 1, 4, 0, NULL, 0, 0),
(249, 'كازاخستان', 'Kazakhstan', 'KZ', NULL, '7', '66.923684', '48.019573', '', 'kazakhstan.png', 2, 40, 351, 60, 351, 60, 1, 100, 0, NULL, 0, 0),
(250, 'لبنان', 'Lebanon', 'LB', NULL, '961', '35.862285', '33.854721', '', 'lebanon.png', 2, 40, 67, 12, 67, 12, 1, 100, 0, NULL, 0, 0),
(251, 'سريلانكا', 'Sri Lanka', 'LK', NULL, '94', '80.771797', '7.873054', '', 'sri_lanka.png', 2, 40, 72, 11, 72, 11, 1, 100, 0, NULL, 0, 0),
(252, 'ليبيريا', 'Liberia', 'LR', NULL, '231', '-9.429499', '6.428055', '', 'liberia.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(253, 'لتوانيا', 'Lithuania', 'LT', NULL, '370', '23.881275', '55.169438', '', 'lithuania.png', 1, 40, 388, 35, 0, 0, 1, 100, 0, NULL, 0, 0),
(254, 'لوكسمبورغ', 'Luxembourg', 'LU', NULL, '352', '6.129583', '49.815273', '', 'luxembourg.png', 1, 40, 116, 18, 0, 0, 1, 100, 0, NULL, 0, 0),
(255, 'ليبيا', 'Libya', 'LY', NULL, '218', '17.228331', '26.3351', '', 'libya.png', 1, 40, 162, 34, 0, 0, 1, 100, 0, NULL, 0, 0),
(256, 'المغرب', 'Morocco\n', 'MA', NULL, '212', '-7.09262', '31.791702', '', 'morocco.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(257, 'الجبل الأسود', 'Montenegro', 'ME', NULL, '382', '19.37439', '42.708678', '', 'montenegro.png', 2, 40, 625, 43, 625, 43, 1, 100, 0, NULL, 0, 0),
(258, 'مدغشقر', 'Madagascar', 'MG', NULL, '261', '46.869107', '-18.766947', '', 'madagascar.png', 2, 40, 169, 40, 169, 40, 1, 100, 0, NULL, 0, 0),
(259, 'مقدونيا', 'Macedonia', 'MK', NULL, '389', '21.745275', '41.608635', '', 'macedonia.png', 2, 40, 388, 35, 388, 35, 1, 100, 0, NULL, 0, 0),
(260, 'مالي', 'Mali', 'ML', NULL, '223', '-3.996166', '17.570692', '', 'mali.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(261, 'منغوليا', 'Mongolia', 'MN', NULL, '976', '103.846656', '46.862496', '', 'mongolia.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(262, 'موريتانيا', 'Mauritania', 'MR', NULL, '222', '-10.940835', '21.00789', '', 'mauritania.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(263, 'مالطا', 'Malta', 'MT', NULL, '356', '14.375416', '35.937496', '', 'malta.png', 2, 40, 146, 31, 146, 31, 1, 100, 0, NULL, 0, 0),
(264, 'مالاوي', 'Malawi ', 'MW', NULL, '265', '34.301525', '-13.254308', '', 'malawi.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(265, 'المكسيك', 'Mexico', 'MX', NULL, '52', '-102.552784', '23.634501', '', 'mexico.png', 2, 40, 267, 108, 267, 108, 1, 100, 0, NULL, 0, 0),
(266, 'ماليزيا', 'Malaysia', 'MY', NULL, '60', '101.975766', '4.210484', '', 'malaysia.png', 2, 40, 109, 17, 109, 17, 1, 100, 0, NULL, 0, 0),
(267, 'موزمبيق', 'Mozambique', 'MZ', NULL, '258', '35.529562', '-18.665695', '', 'mozambique.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(268, 'ناميبيا', 'Namibia ', 'NA', NULL, '264', '18.49041', '-22.95764', '', 'namibia.png', 2, 40, 169, 41, 169, 41, 1, 100, 0, NULL, 0, 0),
(269, 'النيجر', 'Niger ', 'NE', NULL, '227', '8.081666', '17.607789', '', 'niger.png', 2, 40, 170, 41, 170, 41, 1, 100, 0, NULL, 0, 0),
(270, 'نيجيريا', 'Nigeria', 'NG', NULL, '234', '8.675277', '9.081999', '', 'nigeria.png', 2, 40, 169, 40, 169, 40, 1, 100, 0, NULL, 0, 0),
(271, 'نيكاراجوا', 'Nicaragua', 'NI', NULL, '505', '-85.207229', '12.865416', '', 'nicaragua.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(272, 'هولندا', 'Netherlands', 'NL', NULL, '31', '5.291266', '52.132633', '', 'netherlands.png', 2, 40, 132, 17, 132, 16, 1, 100, 0, NULL, 0, 0),
(273, 'النرويج', 'Norway', 'NO', NULL, '47', '60.472024', '8.468946', '', 'norway.png', 2, 40, 199, 41, 199, 41, 1, 100, 0, NULL, 0, 0),
(274, 'نيبال', 'Nepal\n', 'NP', NULL, '977', '84.124008', '28.394857', '', 'nepal.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(275, 'نيوزيلندا', 'New Zealand', 'NZ', NULL, '64', '174.885971', '-40.900557', '', 'new_zealand.png', 2, 40, 107, 16, 107, 16, 1, 100, 0, NULL, 0, 0),
(276, 'عُمان', 'Oman', 'OM', NULL, '968', '55.923255', '21.512583', '', 'oman.png', 2, 40, 30, 0, 30, 0, 1, 5, 0, NULL, 0, 0),
(277, 'بنما', 'Panama', 'PA', NULL, '507', '-80.782127', '8.537981', '', 'panama.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(278, 'بيرو', 'Peru', 'PE', NULL, '51', '-75.015152', '-9.189967', '', 'peru.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(279, 'باكستان', 'Pakistan', 'PK', NULL, '92', '69.345116', '30.375321', '', 'pakistan.png', 2, 40, 91, 17, 91, 17, 1, 100, 0, NULL, 0, 0),
(280, 'بولندا', 'Poland', 'PL', NULL, '48', '19.145136', '51.919438', '', 'poland.png', 2, 40, 213, 37, 213, 37, 1, 100, 0, NULL, 0, 0),
(281, 'فلسطين', 'Palestine \n', 'PS', NULL, '970', '35.233154', '31.952162', '', 'palestine.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(282, 'البرتغال', 'Portugal', 'PT', NULL, '351', '-8.224454', '39.399872', '', 'portugal.png', 2, 40, 163, 38, 163, 38, 1, 100, 0, NULL, 0, 0),
(283, 'بالاو', 'Palau', 'PW', NULL, '680', '134.58252', '7.51498', '', 'palau.png', 2, 40, 240, 37, 240, 37, 1, 100, 0, NULL, 0, 0),
(284, 'باراغواي', 'Paraguay', 'PY', NULL, '595', '-58.443832', '-23.442503', '', 'paraguay.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(285, 'قطر', 'Qatar\n', 'QA', NULL, '974', '51.183884', '25.354826', '', 'qatar.png', 1, 40, 0, 0, 0, 0, 0, 6, 0, NULL, 0, 0),
(286, 'رومانيا', 'Romania', 'RO', NULL, '40', '24.96676', '45.943161', '', 'romania.png', 2, 40, 239, 35, 239, 35, 1, 100, 0, NULL, 0, 0),
(287, 'صربيا', 'Serbia\n', 'RS', NULL, '381', '21.005859', '44.016521', '', 'serbia.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(288, 'روسيا', 'Russia', 'RU', NULL, '7', '105.318756', '61.52401', '', 'russia.png', 2, 40, 226, 39, 226, 39, 1, 100, 0, NULL, 0, 0),
(289, 'رواندا', 'Rwanda', 'RW', NULL, '250', '29.873888', '-1.940278', '', 'rwanda.png', 2, 40, 175, 175, 175, 43, 1, 100, 0, NULL, 0, 0),
(290, 'سيشيل', 'Seychelles', 'SC', NULL, '248', '55.491977', '-4.679574', '', 'seychelles.png', 2, 40, 169, 43, 169, 43, 1, 100, 0, NULL, 0, 0),
(291, 'السودان', 'Sudan', 'SD', NULL, '249', '30.217636', '12.862807', '', 'sudan.png', 2, 40, 169, 40, 169, 40, 1, 100, 0, NULL, 0, 0),
(292, 'السويد', 'Sweden', 'SE', NULL, '46', '18.643501', '60.128161', '', 'sweden.png', 2, 40, 154, 22, 154, 22, 1, 100, 0, NULL, 0, 0),
(293, 'سنغافورة', 'Singapore', 'SG', NULL, '65', '103.819836', '1.352083', '', 'singapore.png', 2, 40, 91, 18, 91, 18, 1, 100, 0, NULL, 0, 0),
(294, 'سلوفينيا', 'Slovenia', 'SI', NULL, '386', '14.995463', '46.151241', '', 'slovenia.png', 2, 40, 624, 42, 624, 42, 1, 100, 0, NULL, 0, 0),
(295, 'سلوفاكيا', 'Slovakia', 'SK', NULL, '421', '19.699024', '48.669026', '', 'slovakia.png', 2, 40, 239, 40, 239, 40, 1, 100, 0, NULL, 0, 0),
(296, 'سيراليون', 'Sierra Leone', 'SL', NULL, '232', '-11.779889', '8.460555', '', 'sierra_leone.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(297, 'السنغال', 'Senegal', 'SN', NULL, '221', '-14.452362', '14.497401', '', 'senegal.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(298, 'الصومال', 'Somalia', 'SO', NULL, '252', '46.199616', '5.152149', '', 'somalia.png', 2, 40, 308, 71, 308, 71, 1, 100, 0, NULL, 0, 0),
(299, 'سورية', 'Syria\n', 'SY', NULL, '963', '38.996815', '34.802075', '', 'syria.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(300, 'تشاد', 'Chad', 'TD', NULL, '235', '18.732207', '15.454166', '', 'chad.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(301, 'توغو', 'Togo', 'TG', NULL, '228', '0.824782', '8.619543', '', 'togo.png', 2, 40, 169, 41, 169, 41, 1, 100, 0, NULL, 0, 0),
(302, 'تايلندا', 'Thailand\n', 'TH', NULL, '66', '100.992541', '15.870032', '', 'thailand.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(303, 'طاجيكستان', 'Tajikistan', 'TJ', NULL, '992', '71.276093', '38.861034', '', 'tajikistan.png', 2, 40, 353, 60, 353, 60, 1, 100, 0, NULL, 0, 0),
(304, 'تركيا', 'Turkey', 'TR', '+2:0', '90', '35.243322', '38.963745', '', 'turkey.png', 2, 40, 66, 5, 66, 12, 1, 100, 0, '', 44, 0),
(305, 'ترينيداد وتوباغو', 'Republic of Trinidad and Tobago', 'TT', NULL, '1868', '-61.222503', '10.691803', '', 'trinidad_and_tobago.png', 2, 40, 221, 44, 221, 44, 1, 100, 0, NULL, 0, 0),
(306, 'تايوان', 'Taiwan', 'TW', NULL, '886', '120.960515', '23.69781', '', 'taiwan.png', 2, 40, 107, 16, 107, 16, 1, 100, 0, NULL, 0, 0),
(307, 'تنزانيا', 'Tanzania', 'TZ', NULL, '255', '34.888822', '-6.369028', '', 'tanzania.png', 2, 40, 169, 41, 169, 41, 1, 100, 0, NULL, 0, 0),
(308, 'أوكرانيا', 'Ukraine', 'UA', NULL, '380', '31.16558', '48.379433', '', 'ukraine.png', 2, 40, 567, 35, 567, 35, 1, 100, 0, NULL, 0, 0),
(309, 'أوغندا', 'Republic of Uganda', 'UG', NULL, '256', '32.290275', '1.373333', '', 'uganda.png', 2, 40, 169, 43, 169, 43, 1, 100, 0, NULL, 0, 0),
(310, 'الولايات المتحدة', 'United States of America', 'US', NULL, '1', '-95.712891', '37.09024', '', 'united_states.png', 2, 40, 100, 17, 102, 17, 1, 100, 0, NULL, 0, 0),
(311, 'أورغواي', 'Uruguay \n', 'UY', NULL, '598', '-55.765835', '-32.522779', '', 'uruguay.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(312, 'أوزباكستان', 'Uzbekistan', 'UZ', NULL, '998', '64.585262', '41.377491', '', 'uzbekistan.png', 2, 40, 351, 59, 351, 59, 1, 100, 0, NULL, 0, 0),
(313, 'فنزويلا', 'Venezuela', 'VE', NULL, '58', '-66.58973', '6.42375', '', 'venezuela.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(314, 'فيتنام', 'Vietnam', 'VN', NULL, '84', '108.277199', '14.058324', '', 'vietnam.png', 2, 40, 148, 34, 148, 34, 1, 100, 0, NULL, 0, 0),
(316, 'جنوب أفريقيا', 'South Africa', 'ZA', NULL, '27', '22.937506', '-30.559482', '', 'south_africa.png', 2, 40, 196, 41, 196, 41, 1, 100, 0, NULL, 0, 0),
(317, 'زامبيا', 'Zambia', 'ZM', NULL, '260', '27.849332', '-13.133897', '', 'zambia.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(318, 'زمبابوي', 'Zimbabwe', 'ZW', NULL, '263', '29.154857', '-19.015438', '', 'zimbabwe.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(319, 'الكونغو', 'Congo ', 'CG', NULL, '242', '15.82765900000004', '-0.228021', 'congo.png', 'congo.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(320, 'فيجي', 'Fiji', 'FJ', NULL, '679', NULL, '-16.578193', '179.414413', 'fiji.png', 2, 40, 239, 37, 239, 37, 1, 100, 0, NULL, 0, 0),
(321, 'سوازيلاند', 'Swaziland', 'SZ', NULL, '268', '31.465866', '-26.522503', '', 'swaziland.png', 2, 40, 175, 43, 175, 43, 1, 100, 0, NULL, 0, 0),
(322, 'السلفادور', 'El Salvador', 'SV', NULL, '503', '-88.89653', '13.794185', '', 'salvador.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(323, 'بابوا غينيا الجديدة', 'Papua New Guinea', 'PG', NULL, '675', '143.95555', '-6.314993', '', 'PapuaNewGuinea.png', 2, 40, 240, 37, 240, 37, 1, 100, 0, NULL, 0, 0),
(324, 'باربادوس', 'Barbados', 'BB', NULL, '1246', '-59.543198', '13.193887', '', 'Barbados.png', 2, 40, 221, 43, 221, 43, 1, 100, 0, NULL, 0, 0),
(325, 'بروناي', 'Brunei', 'BN', NULL, '673', '114.727669', '4.535277', '', 'Brunei.png', 2, 40, 209, 36, 209, 36, 1, 100, 0, NULL, 0, 0),
(326, 'بليز', 'Belize', 'BZ', NULL, '501', '-88.49765', '17.189877', '', 'Belize.png', 2, 40, 203, 44, 203, 44, 1, 100, 0, NULL, 0, 0),
(327, 'بورتوريكو', 'Puerto Rico', 'PR', NULL, '1787', '-53.3178', '-22.829', '', 'protorico.png', 2, 40, 225, 44, 335, 44, 1, 100, 0, NULL, 0, 0),
(328, 'تونغا', 'Tonga', 'TO', NULL, '676', '-175.198242', '-21.178986', '', 'tonga.png', 1, 40, 240, 37, 0, 0, 1, 100, 0, NULL, 0, 0),
(329, 'جزر الآنتيل الهولندية', 'Netherlands Antilles', 'NL', NULL, '599', '-69.060087', '12.226079', '', 'NetherlandsAntilles.png', 2, 40, 262, 45, 262, 45, 1, 100, 0, NULL, 0, 0),
(330, 'جزر البهاما', 'Bahamas Islands', 'BS', NULL, '1242', '-77.39628', '25.03428', '', 'bahamas.png', 2, 40, 221, 43, 221, 43, 1, 100, 0, NULL, 0, 0),
(331, 'جزر سليمان', 'Solomon Islands', 'SB', NULL, '677', '160.156194', '-9.64571', '', 'solomonislands.png', 2, 40, 240, 37, 240, 37, 1, 100, 0, NULL, 0, 0),
(332, 'جزر سيشل', 'Seychelles Islands', 'SC', NULL, '248', '55.491977', '-4.679574', '', 'seychelles.png', 2, 40, 169, 40, 169, 40, 1, 100, 0, NULL, 0, 0),
(333, 'جزر كايمان', 'Cayman Islands', 'KY', NULL, '1345', '-80.566956', '19.513469', '', 'CaymanIsland.png', 2, 40, 221, 43, 221, 43, 1, 100, 0, NULL, 0, 0),
(334, 'جزر مارشال', 'Republic of the Marshall Islands', 'MH', NULL, '692', '171.184478', '7.131474', '', 'MarshallIslands.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(335, 'جزر ريونيون', 'Réunion Islands', 'RE', NULL, '262', '55.536384', '-21.115141', '', 'Reunion.png', 2, 40, 169, 40, 169, 40, 1, 100, 0, NULL, 0, 0),
(336, 'جمهورية الدومينيكان', 'Dominican Republic', 'DO', NULL, '1809', '-70.162651', '18.735693', '', 'DominicanRepublic.png', 1, 40, 221, 43, 0, 0, 1, 100, 0, NULL, 0, 0),
(337, 'جنوب السودان', 'South Sudan', 'SS', NULL, '249', '30.217636', '12.862807', '', 'southsudan.png', 2, 40, 169, 40, 169, 40, 1, 100, 0, NULL, 0, 0),
(339, 'الدومينيكان', 'Dominican Republic', 'DO', NULL, '1809', '-61.370976', '15.414999', '', 'dominica.png', 2, 40, 221, 43, 221, 43, 1, 100, 0, NULL, 0, 0),
(340, 'ساموا الأمريكية', 'American Samoa', 'WS', NULL, '684', '-172.104629', '-13.759029', '', 'AmericanSamoa.jpg', 2, 40, 240, 37, 240, 37, 1, 100, 0, NULL, 0, 0),
(341, 'سريلانكا', 'Sri Lanka\n', 'LK', NULL, '94', '80.771797', '7.873054', '', 'srilanka.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(342, 'سانت فنسنت والجرينادين', 'St Vincent and Grenadine', 'VC', NULL, '1784', '-61.287228', '12.984305', '', 'SaintVincentandtheGrenadines.png', 2, 40, 221, 43, 221, 43, 1, 100, 0, NULL, 0, 0),
(344, 'سانت كيتس ونيفيس', 'St Kitts and Nevis', 'KN', NULL, '1869', '-62.782998', '17.357822', '', 'SaintKittsandNevis.png', 2, 40, 221, 43, 221, 43, 1, 100, 0, NULL, 0, 0),
(345, 'سانت لوسيا', 'St Lucia', 'LC', NULL, '1758', '-60.978893', '13.909444', '', 'SaintLucia.png', 2, 40, 221, 43, 221, 43, 1, 100, 0, NULL, 0, 0),
(346, 'ماكاو', 'Macau', 'MO', NULL, '853', '113.543873', '22.198745', '', 'Macau.png', 2, 40, 142, 29, 142, 29, 1, 100, 0, NULL, 0, 0),
(347, 'موريشيوس', 'Mauritius\n', 'MU', NULL, '230', '57.552152', '-20.348404', '', 'Mauritius.png', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(348, 'مولدوفا', 'Moldova', 'MD', NULL, '373', '28.369885', '47.411631', '', 'Moldova.png', 2, 40, 351, 60, 351, 60, 1, 100, 0, NULL, 0, 0),
(349, 'ميانمار، بورما', 'Myanmar, Burma', 'MM', NULL, '95', '95.956223', '21.913965', '', 'MyanmarBurma.png', 2, 40, 209, 36, 209, 36, 1, 100, 0, NULL, 0, 0),
(352, 'كوسوفا', 'Kosovo', 'XZ', NULL, '383', NULL, NULL, '', '', 1, 40, 0, 0, 0, 0, 0, 100, 0, NULL, 0, 0),
(353, 'دولة البط', 'Duck Country', 'KH', NULL, NULL, NULL, NULL, '', '', 1, 25, 0, 0, 0, 0, 1, 0, 10, NULL, 0, 0),
(354, 'سوريا', 'Syria', 'Sy', NULL, NULL, NULL, NULL, '', '', 1, 100, 0, 0, 0, 0, 1, 0, 5, NULL, 0, 0),
(355, 'اختبار دولة', 'Saudi Arabia', 'SA', '180+', NULL, NULL, NULL, '', '', 1, 20, 0, 0, 0, 0, 1, 0, 5, '3', 120, 0);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `price` float DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `price`, `status`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 336, 1, 0, '2020-11-07 11:51:03', '2020-11-08 19:41:01'),
(2, 2506, 1, 0, '2020-11-07 11:56:08', '2020-11-09 14:01:06'),
(8, 55, 0, 0, '2020-11-07 12:07:02', '2020-11-08 19:25:45'),
(9, 773, 0, 0, '2020-11-07 12:07:45', '2020-11-15 19:00:35'),
(16, 126, 1, 0, '2020-11-07 19:29:34', '2020-11-08 19:41:13'),
(17, 78, 1, 0, '2020-11-08 20:33:20', '2020-11-08 20:35:48'),
(18, 8883, 1, 0, '2020-11-08 20:34:24', '2020-11-09 13:57:08'),
(19, NULL, 1, 0, '2020-11-08 20:47:26', '2020-11-08 20:47:26'),
(20, 32, 1, 0, '2020-11-08 20:50:18', '2020-11-08 20:50:18'),
(21, 32, 1, 0, '2020-11-08 20:52:42', '2020-11-08 20:52:42'),
(22, 32, 1, 0, '2020-11-08 20:52:56', '2020-11-08 20:52:56'),
(23, 3291, 1, 0, '2020-11-08 20:56:07', '2020-11-09 13:51:39'),
(24, 321, 1, 0, '2020-11-08 20:59:03', '2020-11-09 13:52:12');

-- --------------------------------------------------------

--
-- Table structure for table `courses_trans`
--

CREATE TABLE `courses_trans` (
  `id` int(11) NOT NULL,
  `rowId` int(11) NOT NULL,
  `languageCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `courses_trans`
--

INSERT INTO `courses_trans` (`id`, `rowId`, `languageCode`, `title`, `description`) VALUES
(1, 1, 'AR', 'دورة ٠١', ''),
(2, 2, 'AR', 'دورة بط', ''),
(3, 3, 'AR', 'دورة بط', ''),
(4, 4, 'AR', 'دورة بط', ''),
(5, 5, 'AR', 'ووو', 'ةىىىى'),
(6, 6, 'AR', 'ووو', 'ةىىىى'),
(7, 7, 'AR', 'تت', 'ةة'),
(8, 8, 'AR', 'تت', 'ةة'),
(9, 9, 'AR', 'زز', ''),
(10, 10, 'AR', 'و', ''),
(11, 11, 'AR', 'زززز', ''),
(12, 12, 'AR', 'زوو', ''),
(13, 13, 'AR', 'ننن', ''),
(14, 14, 'AR', 'زوو', ''),
(15, 15, 'AR', 'بطيخة', 'وص'),
(16, 16, 'AR', 'مشمش', ''),
(17, 17, 'AR', 'تاوي', ''),
(18, 18, 'AR', 'ناز', ''),
(19, 19, 'AR', 'فقفق', ''),
(20, 20, 'AR', 'فقفق', ''),
(21, 21, 'AR', 'فقفق', ''),
(22, 22, 'AR', 'فقفقههههههههه', ''),
(23, 23, 'AR', 'فقفقهههههههههغ', 'غغغ'),
(24, 24, 'AR', 'فقفقهههههههههغ٦٥ق', 'غغغ');

-- --------------------------------------------------------

--
-- Table structure for table `course_file`
--

CREATE TABLE `course_file` (
  `id` int(11) NOT NULL,
  `rowId` int(11) DEFAULT NULL,
  `fileName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mimeType` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course_file`
--

INSERT INTO `course_file` (`id`, `rowId`, `fileName`, `mimeType`, `status`, `created_at`, `updated_at`) VALUES
(1, 15, '80b3cea5a7d8bb3d.jpg', 'image/jpeg', 1, '2020-11-07 12:13:59', '2020-11-07 12:13:59'),
(2, 17, 'aafddeae06b766c8.jpg', 'image/jpeg', 1, '2020-11-08 20:33:24', '2020-11-08 20:33:24'),
(4, 19, 'f78640623168aad9.jpg', 'image/jpeg', 1, '2020-11-08 20:47:32', '2020-11-08 20:47:32'),
(5, 20, '441c18b62682b0c0.jpg', 'image/jpeg', 1, '2020-11-08 20:50:26', '2020-11-08 20:50:26'),
(9, 18, '6c06de592acb6c98.jpg', 'image/jpeg', 1, '2020-11-09 13:57:19', '2020-11-09 13:57:19'),
(10, 2, '3238aa94da476fa1.jpg', 'image/jpeg', 1, '2020-11-09 14:00:47', '2020-11-09 14:00:47'),
(11, 2, 'b926d1370c33bf19.jpg', 'image/jpeg', 1, '2020-11-09 14:01:39', '2020-11-09 14:01:39'),
(12, 24, '15650de8f50bfb48.jpg', 'image/jpeg', 1, '2020-11-09 14:05:52', '2020-11-09 14:05:52'),
(14, 22, '54631a3b8ad8f2b7.jpg', 'image/jpeg', 1, '2020-11-09 14:16:17', '2020-11-09 14:16:17'),
(15, 22, 'c5fe22b3ecc39c55.jpg', 'image/jpeg', 1, '2020-11-09 14:18:55', '2020-11-09 14:18:55'),
(16, 18, 'c37a12ea471b2c8e.jpg', 'image/jpeg', 1, '2020-11-09 14:29:12', '2020-11-09 14:29:12'),
(17, 18, '4a53bde8711354fe.jpg', 'image/jpeg', 1, '2020-11-09 14:30:06', '2020-11-09 14:30:06'),
(18, 2, 'f82ff3a830ee0fc9.jpg', 'image/jpeg', 1, '2020-11-09 14:30:17', '2020-11-09 14:30:17'),
(19, 9, '1a46493e7de980f7.jpg', 'image/jpeg', 1, '2020-11-09 14:30:54', '2020-11-09 14:30:54');

-- --------------------------------------------------------

--
-- Table structure for table `cpages`
--

CREATE TABLE `cpages` (
  `id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0: inactive, 1:active',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `photo` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cpages`
--

INSERT INTO `cpages` (`id`, `status`, `isDeleted`, `photo`, `video`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '7e32fe625674b304.jpg', '-hPcKAGSfT8', '2020-09-10 19:41:38', '2020-09-10 19:41:38'),
(2, 1, 0, 'ec1e397967ee5014.jpg', '-hPcKAGSfT8', '2020-09-10 19:41:51', '2020-09-10 19:41:51');

-- --------------------------------------------------------

--
-- Table structure for table `cpages_trans`
--

CREATE TABLE `cpages_trans` (
  `id` int(11) NOT NULL,
  `rowId` int(11) NOT NULL,
  `languageCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'AR',
  `title` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cpages_trans`
--

INSERT INTO `cpages_trans` (`id`, `rowId`, `languageCode`, `title`, `description`) VALUES
(14, 1, 'AR', 'عن الشركة', '<p>عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;</p>'),
(15, 2, 'AR', 'سياسة البطيخ', '<p>عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;عن الشركة&nbsp;</p>'),
(16, 1, 'EN', 'about us', '<p>about us</p>');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL,
  `title` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currencyCode` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exchangeRate` float NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `title`, `currencyCode`, `exchangeRate`, `status`, `photo`) VALUES
(1, 'Lira', 'TRY', 0.14, 1, NULL),
(2, 'Dollar', 'USD', 1, 1, NULL),
(3, 'Euro', 'EUR', 1.2, 1, NULL),
(4, 'جنيه مصري', 'EGP', 0.6, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_msg`
--

CREATE TABLE `custom_msg` (
  `id` int(10) UNSIGNED NOT NULL,
  `msgId` smallint(6) NOT NULL,
  `titleAR` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titleEN` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contentAR` text COLLATE utf8_unicode_ci,
  `contentEN` text COLLATE utf8_unicode_ci,
  `forOrders` tinyint(4) NOT NULL COMMENT 'specifies which msgs are related to orders',
  `whoSeeIt` mediumint(9) NOT NULL,
  `userGroupId` mediumint(9) NOT NULL COMMENT 'stored in orders table and enables us to get all orders for each groupId.Also refers to whom order returned'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_msg`
--

INSERT INTO `custom_msg` (`id`, `msgId`, `titleAR`, `titleEN`, `contentAR`, `contentEN`, `forOrders`, `whoSeeIt`, `userGroupId`) VALUES
(1, 0, 'التسجيل كعضو جديد', 'registration', 'عميلنا  customer_name  ،\r\nتم تسجيلك بنجاح في موقعنا).', 'عميلنا  customer_name\r\nYour register has been ...\r\n\r\n123', 0, 0, 0),
(2, 0, 'رسالة تنبيه رفع الإيصالات', 'wwwwwwww', 'عميلنا العزيز  customer_name\r\nشكرا لإتمامكم إجراءات الطلب .\r\nسوف نقوم بإشعاركم عند تجهيز الطلب \r\nإذا قمت برفع طلبك بنفسك في الموقع الرجاء رفع الايصال قبل مرور 24 ساعة من الطلب أو سيتم إلغاء الطلب.\r\n\r\nAnas\r\n\r\nwwwwwwwwwwww', 'wwwwwwwwwwwww', 0, 0, 0),
(3, 0, 'طلبية تمت', '', 'ارجان باكيج تهنئك باستلام طلبك \r\nكما يسعدنا تقييمك لنا على هذا الرابط  https://maroof.sa/40924\r\n\r\nهدفنا خدمتكم و رضاكم نجاحنا.', 'Dear customer\r\nYour request is done\r\nThank you \r\n cargoPhone  shipmentFirm  ticketId  order_id customer_name', 1, 0, 0),
(4, 4, 'طلبية ملغاة', 'Cancelled', 'تم إلغاء الطلبية رقم  orderId للعميل اللي اسمه  customer_name', 'Order  orderId was cancelled for  customer_name', 1, 0, 0),
(5, 5, 'طلبية ينبغي استلامها', NULL, 'عميلنا العزيز  customer_name\r\nشحنتك  الآن في منطقتك جاهزة للتسليم  shipmentId\r\nسارع باستلامها أو التواصل مع شركة الشحن قبل أن تعود إلينا\r\nولاتنسى إبلاغنا في حال إستلام الشحنة ', NULL, 1, 0, 0),
(6, 6, 'تم التسليم للشحن', NULL, 'مرحبا customer_name ،لقد تم شحن طلبيتك \r\n\r\nوهذا رقم شحنتك  shipmentId \r\n\r\nمع  shipmentFirm                        \r\n\r\nوهذا رابط التتبع  trackLink', NULL, 1, 0, 0),
(7, 7, 'طلبية مرجعة', NULL, 'مرحبا customer_name.\r\nتم إرجاع طلبك لدينا , التفاصيل أكثر الرجاء التواصل معنا على هذا الرقم 0569861378', NULL, 1, 0, 0),
(8, 8, 'انتظار', NULL, 'تم أستلام طلبك، و سيتم إبلاغك برسالة نصية عند تجهيزه. ', NULL, 1, 0, 0),
(9, 0, 'طلب دفع لاحقا', '', 'عميلنا  customer_name،\r\nجاري تجهيز شحنتك برقم  order_id\r\nويمكنك الاطلاع على تفاصيل الطلبية من خلال هذا الرابط https://arganpackage.com/orders-by-client والدخول برقم جوالك وسيتم إرسال رسالة عند الشحن .', 'Dear  customer_name \r\ntest test  order_id order_id \r\n shipmentFirm\r\n shipmentId\r\n ourTrackLink\r\n ticketId\r\n cargoPhone', 0, 0, 0),
(10, 0, 'طلب و رفع إيصالات التحويل', NULL, 'مرحبا  customer_name.\r\nتم رفع طلبك رقم    order_id\r\nوسوف نبلغك في حال اكتمال الطلب  برسالة نصية', NULL, 0, 0, 0),
(11, 0, 'طلب و دفع أونلاين ', 'wwwwwww', 'مرحبا  customer_name ،\r\nتم قبول طلبك وسوف تصلك رسالة نصية في حال اكتمال الشحن\r\n\r\nww', ' shipmentFirm shipmentId customer_name order_id order_id order_id order_id', 0, 0, 0),
(12, 0, 'تأكيد الحوالة', 'Transfer confirmed', 'عميلنا العزيز    customer_name\r\nتم تأكيد استلام المبلغ لطلبك رقم  order_id', 'Transfer customer_name confirmed', 0, 0, 0),
(13, 0, 'فشل التأكد من الحوالة - رفض الحوالة', NULL, 'عزيزنا  customer_name\r\nيؤسفنا أن نبلغك بإن الحوالة رقم   order_id لم نستطيع التأكد منها ، ولمعرفة المزيد يرجاء التواصل على الرقم 0569861378 و السبب هو :', NULL, 0, 0, 0),
(14, 0, 'تنبيه حجز الطلب', '', 'سيتم حجز طلبك 24 ساعة ثم يُلغى، يجب تحويل المبلغ ورفع الإيصال خلال هذه المدة.', 'Your Order will be reserved for 24 hours', 0, 0, 0),
(18, 0, 'إرجاع لمسئول المبيعات', NULL, 'تم إرجاع طلبية لمسئول المبيعات من مسئول بوليصات', NULL, 1, 11, 3),
(19, 0, 'إرجاع إلى مسئول بوليصات', NULL, 'إرجاع إلى مسئول بوليصات', NULL, 1, 9, 11),
(20, 0, 'اعتمد وأرسل لمسئول التعبئة', NULL, 'تم اعتماد الطلبية من مسئول البوليصات وأرسلها بدوره لمسئول التعبئة', NULL, 1, 11, 9),
(21, 0, 'أعتمد وأرسل إلى مسئول الشحن', NULL, 'تم وصول طلبية لمسئول الشحن', NULL, 1, 9, 12),
(22, 0, 'نقل للطلبيات المشحونة', NULL, 'نقل للطلبيات المشحونة', NULL, 1, 12, 3),
(23, 0, 'أرجع لمسئول تعبئة', NULL, 'أرجع لمسئول تعبئة', NULL, 1, 12, 9),
(24, 0, 'اعتمد وأسل لمسئول البوليصات', NULL, 'اعتمد وأسل لمسئول البوليصات', NULL, 1, 3, 11),
(25, 0, 'إرجاع مبلغ للعميل', NULL, 'تم إرجاع المبلغ بنجاح\r\n customer_name', NULL, 1, 0, 0),
(26, 0, 'إكمال الطلب كزائر', NULL, 'زائرنا الكريم يمكنك الدخول على لوحة تحكمك برقم جوالك وكلمة مرور 123456', NULL, 0, 0, 0),
(28, 0, 'تنبيه مورد لطلب شراء', NULL, 'تم تحرير طلب شراء', NULL, 0, 0, 0),
(29, 8, 'انتظار', 'pending', 'انتظار سماح', 'pending msg of samah', 0, 0, 0),
(30, 3, 'طلبية تمت', 'Order Done', ' طلبية تمت يا مهند بيه أنت وصديق بيه', 'order done', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `custom_page`
--

CREATE TABLE `custom_page` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nameEn` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleEn` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `the_content` text COLLATE utf8_unicode_ci,
  `the_contentEn` longtext COLLATE utf8_unicode_ci,
  `mediaFile` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `custom_page`
--

INSERT INTO `custom_page` (`id`, `name`, `nameEn`, `title`, `titleEn`, `the_content`, `the_contentEn`, `mediaFile`, `status`, `created_at`, `updated_at`) VALUES
(15, 'الشروط والاحكام', NULL, 'الشروط والاحكام', NULL, '<p dir=\"rtl\"><strong>&nbsp;البند الأول : </strong></p>\r\n\r\n<p dir=\"rtl\"><strong>مقدمة&nbsp;:-1-</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>مرحبا بكم في متجر أرجان بكيج فيما يلي الشروط والأحكام التي تخص استخدامك ودخولك لمتجر أرجان بكيج </strong></p>\r\n\r\n<p dir=\"rtl\"><strong>إن استخدامك لمتجر أرجان بكيج هو موافقة منك على القبول ببنود وشروط هذه الاتفاقية والتي تشمل كافة التفاصيل أدناه وهو تأكيد لالتزامك بالاستجابة لمضمون هذه الاتفاقية الخاصة بـ متجر أرجان بكيج&nbsp; باسم &quot;نحن&quot; والمشار إليه أيضا بـ&quot;متجر لأرجان بكيج&quot; فيما يتعلق باستخدامك للموقع، والمشار إليه فيما يلي بـ &quot; الشروط والأحكام&quot; وتعتبر هذه الاتفاقية سارية المفعول حال قبولك بمجرد تنشيط حسابك.</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>البند الثاني : التأهل للعضوية&nbsp;:&nbsp;--</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>=============================&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>لا يحق لأي شخص استخدام الموقع إذا ألغيت عضويته من متجر أرجان بكيج</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>لا يحق لأي عضو&nbsp; أن تقوم بفتح حسابين في آن واحد لأي سبب كان، ولإدارة الموقع الحق بتجميد الحسابين أو إلغاء أحدهما مع الالتزام بتصفية كافة الالتزامات المالية المتعلقة بالحساب قبل إغلاقه.</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p dir=\"rtl\"><br />\r\n<strong>البند الثالث : الحسابات والتزامات التسجيل&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;<br />\r\nفور تقديم طلب التسجيل للحصول على عضوية في الموقع تكون مطالباً بالإفصاح عن معلومات محددة واختيار اسم مستخدم وكلمة مرور سرية لاستعمالها عند الدخول للموقع. وعند تنشيط حسابك ستعتبر عضوا بالموقع وبذلك تكون قد وافقت على:</strong></p>\r\n\r\n<ul>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>.أن تكون مسؤولاً عن المحافظة على سرية معلومات حسابك وكلمة المرور السرية. وتكون بذلك موافقاً على إعلام متجر أرجان بكيج حالاً بأي استخدام غير مفوض به لكلمة دخولك أو حسابك أو أي اختراق آخر لمعلوماتك السرية.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>. لن يكون متجر أرجان بكيج بأي حال من الأحوال مسؤولاً عن أي خسارة قد تلحق بك بشكل مباشر أو غير مباشر معنويا أو ماديا نتيجة كشف معلومات اسم المستخدم أو كلمة الدخول.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>أنت مسؤول عن المحافظة من ناحية استخدام متجر أرجان بكيج بكل جدية و مصداقية وتعتبر ملزما بتعويض متجر أرجان بكيج عن أي خسائر مباشرة أو غير مباشرة قد تلحق بـ متجر أرجان بكيج نتيجة أي استخدام غير شرعي أو حقيقي أو مفوض لحسابك من طرفك أو من طرف أي شخص آخر حصل على مفاتيح الوصول إلى حسابك بالموقع سواء كان لإنجاز خدمات أوغيرها باستعمال اسم المستخدم وكلمة الدخول، سواء بتفويض منك أو بلا تفويض.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>&nbsp;أنت موافق على الإفصاح بمعلومات حقيقية وصحيحة و محدثة و كاملة عن نفسك حسبما هو مطلوب في استمارة التسجيل لدى متجر أرجان بكيج</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>.&nbsp;أن لا تدرج ضمن اسم المستخدم أي من تفاصيل الاتصال بك كعناوين البريد الإلكتروني أو أرقام هواتفك أو أي تفاصيل شخصية، أو كلمة متجر أرجان بكيج &nbsp;يلتزم متجر أرجان بكيج &nbsp;بالتعامل مع معلوماتك الشخصية وعناوين الاتصال بك بسريّة تامة</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>متجر أرجان بكيج مطلق الإرادة وفي أي وقت أن يجري أي تحقيقات يراها ضرورية (مباشرة أو عبر طرف ثالث) ويطالبك بالإفصاح عن معلومات إضافية أو وثائق مهما كان حجمها لإثبات هويتك و/أو ملكيتك لأدواتك المالية.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>.في حالة عدم الالتزام بأي مما ورد أعلاه فإن لإدارة متجر أرجان بكيج الحق في إيقاف أو إلغاء عضويتك و حجبك عن الموقع. ونحتفظ كذلك بالحق في إلغاء أي حسابات غير مؤكدة وغير مثبتة أو عمليات أو حسابات مر عليها مدة طويلة دون نشاط.</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>البند الرابع :الاتصالات الإلكترونية&nbsp;:- -&nbsp;</strong><br />\r\n&nbsp;</p>\r\n\r\n<ul>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>أنت موافق على أن يتم التواصل معك عبر البريد الإلكتروني، أو من خلال بث إعلانات ترويجية على الموقع، وأنت موافق على أن جميع الاتفاقيات والإعلانات والبيانات والاتصالات الأخرى التي تزود بها الكترونياً تقوم مقام مثيلاتها المكتوبة، في تلبية الاحتياجات القانونية.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>سيقوم متجر أرجان بكيج خلال فترة عضويتك بإرسال رسائل إلكترونية ترويجية لتعريفك بأي تغيرات أو إجراءات أو نشاطات دعائية جديدة قد تضاف إلى الموقع</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><br />\r\n<strong>البند الخامس : التعديل على الشروط والأحكام&nbsp;:&nbsp;--</strong></p>\r\n\r\n<ul>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>.أنت تعلم وموافق على أن يقوم متجر أرجان بكيج بإعلامك عن أي تعديل على اتفاقية الشروط والأحكام، وبموجبه تتضاعف التزاماتك أو تتضاءل حقوقك وفقاً لأي تعديلات قد تجرى على اتفاقية الشروط والأحكام.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>. أنت توافق على أن متجر أرجان بكيج يملك بمطلق صلاحيته ودون تحمله المسؤولية القانونية أن يجري تعديلات أساسية أو فرعية على هذه الاتفاقية دون أن يتطلب ذاك موافقة إضافية من طرفك، وذلك في أي وقت وبأثر فوري، بواسطة بث إعلان عن التعديل على الموقع.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>متجر أرجان بكيج يفرض رسوم التوصيل والشحن على المشترين وهي 40 ريال سعودي فقط داخل المملكة العربية السعودية . تختلف قيمة الشحن على حسب الدولة</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>. للموقع الحق بعمل تعديلات على رسوم عمليات الشحن والتوصيل بما يراه مناسبا وسيتم الإعلان عن أي تعديل من خلال مراسلتك على بريدك الإلكتروني أو على الموقع، و قد تكون التعديلات مؤقتة أو مستمرة سواء كانت على الرسوم أو تفاصيل خدمات الشحن وعليك الالتزام بها وفق بنودها حين يتم الإعلان عنها.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>كافة الرسوم تحتسب بالريال السعودي، عليك دفع كافة الرسوم المستحقة على عملياتك بالموقع إلى متجر أرجان بكيج مضافاً إليها أي نفقات أخرى يضيفها متجر أرجان بكيج ، على أن يتم السداد بواسطة الوسائل المعتمدة لذلك.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>.في حال عدم التزامك بدفع الرسوم أو النفقات المحتسبة على عملياتك في الموقع وتجاوزت المدة 3 أيام عمل ، فأن متجر أرجان بكيج وبدون أدنى مسؤولية قانونية يحتفظ بحقه في إلغاء طلب الشراء الخاصة بك.</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p dir=\"rtl\"><br />\r\n<strong>البند السادس: الدفع للمشتريات في متجر أرجان بكيج -</strong></p>\r\n\r\n<ul>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>يعتبر نظام الدفع في متجر أرجان بكيج أن الدفع لشراء بضائع من الموقع يمكن أن يتم عبر الانترنت كليا من خلال خيارات الدفع المتوفرة على الـ متجر أرجان بكيج وهي الدفع عن طريق البطاقات الائتمانية أو من خلال أي طريقة دفع يوفرها متجر أرجان بكيج على الموقع من حين لآخر.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>ساسية استرجاع المبلغ:</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<ol>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>-اذا تم تحويل المبلغ ورفعت الفاتورة لايسترجع المبلغ ولا يضاف او يحذف اي منتج&nbsp;</strong></p>\r\n	</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol start=\"2\">\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>-اذا طلعت الطلبية مع شركة الشحن ورجعت لنا مره اخرى لايمكن استرجاع المبلغ</strong></p>\r\n	</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol start=\"3\">\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>-يمكن استرجاع المبلغ في حال عدم توفر المنتج &nbsp;أو في حال تلف الشحنة</strong></p>\r\n	</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p dir=\"rtl\"><strong>البند السابع :معلوماتك الشخصية ومعلومات تفاصيل العمليات&nbsp;: -&nbsp;<br />\r\n=============================&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>لا مانع لديك بأن تمنح متجر أرجان بكيج حقاً غير محدود، وعالمي، ودائم وغير قابل للإلغاء، ومعفي من المصاريف، و مرخص باستخدام معلومات أو مواد شخصية أو غير ذلك، زودتنا بها للموقع و/أو أعلنتها على الموقع من خلال التسجيل، الشراء، وذلك عبر النماذج المخصصة للتواصل و التسجيل، أو عبر أية رسالة إلكترونية أو أي من قنوات الاتصال المتاحة بالموقع. وذلك بهدف تشغيل وترويج الموقع وفق اتفاقية إخلاء المسؤولية وبيان الخصوصية.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>..أنت الوحيد المسؤول عن المعلومات التي قمت بإرسالها أو نشرها وينحصر دور متجر أرجان بكيج بالسماح لك بعرض هذه المعلومات على صفحات الموقع ومن خلال قنواته الإعلانية.</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>البند الثامن : تتعهد وتعلن وتضمن أنك&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>-أنك سوف تلتزم بكافة القوانين المحلية والدولية المعمول بها في هذا الشأن، وكذا الشروط و الأحكام المعمول بها في شأن استخدام الموقع.</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p dir=\"rtl\"><br />\r\n<strong>البند التاسع : حقوق الطبع&nbsp;:&nbsp;--<br />\r\n=============================</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>.كافة المحتويات التي يتضمنها الموقع، بما في ذلك وليس محصوراً في النصوص، التصاميم الجرافيكية، الشعارات، أيقونات الأزرار، الرموز، المقاطع الصوتية، الأحمال الرقمية، البيانات المجمّعة والبرامج الإلكترونية، هي ملك وحقوقها محفوظة إما للـ متجر أرجان بكيج أو لمستخدميه، وللمعدين لهذه المحتويات وللمفوضين وهي محمية ضمن حقوق الطبع المحفوظة، والعلامات التجارية وحقوق وقوانين الملكية الفكرية والإبداعية.</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>البند العاشر :العلامات التجارية&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>متجر أرجان بكيج والشعارات المملوكة، والكلمات والشعارات الأخرى على الموقع، هي محمية بحقوق وقوانين ملكية العلامات التجارية الدولية والفكرية الأخرى.</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><br />\r\n<strong>البند الحادي عشر : السرية&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>-.يتخذ متجر أرجان بكيج معايير (ملموسة وتنظيمية وتكنولوجية) للحماية من وصول شخص غير مفوض إلى معلومات هويتك الشخصية، وحفظها. مع العلم أن الانترنت ليس وسيلة آمنة، وسرية معلوماتك الشخصية لا يمكن أن تكون مضمونة 100%.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>متجر أرجان بكيج ليس له سيطرة على أفعال أي طرف ثالث، مثل صفحات الانترنت الأخرى الموصولة بهذا الموقع، أو أطراف ثالثة تدعي أنها تمثلك وتمثل آخرين...</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>. أنت تعلم وموافق أن متجر أرجان بكيج قد يستخدم معلوماتك التي زودته بها، بهدف تقديم الخدمات لك في متجر أرجان بكيج ولإرسال رسائل تسويقية لك، وان بيان الخصوصية في هذا الموقع يضبط عمليات الجمع والمعالجة والاستخدام والتحويل لمعلومات هويتك الشخصية.</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p dir=\"rtl\"><strong>البند الثاني عشر : نقض اتفاقية الشروط والأحكام&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;<br />\r\nإن متجر أرجان بكيج بحسب اتفاقية الشروط والأحكام وبحسب القانون قد يلجأ إلى وقف مؤقت أو دائم أو سحب وإلغاء عضويتك و/أو تحديد أو إلغاء وصولك إلى الموقع دون الإضرار بحقوقه الأخرى ووسائله المشروعة في استرداد حقوقك في حالة:</strong></p>\r\n\r\n<ul>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>.إذا انتهكت اتفاقية الشروط والأحكام.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>..إذا قرر متجر أرجان بكيج أن نشاطاتك قد تتسبب لك أو لمستخدمين آخرين و لـ سوق.نت في إشكالات قانونية..</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>قد يلجأ متجر أرجان بكيج &quot;بحسب تقييمه &quot; إلى إعادة نشاط المستخدمين الموقوفين، حيث أن المستخدم الذي أوقف نشاطه نهائياً أو سحبت عضويته، قد لا يكون بإمكانه التسجيل أو محاولة التسجيل في متجر أرجان بكيج ، أو استخدام الموقع بأي طريقة كانت مهما كانت الظروف، لحين السماح له بإعادة نشاطه في متجر أرجان بكيج. ومع ذلك فإنك إن قمت بانتهاك اتفاقية الشروط والاحكام هذه، فإن متجر أرجان بكيج يحتفظ بحقه في استعادة أية مبالغ مستحقة للموقع عليك، وأي خسائر وأضرار تسببت بها لـ متجر أرجان بكيج كما أن له الحق باتخاذ الإجراءات القانونية و/أو اللجوء لمحاكم المملكة العربية السعودية لرفع قضية جنائية ضدك حسبما يراه متجر أرجان بكيج مناسباً.</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>.إن قيامك أو قيام آخرين بانتهاك الاتفاقية هذه لا يلزم متجر أرجان بكيج بالتنازل عن حقه في اتخاذ الإجراءات المناسبة لمثل هذا الفعل ولغيره من أفعال الانتهاك المشابهة. و متجر أرجان بكيج لا يضمن اتخاذه إجراءات ضد كل الانتهاكات لاتفاقية الشروط والأحكام.</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>البند الثالث عشر : عملية الشراء&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;</strong></p>\r\n\r\n<ul>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>.أنت تلتزم من طرفك بإتمام عملية الشراء للبضاعة التي اشتريتها ودفع قيمتها من خلال طرق الدفع التي يوفرها متجر أرجان بكيج لمستخدميه</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>متجر أرجان بكيج له الحق في رفض أو إلغاء عمليات الشراء سواء تم الدفع أم لم يتم</strong></p>\r\n	</li>\r\n	<li dir=\"rtl\">\r\n	<p dir=\"rtl\"><strong>أنت موافق على عدم القيام بعمليات شراء زائفة على الموقع، ولا باستخدام اسم مزيف أو أي معلومات شخصية مزيفة، أو استخدام بطاقة ائتمان لغيرك دون تفويض بالشراء، حيث أن متجر أرجان بكيج له الحق في اتخاذ الإجراءات القانونية المناسبة لمقاضاة كل من يقوم بعمليات احتيالية من هذا النوع.</strong></p>\r\n	</li>\r\n</ul>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;<br />\r\nالبند الرابع عشر : محتويات غير قانونية&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>.كعضو مسجل في الموقع، يمكن لك إدراج تعليقات في الموقع وفق القوانين المعمول بها، كما أنه ممنوع عليك الإعلان عن أرقام هواتفك أو بريدك الإلكتروني من خلال أي جزء من الموقع، ويعتبر ذلك بمثابة انتهاك للشروط والأحكام.</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>البند الخامس عشر : التغذية الرجعية&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;<br />\r\nمتجر أرجان بكيج يشجع جميع الأعضاء على الموقع لتقديم اقتراحات أو تغذية رجعية (فيد باك)، وسوف تعرض على الموقع في أي مكان تحدده إدارة الموقع.</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>البند السادس عشر : إلغاء الوصول و/أو العضوية&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;<br />\r\nبدون إلحاق الضرر بحقوقه الأخرى ووسائله المشروعة لاسترداد حقوقه، يمكن لـ متجر أرجان بكيج وقف أو إلغاء عضويتك و/أو وصولك إلى الموقع في أي وقت وبدون إنذار ولأي سبب، ودون تحديد، ويمكنه أيضا إلغاء اتفاقية الشروط والأحكام هذه.</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>البند السابع عشر: تحديد المسؤوليات&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;<br />\r\nبحسب ما هو مسموح في القانون فإن متجر أرجان بكيج وموظفيه ومدرائه ووكلائه والمتفرعين عنه والمجهزين له لن يكونوا مسؤولين عن أي خسارة مباشرة أو غير مباشرة أو أعطال تنشأ عن استخدامك للموقع. إذا لم تكن راض عن الموقع أو عن أي من محتوياته فالحل و هو بعدم استمرارك باستخدامه، علاوة على ذلك فأنت موافق أن أي استخدام غير مفوض للموقع وخدماته، بسبب إهمالك سوف يتسبب بإلحاق الأذى متجر أرجان بكيج وعليه سيضطر الموقع حينها إلى اللجوء إلى الشروط والأحكام.</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>البند الثامن عشر: الأمان&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;<br />\r\nأنت موافق على توفير الأمان لـ متجر أرجان بكيج ومدرائه وموظفيه ووكلائه ومجهزيه ووقايتهم من أي ضرر قد يلحق بهم جراء مطالبات وخسائر وأعطال وتكاليف ونفقات، تحدث بسبب انتهاكك لاتفاقية الشروط والأحكام أو خرقك لأي قانون أو تعديلات أو تعدي على حقوق أطراف ثالثة.</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;<br />\r\nالبند التاسع عشر تحويل الحقوق والالتزامات:&nbsp;-<br />\r\n=============================&nbsp;<br />\r\nأنت هنا تمنح متجر أرجان بكيج الحق في تحويل جزء أو كل حقوقه ومنافعه والتزاماته ومسؤولياته، إلى أطراف أخرى تعمل معه، دون الحاجة للرجوع إليك، وذلك بحسب نصوص اتفاقية الشروط والحكام، و متجر أرجان بكيج ملتزم بإشعارك عن مثل هذه التحويلات إذا حصلت وكذلك بالنشر على الموقع.</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>البند العشرين: معلومات عامة:&nbsp; -<br />\r\n=============================&nbsp;<br />\r\nإذا كانت أية فقرة واردة في اتفاقية الشروط والأحكام هذه غير صالحة أو ملغاة أو أنها لأي سبب لم تعد نافذة، فإن مثل هذه الفقرة لا تلغي صلاحية بقية الفقرات الواردة في الاتفاقية. هذه الاتفاقية (والتي تعدل بين حين وآخر بحسب بنودها) تضع كافة الخطوط العريضة للتفاهم والاتفاق بينك وبين متجر أرجان بكيج مع الاعتبار لما يلي&nbsp;:&nbsp;<br />\r\n-ليس من حق أي شخص لا يكون طرفاً في هذه الاتفاقية أن يفرض أية بنود أو شروط فيها.&nbsp;<br />\r\n-إذا تمت ترجمة اتفاقية الشروط والأحكام لأي لغة أخرى، سواء على الموقع أو بطرق أخرى، فإن النص العربي لها يظل هو السائد.</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>&nbsp;<br />\r\nالبند الحادي و العشرين: القانون والتشريع الحاكمان&nbsp;:&nbsp;--<br />\r\n=============================&nbsp;<br />\r\nاتفاقية الشروط والأحكام هذه محكومة ومصاغة بحسب قانون المملكة العربية السعودية، وهي خاضعة تماماً وكلياً للتشريع المعمول به في المحاكم في المملكة العربية السعودية، هذه الفقرة توجد بديلاً يتم اللجوء إليه في حال انتهاء مفعول اتفاقية الشروط والأحكام هذه أو إلغاؤها لأي سبب كان ...</strong></p>\r\n', 'privacy in en', '', 1, '2018-05-16 05:38:18', '2018-06-04 00:15:10'),
(16, 'سياسة الاستبدال والاسترجاع', 'ww', 'سياسة الاستبدال والاسترجاع', 'www', '<p><strong>ساسية استرجاع المبلغ&nbsp;</strong></p>\r\n\r\n<ul dir=\"rtl\">\r\n	<li><strong>اذا تم تحويل المبلغ ورفعت الفاتورة لايسترجع المبلغ ولا يضاف او يحذف اي منتج.</strong></li>\r\n	<li><strong>اذا تم شحن الشحنة &nbsp;مع شركة الشحن ورجعت إلى <span style=\"color:#00FF00\">أرجان بكيج</span> مرة &nbsp;اخرى لايمكن استرجاع المبلغ.</strong></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>wwwwwww</strong></p>\r\n', 'privacy', '', 1, '2018-05-16 05:38:53', '2019-11-14 06:15:18'),
(17, 'مدة توصيل الطلب', NULL, 'مدة توصيل الطلب ', NULL, '<p dir=\"rtl\"><strong>مدة التوصيل داخل المملكة العربية السعودية من 7 إلى 10 أيام ,, من إرسال الشحنة إلى شركة الشحن غالبا تصل أسرع من ذلك .</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>مدة التوصيل إلى دول خارج المملكة العربية السعودية من 10 إلى 20 يوم ,, من إرسال الشحنة إلى شركة الشحن غالبا تصل أسرع من ذلك .</strong></p>\r\n', NULL, '', 1, '2018-05-16 12:19:47', '2018-06-04 00:05:23'),
(18, 'عنوان الشركة', 'Company Adrs', 'عنوان الشركة', 'Company Adrs', '<p dir=\"rtl\"><strong>العنوان :</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>المملكة العربية السعودية </strong></p>\r\n\r\n<p dir=\"rtl\"><strong>الرياض - حي الحمراء</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>وسائل الاتصال :</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>ايميل : shabab.ale3lam@gmail.com</strong></p>\r\n\r\n<p dir=\"rtl\"><strong>جوال :00966560872874</strong></p>\r\n', NULL, '', 1, '2018-05-16 12:20:28', '2020-04-02 05:38:20'),
(19, 'سياسة الخصوصية', 'policy', 'سياسة الخصوصية', 'policy', 'سياسة الخصوصيةسياسة الخصوصيةسياسة الخصوصيةسياسة الخصوصيةسياسة الخصوصيةسياسة الخصوصيةسياسة الخصوصيةسياسة الخصوصيةسياسة الخصوصيةسياسة الخصوصيةسياسة الخصوصيةسياسة الخصوصيةسياسة الخصوصية', NULL, NULL, 1, '2020-02-10 10:38:49', '0000-00-00 00:00:00'),
(20, 'حساباتنا البنكية', 'Our Accounts', 'حساباتنا البنكية', 'Bank Accounts', '<p>أرقام الحسابات البنكية للتحويل البنكي</p>\r\n\r\n<p><img alt=\"\" src=\"http://d.up-00.com/2018/09/153802984437464.jpeg\" style=\"height:400px; width:400px\" /></p>\r\n', NULL, '', 1, '2018-09-27 03:32:26', '2019-09-29 05:20:53'),
(99, 'نبذة عن المؤسسة', 'contact us', 'نبذة عن أرجان', 'About Argan', '<p>نبذة عن أرجان نبذة عن أرجان نبذة عن أرجان نبذة عن أرجان</p>\r\n', 'abous us in english', '73d32a1f220f035e.png', 1, '2019-10-28 09:18:45', '2020-01-15 22:06:59');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `status`, `created_at`, `updated_at`) VALUES
(3, 1, '2020-09-13 17:48:33', '2020-09-13 17:48:33'),
(4, 1, '2020-09-13 17:49:06', '2020-09-13 17:49:06');

-- --------------------------------------------------------

--
-- Table structure for table `faqs_trans`
--

CREATE TABLE `faqs_trans` (
  `id` int(11) NOT NULL,
  `rowId` int(11) NOT NULL,
  `languageCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT 'AR',
  `description1` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description2` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `faqs_trans`
--

INSERT INTO `faqs_trans` (`id`, `rowId`, `languageCode`, `description1`, `description2`) VALUES
(1, 3, 'AR', 'ما هو البطيخ', '<p>فاكهة لذلذة</p>'),
(2, 4, 'AR', 'ما هو الخوخ', '<p>فاكهة جميلة</p>'),
(3, 4, 'EN', 'What is Khukh', '<p>good fruit</p>');

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `status`, `photo`) VALUES
(1, 1, '9cbe3785ff841d47.jpg'),
(2, 1, 'afbdfcf5c3cfa45b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `features_trans`
--

CREATE TABLE `features_trans` (
  `id` int(11) NOT NULL,
  `rowId` int(11) NOT NULL,
  `languageCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description1` mediumtext COLLATE utf8_unicode_ci,
  `description2` mediumtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `features_trans`
--

INSERT INTO `features_trans` (`id`, `rowId`, `languageCode`, `title`, `description1`, `description2`) VALUES
(1, 1, 'AR', 'بعيد', '<p>وصف بعيد 1</p>', '<p>وصف بعيد 2</p>'),
(2, 2, 'AR', 'قصير', '<p>وصف قصير 1</p>', '<p>وصف قصير 2</p>'),
(3, 2, 'EN', 'Short', '<p>Short Desc 1</p>', '<p>Short Desc 2</p>'),
(4, 1, 'EN', 'Far', '<p>Far description 1</p>', '<p>Far description 2</p>');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `title` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `languageCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `title`, `languageCode`, `status`, `photo`) VALUES
(1, 'عربي', 'AR', 1, NULL),
(2, 'English', 'EN', 1, NULL),
(3, 'Türkçe', 'TR', 1, NULL),
(4, 'Frencha', 'FR', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_name` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `fileName` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumb` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `mimeType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `old_name`, `fileName`, `thumb`, `size`, `mimeType`, `created_at`, `updated_at`) VALUES
(1, 'talabat.jpg', '0a57bc701b4aadc7.jpg', 'thumb-0a57bc701b4aadc7.jpg', '31336', 'image/jpeg', '2018-05-08 21:20:10', '2018-05-08 21:20:10'),
(2, 'تنزيل (2).png', 'f7bc317f2a7d9239.png', 'thumb-f7bc317f2a7d9239.png', '2462', 'image/png', '2018-05-12 10:47:04', '2018-05-12 10:47:04'),
(3, 'تنزيل (1).png', '25c7f44dfe235d85.png', 'thumb-25c7f44dfe235d85.png', '4154', 'image/png', '2018-05-12 10:47:19', '2018-05-12 10:47:19'),
(4, 'تنزيل.png', '0b0c5a2766ff439e.png', 'thumb-0b0c5a2766ff439e.png', '3144', 'image/png', '2018-05-12 10:47:19', '2018-05-12 10:47:19'),
(5, 'بيفورت.png', '9b2f84916e619bd6.png', 'thumb-9b2f84916e619bd6.png', '18091', 'image/png', '2018-05-12 10:47:20', '2018-05-12 10:47:20'),
(6, 'unnamed-187.jpg', 'be3399d7e6cb34f2.jpg', 'thumb-be3399d7e6cb34f2.jpg', '45704', 'image/jpeg', '2018-05-12 10:47:21', '2018-05-12 10:47:21'),
(7, 'تنزيل.jpg', '63b7589d5e68ebd6.jpg', 'thumb-63b7589d5e68ebd6.jpg', '4498', 'image/jpeg', '2018-05-12 10:50:05', '2018-05-12 10:50:05'),
(8, '0e823bfd-d36c-43d7-ad4b-2633da203b7f.jpg', '9c93ae9e24a778a5.jpg', 'thumb-9c93ae9e24a778a5.jpg', '103005', 'image/jpeg', '2018-05-26 19:52:27', '2018-05-26 19:52:27'),
(9, 'لقطة الشاشة ١٤٣٩-٠٩-١٣ في ٥.٤١.٥٢ ص.png', '49606172090dae76.png', 'thumb-49606172090dae76.png', '78981', 'image/png', '2018-05-28 16:48:43', '2018-05-28 16:48:43'),
(527, 'IMG-20190211-WA0110.jpg', 'a4457e8fb7a5846c.jpg', 'thumb-a4457e8fb7a5846c.jpg', '41531', 'image/jpeg', '2019-03-16 05:38:39', '2019-03-16 05:38:39'),
(528, 'IMG-20190212-WA0062.jpg', '98c6abebafd42c49.jpg', 'thumb-98c6abebafd42c49.jpg', '164661', 'image/jpeg', '2019-03-16 05:38:47', '2019-03-16 05:38:47'),
(529, 'IMG-20190212-WA0042.jpg', '344a2621da3447bd.jpg', 'thumb-344a2621da3447bd.jpg', '58801', 'image/jpeg', '2019-03-16 05:41:25', '2019-03-16 05:41:25'),
(530, 'IMG-20190212-WA0065.jpg', '8b4f960701b3a176.jpg', 'thumb-8b4f960701b3a176.jpg', '64071', 'image/jpeg', '2019-03-16 05:41:26', '2019-03-16 05:41:26'),
(531, 'IMG-20190212-WA0066.jpg', '33d4f2578b0a0686.jpg', 'thumb-33d4f2578b0a0686.jpg', '69501', 'image/jpeg', '2019-03-16 05:41:26', '2019-03-16 05:41:26'),
(532, 'IMG-20190212-WA0040.jpg', '04bf019155f7e296.jpg', 'thumb-04bf019155f7e296.jpg', '64924', 'image/jpeg', '2019-03-16 05:41:27', '2019-03-16 05:41:27'),
(533, 'IMG-20190205-WA0197.jpg', 'b18c798b2068069f.jpg', 'thumb-b18c798b2068069f.jpg', '56785', 'image/jpeg', '2019-03-16 06:01:46', '2019-03-16 06:01:46'),
(534, 'IMG-20190205-WA0201.jpg', 'a28d2b99456014f2.jpg', 'thumb-a28d2b99456014f2.jpg', '69307', 'image/jpeg', '2019-03-16 06:01:47', '2019-03-16 06:01:47'),
(535, 'IMG-20190205-WA0214.jpg', '676a30175985c894.jpg', 'thumb-676a30175985c894.jpg', '60775', 'image/jpeg', '2019-03-16 06:01:48', '2019-03-16 06:01:48'),
(536, 'IMG-20190205-WA0187.jpg', '5fa20bad3bd63c1a.jpg', 'thumb-5fa20bad3bd63c1a.jpg', '69307', 'image/jpeg', '2019-03-16 06:01:50', '2019-03-16 06:01:50'),
(537, 'IMG-20190205-WA0204.jpg', 'a23722b172f6c1c4.jpg', 'thumb-a23722b172f6c1c4.jpg', '76804', 'image/jpeg', '2019-03-16 06:01:56', '2019-03-16 06:01:56'),
(538, 'IMG-20190205-WA0213.jpg', '41f097bc03c13e0d.jpg', 'thumb-41f097bc03c13e0d.jpg', '52572', 'image/jpeg', '2019-03-16 06:01:57', '2019-03-16 06:01:57'),
(539, 'IMG-20190205-WA0219.jpg', '54206b4cd5a65d52.jpg', 'thumb-54206b4cd5a65d52.jpg', '44206', 'image/jpeg', '2019-03-16 06:01:57', '2019-03-16 06:01:57'),
(540, 'IMG-20190205-WA0221.jpg', '329c00fc8e84974b.jpg', 'thumb-329c00fc8e84974b.jpg', '106552', 'image/jpeg', '2019-03-16 06:01:59', '2019-03-16 06:01:59'),
(541, 'IMG-20190205-WA0196.jpg', '09747edb2e60b02b.jpg', 'thumb-09747edb2e60b02b.jpg', '50169', 'image/jpeg', '2019-03-16 06:01:59', '2019-03-16 06:01:59'),
(542, 'IMG-20190205-WA0199.jpg', '8407a6b327674eca.jpg', 'thumb-8407a6b327674eca.jpg', '56980', 'image/jpeg', '2019-03-16 06:02:08', '2019-03-16 06:02:08'),
(543, 'IMG-20190205-WA0206 (1).jpg', '48be04b3eb5f89ba.jpg', 'thumb-48be04b3eb5f89ba.jpg', '66074', 'image/jpeg', '2019-03-16 06:02:09', '2019-03-16 06:02:09'),
(544, 'IMG-20190205-WA0206.jpg', '6a2a881748aadb93.jpg', 'thumb-6a2a881748aadb93.jpg', '66074', 'image/jpeg', '2019-03-16 06:02:10', '2019-03-16 06:02:10'),
(545, 'IMG-20190205-WA0215 (1).jpg', '78f16efac3e67e87.jpg', 'thumb-78f16efac3e67e87.jpg', '58902', 'image/jpeg', '2019-03-16 06:02:10', '2019-03-16 06:02:10'),
(546, 'IMG-20190205-WA0215.jpg', '6b73333aa3f18a8c.jpg', 'thumb-6b73333aa3f18a8c.jpg', '58902', 'image/jpeg', '2019-03-16 06:02:11', '2019-03-16 06:02:11'),
(547, 'IMG-20190205-WA0216 (1).jpg', '94525e8cbcd5f39d.jpg', 'thumb-94525e8cbcd5f39d.jpg', '40631', 'image/jpeg', '2019-03-16 06:02:12', '2019-03-16 06:02:12'),
(548, 'IMG-20190205-WA0216.jpg', 'f04eac74512665e4.jpg', 'thumb-f04eac74512665e4.jpg', '40631', 'image/jpeg', '2019-03-16 06:02:12', '2019-03-16 06:02:12'),
(549, 'IMG-20190205-WA0288.jpg', '6a72de173960c3c3.jpg', 'thumb-6a72de173960c3c3.jpg', '43687', 'image/jpeg', '2019-03-16 06:02:13', '2019-03-16 06:02:13'),
(550, 'IMG-20190205-WA0305 (1).jpg', '3f4d63eadaf2726c.jpg', 'thumb-3f4d63eadaf2726c.jpg', '78085', 'image/jpeg', '2019-03-16 06:02:14', '2019-03-16 06:02:14'),
(551, 'IMG-20190205-WA0305.jpg', '18accdb341d53846.jpg', 'thumb-18accdb341d53846.jpg', '78085', 'image/jpeg', '2019-03-16 06:02:15', '2019-03-16 06:02:15'),
(552, 'IMG-20190211-WA0053 (1).jpg', '293a525ff9e43329.jpg', 'thumb-293a525ff9e43329.jpg', '50328', 'image/jpeg', '2019-03-16 06:02:15', '2019-03-16 06:02:15'),
(553, 'IMG-20190211-WA0053.jpg', 'c7263e1d34a70cf9.jpg', 'thumb-c7263e1d34a70cf9.jpg', '50328', 'image/jpeg', '2019-03-16 06:02:16', '2019-03-16 06:02:16'),
(554, 'IMG-20190205-WA0184.jpg', '94816e3c116048bc.jpg', 'thumb-94816e3c116048bc.jpg', '65620', 'image/jpeg', '2019-03-16 06:02:17', '2019-03-16 06:02:17'),
(555, 'IMG-20190205-WA0190 (1).jpg', '2f03c5719fc1e48b.jpg', 'thumb-2f03c5719fc1e48b.jpg', '51640', 'image/jpeg', '2019-03-16 06:02:17', '2019-03-16 06:02:17'),
(556, 'IMG-20190205-WA0190.jpg', 'b4db6f39d773b339.jpg', 'thumb-b4db6f39d773b339.jpg', '51640', 'image/jpeg', '2019-03-16 06:02:18', '2019-03-16 06:02:18'),
(557, 'IMG-20190205-WA0199 (1).jpg', '339c012797a153dd.jpg', 'thumb-339c012797a153dd.jpg', '56980', 'image/jpeg', '2019-03-16 06:02:19', '2019-03-16 06:02:19'),
(558, 'IMG-20190205-WA0287 (1).jpg', 'd0d134b827f80a62.jpg', 'thumb-d0d134b827f80a62.jpg', '41788', 'image/jpeg', '2019-03-16 06:02:23', '2019-03-16 06:02:23'),
(559, 'IMG-20190205-WA0287.jpg', 'e4e7c1e349479666.jpg', 'thumb-e4e7c1e349479666.jpg', '41788', 'image/jpeg', '2019-03-16 06:02:23', '2019-03-16 06:02:23'),
(560, 'IMG-20190205-WA0309.jpg', '0ff18d7fe2029446.jpg', 'thumb-0ff18d7fe2029446.jpg', '81088', 'image/jpeg', '2019-03-16 06:02:24', '2019-03-16 06:02:24'),
(561, 'IMG-20190205-WA0203.jpg', 'd151907e6c5bd3e6.jpg', 'thumb-d151907e6c5bd3e6.jpg', '78594', 'image/jpeg', '2019-03-16 06:02:25', '2019-03-16 06:02:25'),
(562, 'IMG-20190205-WA0211.jpg', '44a7a2c811a4105c.jpg', 'thumb-44a7a2c811a4105c.jpg', '51493', 'image/jpeg', '2019-03-16 06:02:26', '2019-03-16 06:02:26'),
(563, 'IMG-20190205-WA0195.jpg', 'adb2179300e315e0.jpg', 'thumb-adb2179300e315e0.jpg', '45556', 'image/jpeg', '2019-03-16 06:02:32', '2019-03-16 06:02:32'),
(564, 'IMG-20190205-WA0200.jpg', 'f09e0cd78397c06c.jpg', 'thumb-f09e0cd78397c06c.jpg', '74860', 'image/jpeg', '2019-03-16 06:02:33', '2019-03-16 06:02:33'),
(565, 'IMG-20190205-WA0212.jpg', '07a117bdf8f0b819.jpg', 'thumb-07a117bdf8f0b819.jpg', '52839', 'image/jpeg', '2019-03-16 06:02:34', '2019-03-16 06:02:34'),
(566, 'IMG-20190205-WA0189.jpg', '834ec4374cd145a3.jpg', 'thumb-834ec4374cd145a3.jpg', '74860', 'image/jpeg', '2019-03-16 06:02:34', '2019-03-16 06:02:34'),
(567, 'IMG-20190205-WA0193.jpg', 'be3dde092030dd9b.jpg', 'thumb-be3dde092030dd9b.jpg', '52570', 'image/jpeg', '2019-03-16 06:02:35', '2019-03-16 06:02:35'),
(568, '3966cc31-355a-4874-8b75-7c7abb4c782f.jpg', '3b88ec63937f177d.jpg', 'thumb-3b88ec63937f177d.jpg', '75092', 'image/jpeg', '2019-03-16 06:02:43', '2019-03-16 06:02:43'),
(569, '4844d777-79ed-4bd3-88ae-6ee728f7a11f.jpg', '45c8fa4c8ca3c52e.jpg', 'thumb-45c8fa4c8ca3c52e.jpg', '74013', 'image/jpeg', '2019-03-16 06:02:43', '2019-03-16 06:02:43'),
(570, 'IMG-20190205-WA0302.jpg', '16d7dd5798f9e663.jpg', 'thumb-16d7dd5798f9e663.jpg', '55866', 'image/jpeg', '2019-03-16 06:02:44', '2019-03-16 06:02:44'),
(571, 'IMG-20190212-WA0054.jpg', 'debbfcf563e77618.jpg', 'thumb-debbfcf563e77618.jpg', '49672', 'image/jpeg', '2019-03-16 06:02:45', '2019-03-16 06:02:45'),
(572, 'IMG-20190212-WA0055.jpg', 'c5972415a7d02e24.jpg', 'thumb-c5972415a7d02e24.jpg', '35196', 'image/jpeg', '2019-03-16 06:02:45', '2019-03-16 06:02:45'),
(573, 'IMG-20190212-WA0056.jpg', 'ef0eee25d7337bf0.jpg', 'thumb-ef0eee25d7337bf0.jpg', '31014', 'image/jpeg', '2019-03-16 06:02:46', '2019-03-16 06:02:46'),
(574, '7fd25df1-e0c5-428f-a39c-09da55534a3d.jpg', '28048bd647a92cbc.jpg', 'thumb-28048bd647a92cbc.jpg', '68065', 'image/jpeg', '2019-03-16 06:02:47', '2019-03-16 06:02:47'),
(575, '0163f600-232a-40b7-8732-35eb0f5391f2.jpg', 'c84b6695e24dffaf.jpg', 'thumb-c84b6695e24dffaf.jpg', '72124', 'image/jpeg', '2019-03-16 06:02:48', '2019-03-16 06:02:48'),
(576, 'IMG-20190205-WA0292.jpg', 'c20c51ab25f56659.jpg', 'thumb-c20c51ab25f56659.jpg', '44039', 'image/jpeg', '2019-03-16 06:02:53', '2019-03-16 06:02:53'),
(577, 'IMG-20190211-WA0145.jpg', 'de87b304e8988aaf.jpg', 'thumb-de87b304e8988aaf.jpg', '62496', 'image/jpeg', '2019-03-16 06:02:53', '2019-03-16 06:02:53'),
(578, 'IMG-20190205-WA0254.jpg', '96f20ba20de89c23.jpg', 'thumb-96f20ba20de89c23.jpg', '96129', 'image/jpeg', '2019-03-16 06:02:54', '2019-03-16 06:02:54'),
(579, 'IMG-20190212-WA0060 (1).jpg', 'dcb797e833e69c30.jpg', 'thumb-dcb797e833e69c30.jpg', '49850', 'image/jpeg', '2019-03-16 06:03:02', '2019-03-16 06:03:02'),
(580, 'IMG-20190212-WA0060.jpg', 'f3ff541e2acd5f38.jpg', 'thumb-f3ff541e2acd5f38.jpg', '49850', 'image/jpeg', '2019-03-16 06:03:02', '2019-03-16 06:03:02'),
(581, 'IMG-20190212-WA0062.jpg', '254ec8a659a3d0ff.jpg', 'thumb-254ec8a659a3d0ff.jpg', '164661', 'image/jpeg', '2019-03-16 06:03:03', '2019-03-16 06:03:03'),
(582, 'IMG-20190211-WA0110.jpg', '2c0a40cc6f1b0d1c.jpg', 'thumb-2c0a40cc6f1b0d1c.jpg', '41531', 'image/jpeg', '2019-03-16 06:03:04', '2019-03-16 06:03:04'),
(583, 'IMG-20190220-WA0050.jpg', 'bab47daf23a2312c.jpg', 'thumb-bab47daf23a2312c.jpg', '135750', 'image/jpeg', '2019-03-16 06:03:18', '2019-03-16 06:03:18'),
(584, 'IMG-20190220-WA0051.jpg', '91bc98925eb8400a.jpg', 'thumb-91bc98925eb8400a.jpg', '175051', 'image/jpeg', '2019-03-16 06:03:19', '2019-03-16 06:03:19'),
(585, 'IMG-20190220-WA0052.jpg', 'df644818a3251694.jpg', 'thumb-df644818a3251694.jpg', '166086', 'image/jpeg', '2019-03-16 06:03:20', '2019-03-16 06:03:20'),
(586, 'IMG-20190220-WA0049.jpg', '922f8242ef9a8993.jpg', 'thumb-922f8242ef9a8993.jpg', '150932', 'image/jpeg', '2019-03-16 06:03:21', '2019-03-16 06:03:21'),
(587, 'IMG-20190214-WA0064.jpg', 'da799024f2be12cf.jpg', 'thumb-da799024f2be12cf.jpg', '107131', 'image/jpeg', '2019-03-16 06:03:29', '2019-03-16 06:03:29'),
(588, 'IMG-20190219-WA0058.jpg', 'd9df1e26856710f8.jpg', 'thumb-d9df1e26856710f8.jpg', '73275', 'image/jpeg', '2019-03-16 06:03:30', '2019-03-16 06:03:30'),
(589, 'IMG-20190219-WA0059.jpg', '4f9676955cc6bfbc.jpg', 'thumb-4f9676955cc6bfbc.jpg', '73393', 'image/jpeg', '2019-03-16 06:03:30', '2019-03-16 06:03:30'),
(590, 'IMG-20190211-WA0109.jpg', 'ad594537a6d2f3bd.jpg', 'thumb-ad594537a6d2f3bd.jpg', '53198', 'image/jpeg', '2019-03-16 06:03:31', '2019-03-16 06:03:31'),
(591, 'IMG-20190214-WA0051.jpg', '7e80ba92300921fe.jpg', 'thumb-7e80ba92300921fe.jpg', '245081', 'image/jpeg', '2019-03-16 06:03:32', '2019-03-16 06:03:32'),
(592, 'IMG-20190211-WA0115.jpg', '7a77c0ca9ff4ccce.jpg', 'thumb-7a77c0ca9ff4ccce.jpg', '55852', 'image/jpeg', '2019-03-16 06:03:37', '2019-03-16 06:03:37'),
(593, 'IMG-20190212-WA0058.jpg', 'fa59d8b53ca57b8e.jpg', 'thumb-fa59d8b53ca57b8e.jpg', '131075', 'image/jpeg', '2019-03-16 06:03:37', '2019-03-16 06:03:37'),
(594, 'IMG-20190214-WA0045.jpg', '2a0b06fab1bcdc7a.jpg', 'thumb-2a0b06fab1bcdc7a.jpg', '126278', 'image/jpeg', '2019-03-16 06:03:38', '2019-03-16 06:03:38'),
(595, 'IMG-20190211-WA0113.jpg', 'f6935027ec8ebe5f.jpg', 'thumb-f6935027ec8ebe5f.jpg', '45783', 'image/jpeg', '2019-03-16 06:03:39', '2019-03-16 06:03:39'),
(596, 'IMG-20190219-WA0043.jpg', 'dd3fddef2c409737.jpg', 'thumb-dd3fddef2c409737.jpg', '123869', 'image/jpeg', '2019-03-16 06:03:46', '2019-03-16 06:03:46'),
(597, 'IMG-20190219-WA0049.jpg', 'c46d76a74e9eab1e.jpg', 'thumb-c46d76a74e9eab1e.jpg', '92238', 'image/jpeg', '2019-03-16 06:03:46', '2019-03-16 06:03:46'),
(598, 'IMG-20190219-WA0050.jpg', 'f234b08b3796ded4.jpg', 'thumb-f234b08b3796ded4.jpg', '79154', 'image/jpeg', '2019-03-16 06:03:47', '2019-03-16 06:03:47'),
(599, 'IMG-20190219-WA0051.jpg', '994bb9db9c37628b.jpg', 'thumb-994bb9db9c37628b.jpg', '79040', 'image/jpeg', '2019-03-16 06:03:48', '2019-03-16 06:03:48'),
(600, 'IMG-20190212-WA0057.jpg', '587cbaf941bab069.jpg', 'thumb-587cbaf941bab069.jpg', '71950', 'image/jpeg', '2019-03-16 06:03:49', '2019-03-16 06:03:49'),
(601, 'IMG-20190212-WA0064.jpg', 'e0a7c12e0a343688.jpg', 'thumb-e0a7c12e0a343688.jpg', '93395', 'image/jpeg', '2019-03-16 06:03:49', '2019-03-16 06:03:49'),
(602, 'IMG-20190211-WA0046.jpg', '2f426a730a6c081c.jpg', 'thumb-2f426a730a6c081c.jpg', '61194', 'image/jpeg', '2019-03-16 06:03:54', '2019-03-16 06:03:54'),
(603, 'IMG-20190218-WA0037.jpg', '6b429830033e7f6f.jpg', 'thumb-6b429830033e7f6f.jpg', '106964', 'image/jpeg', '2019-03-16 06:03:55', '2019-03-16 06:03:55'),
(604, 'IMG-20190218-WA0039.jpg', 'ae5bcddb99c78aa1.jpg', 'thumb-ae5bcddb99c78aa1.jpg', '93799', 'image/jpeg', '2019-03-16 06:03:55', '2019-03-16 06:03:55'),
(605, 'IMG-20190218-WA0040.jpg', 'a0857ef5302f6fd5.jpg', 'thumb-a0857ef5302f6fd5.jpg', '82284', 'image/jpeg', '2019-03-16 06:03:56', '2019-03-16 06:03:56'),
(606, 'IMG-20190205-WA0298.jpg', '43e0bbfc7ff480c4.jpg', 'thumb-43e0bbfc7ff480c4.jpg', '43727', 'image/jpeg', '2019-03-16 06:03:57', '2019-03-16 06:03:57'),
(607, 'IMG-20190218-WA0033.jpg', '15424c8a7cb32820.jpg', 'thumb-15424c8a7cb32820.jpg', '55565', 'image/jpeg', '2019-03-16 06:04:02', '2019-03-16 06:04:02'),
(608, 'IMG-20190218-WA0034.jpg', '3b0c024a5c7aac43.jpg', 'thumb-3b0c024a5c7aac43.jpg', '75240', 'image/jpeg', '2019-03-16 06:04:03', '2019-03-16 06:04:03'),
(609, 'IMG-20190218-WA0035.jpg', 'd6eefd6e0eb4bc16.jpg', 'thumb-d6eefd6e0eb4bc16.jpg', '85003', 'image/jpeg', '2019-03-16 06:04:03', '2019-03-16 06:04:03'),
(610, 'IMG-20190218-WA0036.jpg', '5cdcf1218acaaed6.jpg', 'thumb-5cdcf1218acaaed6.jpg', '48824', 'image/jpeg', '2019-03-16 06:04:04', '2019-03-16 06:04:04'),
(611, 'IMG-20190218-WA0031.jpg', '4b317f7dabc511bc.jpg', 'thumb-4b317f7dabc511bc.jpg', '74574', 'image/jpeg', '2019-03-16 06:04:05', '2019-03-16 06:04:05'),
(612, 'IMG-20190218-WA0032.jpg', '004529c3d53eec81.jpg', 'thumb-004529c3d53eec81.jpg', '48833', 'image/jpeg', '2019-03-16 06:04:06', '2019-03-16 06:04:06'),
(613, 'IMG-20190206-WA0038.jpg', '2bad27a2f28531c9.jpg', 'thumb-2bad27a2f28531c9.jpg', '43223', 'image/jpeg', '2019-03-16 06:04:15', '2019-03-16 06:04:15'),
(614, 'IMG-20190206-WA0042.jpg', '77eb978f757a3a2f.jpg', 'thumb-77eb978f757a3a2f.jpg', '40437', 'image/jpeg', '2019-03-16 06:04:16', '2019-03-16 06:04:16'),
(615, 'IMG-20190206-WA0043.jpg', 'c0e2f61712889366.jpg', 'thumb-c0e2f61712889366.jpg', '35779', 'image/jpeg', '2019-03-16 06:04:17', '2019-03-16 06:04:17'),
(616, 'IMG-20190211-WA0107.jpg', '13f5431dda015b32.jpg', 'thumb-13f5431dda015b32.jpg', '41859', 'image/jpeg', '2019-03-16 06:04:17', '2019-03-16 06:04:17'),
(617, 'IMG-20190211-WA0108.jpg', '631210cb3704efa3.jpg', 'thumb-631210cb3704efa3.jpg', '41018', 'image/jpeg', '2019-03-16 06:04:18', '2019-03-16 06:04:18'),
(618, 'IMG-20190205-WA0276.jpg', '7c345bf3c0f948c1.jpg', 'thumb-7c345bf3c0f948c1.jpg', '27406', 'image/jpeg', '2019-03-16 06:04:19', '2019-03-16 06:04:19'),
(619, 'IMG-20190205-WA0281.jpg', '7d3015b037b36ef0.jpg', 'thumb-7d3015b037b36ef0.jpg', '32320', 'image/jpeg', '2019-03-16 06:04:19', '2019-03-16 06:04:19'),
(620, 'IMG-20190206-WA0037 (1).jpg', 'aa4b4cf432ae9767.jpg', 'thumb-aa4b4cf432ae9767.jpg', '100578', 'image/jpeg', '2019-03-16 06:04:24', '2019-03-16 06:04:24'),
(621, 'IMG-20190206-WA0037.jpg', '03d27d1e604bf13f.jpg', 'thumb-03d27d1e604bf13f.jpg', '100578', 'image/jpeg', '2019-03-16 06:04:25', '2019-03-16 06:04:25'),
(622, 'IMG-20190206-WA0047.jpg', 'daa685b04ca2569c.jpg', 'thumb-daa685b04ca2569c.jpg', '40690', 'image/jpeg', '2019-03-16 06:04:26', '2019-03-16 06:04:26'),
(623, 'IMG-20190206-WA0048.jpg', '08cffbe2401772b3.jpg', 'thumb-08cffbe2401772b3.jpg', '40168', 'image/jpeg', '2019-03-16 06:04:26', '2019-03-16 06:04:26'),
(624, 'IMG-20190206-WA0049.jpg', '8398938e8162e9d1.jpg', 'thumb-8398938e8162e9d1.jpg', '43357', 'image/jpeg', '2019-03-16 06:04:27', '2019-03-16 06:04:27'),
(625, 'IMG-20190211-WA0101.jpg', 'a282c863ab91ec39.jpg', 'thumb-a282c863ab91ec39.jpg', '59629', 'image/jpeg', '2019-03-16 06:04:28', '2019-03-16 06:04:28'),
(626, 'IMG-20190211-WA0102.jpg', 'dbc710a404ba4c8e.jpg', 'thumb-dbc710a404ba4c8e.jpg', '58240', 'image/jpeg', '2019-03-16 06:04:28', '2019-03-16 06:04:28'),
(627, 'IMG-20190205-WA0269.jpg', 'a435ea26dbbdbf8a.jpg', 'thumb-a435ea26dbbdbf8a.jpg', '34243', 'image/jpeg', '2019-03-16 06:04:29', '2019-03-16 06:04:29'),
(628, 'IMG-20190205-WA0271.jpg', '06f45018b6775d56.jpg', 'thumb-06f45018b6775d56.jpg', '29605', 'image/jpeg', '2019-03-16 06:04:30', '2019-03-16 06:04:30'),
(629, 'IMG-20190205-WA0274.jpg', 'f45865946904b877.jpg', 'thumb-f45865946904b877.jpg', '35068', 'image/jpeg', '2019-03-16 06:04:30', '2019-03-16 06:04:30'),
(630, 'IMG-20190206-WA0034.jpg', 'efcac2adfa364c47.jpg', 'thumb-efcac2adfa364c47.jpg', '46034', 'image/jpeg', '2019-03-16 06:04:35', '2019-03-16 06:04:35'),
(631, 'IMG-20190206-WA0035.jpg', 'f987ac7ee4d93275.jpg', 'thumb-f987ac7ee4d93275.jpg', '28224', 'image/jpeg', '2019-03-16 06:04:35', '2019-03-16 06:04:35'),
(632, 'IMG-20190206-WA0036.jpg', '86918d30f5a80346.jpg', 'thumb-86918d30f5a80346.jpg', '26054', 'image/jpeg', '2019-03-16 06:04:36', '2019-03-16 06:04:36'),
(633, 'IMG-20190211-WA0065.jpg', 'e3928b278d0fd4c7.jpg', 'thumb-e3928b278d0fd4c7.jpg', '28991', 'image/jpeg', '2019-03-16 06:04:36', '2019-03-16 06:04:36'),
(634, 'IMG-20190211-WA0116.jpg', 'df3ff85f0e1dfba4.jpg', 'thumb-df3ff85f0e1dfba4.jpg', '28602', 'image/jpeg', '2019-03-16 06:04:37', '2019-03-16 06:04:37'),
(635, 'IMG-20190206-WA0034 (1).jpg', '718206076027080b.jpg', 'thumb-718206076027080b.jpg', '46034', 'image/jpeg', '2019-03-16 06:04:38', '2019-03-16 06:04:38'),
(636, 'IMG-20190206-WA0045.jpg', 'd8fb1a5342192184.jpg', 'thumb-d8fb1a5342192184.jpg', '34119', 'image/jpeg', '2019-03-16 06:04:42', '2019-03-16 06:04:42'),
(637, 'IMG-20190211-WA0062.jpg', '29c07c820bc462f7.jpg', 'thumb-29c07c820bc462f7.jpg', '29763', 'image/jpeg', '2019-03-16 06:04:43', '2019-03-16 06:04:43'),
(638, 'IMG-20190211-WA0125.jpg', 'be1dae143a9ad104.jpg', 'thumb-be1dae143a9ad104.jpg', '32134', 'image/jpeg', '2019-03-16 06:04:44', '2019-03-16 06:04:44'),
(639, 'IMG-20190212-WA0043.jpg', '985347bd17b2c504.jpg', 'thumb-985347bd17b2c504.jpg', '53241', 'image/jpeg', '2019-03-16 06:04:44', '2019-03-16 06:04:44'),
(640, 'IMG-20190212-WA0044 (1).jpg', '37eece178a82686a.jpg', 'thumb-37eece178a82686a.jpg', '54822', 'image/jpeg', '2019-03-16 06:04:45', '2019-03-16 06:04:45'),
(641, 'IMG-20190212-WA0044.jpg', 'b2cbc280a3b905f2.jpg', 'thumb-b2cbc280a3b905f2.jpg', '54822', 'image/jpeg', '2019-03-16 06:04:46', '2019-03-16 06:04:46'),
(642, 'IMG-20190205-WA0275.jpg', '0f6e1350bb71f55e.jpg', 'thumb-0f6e1350bb71f55e.jpg', '30502', 'image/jpeg', '2019-03-16 06:04:46', '2019-03-16 06:04:46'),
(643, 'IMG-20190206-WA0044.jpg', 'c5f52d9c08c8e42b.jpg', 'thumb-c5f52d9c08c8e42b.jpg', '40353', 'image/jpeg', '2019-03-16 06:04:47', '2019-03-16 06:04:47'),
(644, 'IMG-20190205-WA0226.jpg', '87f109096caa923e.jpg', 'thumb-87f109096caa923e.jpg', '50203', 'image/jpeg', '2019-03-16 06:04:52', '2019-03-16 06:04:52'),
(645, 'IMG-20190205-WA0227.jpg', 'a5707e7a7f03ec1d.jpg', 'thumb-a5707e7a7f03ec1d.jpg', '41805', 'image/jpeg', '2019-03-16 06:04:52', '2019-03-16 06:04:52'),
(646, 'IMG-20190205-WA0239.jpg', 'ab25711988bcc36e.jpg', 'thumb-ab25711988bcc36e.jpg', '35031', 'image/jpeg', '2019-03-16 06:04:53', '2019-03-16 06:04:53'),
(647, 'IMG-20190205-WA0222.jpg', '1e3e967a0c45d27e.jpg', 'thumb-1e3e967a0c45d27e.jpg', '43391', 'image/jpeg', '2019-03-16 06:04:53', '2019-03-16 06:04:53'),
(648, 'IMG-20190205-WA0224.jpg', '0ace788476b13e49.jpg', 'thumb-0ace788476b13e49.jpg', '45581', 'image/jpeg', '2019-03-16 06:05:01', '2019-03-16 06:05:01'),
(649, 'IMG-20190205-WA0225.jpg', '96de8fdfd61302a7.jpg', 'thumb-96de8fdfd61302a7.jpg', '69385', 'image/jpeg', '2019-03-16 06:05:02', '2019-03-16 06:05:02'),
(650, 'IMG-20190205-WA0235.jpg', 'f6a1fb60d2d498ef.jpg', 'thumb-f6a1fb60d2d498ef.jpg', '44028', 'image/jpeg', '2019-03-16 06:05:04', '2019-03-16 06:05:04'),
(651, 'IMG-20190205-WA0223.jpg', '5a7f7c486ab10bcd.jpg', 'thumb-5a7f7c486ab10bcd.jpg', '47093', 'image/jpeg', '2019-03-16 06:05:05', '2019-03-16 06:05:05'),
(652, 'IMG-20190219-WA0064.jpg', 'c3f5164888755830.jpg', 'thumb-c3f5164888755830.jpg', '74585', 'image/jpeg', '2019-03-16 06:05:07', '2019-03-16 06:05:07'),
(653, 'IMG-20190219-WA0065 (1).jpg', 'dc5c3b5a885d3116.jpg', 'thumb-dc5c3b5a885d3116.jpg', '54958', 'image/jpeg', '2019-03-16 06:05:09', '2019-03-16 06:05:09'),
(654, 'IMG-20190219-WA0065.jpg', '571bb767f706ffda.jpg', 'thumb-571bb767f706ffda.jpg', '54958', 'image/jpeg', '2019-03-16 06:05:09', '2019-03-16 06:05:09'),
(655, 'IMG-20190219-WA0063.jpg', '57a1616b67d2a72e.jpg', 'thumb-57a1616b67d2a72e.jpg', '54963', 'image/jpeg', '2019-03-16 06:05:11', '2019-03-16 06:05:11'),
(656, '58da6cd4-42c5-40d2-adb0-34185ee06ea1.jpg', '6b3bced7f94ee6d1.jpg', 'thumb-6b3bced7f94ee6d1.jpg', '55877', 'image/jpeg', '2019-03-16 06:05:18', '2019-03-16 06:05:18'),
(657, '89dd643d-4bce-486c-b148-2566ccdf9d00.jpg', '9c8019ebd8ff15d5.jpg', 'thumb-9c8019ebd8ff15d5.jpg', '51291', 'image/jpeg', '2019-03-16 06:05:19', '2019-03-16 06:05:19'),
(658, '1995d996-489f-4e47-b836-b892ecb06053.jpg', '4b0d18dced53c7c9.jpg', 'thumb-4b0d18dced53c7c9.jpg', '65425', 'image/jpeg', '2019-03-16 06:05:20', '2019-03-16 06:05:20'),
(659, '5062efcb-ba3f-4678-b5ef-948674156ee7.jpg', '6b2a0a3fcddf540a.jpg', 'thumb-6b2a0a3fcddf540a.jpg', '34425', 'image/jpeg', '2019-03-16 06:05:20', '2019-03-16 06:05:20'),
(660, '201862cf-9475-49e8-9440-365240af632d.jpg', 'bbe4d0b3ad199e8d.jpg', 'thumb-bbe4d0b3ad199e8d.jpg', '54229', 'image/jpeg', '2019-03-16 06:05:21', '2019-03-16 06:05:21'),
(661, 'a3f82887-e660-47c8-a7b2-6e416d9b195f.jpg', '6a6df23ef2b8942e.jpg', 'thumb-6a6df23ef2b8942e.jpg', '54894', 'image/jpeg', '2019-03-16 06:05:22', '2019-03-16 06:05:22'),
(662, 'd07e12f6-22ec-42e1-acae-67e38ccd68b3.jpg', 'b48068b4379c9d84.jpg', 'thumb-b48068b4379c9d84.jpg', '56834', 'image/jpeg', '2019-03-16 06:05:23', '2019-03-16 06:05:23'),
(663, 'df741293-899d-4767-ba75-224b86fef351.jpg', '176bd619de867000.jpg', 'thumb-176bd619de867000.jpg', '61928', 'image/jpeg', '2019-03-16 06:05:24', '2019-03-16 06:05:24'),
(664, 'IMG-20190212-WA0051.jpg', '560b946a8920f1a2.jpg', 'thumb-560b946a8920f1a2.jpg', '69482', 'image/jpeg', '2019-03-16 06:05:24', '2019-03-16 06:05:24'),
(665, 'IMG-20190212-WA0052.jpg', 'b70bd859bc2bb02c.jpg', 'thumb-b70bd859bc2bb02c.jpg', '78917', 'image/jpeg', '2019-03-16 06:05:25', '2019-03-16 06:05:25'),
(666, 'IMG-20190212-WA0053.jpg', 'e465987444023ab0.jpg', 'thumb-e465987444023ab0.jpg', '66711', 'image/jpeg', '2019-03-16 06:05:26', '2019-03-16 06:05:26'),
(667, 'IMG-20190218-WA0045.jpg', 'b176dc6251338898.jpg', 'thumb-b176dc6251338898.jpg', '49493', 'image/jpeg', '2019-03-16 06:05:26', '2019-03-16 06:05:26'),
(668, 'IMG-20190218-WA0046.jpg', 'def4f2f882cf889d.jpg', 'thumb-def4f2f882cf889d.jpg', '43314', 'image/jpeg', '2019-03-16 06:05:27', '2019-03-16 06:05:27'),
(669, 'IMG-20190218-WA0047.jpg', '12d33690e6196c80.jpg', 'thumb-12d33690e6196c80.jpg', '40243', 'image/jpeg', '2019-03-16 06:05:28', '2019-03-16 06:05:28'),
(670, '0a11eeda-f604-49c0-b125-b5159a9a28a1.jpg', '40680fe190a47f62.jpg', 'thumb-40680fe190a47f62.jpg', '49080', 'image/jpeg', '2019-03-16 06:05:28', '2019-03-16 06:05:28'),
(671, '2f544bd5-1bdf-4945-81c2-66a32e258dfc.jpg', '0944a0d2ae6e7649.jpg', 'thumb-0944a0d2ae6e7649.jpg', '60463', 'image/jpeg', '2019-03-16 06:05:29', '2019-03-16 06:05:29'),
(672, '6d22a57f-3661-4197-941e-afdffb6f9880.jpg', '1a550a416c91c491.jpg', 'thumb-1a550a416c91c491.jpg', '39969', 'image/jpeg', '2019-03-16 06:05:30', '2019-03-16 06:05:30'),
(673, '2146f698-3711-4984-b197-5c76fdbca724.jpg', '7aa5c55692566a3d.jpg', 'thumb-7aa5c55692566a3d.jpg', '50376', 'image/jpeg', '2019-03-16 06:05:31', '2019-03-16 06:05:31'),
(674, '94630e89-5164-48dc-8a92-225bf45648db.jpg', 'a8d558a0c480c7ef.jpg', 'thumb-a8d558a0c480c7ef.jpg', '53513', 'image/jpeg', '2019-03-16 06:05:31', '2019-03-16 06:05:31'),
(675, 'd5f05e18-c186-40f1-9604-f79af1f71fb4.jpg', '510b4ce606ec46d5.jpg', 'thumb-510b4ce606ec46d5.jpg', '65608', 'image/jpeg', '2019-03-16 06:05:32', '2019-03-16 06:05:32'),
(676, 'IMG-20190205-WA0296.jpg', '2392e299720bbbc6.jpg', 'thumb-2392e299720bbbc6.jpg', '36180', 'image/jpeg', '2019-03-16 06:05:33', '2019-03-16 06:05:33'),
(677, 'IMG-20190211-WA0047.jpg', '793ab56d8e74ca58.jpg', 'thumb-793ab56d8e74ca58.jpg', '67504', 'image/jpeg', '2019-03-16 06:05:34', '2019-03-16 06:05:34'),
(678, 'IMG-20190212-WA0045.jpg', 'c02bf11e36bdebf4.jpg', 'thumb-c02bf11e36bdebf4.jpg', '56675', 'image/jpeg', '2019-03-16 06:05:34', '2019-03-16 06:05:34'),
(679, 'IMG-20190212-WA0046.jpg', '8b933274cbf7b6db.jpg', 'thumb-8b933274cbf7b6db.jpg', '50087', 'image/jpeg', '2019-03-16 06:05:35', '2019-03-16 06:05:35'),
(680, 'IMG-20190212-WA0047.jpg', '9c582774e869e5b0.jpg', 'thumb-9c582774e869e5b0.jpg', '51090', 'image/jpeg', '2019-03-16 06:05:36', '2019-03-16 06:05:36'),
(681, '975fa2d6-b870-427b-8df7-5f64e1e101e4.jpg', '8e4675696ec40e85.jpg', 'thumb-8e4675696ec40e85.jpg', '33084', 'image/jpeg', '2019-03-16 06:05:36', '2019-03-16 06:05:36'),
(682, '5cb8d6e6-3c29-4965-82f5-56f0c97b0ee6.jpg', 'eb2b37a5913f646c.jpg', 'thumb-eb2b37a5913f646c.jpg', '113045', 'image/jpeg', '2019-03-16 06:05:37', '2019-03-16 06:05:37'),
(683, '6cd68f34-986e-4b24-bbea-2f696a4ff215.jpg', '1ecf9d23080a806d.jpg', 'thumb-1ecf9d23080a806d.jpg', '36177', 'image/jpeg', '2019-03-16 06:05:38', '2019-03-16 06:05:38'),
(684, '6d7bcb09-8980-467f-8cc3-8d9c33f322c8.jpg', '6de3a99380b288c2.jpg', 'thumb-6de3a99380b288c2.jpg', '48824', 'image/jpeg', '2019-03-16 06:05:39', '2019-03-16 06:05:39'),
(685, '49245a6c-6590-490f-adb1-3ed68fe83edd.jpg', '1ea886dc094b446d.jpg', 'thumb-1ea886dc094b446d.jpg', '113045', 'image/jpeg', '2019-03-16 06:05:39', '2019-03-16 06:05:39'),
(686, 'IMG-20190205-WA0316.jpg', '40a6674a943ecda9.jpg', 'thumb-40a6674a943ecda9.jpg', '36582', 'image/jpeg', '2019-03-16 06:05:40', '2019-03-16 06:05:40'),
(687, 'IMG-20190205-WA0317.jpg', '2a50dd33e28187b0.jpg', 'thumb-2a50dd33e28187b0.jpg', '45645', 'image/jpeg', '2019-03-16 06:05:41', '2019-03-16 06:05:41'),
(688, 'IMG-20190211-WA0139.jpg', '46a522155dafe1ef.jpg', 'thumb-46a522155dafe1ef.jpg', '29629', 'image/jpeg', '2019-03-16 06:05:41', '2019-03-16 06:05:41'),
(689, 'IMG-20190211-WA0148.jpg', 'a14c2945f2b0bfb0.jpg', 'thumb-a14c2945f2b0bfb0.jpg', '71157', 'image/jpeg', '2019-03-16 06:05:42', '2019-03-16 06:05:42'),
(690, '3f8404cb-b9ff-42b6-9ab4-202fec7233b0.jpg', '93727cd0b3574e87.jpg', 'thumb-93727cd0b3574e87.jpg', '49946', 'image/jpeg', '2019-03-16 06:05:43', '2019-03-16 06:05:43'),
(691, 'IMG-20190219-WA0063.jpg', '87e6eeb6e79e4690.jpg', 'thumb-87e6eeb6e79e4690.jpg', '54963', 'image/jpeg', '2019-03-16 06:05:48', '2019-03-16 06:05:48'),
(692, 'IMG-20190219-WA0064.jpg', 'bce8c90e6ef31f9f.jpg', 'thumb-bce8c90e6ef31f9f.jpg', '74585', 'image/jpeg', '2019-03-16 06:05:49', '2019-03-16 06:05:49'),
(693, 'IMG-20190219-WA0065 (1).jpg', 'cf5312ec7af45f8f.jpg', 'thumb-cf5312ec7af45f8f.jpg', '54958', 'image/jpeg', '2019-03-16 06:05:49', '2019-03-16 06:05:49'),
(694, 'IMG-20190219-WA0065.jpg', 'c666c69086399ef5.jpg', 'thumb-c666c69086399ef5.jpg', '54958', 'image/jpeg', '2019-03-16 06:05:50', '2019-03-16 06:05:50'),
(695, 'WhatsApp Image 2019-03-10 at 9.03.42 AM.jpeg', '4f4c67f69c735c10.jpeg', 'thumb-4f4c67f69c735c10.jpeg', '242001', 'image/jpeg', '2019-03-16 09:49:07', '2019-03-16 09:49:07'),
(696, 'WhatsApp Image 2019-03-10 at 9.07.49 AM.jpeg', '2607437f5a070005.jpeg', 'thumb-2607437f5a070005.jpeg', '91871', 'image/jpeg', '2019-03-16 09:49:09', '2019-03-16 09:49:09'),
(697, 'WhatsApp Image 2019-03-10 at 9.08.46 AM.jpeg', 'e000357ba6c7713c.jpeg', 'thumb-e000357ba6c7713c.jpeg', '67646', 'image/jpeg', '2019-03-16 09:49:09', '2019-03-16 09:49:09'),
(698, 'WhatsApp Image 2019-03-10 at 9.09.37 AM.jpeg', '41ba3cb2b37cd809.jpeg', 'thumb-41ba3cb2b37cd809.jpeg', '68127', 'image/jpeg', '2019-03-16 09:49:10', '2019-03-16 09:49:10'),
(699, 'WhatsApp Image 2019-03-10 at 9.33.22 AM.jpeg', '6a1561a1284231e1.jpeg', 'thumb-6a1561a1284231e1.jpeg', '77584', 'image/jpeg', '2019-03-16 09:49:11', '2019-03-16 09:49:11'),
(700, 'WhatsApp Image 2019-03-10 at 9.05.25 AM.jpeg', '7447d6736d695e39.jpeg', 'thumb-7447d6736d695e39.jpeg', '65203', 'image/jpeg', '2019-03-16 09:49:12', '2019-03-16 09:49:12'),
(701, 'WhatsApp Image 2019-03-10 at 9.06.57 AM.jpeg', 'fc5db885ad7dd266.jpeg', 'thumb-fc5db885ad7dd266.jpeg', '89984', 'image/jpeg', '2019-03-16 09:49:14', '2019-03-16 09:49:14'),
(702, '8eb6539b-ee16-4997-9eea-3ba2a8aaa191.jpg', 'c1180c010305fff0.jpg', 'thumb-c1180c010305fff0.jpg', '71688', 'image/jpeg', '2019-03-17 03:04:15', '2019-03-17 03:04:15'),
(703, '7ade4ff5-e32f-4ded-9fd0-37295a9b1a37.jpg', '00a450bf2a80aea3.jpg', 'thumb-00a450bf2a80aea3.jpg', '69698', 'image/jpeg', '2019-03-17 03:07:49', '2019-03-17 03:07:49'),
(704, '58abbcf9-091a-458b-90b4-870f99f27187.jpg', '3497dde3500a6c56.jpg', 'thumb-3497dde3500a6c56.jpg', '84056', 'image/jpeg', '2019-03-17 03:07:50', '2019-03-17 03:07:50'),
(705, '68caaaef-03c8-4ab6-8b04-2b1cd0b0d696.jpg', '2b1705dda4423f9d.jpg', 'thumb-2b1705dda4423f9d.jpg', '84703', 'image/jpeg', '2019-03-17 03:07:51', '2019-03-17 03:07:51'),
(706, '90ba9905-74a5-476e-91ca-753448c11e51.jpg', 'fa7007a825034f39.jpg', 'thumb-fa7007a825034f39.jpg', '64696', 'image/jpeg', '2019-03-17 03:07:51', '2019-03-17 03:07:51'),
(707, '96a3ac96-a12d-4377-8499-6bbd0f0ad157.jpg', 'b5c44d7701407799.jpg', 'thumb-b5c44d7701407799.jpg', '45768', 'image/jpeg', '2019-03-17 03:07:52', '2019-03-17 03:07:52'),
(708, '718e6124-8772-4418-98db-d534dd6fb102.jpg', '0f1d5b6305098f23.jpg', 'thumb-0f1d5b6305098f23.jpg', '74972', 'image/jpeg', '2019-03-17 03:07:53', '2019-03-17 03:07:53'),
(709, '9094ba53-609d-495f-bd41-4e7423a7bf24.jpg', 'ad52ed86b6862a4f.jpg', 'thumb-ad52ed86b6862a4f.jpg', '63378', 'image/jpeg', '2019-03-17 03:07:53', '2019-03-17 03:07:53'),
(710, '93903742-54c2-4fe8-8991-2fc94670b79d.jpg', '21058361819926e0.jpg', 'thumb-21058361819926e0.jpg', '84776', 'image/jpeg', '2019-03-17 03:07:54', '2019-03-17 03:07:54'),
(711, 'bedfe44e-3d46-460b-a5ca-29a103addb0a.jpg', '08cc8c63a41ed4f0.jpg', 'thumb-08cc8c63a41ed4f0.jpg', '52116', 'image/jpeg', '2019-03-17 03:07:54', '2019-03-17 03:07:54'),
(712, 'c94ea8c7-1560-4881-bb93-b3c58c31f554.jpg', '0f55886a4ed44815.jpg', 'thumb-0f55886a4ed44815.jpg', '54223', 'image/jpeg', '2019-03-17 03:07:55', '2019-03-17 03:07:55'),
(713, 'cf402b4f-cb17-4d54-a1ae-a2d83978ca7f.jpg', '1fbe124137b9325c.jpg', 'thumb-1fbe124137b9325c.jpg', '230273', 'image/jpeg', '2019-03-17 03:07:56', '2019-03-17 03:07:56'),
(720, 'IMG-20190211-WA0043.jpg', '3f6ccabf7cbaa285.jpg', 'thumb-3f6ccabf7cbaa285.jpg', '67494', 'image/jpeg', '2019-03-17 04:22:23', '2019-03-17 04:22:23'),
(721, '59e19235-7a0b-4772-adfa-5e013def77de.jpg', 'a6b20d059216fe0b.jpg', 'thumb-a6b20d059216fe0b.jpg', '57721', 'image/jpeg', '2019-03-17 05:07:03', '2019-03-17 05:07:03'),
(722, '7494cbd6-3ecc-4741-ac5d-2412e702e274.jpg', '5d2c0f1feb8d8513.jpg', 'thumb-5d2c0f1feb8d8513.jpg', '62396', 'image/jpeg', '2019-03-17 05:09:37', '2019-03-17 05:09:37'),
(723, 'a708574b-6254-4c93-b682-5f2e13336a95.jpg', 'e3ade59dc06d2834.jpg', 'thumb-e3ade59dc06d2834.jpg', '41434', 'image/jpeg', '2019-03-17 05:10:56', '2019-03-17 05:10:56'),
(724, 'IMG-20190206-WA0048.jpg', '01fa2b38beb0356d.jpg', 'thumb-01fa2b38beb0356d.jpg', '40168', 'image/jpeg', '2019-03-17 05:27:33', '2019-03-17 05:27:33'),
(725, 'IMG-20190205-WA0332.jpg', '544c6a6125db4fb0.jpg', 'thumb-544c6a6125db4fb0.jpg', '37672', 'image/jpeg', '2019-03-17 07:34:33', '2019-03-17 07:34:33'),
(726, 'a127e9da-d897-4826-9cfe-866eef1a88f2.jpg', '0a295ac9954f6df8.jpg', 'thumb-0a295ac9954f6df8.jpg', '188901', 'image/jpeg', '2019-03-18 04:55:01', '2019-03-18 04:55:01'),
(727, '9554ed4e-8690-4731-9dc9-6d4719bc9063.jpg', '569353c805441a79.jpg', 'thumb-569353c805441a79.jpg', '190225', 'image/jpeg', '2019-03-18 04:55:07', '2019-03-18 04:55:07'),
(728, '6ce6c4a5-8231-4b0f-bc0e-03514f53d28a.jpg', '6c4b66b6ae37cf4d.jpg', 'thumb-6c4b66b6ae37cf4d.jpg', '208027', 'image/jpeg', '2019-03-18 05:45:53', '2019-03-18 05:45:53'),
(729, 'e21db514-ae44-4bd6-9f0a-3d414c8f92f5.jpg', '446ab3ac53eeb1de.jpg', 'thumb-446ab3ac53eeb1de.jpg', '205125', 'image/jpeg', '2019-03-18 05:49:56', '2019-03-18 05:49:56'),
(730, 'WhatsApp Image 2019-03-20 at 3.20.05 PM.jpeg', '1d5b2d3e4baaf824.jpeg', 'thumb-1d5b2d3e4baaf824.jpeg', '60529', 'image/jpeg', '2019-03-20 09:34:59', '2019-03-20 09:34:59'),
(731, 'WhatsApp Image 2019-03-20 at 3.41.25 PM.jpeg', '3a5356ea401cb4e7.jpeg', 'thumb-3a5356ea401cb4e7.jpeg', '71558', 'image/jpeg', '2019-03-20 09:43:53', '2019-03-20 09:43:53'),
(732, 'Copy of BW0A2806 copy.jpg', '58d185b276b5da09.jpg', 'thumb-58d185b276b5da09.jpg', '2750783', 'image/jpeg', '2019-04-04 06:29:10', '2019-04-04 06:29:10'),
(733, '39.jpeg', '2f9e5fff4699d3d9.jpeg', 'thumb-2f9e5fff4699d3d9.jpeg', '39893', 'image/jpeg', '2019-04-04 06:29:40', '2019-04-04 06:29:40'),
(734, 'c7a5bfee-1aad-4e26-a23a-3416aac588a0.JPG', '49517b015bfb21dc.JPG', 'thumb-49517b015bfb21dc.JPG', '139207', 'image/jpeg', '2019-04-06 13:08:44', '2019-04-06 13:08:44'),
(735, 'test.png', '89800a80d883569e.png', 'thumb-89800a80d883569e.png', '384312', 'image/png', '2019-04-07 09:27:41', '2019-04-07 09:27:41'),
(736, '8f03dd73-a332-4087-96f0-31be4d1e8d65.jpg', '0ccf33172fdbf2a7.jpg', 'thumb-0ccf33172fdbf2a7.jpg', '110512', 'image/jpeg', '2019-04-14 13:10:22', '2019-04-14 13:10:22'),
(737, '36c69134-c072-419f-afa5-3a109183aec4.jpg', '205a55242cb08723.jpg', 'thumb-205a55242cb08723.jpg', '309053', 'image/jpeg', '2019-04-15 03:05:11', '2019-04-15 03:05:11'),
(738, '02d24752-6e1d-4df3-95ca-6d80a90347e0.jpg', '23275c670538e27b.jpg', 'thumb-23275c670538e27b.jpg', '92802', 'image/jpeg', '2019-04-15 03:58:09', '2019-04-15 03:58:09'),
(739, 'b3a26f8c-5ae9-4e1d-b72f-587fa439f8f3.jpg', 'be7a9c264437d559.jpg', 'thumb-be7a9c264437d559.jpg', '25391', 'image/jpeg', '2019-04-15 04:08:13', '2019-04-15 04:08:13'),
(740, '5ba88ecb-60d3-42ea-b800-b0d541f8ffa4.jpg', '0bca889a35b7de6a.jpg', 'thumb-0bca889a35b7de6a.jpg', '236244', 'image/jpeg', '2019-05-02 03:29:39', '2019-05-02 03:29:39'),
(741, '8d2cfcf3-22ac-49bb-ab81-77b8f769deff.jpg', '85111df818de1949.jpg', 'thumb-85111df818de1949.jpg', '215953', 'image/jpeg', '2019-05-02 03:29:48', '2019-05-02 03:29:48'),
(742, '79c66681-129f-42a3-8430-ae4b31c5aee2.jpg', 'b4191b929a137c88.jpg', 'thumb-b4191b929a137c88.jpg', '215816', 'image/jpeg', '2019-05-02 03:29:54', '2019-05-02 03:29:54'),
(743, '1cb6e269-b4e0-4111-ae91-ed8fa371817b.jpg', 'aafbcce592d23b0c.jpg', 'thumb-aafbcce592d23b0c.jpg', '77989', 'image/jpeg', '2019-05-02 04:17:21', '2019-05-02 04:17:21'),
(744, '9ae96f1b-874a-4aac-b5fe-873555a31d1c.jpg', '4ae91a000fe1d0a9.jpg', 'thumb-4ae91a000fe1d0a9.jpg', '71095', 'image/jpeg', '2019-05-02 04:17:25', '2019-05-02 04:17:25'),
(745, 'k01.jpg', '99329b229ef153df.jpg', 'thumb-99329b229ef153df.jpg', '62327', 'image/jpeg', '2019-05-02 04:19:10', '2019-05-02 04:19:10'),
(746, '9cbc99a1-b0e8-41a8-838f-dde3c4eb2784.jpg', '38cfae3a00da7978.jpg', 'thumb-38cfae3a00da7978.jpg', '69780', 'image/jpeg', '2019-05-02 04:21:12', '2019-05-02 04:21:12'),
(747, 'cfbc905a-17ec-455f-9521-f04c453d4c00.jpg', 'b62e5dc032f0daea.jpg', 'thumb-b62e5dc032f0daea.jpg', '86248', 'image/jpeg', '2019-05-02 04:33:26', '2019-05-02 04:33:26'),
(748, 'b5751b02-0bb4-480c-a372-c07f90953ed3.jpg', '59ad9ae69abe0381.jpg', 'thumb-59ad9ae69abe0381.jpg', '232020', 'image/jpeg', '2019-05-04 03:32:30', '2019-05-04 03:32:30'),
(749, '5950c3ee711051e8.jpeg', '85e030457343f8c2.jpeg', 'thumb-85e030457343f8c2.jpeg', '36641', 'image/jpeg', '2019-05-04 09:09:46', '2019-05-04 09:09:46'),
(750, 'a4a292dfa45d1988 (1).jpeg', 'ba38d860c1d63bcc.jpeg', 'thumb-ba38d860c1d63bcc.jpeg', '87958', 'image/jpeg', '2019-05-04 09:09:55', '2019-05-04 09:09:55'),
(751, 'fc7664d2d33ea862.jpeg', '619183034ab4bf17.jpeg', 'thumb-619183034ab4bf17.jpeg', '65595', 'image/jpeg', '2019-05-04 09:10:06', '2019-05-04 09:10:06'),
(752, '9b1c4d333c9d5495.jpg', '6e0f30c3c7b4b617.jpg', 'thumb-6e0f30c3c7b4b617.jpg', '222593', 'image/jpeg', '2019-05-04 09:12:00', '2019-05-04 09:12:00'),
(753, 'b7114755ee29d20f (1).jpeg', 'f8bdb2d00c9a85e4.jpeg', 'thumb-f8bdb2d00c9a85e4.jpeg', '108769', 'image/jpeg', '2019-05-04 09:13:48', '2019-05-04 09:13:48'),
(754, '7a7f35d46ee97cbb.jpg', 'a034001aee3a0829.jpg', 'thumb-a034001aee3a0829.jpg', '87569', 'image/jpeg', '2019-05-04 09:16:21', '2019-05-04 09:16:21'),
(755, '5950c3ee711051e8 (1).jpeg', '2ab170cedbffbdf9.jpeg', 'thumb-2ab170cedbffbdf9.jpeg', '36641', 'image/jpeg', '2019-05-04 09:18:33', '2019-05-04 09:18:33'),
(756, '2a38316521af85f3.jpg', '881543bf2629add7.jpg', 'thumb-881543bf2629add7.jpg', '68930', 'image/jpeg', '2019-05-04 09:19:14', '2019-05-04 09:19:14'),
(757, 'f172cd76790227b0.jpeg', 'eca696c3028107b5.jpeg', 'thumb-eca696c3028107b5.jpeg', '76178', 'image/jpeg', '2019-05-04 09:19:20', '2019-05-04 09:19:20'),
(758, 'f04ff8ddbfecd16c.jpeg', '0b84e2b72d6126e4.jpeg', 'thumb-0b84e2b72d6126e4.jpeg', '35445', 'image/jpeg', '2019-05-04 09:19:22', '2019-05-04 09:19:22'),
(759, 'e9c598dc96dee3eb.jpeg', '4180df6b5f09e9e3.jpeg', 'thumb-4180df6b5f09e9e3.jpeg', '97258', 'image/jpeg', '2019-05-04 09:19:24', '2019-05-04 09:19:24'),
(760, '67983f05b5adc810.jpeg', '4f71efdb961e1d12.jpeg', 'thumb-4f71efdb961e1d12.jpeg', '25563', 'image/jpeg', '2019-05-04 09:19:25', '2019-05-04 09:19:25'),
(761, 'eff58e40ddfa4913.jpg', 'ad42a3b12904d1d0.jpg', 'thumb-ad42a3b12904d1d0.jpg', '149909', 'image/jpeg', '2019-05-05 03:17:24', '2019-05-05 03:17:24'),
(762, 'dddde09b692467eb.jpeg', 'b890010885e9bb77.jpeg', 'thumb-b890010885e9bb77.jpeg', '107028', 'image/jpeg', '2019-05-05 03:18:42', '2019-05-05 03:18:42'),
(763, '3a790f75f40e91d5.jpeg', '0111711586817d8a.jpeg', 'thumb-0111711586817d8a.jpeg', '49517', 'image/jpeg', '2019-05-05 03:23:24', '2019-05-05 03:23:24'),
(764, '80f51c4ff5210c3e.jpeg', '29aa1130ca7140e7.jpeg', 'thumb-29aa1130ca7140e7.jpeg', '144463', 'image/jpeg', '2019-05-05 03:24:09', '2019-05-05 03:24:09'),
(765, '0934e4480eedc84d.jpeg', '33c207d5865a4e92.jpeg', 'thumb-33c207d5865a4e92.jpeg', '107621', 'image/jpeg', '2019-05-05 03:26:10', '2019-05-05 03:26:10'),
(766, '67983f05b5adc810 (1).jpeg', '175a979939afb8fe.jpeg', 'thumb-175a979939afb8fe.jpeg', '25563', 'image/jpeg', '2019-05-05 03:27:11', '2019-05-05 03:27:11'),
(767, 'e9c598dc96dee3eb (1).jpeg', '2decf249c96f13f2.jpeg', 'thumb-2decf249c96f13f2.jpeg', '97258', 'image/jpeg', '2019-05-05 03:28:11', '2019-05-05 03:28:11'),
(768, 'f04ff8ddbfecd16c (1).jpeg', '9613af1f13a8f8ac.jpeg', 'thumb-9613af1f13a8f8ac.jpeg', '35445', 'image/jpeg', '2019-05-05 03:29:05', '2019-05-05 03:29:05'),
(769, '078c48d4a6114d8d.jpeg', '6b2e508b1c7dfd2b.jpeg', 'thumb-6b2e508b1c7dfd2b.jpeg', '59353', 'image/jpeg', '2019-05-05 03:29:41', '2019-05-05 03:29:41'),
(770, '82fb7477fc7a13e0.jpg', '08a5fc98d13dac26.jpg', 'thumb-08a5fc98d13dac26.jpg', '140739', 'image/jpeg', '2019-05-05 03:30:44', '2019-05-05 03:30:44'),
(771, 'd78e4d513c4c1f7e.JPG', 'f5dc0636f60c8e10.JPG', 'thumb-f5dc0636f60c8e10.JPG', '248155', 'image/jpeg', '2019-05-05 03:31:44', '2019-05-05 03:31:44'),
(772, 'f172cd76790227b0 (1).jpeg', '1105900a598a844c.jpeg', 'thumb-1105900a598a844c.jpeg', '76178', 'image/jpeg', '2019-05-05 03:32:26', '2019-05-05 03:32:26'),
(773, '2a38316521af85f3 (1).jpg', '01d94f33cad84d05.jpg', 'thumb-01d94f33cad84d05.jpg', '68930', 'image/jpeg', '2019-05-05 03:33:20', '2019-05-05 03:33:20'),
(774, 'a16695138aef6f43.jpg', 'cfb4f3b14c3d49bf.jpg', 'thumb-cfb4f3b14c3d49bf.jpg', '36249', 'image/jpeg', '2019-05-05 03:33:53', '2019-05-05 03:33:53'),
(775, '77321ff5-b998-487b-82fd-3b81e43c547c.jpg', '917df8e506972b5f.jpg', 'thumb-917df8e506972b5f.jpg', '252160', 'image/jpeg', '2019-05-12 04:58:53', '2019-05-12 04:58:53'),
(776, '2188C83A-B489-42FF-A83C-890B28B9EDB7.jpeg', 'e4b0b2663b5c0950.jpeg', 'thumb-e4b0b2663b5c0950.jpeg', '233852', 'image/jpeg', '2019-05-16 21:53:51', '2019-05-16 21:53:51'),
(777, 'IMG-20190211-WA0113.jpg', '7ba555eab15a60e3.jpg', 'thumb-7ba555eab15a60e3.jpg', '45783', 'image/jpeg', '2019-05-23 05:10:13', '2019-05-23 05:10:13'),
(778, 'IMG-20190211-WA0115.jpg', '0800f44606e70437.jpg', 'thumb-0800f44606e70437.jpg', '55852', 'image/jpeg', '2019-05-23 05:10:17', '2019-05-23 05:10:17'),
(779, '963865cc-eff4-4571-8328-3314198338f9.jpg', '7070237641c89dea.jpg', 'thumb-7070237641c89dea.jpg', '145669', 'image/jpeg', '2019-05-26 05:36:00', '2019-05-26 05:36:00'),
(781, 'Z8ajl0jt_400x400.jpg', 'fc34e6572bf0df12.jpg', 'thumb-fc34e6572bf0df12.jpg', '14236', 'image/jpeg', '2019-05-29 06:43:45', '2019-05-29 06:43:45'),
(782, 'test-img.jpg', '61e4658e5542e220.jpg', 'thumb-61e4658e5542e220.jpg', '183528', 'image/jpeg', '2019-06-03 04:35:38', '2019-06-03 04:35:38'),
(783, 'WhatsApp Image 2019-06-13 at 8.47.09 AM.jpeg', 'fec12bf87a042f6e.jpeg', 'thumb-fec12bf87a042f6e.jpeg', '124946', 'image/jpeg', '2019-06-15 08:15:11', '2019-06-15 08:15:11'),
(784, 'موقع.jpeg', '3b68dc578f15625d.jpeg', 'thumb-3b68dc578f15625d.jpeg', '121549', 'image/jpeg', '2019-06-19 07:03:37', '2019-06-19 07:03:37'),
(785, '4a27b8ad7717fb1c.jpeg', '9424e6a3cc0cd4f9.jpeg', 'thumb-9424e6a3cc0cd4f9.jpeg', '194192', 'image/jpeg', '2019-06-29 06:57:20', '2019-06-29 06:57:20'),
(786, '89af941aff434845 (1).jpeg', '9fac69ab7b655d3d.jpeg', 'thumb-9fac69ab7b655d3d.jpeg', '79406', 'image/jpeg', '2019-06-30 03:06:37', '2019-06-30 03:06:37'),
(787, 'WhatsApp Image 2019-07-01 at 8.06.14 PM.jpeg', '9b24d5feeeaa8910.jpeg', 'thumb-9b24d5feeeaa8910.jpeg', '165855', 'image/jpeg', '2019-07-01 14:19:44', '2019-07-01 14:19:44'),
(788, 'WhatsApp Image 2019-07-04 at 2.05.35 PM.jpeg', 'c5f48f7663531bda.jpeg', 'thumb-c5f48f7663531bda.jpeg', '498698', 'image/jpeg', '2019-07-04 08:12:12', '2019-07-04 08:12:12'),
(789, 'WhatsApp Image 2019-07-04 at 4.52.29 PM.jpeg', '2868f446fc5a6e02.jpeg', 'thumb-2868f446fc5a6e02.jpeg', '65406', 'image/jpeg', '2019-07-04 11:43:19', '2019-07-04 11:43:19'),
(790, 'WhatsApp Image 2019-07-06 at 11.51.50 AM.jpeg', '9f848e9f5bdc06fb.jpeg', 'thumb-9f848e9f5bdc06fb.jpeg', '85715', 'image/jpeg', '2019-07-06 05:54:10', '2019-07-06 05:54:10'),
(791, 'WhatsApp Image 2019-07-07 at 3.24.20 PM (1).jpeg', 'd1462357ee111e4f.jpeg', 'thumb-d1462357ee111e4f.jpeg', '50857', 'image/jpeg', '2019-07-07 09:32:23', '2019-07-07 09:32:23'),
(792, 'WhatsApp Image 2019-07-07 at 3.42.20 PM.jpeg', '627e8c6e97236066.jpeg', 'thumb-627e8c6e97236066.jpeg', '11371', 'image/jpeg', '2019-07-07 09:45:25', '2019-07-07 09:45:25'),
(793, 'WhatsApp Image 2019-07-10 at 9.51.50 AM.jpeg', 'cdf8447c2aaf7ec8.jpeg', 'thumb-cdf8447c2aaf7ec8.jpeg', '194123', 'image/jpeg', '2019-07-10 04:00:52', '2019-07-10 04:00:52'),
(794, 'btm-sadad.png', 'd7b7991d72e3609d.png', 'thumb-d7b7991d72e3609d.png', '1674', 'image/png', '2019-09-03 05:31:12', '2019-09-03 05:31:12'),
(795, 'btm-mada.png', '3d42788462e41930.png', 'thumb-3d42788462e41930.png', '1860', 'image/png', '2019-09-03 05:31:16', '2019-09-03 05:31:16'),
(796, 'contact.jpg', '26c8f3fce095e975.jpg', 'thumb-26c8f3fce095e975.jpg', '55218', 'image/jpeg', '2019-09-03 06:19:56', '2019-09-03 06:19:56'),
(797, 'Saudi_Arabia.svg.png', '52c46ecbbc703c6b.png', 'thumb-52c46ecbbc703c6b.png', '2919', 'image/png', '2019-09-10 03:04:55', '2019-09-10 03:04:55'),
(798, 'Egypt.svg.png', 'a92565fd4e5e49ef.png', 'thumb-a92565fd4e5e49ef.png', '2062', 'image/png', '2019-09-10 03:04:58', '2019-09-10 03:04:58'),
(800, 'american-flag-backdrop_35_1048.jpg', 'ff72f7be0650180f.jpg', 'thumb-ff72f7be0650180f.jpg', '198467', 'image/jpeg', '2019-09-22 07:36:08', '2019-09-22 07:36:08'),
(801, 'united-arab-emirates-flag.jpg', 'fc1e6b9a4e6038d6.jpg', 'thumb-fc1e6b9a4e6038d6.jpg', '12688', 'image/jpeg', '2019-09-22 07:36:40', '2019-09-22 07:36:40'),
(802, '0.jpg', 'b77b31f87de42831.jpg', 'thumb-b77b31f87de42831.jpg', '19804', 'image/jpeg', '2019-11-09 12:35:04', '2019-11-09 12:35:04'),
(805, '4.png', '7a566138c4cbcd6f.png', 'thumb-7a566138c4cbcd6f.png', '2884', 'image/png', '2019-11-14 14:43:08', '2019-11-14 14:43:08'),
(806, '5.png', '73d32a1f220f035e.png', 'thumb-73d32a1f220f035e.png', '611', 'image/png', '2019-11-14 14:43:09', '2019-11-14 14:43:09'),
(807, '3.png', '61f286280790d56e.png', 'thumb-61f286280790d56e.png', '2884', 'image/png', '2019-11-16 15:36:58', '2019-11-16 15:36:58'),
(808, 'r1.png', '91cfc663a3beca55.png', 'thumb-91cfc663a3beca55.png', '16519', 'image/png', '2019-11-20 10:21:13', '2019-11-20 10:21:13'),
(809, 'r2.png', '4f972d722cd03eb7.png', 'thumb-4f972d722cd03eb7.png', '16087', 'image/png', '2019-11-20 10:21:34', '2019-11-20 10:21:34'),
(810, 'r3.png', '7d5441fa142bdc24.png', 'thumb-7d5441fa142bdc24.png', '17316', 'image/png', '2019-11-20 10:21:35', '2019-11-20 10:21:35'),
(811, 'r4.png', 'ddbab8dd649a3cbe.png', 'thumb-ddbab8dd649a3cbe.png', '16475', 'image/png', '2019-11-20 10:21:37', '2019-11-20 10:21:37'),
(812, 'p1.png', '45a11cdaab060b1c.png', 'thumb-45a11cdaab060b1c.png', '17757', 'image/png', '2019-11-20 10:28:46', '2019-11-20 10:28:46'),
(813, 'p2.png', '4222ed1ea43e757d.png', 'thumb-4222ed1ea43e757d.png', '17694', 'image/png', '2019-11-20 10:28:46', '2019-11-20 10:28:46'),
(814, 'p3.png', '7abfda042de558a6.png', 'thumb-7abfda042de558a6.png', '17375', 'image/png', '2019-11-20 10:28:47', '2019-11-20 10:28:47'),
(815, '7.png', 'dff82a1bd3561ec7.png', 'thumb-dff82a1bd3561ec7.png', '16634', 'image/png', '2019-11-20 17:59:18', '2019-11-20 17:59:18'),
(816, '6.png', '8b7190519ed8830d.png', 'thumb-8b7190519ed8830d.png', '16463', 'image/png', '2019-11-20 17:59:22', '2019-11-20 17:59:22'),
(817, '5.png', 'cc49bf91e8bf1460.png', 'thumb-cc49bf91e8bf1460.png', '16715', 'image/png', '2019-11-20 17:59:24', '2019-11-20 17:59:24'),
(818, '4.png', '7fe117aa8d6b666b.png', 'thumb-7fe117aa8d6b666b.png', '16519', 'image/png', '2019-11-20 17:59:27', '2019-11-20 17:59:27'),
(819, '3.png', 'e7016fb1d0670580.png', 'thumb-e7016fb1d0670580.png', '16334', 'image/png', '2019-11-20 17:59:29', '2019-11-20 17:59:29'),
(820, '2.png', 'b9e50fefab862d95.png', 'thumb-b9e50fefab862d95.png', '17437', 'image/png', '2019-11-20 17:59:32', '2019-11-20 17:59:32'),
(821, '1.png', '79cdd59c66e9f0f3.png', 'thumb-79cdd59c66e9f0f3.png', '16801', 'image/png', '2019-11-20 17:59:34', '2019-11-20 17:59:34'),
(822, '7.png', 'f6993457b5241c30.png', 'thumb-f6993457b5241c30.png', '16634', 'image/png', '2019-11-25 11:44:40', '2019-11-25 11:44:40'),
(823, '6.png', '34d25ddb8c90e8bd.png', 'thumb-34d25ddb8c90e8bd.png', '16463', 'image/png', '2019-11-25 11:44:45', '2019-11-25 11:44:45'),
(824, '5.png', 'c708a5d30274d45a.png', 'thumb-c708a5d30274d45a.png', '16715', 'image/png', '2019-11-25 11:44:49', '2019-11-25 11:44:49'),
(825, '4.png', 'd7ed8d613be7b673.png', 'thumb-d7ed8d613be7b673.png', '16519', 'image/png', '2019-11-25 11:44:53', '2019-11-25 11:44:53'),
(826, '3.png', '6bf192cfac5a1708.png', 'thumb-6bf192cfac5a1708.png', '16334', 'image/png', '2019-11-25 11:44:54', '2019-11-25 11:44:54'),
(828, '1.png', '4f5683ccb0180722.png', 'thumb-4f5683ccb0180722.png', '16801', 'image/png', '2019-11-25 11:44:58', '2019-11-25 11:44:58'),
(831, '2 - Copy.png', '9acd3951d0424c3f.png', 'thumb-9acd3951d0424c3f.png', '17317', 'image/png', '2019-11-25 12:05:17', '2019-11-25 12:05:17'),
(832, '02.jpg', '50420751fab6ec0c.jpg', 'thumb-50420751fab6ec0c.jpg', '72789', 'image/jpeg', '2020-02-09 15:28:02', '2020-02-09 15:28:02'),
(833, '03.jpg', '8f860bc81731e960.jpg', 'thumb-8f860bc81731e960.jpg', '158535', 'image/jpeg', '2020-02-09 15:28:05', '2020-02-09 15:28:05'),
(834, '01.jpg', '0e0af1dc82c24768.jpg', 'thumb-0e0af1dc82c24768.jpg', '299361', 'image/jpeg', '2020-02-09 15:28:08', '2020-02-09 15:28:08'),
(835, '05.jpg', 'b145f1e556e0becf.jpg', 'thumb-b145f1e556e0becf.jpg', '140739', 'image/jpeg', '2020-02-09 15:28:13', '2020-02-09 15:28:13'),
(836, '04.jpg', '65d1cf216a106862.jpg', 'thumb-65d1cf216a106862.jpg', '216097', 'image/jpeg', '2020-02-09 15:28:17', '2020-02-09 15:28:17');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2019_06_26_082601_create_supplier_purchase_file_table', 1),
('2019_06_26_084925_create_supplierpurchase_table', 1),
('2019_06_26_092112_create_supplierpurchase_element_table', 1),
('2019_04_07_220510_create_ratings_table', 2),
('2019_04_09_155543_create_favorites_table', 2),
('2019_04_10_100413_create_advertising_table', 2),
('2019_06_25_214211_create_advertising_file', 2),
('2019_06_25_224757_add_discount_duration_to_product', 2),
('2019_06_26_110102_create_advertising_test_table', 2),
('2019_06_26_131223_create_is_deleted_column_to_advertising_table', 2),
('2019_06_26_104218_add_theOrder_to_product_dep', 3),
('2019_06_28_163348_usercart', 3),
('2019_07_01_091911_add_otherExpenses_to_supplierpurchase', 3),
('2019_07_04_134644_create_user_credit_table', 3),
('2019_06_20_183606_orderchanges', 4),
('2019_07_04_135221_create_settings_table', 5),
('2019_07_06_071820_add_userCredit_to_orders', 6),
('2019_07_10_060646_add_isPackage_to_product', 6),
('2019_07_10_062346_create_product_package_element_table', 6),
('2019_07_10_065709_add_isPackage_to_order_element', 6),
('2016_06_01_000001_create_oauth_auth_codes_table', 7),
('2016_06_01_000002_create_oauth_access_tokens_table', 7),
('2016_06_01_000003_create_oauth_refresh_tokens_table', 7),
('2016_06_01_000004_create_oauth_clients_table', 7),
('2016_06_01_000005_create_oauth_personal_access_clients_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `userId` mediumint(9) NOT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `the_content` text,
  `mediaFile` varchar(70) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsletter`
--

INSERT INTO `newsletter` (`id`, `userId`, `full_name`, `email`, `title`, `the_content`, `mediaFile`, `status`, `created_at`, `updated_at`) VALUES
(4, 16240, NULL, 'pinkrose.samah@gmail.com', 'تجربة بريد حملة إعلانية', '<p>تجربة حملة إعلانية</p>\r\n', '', 1, NULL, '2019-12-16 09:24:47'),
(7, 0, 'خالد', 'sun.stronghold@gmail.com', 'جاهز يا خالد', '<p>لا مش جاهزر</p>\r\n', '', 1, '2019-11-25 05:59:53', '2019-12-16 09:24:47'),
(8, 0, 'تجربة', 'it@arganpackage.com', 'test', '<p>,Hello World</p>\r\n', 'c708a5d30274d45a.png', 1, '2019-12-05 05:31:11', '2019-12-16 09:24:47'),
(9, 1869, NULL, 'salahalawy317@gmail.com', 'تجربة', '<p>تجربة</p>\r\n', '', 1, NULL, '2019-12-16 09:24:47'),
(10, 19682, NULL, 'anas.abuwael@gmail.com', 'تجربة', '<p>تجربة</p>\r\n', '', 1, NULL, '2019-12-16 09:24:47'),
(11, 19683, NULL, 'anas.abuwael@gmail.com', 'تجربة', '<p>تجربة</p>\r\n', '', 1, NULL, '2019-12-16 09:24:47'),
(12, 19683, ' anas Elmadhoon ', 'anas.abuwael@gmail.com', 'تجربة', '<p>تجربة&nbsp;</p>\r\n', '', 1, '2019-12-11 07:27:06', '2019-12-16 09:24:47'),
(13, 1869, NULL, 'salahalawy317@gmail.com', 'تجربة ', '<p>تجربة&nbsp;</p>\r\n', '', 1, NULL, '2019-12-16 09:24:47'),
(14, 19682, NULL, 'anas.abuwael@gmail.com', 'تجربة ', '<p>تجربة&nbsp;</p>\r\n', '', 1, NULL, '2019-12-16 09:24:47'),
(15, 19683, NULL, 'anas.abuwael@gmail.com', 'تجربة ', '<p>تجربة&nbsp;</p>\r\n', '', 1, NULL, '2019-12-16 09:24:47'),
(16, 19751, 'Khaled Abdelfattah', 'sun.stronghold@gmail.com', 'تجربة أرجان', '<p>أجرب</p>\r\n', '', 1, '2019-12-16 05:45:33', '2019-12-16 09:24:47'),
(17, 19751, 'Khaled Abdelfattah', 'sun.stronghold@gmail.com', 'رسائل دعائية', '<p>رساااااااائل</p>\r\n', '', 1, '2019-12-16 05:46:37', '2019-12-16 09:24:47'),
(18, 19677, NULL, NULL, 'تجربة دعائية', '<p>تجربة دعائية</p>\r\n', '', 1, NULL, '2019-12-16 09:24:47'),
(19, 19677, NULL, NULL, 'تجربة دعائية', '<p>تجربة دعائية</p>\r\n', '', 1, NULL, '2019-12-16 09:24:47'),
(20, 19677, NULL, NULL, 'تجربة دعائية', '<p>تجربة دعائية</p>\r\n', '', 1, NULL, '2019-12-16 09:24:47'),
(21, 19677, NULL, 'sun_stronghold@yahoo.com', 'bbb', '<p>nnnnn</p>\r\n', '', 1, NULL, '2019-12-16 06:27:53'),
(22, 0, 'خالد', 'sun.stronghold@gmail.com', 'تجربة مرفق', '<p>تجربة مرفق</p>\r\n', '34d25ddb8c90e8bd.png', 1, '2019-12-17 08:56:32', '2020-01-10 17:16:28'),
(23, 19782, ' مشمش  الثاني عشر ', 'sun.stronghold@gmail.com', 'تجربة مشمش', '<p>مشمش تجارب</p>\r\n', '', 1, '2020-01-10 17:07:58', '2020-01-10 17:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(11) NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `email`, `created_at`, `updated_at`) VALUES
(2, 'agent@example.com', '2020-08-17 19:20:46', '2020-08-17 19:20:46');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('08c2538a652e20ae3d296e82f8496dbb7ad3aeaace2fe8d6124214f5f5f45bcbb02cd78a2bfe895b', 20, 1, 'authToken', '[]', 0, '2020-10-24 12:34:05', '2020-10-24 12:34:05', '2021-10-24 12:34:05'),
('09a2bc1305b9ffc3bd3922a3adf37ae533ec56092b98de9329c3fbdd191c8bd08272802c0f39e1f8', 1, 1, 'authToken', '[]', 0, '2020-09-27 08:30:29', '2020-09-27 08:30:29', '2021-09-27 11:30:29'),
('208150c687f9cb5628d6e65d76dc5f46c179e8855164ea88778edd09f4dfab780bc7d6d9836fabe2', 12, 1, 'authToken', '[]', 0, '2020-08-24 18:06:17', '2020-08-24 18:06:17', '2021-08-24 21:06:17'),
('2b1b62333d016d00fed00c022c822cec23e9e13bf141a4b3978325df431b20563850fb268af780a0', 17, 1, 'authToken', '[]', 0, '2020-10-23 18:16:40', '2020-10-23 18:16:40', '2021-10-23 18:16:40'),
('2bada4b3032bf43187fa2f5d771c2eb0f6be61c291db093b721c59f1446510a581d7663e18210c86', 19, 1, 'authToken', '[]', 0, '2020-10-24 12:25:08', '2020-10-24 12:25:08', '2021-10-24 12:25:08'),
('2cdfedadf155255aecfa451fde538ce5caee897849db234a5160c22dd6d4aba12490e7d13ecd2822', 1, 1, 'authToken', '[]', 0, '2020-09-16 17:34:18', '2020-09-16 17:34:18', '2021-09-16 20:34:18'),
('307c639775c7a68905ba2398020de913680b44143ed16f8417fdac1d4fc7b84f2c25fc0b1b753113', 16, 1, 'authToken', '[]', 0, '2020-10-23 18:14:07', '2020-10-23 18:14:07', '2021-10-23 18:14:07'),
('39bd75cac7857a1fb3007e0c4804ae36b3993d91b96b6579265a261b1cb4782c12dfa4d402ed16d4', 1, 1, 'authToken', '[]', 0, '2020-09-03 06:37:25', '2020-09-03 06:37:25', '2021-09-03 09:37:25'),
('406b55249f2ca9b551439e0f61c0514a9e67fd1fef7456e23dc6d21f19f7193cae6976a35c452f22', 1, 1, 'authToken', '[]', 0, '2020-09-19 04:14:09', '2020-09-19 04:14:09', '2021-09-19 07:14:09'),
('421d3b0334f3841937634137c27eabb7c2308f89875db5778b445a53a8142038d45b1e6c58418d58', 1, 1, 'authToken', '[]', 0, '2020-09-01 11:38:40', '2020-09-01 11:38:40', '2021-09-01 14:38:40'),
('4331f64d3d433a8262767cb8aefc5d466750b2df5dad6f84f6e6426c5cd71ca98d16a76e75762be0', 24, 1, 'authToken', '[]', 0, '2020-10-24 20:26:18', '2020-10-24 20:26:18', '2021-10-24 20:26:18'),
('495f3e21c927e4951a6ef80143dba25facb580f1752b9abde0828947364db7fe1e3e49c13b3eca98', 25, 1, 'authToken', '[]', 0, '2020-10-24 20:30:41', '2020-10-24 20:30:41', '2021-10-24 20:30:41'),
('4af09e1c9929d514dda47fd065b9b0321c56000632cfd199fc9d6e22e85cc59114409e3f91393b69', 12, 1, 'authToken', '[]', 0, '2020-08-24 18:15:13', '2020-08-24 18:15:13', '2021-08-24 21:15:13'),
('4b342d81e3c8f7cc0ce1468cdbf40bebe24c4e6fc83bdde01329a0f62adefb750eb8402235459833', 1, 1, 'authToken', '[]', 0, '2020-08-21 13:31:11', '2020-08-21 13:31:11', '2021-08-21 16:31:11'),
('5327836d9a6d412358a02ac2d9019bddb7021097efc633c9e47c206a521e47576d29185422078eb6', 1, 1, 'authToken', '[]', 0, '2020-09-12 18:53:37', '2020-09-12 18:53:37', '2021-09-12 21:53:37'),
('57a917df7d29140fa9fa3d7e862cc05d3117be83d31a3d1d78b06206bc8e8e20a79f486b86c15323', 24, 1, 'authToken', '[]', 0, '2020-10-24 20:37:48', '2020-10-24 20:37:48', '2021-10-24 20:37:48'),
('5924a825112a6dd8dfeab5075dc0aeee51ce49508ab5260cd0e3d27203e3e56cfa38469ec1db03d4', 1, 1, 'authToken', '[]', 0, '2020-08-22 08:53:48', '2020-08-22 08:53:48', '2021-08-22 11:53:48'),
('5b5eb6f70fb5cf87b1a2e8be3f5fbb1495c7bf13e1a588fc394b804b3ce50e1b2f54998f652b2e2d', 24, 1, 'authToken', '[]', 0, '2020-11-07 11:48:12', '2020-11-07 11:48:12', '2021-11-07 11:48:12'),
('5cd187d3615200c6fd7a9c707df73acd2aa0c33aacab4969c11cbd64a56a2438f3727cdbea112ce4', 24, 1, 'authToken', '[]', 0, '2020-11-07 11:55:39', '2020-11-07 11:55:39', '2021-11-07 11:55:39'),
('65dfb4eb61566dc71c70c604b0c71286fddddfe54776c6c28752b397ed9401d1707564ab5f98c058', 21, 1, 'authToken', '[]', 0, '2020-10-24 12:37:39', '2020-10-24 12:37:39', '2021-10-24 12:37:39'),
('722d846b612fec0725a5b80defa298f8f30ec540279e54506d97b7c3bad804b0a447d420e361fc1c', 1, 1, 'authToken', '[]', 0, '2020-10-23 18:42:40', '2020-10-23 18:42:40', '2021-10-23 18:42:40'),
('78946f5e5f02c8eebae717f7fdab6a861ec3006ce0569a7d9fc3e892c1be321d1866efae4d8f463f', 1, 1, 'authToken', '[]', 0, '2020-09-28 05:51:20', '2020-09-28 05:51:20', '2021-09-28 08:51:20'),
('7fe49787654471752ecc7fd1affd1859fb6d45f7fe32396581bc8d11db87279ff8369af4b33acb4f', 24, 1, 'authToken', '[]', 0, '2020-10-24 20:40:24', '2020-10-24 20:40:24', '2021-10-24 20:40:24'),
('80c39ff885b3e976d34ff01c358e3c0084cad66cac8a1986732edab2583254de32509c4d6ce91708', 1, 1, 'authToken', '[]', 0, '2020-08-19 20:01:55', '2020-08-19 20:01:55', '2021-08-19 23:01:55'),
('8606cdd7769fdaa52dd921ebc808cfccaa2daca87738a7da66848d808fa49e0f5a14f99da816d302', 1, 1, 'authToken', '[]', 0, '2020-09-12 18:44:21', '2020-09-12 18:44:21', '2021-09-12 21:44:21'),
('927d7aec914e8680928710fbd81538f9c980da8c5c09908050f62cbe6c7b6f962b12dcf33ac338df', 1, 1, 'authToken', '[]', 0, '2020-08-17 10:14:02', '2020-08-17 10:14:02', '2021-08-17 13:14:02'),
('977ea49f1c26558d484aabdc0a584a31b4db97ad7268b212e6d99c0a3a9a6c4b4095a196717ecbb7', 1, 1, 'authToken', '[]', 0, '2020-08-17 10:01:55', '2020-08-17 10:01:55', '2021-08-17 13:01:55'),
('9adb1396cb3bed80daa7297b4ded05233f0ca5065b10bb6804b8d5f4f1d284c349f5fefb2552736b', 1, 1, 'authToken', '[]', 0, '2020-10-10 17:07:59', '2020-10-10 17:07:59', '2021-10-10 20:07:59'),
('a0a7e056297ec0f681a11b499a1acda89afafbf45e374a350979de2497a717a03b56f1afa1d82d38', 13, 1, 'authToken', '[]', 0, '2020-09-03 06:48:20', '2020-09-03 06:48:20', '2021-09-03 09:48:20'),
('a5807e37eda1d2723d7581d5afdc6814f38040085e203578338ae2b18b837cd53930034861b6f2b2', 23, 1, 'authToken', '[]', 0, '2020-10-24 20:06:40', '2020-10-24 20:06:40', '2021-10-24 20:06:40'),
('a5bb3c323a9ca639f5b813727c4fac15f98cffcd55d44ebbeeaf6870d27aa0984290ec8d80e6cca0', 1, 1, 'authToken', '[]', 0, '2020-08-19 18:04:49', '2020-08-19 18:04:49', '2021-08-19 21:04:49'),
('aaf764ec014e7f6b382f0c9a6f3b64af3be07532d282722f0352fdc95c34856631690a65a1468b65', 18, 1, 'authToken', '[]', 0, '2020-10-24 12:19:59', '2020-10-24 12:19:59', '2021-10-24 12:19:59'),
('b205b03018dbec3a8541c37bfd4aef64da0d3ab24503f0cb0fe56510e612a0f5b48e7ca402b10677', 13, 1, 'authToken', '[]', 0, '2020-08-24 18:16:44', '2020-08-24 18:16:44', '2021-08-24 21:16:44'),
('bdafca1e2146e7d09eedc86b6ac8a1ac9039df91520a38719674f2306a0ef15a35a90b747c4041f6', 1, 1, 'authToken', '[]', 0, '2020-09-03 18:37:40', '2020-09-03 18:37:40', '2021-09-03 21:37:40'),
('c29605cb5da7052c0ea3b1c7aae37835fcb5661499bdff9e0c9df5bb0523f26fdc8701a03aa84d0c', 22, 1, 'authToken', '[]', 0, '2020-10-24 16:40:32', '2020-10-24 16:40:32', '2021-10-24 16:40:32'),
('c7984d2e01e9503febf6443b17d179a231d94ed501dadeaeb8747f82903c053a12446c304a7cdb24', 1, 1, 'authToken', '[]', 0, '2020-08-28 06:52:39', '2020-08-28 06:52:39', '2021-08-28 09:52:39'),
('d17acf587ddbeaa6c59dd09c0976b814766e5046e126ffb3658db43ed3da59143269475db6761122', 1, 1, 'authToken', '[]', 0, '2020-09-07 08:36:37', '2020-09-07 08:36:37', '2021-09-07 11:36:37'),
('d40965a047f5e5a5da87906222dd33733082c0d40cecbc8ecf80332cc0ce404ba5572e785f473505', 24, 1, 'authToken', '[]', 0, '2020-10-24 20:39:14', '2020-10-24 20:39:14', '2021-10-24 20:39:14'),
('dd80be7ca43ff85fd67e9ad363213db0d931b12e9bafea90188f5c935804fd7027fa505183d8e7ec', 12, 1, 'authToken', '[]', 0, '2020-08-24 18:14:54', '2020-08-24 18:14:54', '2021-08-24 21:14:54'),
('f172c655260e1caf1496332b5e24099b73893e8e73e4f036c76bf6bc870f0b26b10773836b7ef86c', 12, 1, 'authToken', '[]', 0, '2020-08-24 18:07:47', '2020-08-24 18:07:47', '2021-08-24 21:07:47'),
('fb0a27101359c686e8bbfb7c3f527d0f6b9f826093da5f9d0a2abfaaa92583142f0a77e6eff5c1e3', 1, 1, 'authToken', '[]', 0, '2020-09-12 18:54:35', '2020-09-12 18:54:35', '2021-09-12 21:54:35'),
('feea37eacebbba4c405b1cf451fe7a3c3b3ced84f5a2ab5e56a32b39224257d20ce572b09f158b68', 26, 1, 'authToken', '[]', 0, '2020-10-24 20:31:32', '2020-10-24 20:31:32', '2021-10-24 20:31:32');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'OIatXFQACKDOtpEROi4EDqpkCrP2yhfTmhhkpIIU', NULL, 'http://localhost', 1, 0, 0, '2020-08-17 08:49:54', '2020-08-17 08:49:54'),
(2, NULL, 'Laravel Password Grant Client', 'jTNUMe6AeK880jZaeDPQErIhpKaN9H71oB7k5e8S', 'users', 'http://localhost', 0, 1, 0, '2020-08-17 08:49:54', '2020-08-17 08:49:54');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-08-17 08:49:54', '2020-08-17 08:49:54');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `orderId` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userId` int(11) NOT NULL DEFAULT '0',
  `transaction_id` int(11) NOT NULL DEFAULT '0',
  `response_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `response_message` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction_amount` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction_currency` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subTotal` double(8,2) NOT NULL DEFAULT '0.00',
  `shipmentCost` double(8,2) NOT NULL DEFAULT '0.00',
  `couponValue` double(8,2) NOT NULL DEFAULT '0.00',
  `userCredit` double(8,2) NOT NULL DEFAULT '0.00',
  `finalTotal` float NOT NULL DEFAULT '0',
  `last_4_digits` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_4_digits` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_brand` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trans_date` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipmentAdrsId` mediumint(9) NOT NULL DEFAULT '0',
  `secure_sign` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shippmentId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shippmentPdfUrl` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shippmentFirmId` tinyint(4) DEFAULT '0',
  `delegateId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentStatus` tinyint(4) NOT NULL DEFAULT '3' COMMENT '3: waiting to be verified , 1:verified, 2:failed, 0: user has not gone to payment process yet',
  `orderStatus` tinyint(4) NOT NULL DEFAULT '8' COMMENT 'takes its status from custom_msg table',
  `storeBankId` mediumint(9) DEFAULT NULL,
  `storeAccountNumber` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clientBankId` mediumint(9) DEFAULT NULL,
  `clientAccountNumber` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` mediumtext COLLATE utf8_unicode_ci,
  `paymentRepeated` tinyint(4) DEFAULT NULL,
  `repeatedWith` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentType` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:electronic, 2:bank transfer, 3:uponReceiptö 4:online/visa bank transfer',
  `isReturnedToAdvertiser` tinyint(4) NOT NULL DEFAULT '0',
  `isReturnedToPolicier` tinyint(4) NOT NULL DEFAULT '0',
  `isReturnedToStacker` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `clientBankBeneficiaryName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transferReceiptRef` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refunded` tinyint(4) NOT NULL DEFAULT '0',
  `preferredCargo` mediumint(9) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `orderId`, `userId`, `transaction_id`, `response_code`, `response_message`, `customer_name`, `customer_email`, `transaction_amount`, `transaction_currency`, `customer_phone`, `subTotal`, `shipmentCost`, `couponValue`, `userCredit`, `finalTotal`, `last_4_digits`, `first_4_digits`, `card_brand`, `trans_date`, `shipmentAdrsId`, `secure_sign`, `shippmentId`, `shippmentPdfUrl`, `shippmentFirmId`, `delegateId`, `paymentStatus`, `orderStatus`, `storeBankId`, `storeAccountNumber`, `clientBankId`, `clientAccountNumber`, `notes`, `paymentRepeated`, `repeatedWith`, `paymentType`, `isReturnedToAdvertiser`, `isReturnedToPolicier`, `isReturnedToStacker`, `isDeleted`, `created_at`, `updated_at`, `clientBankBeneficiaryName`, `transferReceiptRef`, `refunded`, `preferredCargo`) VALUES
(39, 'c2905937cc9e', 1, 0, NULL, NULL, NULL, NULL, NULL, 'USD', NULL, 960.00, 0.00, 0.00, 0.00, 960, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, 0, NULL, 3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, '2020-09-05 06:49:03', '2020-09-14 12:38:00', NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_comment`
--

CREATE TABLE `order_comment` (
  `id` int(11) NOT NULL,
  `writerId` int(11) NOT NULL,
  `writerGroupId` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `theContent` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_file`
--

CREATE TABLE `order_file` (
  `id` int(11) NOT NULL,
  `orderId` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fileName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mimeType` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `transactionType` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1: payment, 2:refund',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_file`
--

INSERT INTO `order_file` (`id`, `orderId`, `fileName`, `mimeType`, `status`, `transactionType`, `created_at`, `updated_at`) VALUES
(1, '6', 'd5c4318e8437be2b.jpg', 'image/jpeg', 1, 1, '2020-08-18 13:40:35', '2020-08-18 13:40:35');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `orderId` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `sku` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` mediumint(9) NOT NULL DEFAULT '0',
  `colourId` mediumint(9) NOT NULL DEFAULT '0',
  `weight` float NOT NULL DEFAULT '0',
  `sizeId` mediumint(9) NOT NULL DEFAULT '0',
  `priceAfterDiscount` double(8,2) NOT NULL DEFAULT '0.00',
  `priceBeforeDiscount` double(8,2) DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `orderId`, `productId`, `sku`, `quantity`, `colourId`, `weight`, `sizeId`, `priceAfterDiscount`, `priceBeforeDiscount`, `created_at`, `updated_at`) VALUES
(1, '5', 16, NULL, 0, 0, 0, 0, 0.00, 0.00, '2020-06-30 20:49:12', '2020-06-30 20:49:12'),
(2, '5', 18, NULL, 0, 0, 0, 0, 0.00, 0.00, '2020-06-30 20:49:12', '2020-06-30 20:49:12'),
(3, '7', 15, 'short1346', 1, 1, 0, 2, 30.00, 40.00, '2020-06-30 21:03:20', '2020-06-30 21:03:20'),
(4, '7', 16, NULL, 1, 1, 0, 2, 70.00, 80.00, '2020-06-30 21:03:20', '2020-06-30 21:03:20'),
(5, '7', 17, 'galabiya123465', 1, 1, 0, 1, 110.00, 140.00, '2020-06-30 21:03:20', '2020-06-30 21:03:20'),
(6, '8', 15, 'short1346', 1, 1, 0, 2, 30.00, 40.00, '2020-07-01 10:43:27', '2020-07-01 10:43:27'),
(7, '8', 16, NULL, 1, 1, 0, 2, 70.00, 80.00, '2020-07-01 10:43:27', '2020-07-01 10:43:27'),
(8, '8', 17, 'galabiya123465', 1, 1, 0, 1, 110.00, 140.00, '2020-07-01 10:43:27', '2020-07-01 10:43:27'),
(9, '9', 15, 'short1346', 2, 1, 0, 2, 30.00, 40.00, '2020-07-02 13:41:08', '2020-07-02 13:41:08'),
(10, '9', 16, NULL, 2, 1, 0, 2, 70.00, 80.00, '2020-07-02 13:41:08', '2020-07-02 13:41:08'),
(11, '9', 17, 'galabiya123465', 1, 1, 0, 1, 110.00, 140.00, '2020-07-02 13:41:08', '2020-07-02 13:41:08'),
(12, '9', 18, 'short123465', 3, 0, 0, 0, 20.50, 30.00, '2020-07-02 13:41:08', '2020-07-02 13:41:08'),
(13, '9', 19, 'p321', 4, 1, 0, 2, 80.50, 90.00, '2020-07-02 13:41:08', '2020-07-02 13:41:08'),
(14, '9', 22, NULL, 1, 0, 0, 0, 0.00, 0.00, '2020-07-02 13:41:08', '2020-07-02 13:41:08'),
(15, '10', 15, 'short1346', 2, 1, 0, 2, 30.00, 40.00, '2020-07-02 14:11:28', '2020-07-02 14:11:28'),
(16, '10', 16, NULL, 2, 1, 0, 2, 70.00, 80.00, '2020-07-02 14:11:28', '2020-07-02 14:11:28'),
(17, '10', 17, 'galabiya123465', 1, 1, 0, 1, 110.00, 140.00, '2020-07-02 14:11:28', '2020-07-02 14:11:28'),
(18, '10', 18, 'short123465', 3, 0, 0, 0, 20.50, 30.00, '2020-07-02 14:11:28', '2020-07-02 14:11:28'),
(19, '10', 19, 'p321', 4, 1, 0, 2, 80.50, 90.00, '2020-07-02 14:11:28', '2020-07-02 14:11:28'),
(20, '10', 22, NULL, 1, 0, 0, 0, 0.00, 0.00, '2020-07-02 14:11:28', '2020-07-02 14:11:28'),
(21, '37f6ec5752edf1e0d0156d4a9bec840a', 15, 'short1346', 2, 1, 0, 2, 30.00, 40.00, '2020-07-02 14:25:51', '2020-07-02 14:25:51'),
(22, '37f6ec5752edf1e0d0156d4a9bec840a', 16, NULL, 2, 1, 0, 2, 70.00, 80.00, '2020-07-02 14:25:51', '2020-07-02 14:25:51'),
(23, '37f6ec5752edf1e0d0156d4a9bec840a', 17, 'galabiya123465', 1, 1, 0, 1, 110.00, 140.00, '2020-07-02 14:25:51', '2020-07-02 14:25:51'),
(24, '37f6ec5752edf1e0d0156d4a9bec840a', 18, 'short123465', 3, 0, 0, 0, 20.50, 30.00, '2020-07-02 14:25:51', '2020-07-02 14:25:51'),
(25, '37f6ec5752edf1e0d0156d4a9bec840a', 19, 'p321', 4, 1, 0, 2, 80.50, 90.00, '2020-07-02 14:25:51', '2020-07-02 14:25:51'),
(26, '37f6ec5752edf1e0d0156d4a9bec840a', 22, NULL, 1, 0, 0, 0, 0.00, 0.00, '2020-07-02 14:25:51', '2020-07-02 14:25:51'),
(27, 'b2a6c144af3fbfa6b71f53997d47674b', 15, 'short1346', 2, 1, 0, 2, 30.00, 40.00, '2020-07-02 14:28:12', '2020-07-02 14:28:12'),
(28, '40045a65f220b38da0d9df822f53790f', 15, 'short1346', 2, 1, 0, 2, 30.00, 40.00, '2020-07-02 14:37:36', '2020-07-02 14:37:36'),
(29, 'a088bd1a94e501170d46dcdf7a087201', 15, 'short1346', 8, 1, 0, 2, 30.00, 40.00, '2020-07-02 14:38:00', '2020-07-02 14:38:00'),
(30, '6586f5a9dc36551a56ba2783ebe90306', 15, 'short1346', 1, 1, 0, 2, 30.00, 40.00, '2020-07-02 14:43:30', '2020-07-02 14:43:30'),
(31, '0089674c18b31169d9102ed3d0f5e720', 15, 'short1346', 1, 1, 0, 2, 30.00, 40.00, '2020-07-02 14:48:21', '2020-07-02 14:48:21'),
(32, '73bc1dadd1e9d77e0e0474f1a9ba1ecb', 18, 'short123465', 1, 0, 0, 0, 20.50, 30.00, '2020-07-02 14:50:22', '2020-07-02 14:50:22'),
(33, 'e23b9ffc3fe3', 1, NULL, 1, 2, 0, 2, 80.00, 90.00, '2020-07-14 07:56:28', '2020-07-14 07:56:28'),
(34, '028a839db8a7', 1, NULL, 2, 2, 0, 1, 80.00, 90.00, '2020-07-14 07:58:28', '2020-07-14 07:58:28'),
(35, '1744d4375680', 1, NULL, 3, 1, 0, 1, 80.00, 90.00, '2020-08-17 13:16:55', '2020-08-17 13:16:55'),
(36, 'd38514a0eef6', 1, NULL, 4, 0, 0, 0, 1500.00, 2000.00, '2020-08-18 09:52:23', '2020-08-18 09:52:23'),
(37, 'd38514a0eef6', 2, NULL, 1, 0, 0, 0, 800.00, 950.00, '2020-08-18 09:52:23', '2020-08-18 09:52:23'),
(38, 'd38514a0eef6', 3, NULL, 2, 0, 0, 0, 44.00, 55.00, '2020-08-18 09:52:23', '2020-08-18 09:52:23'),
(39, '2ed3e4f6bcf2', 2, NULL, 2, 2, 0, 3, 800.00, 950.00, '2020-08-18 16:23:45', '2020-08-18 16:23:45'),
(40, '2ed3e4f6bcf2', 4, NULL, 1, 0, 0, 0, 600.00, 780.00, '2020-08-18 16:23:45', '2020-08-18 16:23:45'),
(41, '35986f1aa584', 2, NULL, 1, 0, 0, 0, 800.00, 950.00, '2020-08-18 16:47:59', '2020-08-18 16:47:59'),
(42, 'fcd7047c9f1d', 2, NULL, 1, 0, 0, 0, 800.00, 950.00, '2020-08-18 16:48:00', '2020-08-18 16:48:00'),
(43, 'cd1244219e7b', 12, NULL, 1, 0, 0, 0, 80.00, 85.00, '2020-08-21 20:41:27', '2020-08-21 20:41:27'),
(44, 'cd1244219e7b', 13, NULL, 1, 0, 0, 0, 900.00, 1000.00, '2020-08-21 20:41:27', '2020-08-21 20:41:27'),
(45, '7ff4e6fb8c4b', 14, NULL, 2, 0, 0, 0, 99.00, 99.00, '2020-09-03 09:57:16', '2020-09-03 09:57:16'),
(46, '7ff4e6fb8c4b', 20, NULL, 2, 0, 0, 0, 44.00, 55.00, '2020-09-03 09:57:16', '2020-09-03 09:57:16'),
(47, '704c2b745d4c', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 09:59:39', '2020-09-03 09:59:39'),
(48, '5f6e1a91ea1a', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 09:59:44', '2020-09-03 09:59:44'),
(49, '44ec3403f168', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 10:59:53', '2020-09-03 10:59:53'),
(50, '06d78a49e437', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 11:03:17', '2020-09-03 11:03:17'),
(51, 'ce019c22042a', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 11:05:34', '2020-09-03 11:05:34'),
(52, 'd24eff2edf4a', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 14:53:23', '2020-09-03 14:53:23'),
(53, '1038eda55010', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 15:54:47', '2020-09-03 15:54:47'),
(54, '654ec97e1c89', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 15:57:46', '2020-09-03 15:57:46'),
(55, '6d7c38270006', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 15:59:26', '2020-09-03 15:59:26'),
(56, '7862878fa26e', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:00:25', '2020-09-03 16:00:25'),
(57, '491c86bdbde9', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:02:03', '2020-09-03 16:02:03'),
(58, 'ad24d00f5939', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:03:38', '2020-09-03 16:03:38'),
(59, 'd636422f58af', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:09:23', '2020-09-03 16:09:23'),
(60, '21872467ddf3', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:10:46', '2020-09-03 16:10:46'),
(61, '8e5dbdbfe94b', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:12:35', '2020-09-03 16:12:35'),
(62, 'eaae3f330fbb', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:14:11', '2020-09-03 16:14:11'),
(63, 'dac7a1a78e5d', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:15:41', '2020-09-03 16:15:41'),
(64, 'dfa828ffe83e', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:16:42', '2020-09-03 16:16:42'),
(65, '42617b01f255', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:16:52', '2020-09-03 16:16:52'),
(66, '8047d1676bd0', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:17:03', '2020-09-03 16:17:03'),
(67, '18cbcee189ab', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:17:41', '2020-09-03 16:17:41'),
(68, 'eb6cddbd3cf2', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:17:45', '2020-09-03 16:17:45'),
(69, 'e8ffbae1219c', 20, NULL, 1, 0, 0, 0, 44.00, 55.00, '2020-09-03 16:23:39', '2020-09-03 16:23:39'),
(70, 'f9fc6c200575', 4, NULL, 1, 0, 0, 0, 60.00, 75.00, '2020-09-05 09:45:19', '2020-09-05 09:45:19'),
(71, 'f9fc6c200575', 5, NULL, 2, 0, 0, 0, 450.00, 500.00, '2020-09-05 09:45:19', '2020-09-05 09:45:19'),
(72, 'f557172338d6', 4, NULL, 1, 0, 0, 0, 60.00, 75.00, '2020-09-05 09:47:51', '2020-09-05 09:47:51'),
(73, 'f557172338d6', 5, NULL, 2, 0, 0, 0, 450.00, 500.00, '2020-09-05 09:47:51', '2020-09-05 09:47:51'),
(74, '07aeaf33e7e8', 4, NULL, 1, 0, 0, 0, 60.00, 75.00, '2020-09-05 09:48:27', '2020-09-05 09:48:27'),
(75, '07aeaf33e7e8', 5, NULL, 2, 0, 0, 0, 450.00, 500.00, '2020-09-05 09:48:27', '2020-09-05 09:48:27'),
(76, 'c2905937cc9e', 4, NULL, 1, 0, 0, 0, 60.00, 75.00, '2020-09-05 09:49:03', '2020-09-05 09:49:03'),
(77, 'c2905937cc9e', 5, NULL, 2, 0, 0, 0, 450.00, 500.00, '2020-09-05 09:49:03', '2020-09-05 09:49:03');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) NOT NULL,
  `titleAR` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleEN` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `methodName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `titleAR`, `titleEN`, `methodName`) VALUES
(1, 'إضافة منتج', 'Create Product', 'CreateProduct'),
(2, 'استعراض المنتجات', 'Products Index', 'AllProducts'),
(3, 'استعراض التصانيف', 'Categories Index', 'AllCategories'),
(4, 'إضافة تصنيف', 'Create Category', 'CreateCategory'),
(6, 'استعراض الطلبيات', 'Orders Index', 'AllOrders'),
(7, 'إضافة مستخدم', 'Create User', 'CreateUser'),
(8, 'استعراض المستخدمين', 'Users Index', 'AllUsers'),
(9, 'تعديل مستخدم', 'Edit User', 'EditUser'),
(10, 'تعديل منتج', 'Edit Product', 'EditProduct'),
(11, 'تعديل تصنيف', 'Edit Category', 'EditCategory'),
(12, 'استعراض الوظائف', 'Roles Index', 'AllRoles'),
(13, 'إضافة وظيفة', 'Create Role', 'CreateRole'),
(14, 'تعديل وظيفة', 'Edit Role', 'EditRole'),
(15, 'استعراض الألوان', 'Colours Index', 'AllColours'),
(16, 'إضافة لون', 'Create Colour', 'CreateColour'),
(17, 'تعديل لون', 'Edit Colour', 'EditColour'),
(18, 'إضافة مقاس', 'Create Size', 'CreateSize'),
(19, 'استعراض المقاسات', 'Sizes Index', 'AllSizes'),
(20, 'تعديل مقاس', 'Edit Size', 'EditSize'),
(21, 'استعراض الكوبونات', 'Coupons Index', 'AllCoupons'),
(22, 'إضافة كوبون', 'Create Coupon', 'CreateCoupon'),
(23, 'تعديل كوبون', 'Edit Coupon', 'EditCoupon'),
(24, 'استعراض المقالات', 'Articles Index', 'AllArticles'),
(25, 'إضافة مقال', 'Create Article', 'CreateArticle'),
(26, 'تعديل مقال', 'Edit Article', 'EditArticle'),
(27, 'استعراض الأسئلة الشائعة', 'Faqs Index', 'AllFaqs'),
(28, 'إضافة سؤال شائع', 'Create Faq', 'CreateFaq'),
(29, 'تعديل سؤال شائع', 'Edit Faq', 'EditFaq'),
(30, 'استعراض الصفحات الخاصة', 'Custom Pages Index', 'AllCpages'),
(31, 'إضافة صفحة خاصة', 'Create Custom Page', 'CreateCpage'),
(32, 'تعديل صفحة خاصة', 'Edit Custom Page', 'EditCpage'),
(33, 'تحديث الإعدادات', 'Edit Settings', 'EditSettings'),
(34, 'الرسائل الواردة', 'Inbox', 'AllContacts'),
(35, 'استعراض الرسائل الخاصة', 'Custom Msgs Index', 'AllCustommsgs'),
(36, 'إضافة رسالة خاصة', 'Create Custom Msg', 'CreateCustommsg');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `priceBeforeDiscount` float DEFAULT NULL,
  `priceAfterDiscount` float DEFAULT NULL,
  `theCount` mediumint(9) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0: inactive, 1:active',
  `isGallery` tinyint(4) NOT NULL DEFAULT '0',
  `isOffer` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: not in offer, 1 : is in offer',
  `isNew` tinyint(4) NOT NULL DEFAULT '0',
  `isAccessory` tinyint(4) DEFAULT '0',
  `shipmentIsFree` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `sku` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` float NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `priceBeforeDiscount`, `priceAfterDiscount`, `theCount`, `status`, `isGallery`, `isOffer`, `isNew`, `isAccessory`, `shipmentIsFree`, `isDeleted`, `sku`, `weight`, `created_at`, `updated_at`) VALUES
(1, 3000, 2500, NULL, 1, 0, 1, 1, 0, 0, 0, NULL, 0, '2020-09-07 12:03:17', '2020-09-07 12:03:17'),
(2, 1000, 1000, NULL, 1, 0, 1, 0, 0, 0, 0, NULL, 0, '2020-09-07 12:16:46', '2020-09-07 12:16:46'),
(3, 500, 400, NULL, 1, 1, 0, 0, 0, 0, 0, NULL, 0, '2020-09-07 12:29:52', '2020-09-07 12:29:52'),
(4, 20, 20, NULL, 1, 0, 0, 1, 0, 0, 1, NULL, 0, '2020-09-07 15:07:01', '2020-09-07 15:43:48'),
(5, 220, 220, NULL, 1, 0, 0, 0, 0, 0, 1, NULL, 0, '2020-09-07 15:38:49', '2020-09-07 15:43:43'),
(6, 200, 200, NULL, 1, 0, 0, 0, 0, 0, 1, NULL, 0, '2020-09-07 15:44:54', '2020-09-07 16:17:32'),
(7, 600, 500, NULL, 1, 0, 1, 1, 0, 0, 1, NULL, 0, '2020-09-07 16:15:25', '2020-09-07 17:22:05'),
(8, 110, 110, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, '2020-09-07 18:07:01', '2020-09-07 18:08:12'),
(9, 5000, 4000, NULL, 1, 0, 1, 1, 0, 0, 0, NULL, 0, '2020-09-07 18:14:33', '2020-09-07 18:14:33'),
(10, 9999, 8888, NULL, 1, 0, 1, 1, 0, 0, 0, NULL, 0, '2020-09-07 18:30:58', '2020-09-07 18:30:58'),
(11, 55, 44, NULL, 1, 0, 1, 1, 0, 0, 1, NULL, 0, '2020-09-07 18:41:43', '2020-09-08 00:26:50'),
(12, 456, 123, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, '2020-09-08 08:54:38', '2020-09-08 08:54:38'),
(13, 456, 123, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, '2020-09-08 08:56:05', '2020-09-08 08:56:05'),
(14, 456, 123, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, '2020-09-08 08:56:55', '2020-09-08 08:56:55'),
(16, 789, 788, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, '2020-09-08 09:19:04', '2020-09-08 09:19:04'),
(17, 88, 77, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, '2020-09-08 09:20:06', '2020-09-08 09:20:06'),
(18, 99, 88, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, '2020-09-08 09:23:05', '2020-09-08 09:23:05'),
(19, 44, 33, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, '2020-09-08 09:29:50', '2020-09-08 09:29:50'),
(20, 11, 8, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, '2020-09-08 09:35:21', '2020-09-08 09:35:21'),
(21, 840, 720, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, '2020-09-08 09:42:26', '2020-09-08 09:42:26'),
(22, 66, 65, NULL, 1, 0, 0, 0, 0, 0, 0, NULL, 0, '2020-09-08 09:49:17', '2020-09-08 09:49:17'),
(23, 99, 88, NULL, 1, 0, 0, 0, 1, 0, 0, NULL, 0, '2020-09-08 09:53:57', '2020-09-08 14:43:42');

-- --------------------------------------------------------

--
-- Table structure for table `products_accessories`
--

CREATE TABLE `products_accessories` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `accessoryId` int(11) NOT NULL,
  `priceBeforeDiscount` float DEFAULT '0',
  `priceAfterDiscount` float DEFAULT '0',
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_accessories`
--

INSERT INTO `products_accessories` (`id`, `productId`, `accessoryId`, `priceBeforeDiscount`, `priceAfterDiscount`, `photo`) VALUES
(5, 1, 23, 22, 12, NULL),
(6, 2, 23, 13, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products_cats`
--

CREATE TABLE `products_cats` (
  `productId` int(11) NOT NULL,
  `catId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_cats`
--

INSERT INTO `products_cats` (`productId`, `catId`) VALUES
(1, 2),
(2, 1),
(3, 2),
(3, 1),
(4, 2),
(5, 2),
(6, 3),
(7, 1),
(8, 3),
(9, 2),
(10, 2),
(11, 3),
(11, 2),
(11, 1),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(20, 2),
(20, 1),
(21, 3),
(22, 3),
(23, 3);

-- --------------------------------------------------------

--
-- Table structure for table `products_trans`
--

CREATE TABLE `products_trans` (
  `id` int(11) NOT NULL,
  `rowId` int(11) NOT NULL,
  `languageCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `metaTags` text COLLATE utf8_unicode_ci,
  `isDeleted` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products_trans`
--

INSERT INTO `products_trans` (`id`, `rowId`, `languageCode`, `title`, `description`, `metaTags`, `isDeleted`) VALUES
(1, 1, 'AR', 'ماكينة استخراج ذهب', '<p>ماكينة استخراج ذهب&nbsp;ماكينة استخراج ذهب&nbsp;ماكينة استخراج ذهب&nbsp;ماكينة استخراج ذهب&nbsp;ماكينة استخراج ذهب&nbsp;ماكينة استخراج ذهب&nbsp;ماكينة استخراج ذهب&nbsp;ماكينة استخراج ذهب&nbsp;ماكينة استخراج ذهب&nbsp;ماكينة استخراج ذهب ررماكينة استخراج ذهب رررماكينة استخراج ذهب ررررماكينة استخراج ذهب ررررماكينة استخراج ذهب رررماكينة استخراج ذهب رررماكينة استخراج ذهب رررماكينة استخراج ذهب&nbsp;ماكينة استخراج ذهب&nbsp;</p>', 'ماكينة، ذهب،', 0),
(2, 1, 'EN', 'Gold Machine', '<p>Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine Description&nbsp;Gold Machine&nbsp;</p>', 'Gold Machine Tags', 0),
(3, 2, 'AR', 'ماكينة استخراج معادن', '<p>ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;ماكينة استخراج معادن&nbsp;</p>', 'ماكينة استخراج معادن مفتاحية', 0),
(4, 3, 'AR', 'منتج خاص بالمعرض فقط', '<p>منتج خاص بالمعرض فقط&nbsp;منتج خاص بالمعرض فقط&nbsp;منتج خاص بالمعرض فقط&nbsp;منتج خاص بالمعرض فقط&nbsp;</p>', 'منتج خاص بالمعرض فقط', 0),
(5, 4, 'AR', 'يب', NULL, NULL, 1),
(6, 5, 'AR', 'يسب', '<p>سيبيشب</p>', NULL, 1),
(7, 6, 'AR', 'الاكسسوار الاول', '<p>هنا شرح الاكسسوار</p>', NULL, 1),
(9, 7, 'AR', 'منتج معادن', '<p>هذا منتج جديد نريد التجربة فقط</p>', NULL, 1),
(10, 8, 'AR', 'اكسسوار الاول', '<p>هنا شرح الاكسسوار</p>', NULL, 0),
(11, 9, 'AR', 'منتج تجريبي', '<p>3d Ground Imaging Metal Detector&rdquo; GROUND SCOPE is a specialized device to detecting gold, treasures, metal and caves underground to maximum depth up to 14 meters, it operates with the radar imaging system and provides 2D and 3D data for the shape of the detected object underground, and measures the depth of the target. This device is one of the best and latest imaging scanner devices in the world, most sophisticated and accurate in the results, it is not complicated and easy to use and does not need external support programs for analysis and imaging, Provides directly results on the device screen, in addition, it is offered at a very economical price to give the opportunity for everyone to buying it.</p>', NULL, 0),
(12, 10, 'AR', 'منتج تجريبي 2', '<p>هنا شرح المنتج</p>', NULL, 0),
(13, 14, 'AR', 'رجل ماكينة معادن', '<p>اكسسوار رجل ماكينة معادن&nbsp;</p>', 'اكسسوار رجل ماكينة معادن مفتاحية', 0),
(15, 16, 'AR', 'قفا ماكينة استخراج معادن', '<p>قفا الماكينة</p>', 'قفا الماكينة', 0),
(16, 17, 'AR', 'بطن ماكينة معادن', '<p>بطن ماكينة معادن</p>', 'بطن معادن ماكينة', 0),
(17, 18, 'AR', 'ذراع ماكينة استخراد معادن', '<p>وصف&nbsp;</p>', 'ذراع', 0),
(18, 19, 'AR', 'كوع ماكينة معادن', '<p>كوع ماكينة وصف</p>', 'كوع ماكينة مفتاحية', 0),
(19, 20, 'AR', 'ظهر ماكينة معادن', '<p>ظهر ماكينة معادن وصف</p>', 'ظهر مفتاح', 0),
(20, 21, 'AR', 'ورك مايكنة نعادن', '<p>ورك مايكنة نعادن&nbsp;ورك مايكنة نعادن&nbsp;ورك مايكنة نعادن ررورك مايكنة نعادن رررورك مايكنة نعادن رر</p>', 'ورك مايكنة نعادن  حتفاحية', 0),
(21, 22, 'AR', 'ركبة ماكينة معادن', '<p>ركبة ماد</p>', 'ركبة', 0),
(22, 23, 'AR', 'كعب معادن', '<p>كعب معادن</p>', 'كعبم ادن', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_feature`
--

CREATE TABLE `product_feature` (
  `productId` int(11) NOT NULL,
  `featureId` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_feature`
--

INSERT INTO `product_feature` (`productId`, `featureId`) VALUES
(1, 1),
(9, 1),
(10, 2),
(11, 1),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(20, 2),
(21, 1),
(22, 1),
(23, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_file`
--

CREATE TABLE `product_file` (
  `id` int(11) NOT NULL,
  `rowId` int(11) DEFAULT NULL,
  `fileName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mimeType` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `languageCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_file`
--

INSERT INTO `product_file` (`id`, `rowId`, `fileName`, `mimeType`, `status`, `languageCode`, `created_at`, `updated_at`) VALUES
(1, 1, 'c806c71f743115f3.jpeg', 'image/jpeg', 1, 'AR', '2020-09-07 12:03:23', '2020-09-07 12:03:23'),
(2, 1, 'e58b939a39b9f92b.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 12:03:24', '2020-09-07 12:03:24'),
(3, 1, '4e205659b678d29f.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 12:03:24', '2020-09-07 12:03:24'),
(4, 1, '8b0c60e83b2f6ef6.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 12:03:24', '2020-09-07 12:03:24'),
(5, 1, '215b0bc070b9aa7f.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 12:03:24', '2020-09-07 12:03:24'),
(6, 1, '371ff2272b85bc05.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 12:03:24', '2020-09-07 12:03:24'),
(7, 2, '5fb9b75879fdd25e.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 12:05:21', '2020-09-07 12:05:21'),
(8, 2, '983fa6ed6a231436.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 12:05:21', '2020-09-07 12:05:21'),
(9, 2, '270507cffd9ce239.png', 'image/png', 1, 'EN', '2020-09-07 12:05:21', '2020-09-07 12:05:21'),
(10, 2, '34d8d494cc9a9156.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 12:05:21', '2020-09-07 12:05:21'),
(11, 2, '4f7426a31edf50ce.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 12:05:21', '2020-09-07 12:05:21'),
(12, 2, 'f3775efb0b404e04.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 12:05:21', '2020-09-07 12:05:21'),
(13, 3, '5424db8e5e889041.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 12:17:02', '2020-09-07 12:17:02'),
(14, 3, '12da924b2e755e9b.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 12:17:02', '2020-09-07 12:17:02'),
(15, 3, '363a1d5789defd3d.png', 'image/png', 1, 'AR', '2020-09-07 12:17:02', '2020-09-07 12:17:02'),
(16, 3, 'c9515178fc1c843d.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 12:17:02', '2020-09-07 12:17:02'),
(17, 3, 'a8fdcfeb04ac8b4d.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 12:17:02', '2020-09-07 12:17:02'),
(18, 3, '81fb9e9739073479.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 12:17:02', '2020-09-07 12:17:02'),
(19, 4, '69fc8ac87a5b6e9b.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 12:29:55', '2020-09-07 12:29:55'),
(20, 8, 'dd06eea85689d706.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 16:04:02', '2020-09-07 16:04:02'),
(21, 8, '582eaf6184c1cbc3.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 16:04:02', '2020-09-07 16:04:02'),
(22, 8, '1606a1e6d251bd53.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 16:04:02', '2020-09-07 16:04:02'),
(23, 8, '9996f39557004dd0.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 16:04:02', '2020-09-07 16:04:02'),
(24, 8, 'b675aeecbcf92860.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 16:04:02', '2020-09-07 16:04:02'),
(25, 8, '8f33d01ca5632844.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 16:04:02', '2020-09-07 16:04:02'),
(26, 8, '45316622eb6a8562.jpg', 'image/jpeg', 1, 'EN', '2020-09-07 16:04:02', '2020-09-07 16:04:02'),
(27, 9, '0baa33400a8dd107.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 16:15:42', '2020-09-07 16:15:42'),
(28, 9, '3ff57ff3f3c15c62.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 16:15:42', '2020-09-07 16:15:42'),
(29, 9, 'edb858842306c021.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 16:15:42', '2020-09-07 16:15:42'),
(30, 9, '34c43b45d7393f93.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 16:15:42', '2020-09-07 16:15:42'),
(31, 9, 'cddedb9fd7d44455.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 16:15:42', '2020-09-07 16:15:42'),
(32, 9, '5a35c085fb2b90a4.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 16:15:42', '2020-09-07 16:15:42'),
(33, 9, '22dc155603a69250.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 16:15:42', '2020-09-07 16:15:42'),
(34, 9, '135b60646d04810d.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 16:15:42', '2020-09-07 16:15:42'),
(35, 11, '2fe1bb980e7d24dc.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:14:53', '2020-09-07 18:14:53'),
(43, 11, '0e96d6a8779f93c3.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:16:52', '2020-09-07 18:16:52'),
(44, 11, '9c00db1fabc62885.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:16:52', '2020-09-07 18:16:52'),
(45, 11, '6dc0a420cfe11b7f.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:16:52', '2020-09-07 18:16:52'),
(46, 11, 'f423b042ab92820e.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:16:52', '2020-09-07 18:16:52'),
(47, 11, '5573e227a994d11d.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:16:52', '2020-09-07 18:16:52'),
(48, 11, '5c510b02433cfa91.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:16:52', '2020-09-07 18:16:52'),
(49, 11, 'b9207c617197652a.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:16:52', '2020-09-07 18:16:52'),
(50, 12, '4632245cab360831.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:31:15', '2020-09-07 18:31:15'),
(51, 12, '61041393e598fe0b.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:31:15', '2020-09-07 18:31:15'),
(52, 12, 'bc45f9b9faf79285.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:31:16', '2020-09-07 18:31:16'),
(53, 12, '97086cf2fe3a8773.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:31:16', '2020-09-07 18:31:16'),
(54, 12, '94a2a46a867811f2.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:31:16', '2020-09-07 18:31:16'),
(55, 12, 'a2b57e88b7e7e0f0.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:31:16', '2020-09-07 18:31:16'),
(56, 12, '6f5c87d62b552d62.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:31:16', '2020-09-07 18:31:16'),
(57, 12, '793128dc2f77fe7c.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:31:16', '2020-09-07 18:31:16'),
(58, 13, 'b117073e97eb2ef9.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:41:49', '2020-09-07 18:41:49'),
(59, 13, '14c1936270fccc87.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:41:49', '2020-09-07 18:41:49'),
(60, 13, '5832bbf1cf9f7b42.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:41:49', '2020-09-07 18:41:49'),
(61, 13, 'efbc90718b8abb9a.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:41:49', '2020-09-07 18:41:49'),
(62, 13, 'f0fce821596dd922.jpg', 'image/jpeg', 1, 'AR', '2020-09-07 18:41:49', '2020-09-07 18:41:49'),
(63, 15, '41bf5136536a16af.jpg', 'image/jpeg', 1, 'AR', '2020-09-08 09:19:05', '2020-09-08 09:19:05'),
(64, 16, '6997743f3e08bdc2.jpg', 'image/jpeg', 1, 'AR', '2020-09-08 09:20:07', '2020-09-08 09:20:07'),
(65, 17, '5764420feb9f7561.jpg', 'image/jpeg', 1, 'AR', '2020-09-08 09:23:06', '2020-09-08 09:23:06'),
(66, 18, '7b59286d46d39b3d.jpg', 'image/jpeg', 1, 'AR', '2020-09-08 09:29:50', '2020-09-08 09:29:50'),
(67, 22, '0099773d31657dd7.jpg', 'image/jpeg', 1, 'AR', '2020-09-08 09:53:58', '2020-09-08 09:53:58'),
(68, 22, 'f0fce821596dd922.jpg', 'image/jpeg', 1, 'AR', '2020-09-08 13:32:21', '2020-09-08 13:32:21'),
(69, 22, '5832bbf1cf9f7b42.jpg', 'image/jpeg', 1, 'AR', '2020-09-08 13:32:21', '2020-09-08 13:32:21'),
(70, 22, '5764420feb9f7561.jpg', 'image/jpeg', 1, 'AR', '2020-09-08 13:32:52', '2020-09-08 13:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `product_props`
--

CREATE TABLE `product_props` (
  `productId` int(11) NOT NULL,
  `propId` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_props`
--

INSERT INTO `product_props` (`productId`, `propId`) VALUES
(5, 4),
(5, 3),
(5, 2),
(5, 1),
(6, 4),
(6, 3),
(6, 2),
(6, 1),
(8, 4),
(8, 3),
(8, 2),
(8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_props_values`
--

CREATE TABLE `product_props_values` (
  `id` int(11) NOT NULL,
  `productId` int(11) DEFAULT NULL,
  `propId` mediumint(9) DEFAULT NULL,
  `valueId` mediumint(9) DEFAULT NULL,
  `sku` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `theCount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_qty_history`
--

CREATE TABLE `product_qty_history` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `qty` float NOT NULL,
  `userId` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_rate`
--

CREATE TABLE `product_rate` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `rateValue` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_registers`
--

CREATE TABLE `product_registers` (
  `id` int(11) NOT NULL,
  `firstName` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countryCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adrs` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serialNum` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guaranteeNum` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_registers`
--

INSERT INTO `product_registers` (`id`, `firstName`, `lastName`, `email`, `phone`, `countryCode`, `adrs`, `serialNum`, `guaranteeNum`, `created_at`, `updated_at`) VALUES
(1, 'Khaled', 'Abdelfattah', 'sun.stronghold@gmail.com', '05541711737', 'TR', 'بولاق', '999', '888', '2020-10-10 13:52:31', '2020-10-10 13:52:31'),
(3, 'Khaled', 'Abdelfattah', 'sun.stronghold@gmail.com', '05541711737', 'TR', 'vvb', '999', '888', '2020-10-10 13:59:04', '2020-10-10 13:59:04'),
(4, 'Khaled', 'Abdelfattah', 'sun.stronghold@gmail.com', '05541711737', 'TR', 'b', '999b', '888', '2020-10-10 14:00:51', '2020-10-10 14:00:51');

-- --------------------------------------------------------

--
-- Table structure for table `product_tags`
--

CREATE TABLE `product_tags` (
  `productId` int(11) NOT NULL,
  `tagId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `titleAR` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleEN` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `titleAR`, `titleEN`, `status`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'مسئول طلبيات', 'Orders Manager', 1, NULL, '2020-07-11 07:36:19', '2020-07-11 07:36:19'),
(2, 'مسئول منتجات', 'Products Manager', 1, NULL, '2020-07-11 07:36:31', '2020-07-11 07:36:31'),
(3, 'مسئول عملاء', 'Clients Manager', 1, NULL, '2020-07-11 07:36:42', '2020-07-11 07:36:42'),
(4, 'مدير المقالات', 'Articles Manager', 1, NULL, '2020-07-26 13:10:08', '2020-07-26 13:10:08');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `roleId` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`roleId`, `permissionId`) VALUES
(1, 6),
(2, 2),
(2, 1),
(3, 7),
(3, 8),
(2, 4),
(2, 3),
(2, 15),
(2, 17),
(2, 16),
(2, 19);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `titleAR` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleEN` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleTR` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descriptionAR` mediumtext COLLATE utf8_unicode_ci,
  `descriptionEN` mediumtext COLLATE utf8_unicode_ci,
  `descriptionTR` mediumtext COLLATE utf8_unicode_ci,
  `metaDataAR` text COLLATE utf8_unicode_ci,
  `metaDataEN` text COLLATE utf8_unicode_ci,
  `metaDataTR` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0: inactive, 1:active',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `photo` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `titleAR`, `titleEN`, `titleTR`, `descriptionAR`, `descriptionEN`, `descriptionTR`, `metaDataAR`, `metaDataEN`, `metaDataTR`, `status`, `isDeleted`, `photo`, `created_at`, `updated_at`) VALUES
(5, 'خدمة 04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '5092333edc775601.png', '2020-06-10 14:15:19', '2020-06-10 14:15:19');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `titleAR` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleEN` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleTR` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone1` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `phone2` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email1` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email2` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `adrsAR` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adrsTR` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adrsEN` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb_url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `snapchat` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `whatsapp` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `welcomeMsgAR` text COLLATE utf8_unicode_ci,
  `welcomeMsgTR` text COLLATE utf8_unicode_ci,
  `welcomeMsgEN` text COLLATE utf8_unicode_ci,
  `copyRightsEN` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `copyRightsTR` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `copyRightsAR` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shippingCharges` float NOT NULL,
  `aboutUs` mediumtext COLLATE utf8_unicode_ci,
  `taxNumber` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showProductCount` tinyint(4) NOT NULL DEFAULT '2',
  `taxAlertMsg` text COLLATE utf8_unicode_ci,
  `taxAlertMsgEn` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enableRating` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mainUrl` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apiUrl` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `openTime` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notificationWay` tinyint(4) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `titleAR`, `titleEN`, `titleTR`, `phone1`, `phone2`, `email1`, `email2`, `adrsAR`, `adrsTR`, `adrsEN`, `fb_url`, `twitter_url`, `google_url`, `youtube_url`, `instagram`, `snapchat`, `whatsapp`, `welcomeMsgAR`, `welcomeMsgTR`, `welcomeMsgEN`, `copyRightsEN`, `copyRightsTR`, `copyRightsAR`, `shippingCharges`, `aboutUs`, `taxNumber`, `showProductCount`, `taxAlertMsg`, `taxAlertMsgEn`, `created_at`, `updated_at`, `enableRating`, `photo`, `mainUrl`, `apiUrl`, `openTime`, `notificationWay`) VALUES
(1, 'موقع آي بي اس', 'eCommerce', 'eTicaret', '90554171173', '905525862468', 'supports@ibs-ksa.com', 'k@gmail.com', 'اليمن، صنعاء', 'Türkiye. Istanbul', 'Turkey, Istanbul, Esenyurt', 'fa123cebook.com', 'https://twitte123r.com/argan_package', 'goo123gle.com', 'https://www.youtube.com/channel/UC3z2SXqph0_fZUQLHmvmD8A', 'https://www.instagram.com/argan_package/', 'https://www.snap1chat.com/add/mm123ma', 'https://wa.me/966545133461', 'شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة شركة موف شركة رائدة في الأجهزة', '<p>Hoşgeldiniz uebsitemizde</p>', 'IBS Group Of Factories and Companies, specialized in the Manufacturing and Trading of Metal detectors, Treasures, Gold and groundwater, MWF is an American and Turkish company.', 'Copy rights are reserved', 'Copy Hakkileri rezerverdir', '© حقوق المتجر محفوظة - 2019', 40, 'مؤسسة ارجان باكيج  - Anas\r\nمتخصصة في بيع المنتجات المغربية الطبيعة\r\nللعناية بالبشرة والشعر \r\nنستخرج زيت الأرجان\r\nمن مزارعنا الخاصة بنا بمنطقة سوس المعتمدة كليا على الطبيعة ومواسم الأمطار\r\nنصنع منتجاتنا بأنفسنا وأشرافنا الشخصي بمصانعنا الخاصة الموجودة بمنطقة أغادير لضمان جودة المنتج \r\nتتميز مزارعنا بعدم تواجدها بالقرب من مزراع أخرى التي تستخدم المبيدات الحشرية فهي بعيدة عن جميع الأضرار الخارجية\r\nتتميز أيضاً بانها منطقة لايوجد بها رطوبة التي تؤثر سلباً على حبوب وثمار الأرجان\r\nفهذه المميزات تجعل من بذور الأرجان ذات جودة عالية وتميز منتجاتنا عن غيرها\r\nيستعمل الأرجان الذي يستخلص بطريقة خاصة في أغراض التجميل  كمرطب للبشرة ويدخل في الصناعات التجميلية الراقية\r\nويسمى أيضاً ( الذهب السائل) \r\nيعرف بمعجزة الزيوت المستخرجه من بذور الأرجان  \r\nفهو شجر نادر يتواجد في المغرب وبالتحديد بمنطقة سوس\r\nفيعد زيت الأرجان واحد من أندر وأكثر الزيوت المرغوبة في العالم\r\nنظرا لأرتفاع الطلب العالمي عليها \r\nفيتم قطف الثمار لدينا وتكسريها يدويا لأستخراج بذور الارجان بجودة عالية\r\nفيتم عصر البذور بطريقة جيده و طبيعية \r\nويستخرج منها فقط عصارة ثمرة الأرجان\r\n', '310103659anas300003', 1, 'Anas\r\n123', 'tax msg', '0000-00-00 00:00:00', '2020-10-11 12:27:50', 1, 'd039dc66c95d357e.jpg', 'http://localhost:3000/', 'http://localhost:3000/api/', '9:00 am to 23:00 pm', 0);

-- --------------------------------------------------------

--
-- Table structure for table `shipmentadrs`
--

CREATE TABLE `shipmentadrs` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL DEFAULT '0',
  `receiverMobile` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiverMobile2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adrs` tinytext COLLATE utf8_unicode_ci,
  `firstName` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countryId` mediumint(9) DEFAULT '0',
  `cityId` int(11) NOT NULL DEFAULT '0',
  `region` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blockNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `road` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `building` tinyint(4) NOT NULL DEFAULT '0',
  `apartment` tinyint(4) NOT NULL DEFAULT '0',
  `postalCode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` mediumtext COLLATE utf8_unicode_ci,
  `theDefault` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipmentadrs`
--

INSERT INTO `shipmentadrs` (`id`, `userId`, `receiverMobile`, `receiverMobile2`, `adrs`, `firstName`, `lastName`, `countryId`, `cityId`, `region`, `blockNr`, `road`, `building`, `apartment`, `postalCode`, `notes`, `theDefault`, `created_at`, `updated_at`, `isDeleted`) VALUES
(2, 42, '554171173', '23121221248', '3بولاق', 'Khaled Bey', 'Abdelfattah', 169, 17244, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, '2020-06-29 14:09:29', '2020-06-30 05:32:41', 1),
(3, 42, '5541711737', '5541751111', 'شارع التواز رقم 45 بناء 34 شقة 3', 'هندي', 'التونسي', 190, 355, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, '2020-06-29 14:13:40', '2020-06-30 05:27:18', 0),
(4, 24, '5541711737', '2312122124', 'شارع التواز رقم 45 بناء 34 شقة 3', 'Khaled', 'Abdelfattah', 167, 967, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, '2020-07-08 16:04:20', '2020-07-08 16:04:20', 0),
(5, 1, '966111111111', '966222222222', 'المملكة العربية السعودية - الرياض – الحمراء', 'شكشك', 'المضلع', 163, 8, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, '2020-07-14 04:56:15', '2020-09-14 14:00:27', 0),
(6, 1, '55417117478', '1212388823', 'بولاق', 'Khaled', 'Abdelfattah', 166, 252, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, '2020-07-19 20:10:04', '2020-07-19 21:36:51', 0),
(7, 1, '5541711737', '966222222222', 'المملكة العربية السعودية - الرياض – الحمراء', 'Khaled', 'Abdelfattah', 172, 164, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, '2020-07-19 21:20:22', '2020-07-19 21:32:01', 0),
(8, 2, '934176832', '5541711111', 'المملكة العربية السعودية - الرياض – الحمراء', 'Khaled', 'Abdelfattah', 304, 378405, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, '2020-07-24 12:05:14', '2020-07-24 12:05:14', 0),
(9, 2, '5541711747', '1212313123', 'شارع التواز رقم 45 بناء 34 شقة 3', 'مصطفى', 'محملجي', 248, 160, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, '2020-07-26 09:31:44', '2020-07-26 09:31:44', 0),
(10, 13, '5541711737', '2312122124', 'gazi', 'مشمش', 'فوزي', 304, 378406, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, '2020-09-03 06:53:40', '2020-09-03 06:53:40', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `titleAR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleEN` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `titleAR`, `titleEN`, `status`, `photo`, `isDeleted`) VALUES
(1, 'بعيد', 'Far', 1, NULL, 0),
(2, 'متوسط', 'Medium', 1, 'f8da201c645e1702.jpg', 0),
(3, 'قصير', 'Short', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sub_cat`
--

CREATE TABLE `sub_cat` (
  `id` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL DEFAULT '0',
  `titleAR` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titleEN` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_cat`
--

INSERT INTO `sub_cat` (`id`, `categoryId`, `titleAR`, `titleEN`, `photo`, `status`, `isDeleted`) VALUES
(2, 10, 'لانجيري', 'Langery', '8dc310c599297a34.png', 1, 0),
(3, 9, 'مدير بط', 'Duck Manager', 'b47300b8ec5c3fee.jpg', 1, 0),
(4, 9, 'هشك', 'بشك', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tabs`
--

CREATE TABLE `tabs` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0: inactive, 1:active',
  `photo` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tabs`
--

INSERT INTO `tabs` (`id`, `productId`, `status`, `photo`) VALUES
(5, 2, 1, 'ea7885b1c609e59b.jpg'),
(6, 2, 1, NULL),
(7, 1, 1, '6f4e32af09d637bf.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tabs_trans`
--

CREATE TABLE `tabs_trans` (
  `id` int(11) NOT NULL,
  `rowId` int(11) NOT NULL,
  `languageCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tabs_trans`
--

INSERT INTO `tabs_trans` (`id`, `rowId`, `languageCode`, `title`, `description`) VALUES
(2, 5, 'AR', 'تاب 1', '<p>تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;</p>'),
(3, 6, 'AR', 'تاب 2', '<p>تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;تاب 2&nbsp;</p>'),
(4, 7, 'AR', 'تاب 1', '<p>تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;تاب 1&nbsp;</p>'),
(5, 5, 'EN', 'tab1', '<p>tab1&nbsp;tab1&nbsp;tab1&nbsp;tab1&nbsp;tab1&nbsp;tab1&nbsp;tab1&nbsp;tab1&nbsp;tab1&nbsp;tab1&nbsp;tab1&nbsp;tab1&nbsp;tab1&nbsp;tab1 vvvv</p>');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstName` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `passwordForAdmin` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'halid7',
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationality` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `adrs` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` mediumtext COLLATE utf8_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `super` tinyint(4) NOT NULL DEFAULT '0',
  `groupId` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:manager, 2:client, 3:agent',
  `activationCode` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 :Male  , 2 : Female',
  `photo` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fromMobApp` tinyint(4) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0',
  `city` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `municipality` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `neighborhood` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `building` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apartment` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orphan` tinyint(4) NOT NULL DEFAULT '0',
  `tcNr` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstName`, `lastName`, `phone`, `phone2`, `email`, `website`, `password`, `passwordForAdmin`, `country`, `nationality`, `adrs`, `notes`, `remember_token`, `created_at`, `updated_at`, `status`, `super`, `groupId`, `activationCode`, `gender`, `photo`, `fromMobApp`, `isDeleted`, `city`, `municipality`, `neighborhood`, `street`, `building`, `apartment`, `orphan`, `tcNr`, `birthDate`) VALUES
(1, 'Super', 'Admin', '905541711737', NULL, '905541711737', NULL, '$2y$10$wBPLByCi/2WX5EZFkzHi4.XbSz9IMJzYfv.Fs1M7HCHDdlZqJVYyS', 'halid7', NULL, NULL, NULL, NULL, NULL, '2020-07-09 17:37:55', '2020-10-25 12:50:53', 1, 1, 1, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL),
(16, 'fff', 'ggg', '6666666558', NULL, NULL, NULL, '$2y$10$8M3apd92nIYXVlaWJ5Rp7.Ycppx4BMoqBcv28mswJdyEmle3G6v0u', 'halid7', NULL, 'SY', NULL, NULL, NULL, '2020-10-23 18:14:07', '2020-10-23 18:14:07', 1, 0, 2, '4329', 1, NULL, 0, 0, 'Istanbul', 'Esenyurt', 'Suleymaniye', 'tt', 'gg', '6', 0, '66666655555', 'gg'),
(17, 'fff', 'ggg', '6666666558', NULL, NULL, NULL, '$2y$10$XX7YcWzZUnntOI36GwzxzOeCxivRABlDbGOMLR63rJWEl3YYZHY36', 'halid7', NULL, 'SY', NULL, NULL, NULL, '2020-10-23 18:16:40', '2020-10-23 18:16:40', 1, 0, 2, '4709', 1, NULL, 0, 0, 'Istanbul', 'Esenyurt', 'Suleymaniye', 'tt', 'gg', '6', 0, '6666655555', 'gg'),
(18, 'نسنسنس', NULL, NULL, NULL, NULL, NULL, '$2y$10$Xs2K0PdrciSWGw6GYGypWOSKM453m9GmKBFUnCvIv/n.dFYS80yKO', 'halid7', NULL, 'SY', NULL, NULL, NULL, '2020-10-24 12:19:59', '2020-10-25 12:53:10', 1, 0, 2, '7687', 1, NULL, 1, 0, 'Istanbul', 'Esenyurt', 'Suleymaniye', NULL, NULL, NULL, 0, '66', '2020-10-24T12:21:30.106Z'),
(19, 'مشمش', NULL, NULL, NULL, NULL, NULL, '$2y$10$EnnsOUANH/1xd3L0C8VZTeDzyFAvPPS99QfopHlqDdPKikyZNRU9a', 'halid7', NULL, 'SY', NULL, NULL, NULL, '2020-10-24 12:25:08', '2020-10-24 12:25:08', 1, 0, 2, '8341', 1, NULL, 1, 0, 'Istanbul', 'Esenyurt', 'Suleymaniye', NULL, NULL, NULL, 0, '998855666655', '2020-10-24T12:26:22.413Z'),
(20, 'حواوشي بيه', NULL, NULL, NULL, NULL, NULL, '$2y$10$Pb8nRc0.FQRb3Bk9RlV8qe7hNfrpHkOEPkxAHEUnu5mIUFyAZPXM.', 'halid7', NULL, 'SY', NULL, NULL, NULL, '2020-10-24 12:34:05', '2020-10-24 12:34:05', 1, 0, 2, '4224', 1, NULL, 1, 0, 'Istanbul', 'Esenyurt', 'Suleymaniye', NULL, NULL, NULL, 0, '996', '2020-10-24T12:35:53.157Z'),
(21, 'فهمي', NULL, NULL, NULL, NULL, NULL, '$2y$10$.iINkmSCUSfAM2c93/6SCuFlW2BTCKkgG4inieFNZMoXH7tzYInom', 'halid7', NULL, 'SY', NULL, NULL, NULL, '2020-10-24 12:37:39', '2020-10-24 12:37:39', 1, 0, 2, '6613', 1, NULL, 1, 0, 'Istanbul', 'Esenyurt', 'Suleymaniye', NULL, NULL, NULL, 0, '9999', '2020-10-24T12:39:32.973Z'),
(22, 'وو', NULL, NULL, NULL, NULL, NULL, '$2y$10$rF6PX0re3Ynac7ad9NxE0OWHitUfnKOy4QO7gOh.jwmTyJG/4XvBm', 'halid7', NULL, 'SY', NULL, NULL, NULL, '2020-10-24 16:40:32', '2020-10-24 16:40:32', 1, 0, 2, '6514', 1, NULL, 1, 0, 'Istanbul', 'Esenyurt', 'Suleymaniye', NULL, NULL, NULL, 0, '999999999999', '2020-10-24T16:42:16.924Z'),
(23, 'قصي', 'مدكور', '98569856985', NULL, NULL, NULL, '$2y$10$XAC84qK3pHuLvz3Az/9nWOMOJLo213NmQbrgXyyLmsCnF.8VLQuda', 'halid7', NULL, 'SY', NULL, NULL, NULL, '2020-10-24 20:06:40', '2020-10-24 20:06:40', 1, 0, 2, '6524', 1, NULL, 1, 0, 'Istanbul', 'Esenyurt', 'Suleymaniye', '678', '79', '05', 0, '123456789', '2020-10-24T19:55:19.955Z'),
(24, 'vvvv', NULL, NULL, NULL, NULL, NULL, '$2y$10$oOli0LVAsjy0tfLVZ9.RdOexdDXjntxTmnEv0OZiL2P43EV6K2tei', 'halid7', NULL, 'SY', NULL, NULL, NULL, '2020-10-24 20:26:18', '2020-10-24 20:26:18', 1, 0, 2, '7047', 1, NULL, 1, 0, 'Istanbul', 'Esenyurt', 'Suleymaniye', NULL, NULL, NULL, 0, '69369369', '2020-10-24T20:19:27.197Z'),
(25, 'vvvv', NULL, NULL, NULL, NULL, NULL, '$2y$10$iTP0gv662gA7TY2cb98nkeQaqIj.NIboe.GhQfjp0roM5LfRt.q/y', 'halid7', NULL, 'SY', NULL, NULL, NULL, '2020-10-24 20:30:41', '2020-10-24 20:30:41', 1, 0, 2, '1157', 1, NULL, 1, 0, 'Istanbul', 'Esenyurt', 'Suleymaniye', NULL, NULL, NULL, 0, '333333333', '2020-10-24T20:19:27.197Z'),
(26, 'vvvv', NULL, NULL, NULL, NULL, NULL, '$2y$10$c6Zbqs/.lGELid5B5RRrzu899.o722OgxvkE7cw2ALLxl4Tx.d4gW', 'halid7', NULL, 'SY', NULL, NULL, NULL, '2020-10-24 20:31:32', '2020-10-24 20:31:32', 1, 0, 2, '6262', 1, NULL, 1, 0, 'Istanbul', 'Esenyurt', 'Suleymaniye', NULL, NULL, NULL, 0, '2333333333', '2020-10-24T20:19:27.197Z');

-- --------------------------------------------------------

--
-- Table structure for table `users_courses`
--

CREATE TABLE `users_courses` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `courseId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_credit`
--

CREATE TABLE `user_credit` (
  `id` int(10) UNSIGNED NOT NULL,
  `userId` int(11) NOT NULL,
  `giverUserId` int(11) NOT NULL,
  `credit` double(8,2) NOT NULL DEFAULT '0.00',
  `expireDate` date DEFAULT NULL,
  `orderId` int(11) NOT NULL,
  `used` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_credit`
--

INSERT INTO `user_credit` (`id`, `userId`, `giverUserId`, `credit`, `expireDate`, `orderId`, `used`, `created_at`, `updated_at`) VALUES
(1, 8365, 0, 20.00, '2019-07-31', 0, 0, '2019-07-22 08:53:48', '2019-07-22 08:53:48'),
(2, 302, 15022, 10.00, NULL, 26020, 0, '2019-08-04 11:11:20', '2019-08-04 11:11:20'),
(3, 302, 8365, 15.00, '2019-08-16', 26021, 0, '2019-08-04 14:24:39', '2019-08-04 14:24:39'),
(4, 8365, 0, 50.00, '2019-10-24', 26213, 1, '2019-10-03 04:29:32', '2019-10-03 09:52:56'),
(5, 16240, 0, 50.00, '2019-10-24', 26215, 1, '2019-10-03 09:48:24', '2019-10-12 15:21:22'),
(6, 13432, 0, 50.00, '2019-10-24', 26221, 0, '2019-10-03 10:46:20', '2019-10-03 10:46:20'),
(7, 16240, 0, 10.00, '2019-10-30', 26279, 1, '2019-10-25 16:28:15', '2019-10-25 16:29:41'),
(8, 15022, 0, 50.00, '2019-12-30', 0, 1, '2019-11-25 12:20:52', '2019-11-27 15:33:51'),
(9, 19779, 0, 20.00, '2020-01-17', 0, 1, '2020-01-04 08:20:09', '2020-01-12 05:18:35'),
(10, 19779, 0, 15.00, '2020-01-15', 0, 1, '2020-01-04 08:21:21', '2020-01-12 05:18:35'),
(11, 16240, 0, 22.00, '2020-12-31', 26824, 1, '2020-01-14 11:22:42', '2020-02-16 13:34:03'),
(13, 19779, 16240, 40.00, '2020-03-01', 0, 1, '2020-02-04 06:20:11', '2020-02-12 09:24:39'),
(14, 16240, 19792, 40.00, '2020-03-01', 0, 1, '2020-02-04 06:31:16', '2020-02-16 13:34:03'),
(15, 11450, 0, 22.00, '2020-12-31', 26967, 0, '2020-02-23 03:58:25', '2020-02-23 03:58:25'),
(16, 13227, 19739, 40.00, '2020-03-01', 0, 0, '2020-02-24 08:50:04', '2020-02-24 08:50:04'),
(17, 19779, 0, 15.00, '2020-02-27', 26978, 0, '2020-02-25 05:38:17', '2020-02-25 05:38:17'),
(18, 19830, 19830, 40.00, '0000-00-00', 0, 0, '2020-03-17 10:02:37', '2020-03-17 10:02:37'),
(19, 19830, 0, 10.00, '2020-03-18', 27050, 1, '2020-03-17 10:21:58', '2020-03-17 12:15:20'),
(20, 19832, 0, 60.00, '2020-03-24', 27063, 0, '2020-03-21 09:25:05', '2020-03-21 09:25:05'),
(21, 19832, 0, 800.00, '2020-03-31', 27062, 0, '2020-03-21 09:25:13', '2020-03-21 09:25:13');

-- --------------------------------------------------------

--
-- Table structure for table `user_file`
--

CREATE TABLE `user_file` (
  `id` int(11) NOT NULL,
  `rowId` int(11) DEFAULT NULL,
  `fileName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mimeType` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_file`
--

INSERT INTO `user_file` (`id`, `rowId`, `fileName`, `mimeType`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '46aecd90070f32f9.', 'image/jpeg', 1, '2020-10-24 12:01:41', '2020-10-24 12:01:41'),
(2, 1, '1949b7ac6f9542fc.', 'image/jpeg', 1, '2020-10-24 12:09:02', '2020-10-24 12:09:02'),
(3, 1, '1e792518fce0c17d.', 'image/jpeg', 1, '2020-10-24 12:09:35', '2020-10-24 12:09:35'),
(4, 1, 'fdbf28daf4ea2a65..jpg', 'image/jpeg', 1, '2020-10-24 12:10:01', '2020-10-24 12:10:01'),
(5, 1, '7d68b2b973f458d3..jpg', 'image/jpeg', 1, '2020-10-24 12:25:08', '2020-10-24 12:25:08'),
(6, 20, 'aff0f4123918ec2a.jpg', 'image/jpeg', 1, '2020-10-24 12:34:25', '2020-10-24 12:34:25'),
(7, 21, '05be9c4eb1e0985a.jpg', 'image/jpeg', 1, '2020-10-24 12:38:04', '2020-10-24 12:38:04'),
(8, 22, 'a30b86e1e44bf194.jpg', 'image/jpeg', 1, '2020-10-24 16:40:33', '2020-10-24 16:40:33'),
(9, 23, 'fe370c17120b8da7.jpg', 'image/jpeg', 1, '2020-10-24 20:06:41', '2020-10-24 20:06:41'),
(10, 24, '959d5395da00a373.jpg', 'image/jpeg', 1, '2020-10-24 20:26:35', '2020-10-24 20:26:35'),
(11, 25, '40be3e73fa57c639.jpg', 'image/jpeg', 1, '2020-10-24 20:31:11', '2020-10-24 20:31:11'),
(12, 26, '5bbd4b7d2b6abda1.jpg', 'image/jpeg', 1, '2020-10-24 20:31:32', '2020-10-24 20:31:32');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`userId`, `roleId`) VALUES
(3, 2),
(4, 1),
(11, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles_trans`
--
ALTER TABLE `articles_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories_trans`
--
ALTER TABLE `categories_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_file`
--
ALTER TABLE `category_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `donate_id` (`rowId`);

--
-- Indexes for table `colours`
--
ALTER TABLE `colours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment_file`
--
ALTER TABLE `comment_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `donate_id` (`elementId`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses_trans`
--
ALTER TABLE `courses_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_file`
--
ALTER TABLE `course_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `donate_id` (`rowId`);

--
-- Indexes for table `cpages`
--
ALTER TABLE `cpages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpages_trans`
--
ALTER TABLE `cpages_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_msg`
--
ALTER TABLE `custom_msg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_page`
--
ALTER TABLE `custom_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs_trans`
--
ALTER TABLE `faqs_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `features_trans`
--
ALTER TABLE `features_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `paymentType` (`paymentType`);

--
-- Indexes for table `order_comment`
--
ALTER TABLE `order_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_file`
--
ALTER TABLE `order_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `donate_id` (`orderId`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_accessories`
--
ALTER TABLE `products_accessories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_trans`
--
ALTER TABLE `products_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_file`
--
ALTER TABLE `product_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `donate_id` (`rowId`);

--
-- Indexes for table `product_props_values`
--
ALTER TABLE `product_props_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_qty_history`
--
ALTER TABLE `product_qty_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_rate`
--
ALTER TABLE `product_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_registers`
--
ALTER TABLE `product_registers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipmentadrs`
--
ALTER TABLE `shipmentadrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_cat`
--
ALTER TABLE `sub_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabs`
--
ALTER TABLE `tabs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabs_trans`
--
ALTER TABLE `tabs_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`groupId`);

--
-- Indexes for table `users_courses`
--
ALTER TABLE `users_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_credit`
--
ALTER TABLE `user_credit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_file`
--
ALTER TABLE `user_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `donate_id` (`rowId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `articles_trans`
--
ALTER TABLE `articles_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `categories_trans`
--
ALTER TABLE `categories_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `category_file`
--
ALTER TABLE `category_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `colours`
--
ALTER TABLE `colours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `comment_file`
--
ALTER TABLE `comment_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=356;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `courses_trans`
--
ALTER TABLE `courses_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `course_file`
--
ALTER TABLE `course_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `cpages`
--
ALTER TABLE `cpages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cpages_trans`
--
ALTER TABLE `cpages_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `custom_msg`
--
ALTER TABLE `custom_msg`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `custom_page`
--
ALTER TABLE `custom_page`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `faqs_trans`
--
ALTER TABLE `faqs_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=837;
--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `order_comment`
--
ALTER TABLE `order_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_file`
--
ALTER TABLE `order_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `products_accessories`
--
ALTER TABLE `products_accessories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `products_trans`
--
ALTER TABLE `products_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `product_file`
--
ALTER TABLE `product_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `product_registers`
--
ALTER TABLE `product_registers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shipmentadrs`
--
ALTER TABLE `shipmentadrs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sub_cat`
--
ALTER TABLE `sub_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tabs`
--
ALTER TABLE `tabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tabs_trans`
--
ALTER TABLE `tabs_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `users_courses`
--
ALTER TABLE `users_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_credit`
--
ALTER TABLE `user_credit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `user_file`
--
ALTER TABLE `user_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
