<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_answers', function (Blueprint $table) {
            $table->id();
            $table->integer('userId')->default(0);
            $table->integer('examId')->default(0);
            $table->integer('questionId')->default(0);
            $table->integer('answerId')->default(0);
            $table->tinyInteger('isCorrect')->default(0);
            $table->timestamps();
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_answers');
    }
}
